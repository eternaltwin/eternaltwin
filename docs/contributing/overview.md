# Overview

This document provides an overview of the main Eternaltwin repository.

Eternaltwin is a relatively complex project since it acts as a platform to support other games. In particular, it is
designed to be easily embedded in other projects through different clients.

## Dependencies

- Red (or purple): deployed in Eternaltwin
- Blue (or purple): deployed in the game

![Dependencies](./dependencies.svg)

## Core

The `core` packages define types and interfaces used by Eternaltwin. Most interfaces have two implementations and the
right one is picked depending on context. For example, storage can either use memory or Postgres. Clients can use
the network or a mock target.

## Service

This package contains the shared high-level business logic.

## REST

This package implements HTTP interfaces: both the REST API, but also the "backend-for-frontend" (BFF) API.

## CLI

Command Line Interface for Eternaltwin

## xtask

`xtask` is a support crate for scripts used during development.
