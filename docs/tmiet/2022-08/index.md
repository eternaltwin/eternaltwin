# 2022-08: This Month In Eternaltwin #11

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

## Motion-Twin sources

At the beginning of August, Skool from Motion-Twin
[published](https://github.com/motion-twin/WebGamesArchives) the sources for
most Twinoid games or older. It allowed us to advance, complete, or even
start some projects.

## General

We continue the rollout of the new deployment system: it allows project leads
to manage their own deployment. We started in July with Neoparc; in August we
enabled it for La Brute and Kingdom. In September, we'll finish the migration
and move all the remaining games.

The goal for the end of year will be to fix some long-standing issues around
accounts: email verification, password retrieval, account deletion...

# Application update

The [Eternaltwin application](https://eternaltwin.org/docs/desktop) lets you
play all Eternaltwin and Motion-Twin games, with Flash. We recently released the
brand new version `0.6` thanks to **Aksamyt**.

The main change is the update to various internal components. In particular, it
improves performance and fixes compatibility problems with MyHordes.

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello everyone, see you after a break! Forever more _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

Are you disappointed not to have access to the 102.4 MHz frequency (world forum)
during the attack? _radio crackle_ That's history now! We've updated our Novelty
Amplifier so you can use this frequency during the Night Attack.

Have you dreamed of knowing when to pick the pumpkins or when to pinch your nose
in front of Santa Claus? Now you can know the period of the seasonal events!
_radio crackle_

Do you like tinkering with tools? No worries, we provide you with this amazing
Swagger documentation for just the price of _scrambled_

It's All-Souls-Access time on MyHordes. For only today, tomorrow and all
foreseeable days afterwards, you can enter any remote or small town as long as
you have 100 soul points.

Some of our citizens have noticed that they have found an old Library. But they
weren't really careful with the books, and they have been spread in all the
world beyond. Keep your eyes open, you might find some of those new books around.
You might also find some more traces of the past around when wandering into the
towns, _radio crackle_ you can thank the Architect Wa _scrambled_

That will be all for this time, and above all stay hydrated! _radio crackle_

# PiouPiouZ

Now that Motion-Twin published the source code for most of their game (see above),
we have all we need to work on reviving Pioupiouz. We were  missing the level
editor and latest versions of the game. But now it's fixed.

We are almost done building a website to let you play and share levels using
Flash. We also started work on an HTML5 (non-Flash) version.

# Closing words

You can send [your messages for September](https://gitlab.com/eternaltwin/etwin/-/issues/53).

This article was edited by: Biosha, Demurgos, Evian, Fer, Jonathan, Patate404,
SylvanSH, and Schroedinger.
