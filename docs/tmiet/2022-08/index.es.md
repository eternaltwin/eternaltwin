# 2022-08: Este Mes En Eternaltwin #11

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

## Recursos de Motion-Twin

Al principio de agosto, y gracias a Skool, Motion-Twin
[nos habilitó](https://github.com/motion-twin/WebGamesArchives) la mayoría de
los recursos de los juegos de Twinoid y algunos juegos previos. Esto nos
permitió seguir adelante, completando e incluso comenzando algunos proyectos.

## General

Continuamos el desarrollo del nuevo sistema de desplegamiento: permite a los
líderes de los proyectos a administrar su propio desplegamiento. Comenzamos en
julio con Neoparc; en agosto lo habilitamos para La Brute y Kingdom;
en septiembre terminaremos la migración y moveremos todos los juegos restantes.

La meta para el fin de este año es arreglar algunos problemas que llevan
presentes mucho tiempo respecto a las cuentas: verificación de email,
recuperación de contraseña, eliminación de cuenta, entre otros...

# Actualización de la aplicación

La [aplicación de Eternaltwin](https://eternaltwin.org/docs/desktop) te permite
jugar a todos los juegos de Eternaltwin y Motion-Twin, con Flash. Recientemente,
hemos lanzado la nueva versión "0.6" gracias a **Aksamyt**.

El cambio principal es la actualización de varios componentes internos.
Particularmente, mejora el rendimiento y arregla problemas de compatibilidad
con MyHordes.

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

_ruido de radio_

¡Hola a todos, nos vemos de vuelta luego de un descanso! Siempre más _interferencia_

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

¿Estás decepcionado de no tener acceso a la frecuencia 102.4 MHz (foro del mundo)
durante el ataque? _ruido de radio_ ¡Eso ya es historia! Hemos actualizado nuestro
Amplificador Novedoso para que puedas usar esta frecuencia durante el Ataque Nocturno.

¿Alguna vez has soñado saber cuándo elegir calabazas o cuándo hurgar tu nariz
frente a Papá Noel? ¡Ahora puedes saberlo durante el periodo de los eventos de
temporada! _ruido de radio_

¿Te gusta jugar con herramientas? No te preocupes, te damos esta documentación
Swagger por el bajo de precio de _interferencia_

Es tiempo de Acceso-de-todas-las-Almas en MyHordes. Sólo por hoy, mañana y
todos los días que le siguen luego, puedes entrar en cualquier pueblo remoto o
pequeño mientras que tengas 100 puntos de almas.

Algunos de nuestros ciudadanos nos han comentado que encontraron una vieja
Biblioteca. Pero no fueron realmente cuidadosos con los libros, y han sido
esparcidos a lo largo de todo el mundo. Mantén tus ojos abiertos, podrías
encontrarte con alguno de estos nuevos libros por los alrededores. También
podrías encontrar más huellas del pasado cuando viajes a los pueblos,
_interferencia_ puedes agradecer al Arquitecto Wa _interferencia_

¡Eso será todo por esta vez! Y, por sobre todo, ¡manténganse hidratados con
_transferencia_

# PiouPiouZ

Ahora que Motion-Twin ha publicado el código fuente para la mayoría de sus
juegos (vean arriba), tenemos toda la necesidad de trabajar en revivir
PiouPiouz. Nos ha estado faltando el editor de nivel y las últimas versiones
del juego. Pero ahora está solucionado.

Casi terminamos la construcción del sitio web para que puedan jugar y compartir
niveles usando Flash. También estamos trabajando en una versión HTML5 (sin Flash).

Con la publicación de los recursos de Motion-Twin, ahora tenemos todo lo
necesario para reiniciar el proyecto PiouPiouz. Como recordatorio, no teníamos
los swfs con las últimas actualizaciones, ni el editor de nivel. Eso ahora es
historia con los recursos MT fácilmente compilados.

Un sitio que les permite jugar y publicar sus niveles usando clientes Flash está
casi terminado, y el trabajo básico para recrear el juego en HTML5 ha sido iniciado.

# Palabras finales

Pueden enviarnos [sus mensajes de septiembre](https://gitlab.com/eternaltwin/etwin/-/issues/53).

Este artículo ha sido editado por: Biosha, Demurgos, Evian, Fer, Jonathan,
Patate404, SylvanSH, y Schroedinger.
