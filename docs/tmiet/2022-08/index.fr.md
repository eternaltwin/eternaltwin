# 2022-08 : Ce mois-ci sur Eternaltwin n°11

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

## Sources Motion-Twin

Début août Motion-Twin par le biais de Skool [a mis à
disposition](https://github.com/motion-twin/WebGamesArchives) les sources de
la plupart des jeux Twinoid et quelques jeux antérieurs, cela nous a permis
d'avancer, de compléter et même de commencer certains projets.

## General

Nous continuons l'activation du nouveau système de déploiement : il permet
aux chefs de projet de gérer leurs déploiements eux-mêmes. Nous avons commencé
par Neoparc en juillet ; en août nous l'avons activé pour La Brute et Kingdom.
En septembre nous finirons la migration des jeux restants.

Le but pour la fin de l'année est de régler les problèmes autour des comptes :
vérification des emails, oublis de mot de passe, suppressions de compte...

# Application update

[L'application Eternaltwin](https://eternaltwin.org/docs/desktop) permet de
jouer à tous les jeux Eternaltwin et Motion-Twin, avec Flash. Nous avons récemment
publié la toute nouvelle version `0.6` grâce à **Aksamyt**.

Le principal changement est la mise à jour de composants internes. En particulier,
cela améliore les performances et règle les problèmes de compatibilité avec
MyHordes.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_

Bonjour à vous tous, on se retrouve après une période de pause ! Pour
toujours plus _brouillé_

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Vous êtes déçu de ne pas avoir accès à la fréquence 102.4 MHz (forum mondial)
durant l'attaque ? *crépitement radio* C'est désormais de l'histoire ancienne !

Nous avons mis à jour notre amplificateur Novelty afin que vous puissiez utiliser
cette fréquence durant l'attaque nocturne.

Vous avez rêvé savoir quand il faudra ceuillir les citrouilles ou bien quand il
faudra pincer votre nez face au père noyel ? Désormais vous pouvez savoir la
période des événements saisonniers ! _crépitement radio_

Vous aimez bricoler des outils ? Pas de soucis, nous vous fournissons cette
incroyable documentation Swagger pour seulement le prix de _brouillé_

C'est l'heure d'All-Souls-Access sur MyHordes. Pour seulement aujourd'hui,
demain et tous les jours prévisibles après, vous pouvez entrer dans n'importe
quelle région éloignée ou petite tant que vous avez 100 points d'âme.

Certains de nos citoyens ont remarqué qu'ils ont trouvé une ancienne bibliothèque.
Mais ils n'ont pas vraiment fait attention aux livres, et ils se sont répandus
dans au-delà de l'Outre-Monde. Gardez les yeux ouverts, vous pourriez trouver
certains de ces nouveaux livres. Vous pourriez également trouver d'autres traces
du passé en vous promenant dans les villes, _crépitement radio_ vous pouvez
remercier l'Architecte Wa _brouillé_

Ce sera tout pour cette fois-ci, et surtout restez hydraté ! _crépitement radio_

# PiouPiouZ

Avec la publication des sources de Motion Twin, nous avons désormais tout ce
qu'il faut pour redémarrer le projet PiouPiouz. Pour rappel, nous n'avions pas
les SWFs avec les dernières mises à jour, ni l'éditeur de niveaux. C'est
dorénavant de l'histoire ancienne avec les sources MT facilement compilables.

Un site permettant de jouer et publier ses niveaux en utilisant les clients Flash
est quasi terminé, mais un travail de fond a débuté pour recréer le jeu en HTML5
(non-Flash).

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de septembre](https://gitlab.com/eternaltwin/etwin/-/issues/53).

Cet article a été édité par : Biosha, Demurgos, Evian, Fer, Jonathan,
Patate404, SylvanSH et Schroedinger.
