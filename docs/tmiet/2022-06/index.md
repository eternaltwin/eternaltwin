# 2022-06: This Month In Eternaltwin #9

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

We are still working on letting project lead send updates by themselves.

# Neoparc

Dear Dinoz masters! A sweet [Neoparc](https://neoparc.eternaltwin.org/) update is now live!

It adds the Magic Cauldron to the Mayinca ruins, so your Dinoz can get to cook!
New items are available through the cauldron recipes!

Besides, we fixed many bugs. The skill "Run" no longer breaks movement. We also
fixed the feature to download your messages. You can now click on user profiles
from the a clan page. There's now a confirmation dialogue to level-up.
Finally, the Pruniac is no longer a fusion reward, it was added to the bazar as
any new object!

Thank you and see you soon in Dinoland.

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Bonjour, Hello, Guten Tag and Buenos Días to all of you!

![MyHordes - Wireless technology ad](./myhordes.png)

Don't you hate to see bits and pieces of text in a foreign language everywhere?
I certainly do, which is why I've not been on vacation for years! Well, there's
also this whole zombie apocalypse thing... _radio crackle_ By leveraging the
power of Crowdin, all you lovely people can now help eliminating weird foreign
_radio crackle_ right from MyHordes using our new, high tech In-Context
Translation feature!

But that's not all the good news I have to share! We made some more technical
upgr _grrrrz_ to our *radio crackle* so everything is now bett _beeeeeeep_ _radio crackle_
stable and fast! _Minutes of nothing but radio crackle_ and if you add just a
little bit of battery acid, you now have a 100% percent effective and safe-to-use
homemade ghoul serum.

Speaking of effective communication, we have improved our radio antennas and
reallocated additional wavelengths to upgrade the world forums! Each language
now has distinct forums to discuss aspects of the game, ask for help or just
hang out have fun with other MyHordes players.

# Eternal DinoRPG

Dear [DinoRPG](https://dinorpg.eternaltwin.org/) masters.

We just released the version 0.3.0! It adds a core game mechanic: **leveling**!

It is now possible to move your dinoz so it gets experience to gain levels
and learn new skills.

With this update, the shop is now available in all of Dinoland. There's also
a homepage with news! We also added smaller updates such as a materials page, a
new page footer and a feature to sort skills.

Quelques petites nouveautés annexes sont également disponible telles que la
page des ingrédients, un nouveau bandeau de bas de page ou un mécanisme pour
trier les compétences.

Just like MyHordes, we've set-up [a Crowdin project](https://crowdin.com/project/edinorpg)
to let everyone help with translations. Please check it out and help if you can!

# Eternal Kingdom

Hello lords of [Kingdom](https://kingdom.eternaltwin.org/)!

This month, a new member joined the development team: **Benevolens**. We welcome him warmly.

The development of the Alpha 4 reaches its end and will soon be available.

![Kingdom](./kingdom.jpg)

We'll deploy this version in July. You'll be able to join the different worlds
and explore their maps to find out what's in there. Please check-out the
[Eternaltwin Discord](https://discord.gg/ERc3svy) to be notified when it's released.
This will let you try it out and signal any issue you may find.

Work on the Alpha 5 has started. It will allow using the generals: transfer unit,
move on the map, claim empty areas, ...

Find more information here:
- [Alpha 4 Roadmap](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4)
- [Alpha 5 Roadmap](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-5)

Thank you for your attention and see you soon.

# Closing words

You can send [your messages for July](https://gitlab.com/eternaltwin/etwin/-/issues/51).

This article was edited by:Bibni, Biosha, Brainbox, Demurgos, Dylan57,
Jonathan, MisterSimple, Patate404, and Schroedinger.
