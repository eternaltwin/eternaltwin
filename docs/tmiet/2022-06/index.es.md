# 2022-06: Este mes en Eternaltwin #9

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Eternaltwin

Seguimos trabajando en dejar que los líderes de proyecto envíen actualizaciones
por sí mismos.

# Neoparc

¡Saludos, maestros Dinos! [Neoparc](https://neoparc.eternaltwin.org/)
¡Una buena actualización ya está disponible!

¡Agrega la sección Caldero Mágico a las Ruinas de Mayinca para finalmente poner
a tu Dinoz en la cocina! Hay nuevos elementos disponibles a través de algunas de
las recetas de este Caldero, ¡así que no esperes más!

Se ha realizado una buena ola de correcciones de errores. La habilidad Correr ya
no hace que un movimiento se bloquee. La descarga de mensajes personales en el
buzón ahora está arreglada. Hacer clic en el perfil de un jugador de un clan ya
no debería apuntar a una página en blanco y se ha agregado una ventana emergente
de confirmación a la sección de subir de nivel de Dinoz. Además, el Pruniac ya
no se entrega durante una fusión y se ha agregado al bazar, ¡como todos los
artículos nuevos!

Gracias y nos vemos pronto en Dinoland.

# MyHordes

Reproducir a [MyHordes](https://myhordes.eu/)

_estática de radio_

¡Bonjour, Hello, Guten Tag y Buenos Días a todos ustedes!

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

¿Acaso no odian ver trozos de texto en otro idioma por todas partes? Yo
ciertamente sí, ¡por lo que no me he ido de vacaciones en años! Bueno,
también está todo este tema del apocalipsis zombie… _estática de radio_ Al
empuñar el poder de Crowdin, ¡todos ustedes personas preciosas ahora pueden
ayudar a eliminar extraños textos _estática de radio_ directo desde MyHordes
usando nuestra nueva funcionalidad de alta gama de Traducción en Contexto!

¡Pero esas no son todas las buenas noticias que tengo para compartir!
¡Hemos hecho algunas mejo _grrrrz_ técnicas más a nuestro estática de radio
así que ahora todo es mej _beeeeeeep_ _estática de radio_ estable y rápido!
_Minutos de nada salvo estática de radio_ y si agregas solo un poco de ácido
de batería, ahora tienes un suero para mutantes casero 100% eficaz y seguro de
usar.

Hablando de comunicación eficaz, ¡hemos mejorado nuestras antenas de radio y
redistribuido longitudes de onda adicionales para mejorar los foros globales!
Cada lenguaje ahora tiene foros distintos para discutir aspectos del juego,
pedir ayuda o solo pasar el rato y divertirse con otros jugadores de Myhordes.

# Eternal DinoRPG

Queridos maestros de [DinoRPG](https://dinorpg.eternaltwin.org/).

¡Acabamos de liberar la versión 0.3.0! Agrega una mecánica central: **¡subir de nivel!**

Ahora es posible mover a tu dinoz para que gane experiencia para ganar niveles y aprender nuevas habilidades.
Con esta actualización, la tienda ahora está disponible en todo Dinoland.

¡También hay una página principal con noticias! También agregamos
actualizaciones menores como una página de materiales, un nuevo pie de página y
una funcionalidad para ordenar habilidades.

Tal como MyHordes, hemos creado un proyecto de Crowdin para dejar que todos
ayuden con las traducciones. ¡Por favor échenle un ojo y ayuden si pueden!


# Eternal Kingdom

¡Hola señores de [Kingdom](https://kingdom.eternaltwin.org/)!

Este mes, un nuevo miembro se unió al equipo de desarrollo: **Benevolens**. Le
damos una cálida bienvenida.

El desarrollo de la Alpha 4 llega a su fin y pronto estará disponible.

![Kingdom](./kingdom.jpg)

Lanzaremos esta versión en julio. Serán capaces de unirse a los diferentes
mundos y explorar sus mapas para descubrir qué hay allí. Por favor échenle un
ojo al [Discord de Eternaltwin] (https://discord.gg/ERc3svy) para ser
notificados cuando sea lanzada. Esto les dejará probarla y señalar cualquier
problema que encuentren.

El trabajo en la Alpha 5 ha comenzado. Permitirá usar a los generales: transferir unidades, moverse en el mapa, reclamar zonas vacías, …

Encuentren más información aquí:
• [Roadmap de la Alpha 4](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4)
• [Roadmap de la Alpha 5](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-5)

Gracias por su atención y nos vemos pronto.


# Palabras finales

Pueden mandar sus [mensajes para julio](https://gitlab.com/eternaltwin/etwin/-/issues/51).

Este artículo fue editado por: Bibni, Biosha, Brainbox, Demurgos, Dylan57, Jonathan, MisterSimple, Patate404 y Schroedinger.
