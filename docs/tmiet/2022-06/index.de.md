# 2022-06: Dieser Monat in Eternaltwin #9

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele
und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Wir arbeiten noch daran, dass die Projektleiter selbst Aktualisierungen
vornehmen können.

# Neoparc

Liebe Dinoz-Meister! Ein tolles [Neoparc](https://neoparc.eternaltwin.org/)
Update ist jetzt erschienen!

Es fügt den Mayinca-Ruinen den magischen Kessel hinzu, damit dein Dinoz kochen kann!
Neue Gegenstände sind durch die Kochrezepte im Kessel erhältlich!

Außerdem haben wir viele Bugs behoben. Die Fähigkeit "Laufen" unterbricht die
Bewegung nicht mehr. Wir haben auch die Funktion zum Herunterladen eurer
Nachrichten verbessert. Ihr könnt jetzt auf "Benutzerprofile" von der Clan-Seite
aus anklicken. Es gibt jetzt einen Bestätigungsdialog für den Levelaufstieg.
Schließlich ist das Pruniac nicht länger eine Fusionsbelohnung, sondern wurde
dem Basar hinzugefügt wie jedes neue Objekt!

Vielen Dank und bis bald im Dinoland.

# MyHordes

Spiele [MyHordes](https://myhordes.eu/)!

_Radioknistern_

Bonjour, Hello, Guten Tag and Buenos Días euch allen!

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Hasst ihr es nicht auch, überall Textfetzen in einer Fremdsprache zu sehen?
Ich auf jeden Fall, und deshalb war ich schon seit Jahren nicht mehr im Urlaub! Nun, da ist
auch diese ganze Zombie-Apokalypse-Sache... _Radioknistern_ Durch die Nutzung der
Macht von Crowdin könnt ihr alle lieben Leute helfen, seltsame ausländische _Radioknistern_ direkt von MyHordes aus mit unserer neuen, hochmodernen In-Context Übersetzungsfunktion!

Aber das sind noch nicht alle guten Nachrichten, die ich zu verkünden habe! Wir haben einige weitere technische
Verb _grrrrz_ zu unserem _Radioknistern_, so dass alles jetzt bess _beeeeeeep_ _Radioknistern_ ist
stabil und schnell! _Minutenlang nichts als Radioknistern_ und wenn man nur
ein wenig Batteriesäure hinzufügt, habt ihr ein 100%ig wirksames und sicher anzuwendendes
hausgemachtes Ghoul-Serum.

Apropos effektive Kommunikation: Wir haben unsere Funkantennen verbessert und
zusätzliche Wellenlängen zugewiesen, um die Weltforen zu verbessern! Jede Sprache
hat nun eigene Foren, um Aspekte des Spiels zu diskutieren, um nach Hilfe zu fragen oder einfach
Spaß mit anderen MyHordes-Spielern zu haben.

# Eternal DinoRPG

Liebe [DinoRPG](https://dinorpg.eternaltwin.org/)-Meister.

Wir haben gerade die Version 0.3.0 veröffentlicht! Sie fügt eine zentrale
Spielmechanik hinzu: **Stufenaufstieg**!

Es ist jetzt möglich, deinen Dinoz zu bewegen, damit er Erfahrung sammelt, um
Level zu gewinnen und neue Fähigkeiten zu erlernen.

Mit diesem Update ist der Shop nun im gesamten Dinoland verfügbar. Außerdem gibt es
eine Homepage mit Neuigkeiten! Außerdem haben wir kleinere Aktualisierungen
vorgenommen, wie z. B. eine Materialseite, eine neue Fußzeile und eine Funktion zum Sortieren von Fähigkeiten.

Genau wie MyHordes haben wir [ein Crowdin-Projekt](https://crowdin.com/project/edinorpg)
eingerichtet, damit jeder bei den Übersetzungen helfen kann. Bitte schaut es euch an und helft, wenn ihr könnt!

# Eternal Kingdom

Hallo Lords von [Kingdom](https://kingdom.eternaltwin.org/)!

In diesem Monat hat sich ein neues Mitglied dem Entwicklungsteam angeschlossen: **Benevolens**. Wir heißen ihn herzlich willkommen.

Die Entwicklung der Alpha 4 geht ihrem Ende entgegen und wird in Kürze verfügbar sein.

![Kingdom](./kingdom.jpg)

Wir werden diese Version im Juli bereitstellen. Ihr werdet die verschiedenen Welten betreten können
und ihre Karten erkunden, um herauszufinden, was es dort gibt. Bitte checkt den
[Eternaltwin Discord] (https://discord.gg/ERc3svy), um benachrichtigt zu werden, wenn die Version veröffentlicht wird.
So könnt ihr es ausprobieren und eventuelle Probleme melden.

Die Arbeiten an der Alpha 5 haben begonnen. Sie wird es erlauben, die Generäle zu benutzen: Einheit transferieren,
sich auf der Karte bewegen, leere Gebiete beanspruchen, ...

Weitere Informationen findest du hier:

- [Alpha 4 Entwicklungsplan](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-4)
- [Alpha 5 Entwicklungsplan](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-5)

Vielen Dank für eure Aufmerksamkeit und bis bald.

# Abschließende Worte

Du kannst deine [Nachrichten für Junli](https://gitlab.com/eternaltwin/etwin/-/issues/51) senden.

Dieser Artikel wurde bearbeitet von: Bibni, Biosha, Brainbox, Demurgos, Dylan57,
Jonathan, MisterSimple, Patate404 und Schroedinger.
