# 2021-11: This Month In Eternaltwin #2

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Renamed GitLab group and PHP packages

As announced in the previous edition of "This Month In Eternaltwin", we are in
the process of fixing the spelling of the project to be "Eternaltwin" as a
single word, with no dash. As part of this process, we did two changes that may
impact developers.

The GitLab group with all our project has been renamed. The new URL is
<https://gitlab.com/eternaltwin/>.

Our shared PHP packages have been renamed and updated. The new names are
[eternaltwin/oauth-client](https://packagist.org/packages/eternaltwin/oauth-client)
and [eternaltwin/etwin](https://packagist.org/packages/eternaltwin/etwin).

# Eternaltwin

The work on moving the forum code to Rust continues. The only missing feature
is support for editing posts.

A new contributor joined us: Nayr helps to fix some long-standing issues with
translations. In particular, he helped to remove the "esperanto" translations
because they were outdated.

# Eternal Kingdom

![Eternal Kingdom - Alpha 2](./kingdom.gif)

The Eternal Kingdom Alpha 2 is here!
You can read the full details [here](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-2).
The project is still in early stages: there are no user accounts or turn limits.
You can [try out the current version](https://kingdom.eternaltwin.org/). You may
edit the URL in the management and city pages to change the id (between 43 and
63).

Note for non-french speaker: English and Spanish translations will be part of
the next version.

# Neoparc

[Play to Neoparc, the new Dinoparc version](https://neoparc.eternaltwin.org/)

You'll find the old Dinoparc features (Doniz, Turnamenets, Jazz Island, Fusion
Center, Dark Potion, Hermite arena, Market, etc.), but also new features and
events that will be added over time. The game has been improved overall so
gameplay is more enjoyable.

Only "Clan wars" are left to be implemented and all the original content
will be there (improved!). Next, really new features from the Neoparcian
community will land one by one, sometimes in 2022!

We just ended the Halloween 2021 event. It was a thief hunt over all of of
Dinoland. Over 50 players took part in this competition!

Starting on December 1st 2021, **a special Christmas event** will take place in
Dinoland. There will be many prizes and event a few permanent changes to
discover during the event!

The community is very active on [the Discord server](https://discord.gg/ERc3svy)
and we really love when fresh players (who haven't even played the original
game) join us. Join us! Your first Dinoz awaits you and thirsts for adventure!

Important point: your old Dinoz are not lost! Once you're signed-in to Neoparc
(after registering on Eternaltwin), you may import all of your "en.dinoparc.com"
Dinoz from the "My Account" tab, they'll be kept unchanged (race, appearance,
level, skills).

Friendly, the Neoparc community!

# eMush

![eMush - v0.4.1](./emush.png)

With the release of the version 0.4.1, the eMush team is proud to announce the
launch of its fifth spaceship as a closed alpha test. It was a lot of time since
the last spaceship and many changes and updates landed.

Amongst the main changes, you'll discover the new interface prototype. The
sick bay and lab now have an interface to let you interact with the items,
the shelf, doors and other players, as well as move in the room. This is an
ongoing effort and we still need a new developer to work on the Phaser part
or someone to create the environment in Tiled.

Still on the interface side, tooltips have been added to buttons and icons to
let you know what you're about to do. We also implemented illnesses. At the
moment only food poisoning was added, mind what you eat.

Secret and concealed actions, as well as security cameras, strengthen the
core of the Mush game. It's now possible to play the contamination using a
stealthy style.

The way variables and percentages work was revised. It allows to complete
tool effects, some statuses or illnesses, as well as enabling the addition
of projects, research and skills.

Some actions were added.

PS: for the previous testers, don't worry. Fire chances have been reduced.

# MyHordes

_radio crackle_

Hello folks and thanks you for using our radio-phonic wireless system! I'm
Connor and today I'll summarize the situation of MyHordes. (latest update of
the game: 1.0-beta10, 2021-11-08)

![MyHordes - Wireless technology ad](./myhordes.png)

**- What resources are available to us?**

We have information about the game logic and most of the original assets from
various wikis across the multiple versions of Hordes and many game datas
gathered by us and the community. We don't have the original source code of the
game.

**- Can we play the game?**

Yes. You can find a current development version at <https://myhordes.eu/>.
The game is currently in open-beta phase.

It is fully playable and most features of Hordes and its variants are working,
but some content and functions from the original games are still missing or can
be slightly different.

**- What is being done now?**

We are working on multiple features: refactoring forums organization, tweaking
performances and finishing integration of MyHordes into Etwin.

**- What remains to be done?**

We must correct all remaining bugs, implement missing stuff and polishing
design (in particular the responsive part). There's also still a lot of
translation work still to be done (the game is being developed in German and
translated into French, English and Spanish. The French translation is the one
that is the most complete), everyone can contribute to collect missing elements
and for translations on Gitlab or the World Forums threads in the game.

Our plan for a near future is to finish implementing all missing features to
have all original game features, fixing all the bugs (or at least 99%) and
tweak the game as best as possible to make it faithful to the original
(for example: drop rates of items).

# CroqueMotel

Even if there's no alpha yet, the recreation of CroqueMotel is going well.

We established an initial list of the different features, and from there, the
development could start. The wiki is fairly complete and the deobfuscated
version of the game let's us gather a lot of details on the how the game works.

Right now the main architecture of the project is there (database, web server)
and we're working on rendering the hotel.

The next step is to support the first interactions to build the hotel, then
all the customer management.

# Closing words

Thank you to all the contributors! You can send [your messages for
December](https://gitlab.com/eternaltwin/etwin/-/issues/35).
