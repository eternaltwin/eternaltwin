# 2021-11: Diesen Monat in Eternaltwin #2

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die
Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# GitLab-Gruppe und PHP-Pakete

Wie in der letzten Ausgabe von "Diesen Monat in Eternaltwin" angekündigt, sind
wir dabei, die Schreibweise des Projekts so zu ändern. "Eternaltwin" ohne
Bindestrich. Als Teil dieses Prozesses haben wir zwei Änderungen vorgenommen,
die Auswirkungen für Entwickler haben könnten.

Die GitLab-Gruppe mit allen unseren Projekten wurde umbenannt. Die neue URL ist
<https://gitlab.com/eternaltwin/>.

Unsere gemeinsamen PHP-Pakete wurden umbenannt und aktualisiert. Die neuen Namen sind
[eternaltwin/oauth-client](https://packagist.org/packages/eternaltwin/oauth-client)
und [eternaltwin/etwin](https://packagist.org/packages/eternaltwin/etwin).


# Eternaltwin

Die Arbeit an der Umstellung des Forencodes auf Rust geht weiter. Die einzige
fehlende Funktion ist die Unterstützung für die Bearbeitung von Beiträgen.

Ein neuer Mitwirkender ist zu uns gestoßen: Nayr hilft, einige seit langem
bestehende Probleme mit Übersetzungen zu beheben. Insbesondere hat er geholfen,
die "Esperanto"-Übersetzungen zu entfernen weil sie veraltet waren.


# Eternal Kingdom

![Eternal Kingdom - Alpha 2](./kingdom.gif)

Die Eternal Kingdom Alpha 2 ist da! Du kannst die vollständigen Details
[hier](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-2)
lesen. Das Projekt befindet sich noch im Anfangsstadium: Es gibt keine
Benutzerkonten oder Rundenbegrenzungen. Du kannst [die aktuelle
Version](https://kingdom.eternaltwin.org/) ausprobieren. Du kannst die URL auf
den Verwaltungs- und Stadtseiten bearbeiten, um die ID zu ändern (zwischen 43
und 63).

Hinweis für Nicht-Französischsprachige: Englische und spanische Übersetzungen
werden Teil der der nächsten Version sein.

# Neoparc

[Hier geht es zu Neoparc, der neuen Version von Dinoparc](https://neoparc.eternaltwin.org/)

Du findest hier die alten Dinoparc-Features (Doniz, Turnamenets, Jazz Island,
Fusion Center, Dark Potion, Hermite Arena, Market usw.), aber auch neue
Funktionen und Ereignisse, die mit der Zeit hinzugefügt werden. Das Spiel wurde
insgesamt verbessert, so dass das Gameplay mehr Spaß macht.

Es müssen nur noch die "Clan-Kriege" implementiert werden und der gesamte
ursprüngliche Inhalt wird wieder vorhanden und besser sein! Als nächstes werden
wirklich neue Features aus der Neoparcianer-Community nach und nach einfließen,
irgendwann 2022!

Ab dem 1. Dezember 2021 findet im Dinoland eine **besondere Weihnachtsaktion
statt**. Es wird viele Preise und einige permanente Veränderungen geben!

Die Community ist sehr aktiv auf dem [Discord-Server](https://discord.gg/ERc3svy)
und wir freuen uns sehr, wenn neue Spieler (auch die die noch nicht einmal das
Originalspiel gespielt haben) uns beitreten. Schließt euch uns an! Dein erstes
Dinoz wartet auf dich und dürstet nach Abenteuern!

Wichtiger Hinweis: Deine alten Dinoz ist nicht verloren! Sobald du bei Neoparc
angemeldet bist (nachdem du dich auf Eternaltwin registriert hast), kannst du
alle deine "en.dinoparc.com"-Dinoz aus der Registerkarte "Mein Konto"
importieren. Sie bleiben unverändert! (Rasse, Aussehen, Stufe, Fähigkeiten)

Mit freundlichen Grüßen, die Neoparc-Community!

# eMush

![eMush - v0.4.1](./emush.png)

Mit der Veröffentlichung der Version 0.4.1 ist das eMush-Team stolz darauf, das
fünfte Raumschiff als geschlossenen Alpha-Test zu starten. Es ist viel Zeit
seit dem letzten Raumschiff vergangen und viele Änderungen und Updates sind
gelandet.

Zu den wichtigsten Änderungen gehört der neue Prototyp der Benutzeroberfläche.
Die Krankenstation und das Labor haben jetzt eine Schnittstelle, mit der du mit
den Items interagieren kannst (dem Regal, den Türen und anderen Spielern) sowie
sich im Raum bewegen. Dies ist ein fortlaufender Prozess und wir brauchen immer
noch einen neuen Entwickler, der an dem Phaser-Teil arbeitet oder jemanden, der
die Umgebung in Tiled erstellt.

Was die Benutzeroberfläche betrifft, so wurden den Schaltflächen und Symbolen
Tooltips hinzugefügt, die dir miteilen, was sie bedeuten. Wir haben auch
Krankheiten implementiert. Im Moment wurden nur Lebensmittelvergiftungen
hinzugefügt, also passt auf, was ihr esst.

Geheime und verdeckte Aktionen sowie Überwachungskameras verstärken den Kern
des Mush-Spiels. Es ist nun möglich, Mush mit einem verstohlenen Stil zu
spielen.

Die Art und Weise, wie Variablen und Prozentsätze funktionieren, wurde
überarbeitet. Sie ermöglicht die Ergänzung von Werkzeug-Effekten, einige Status
oder Krankheiten, sowie das Hinzufügen von Projekten, Forschung und Fähigkeiten.

Einige Aktionen wurden hinzugefügt.

PS: Für die früheren Tester: Keine Sorge. Die Brandgefahr wurde verringert.

# MyHordes

_Radioknistern_

Hallo Leute und vielen Dank, dass du unser drahtloses Funksystem nutzt! Ich bin
Connor und heute werde ich die Situation von MyHordes zusammenfassen. (letztes
Update des des Spiels: 1.0-beta10, 2021-11-08)

![MyHordes - Wireless technology ad](./myhordes.png)

**- Welche Mittel stehen uns zur Verfügung?**

Wir haben Informationen über die Spiellogik und die meisten der Original-Assets
aus verschiedenen Wikis über die verschiedenen Versionen von Hordes und viele
Spieldaten, die von uns und der Community gesammelt wurden. Wir haben nicht den
Original-Quellcode des Spiels.

**- Können wir das Spiel spielen?**

Ja. Eine aktuelle Entwicklungsversion findest du unter <https://myhordes.eu/>.
Das Spiel befindet sich derzeit in der Open-Beta-Phase.

Es ist voll spielbar und die meisten Funktionen von Hordes und seinen Varianten
funktionieren, aber einige Inhalte und Funktionen aus den Originalspielen fehlen
noch oder können etwas anders sein.

**- Was wird jetzt getan?**

Wir arbeiten an mehreren Funktionen: Überarbeitung der Forenorganisation,
Verbesserung der Leistungen und Fertigstellung der Integration von MyHordes
nach Eternaltwin.

**- Was muss noch getan werden?**

Wir müssen alle verbleibenden Bugs korrigieren, fehlende Dinge implementieren
und das Design aufpolieren (insbesondere den responsiven Teil). Außerdem gibt
es noch eine Menge Übersetzungsarbeit zu leisten. Das Spiel wird auf Deutsch
entwickelt und ins Französische, Englische und Spanische übersetzt. Die
französische Übersetzung ist diejenige die am vollständigsten ist. Jeder kann
dazu beitragen, fehlende Elemente zu sammeln und für Übersetzungen auf Gitlab
oder in den Weltforum-Threads im Spiel zu posten.

Unser Plan für die nahe Zukunft ist es, die Implementierung aller fehlenden
Features zu beenden, um alle originalen Spielfunktionen zu haben, alle Bugs zu
beheben (oder zumindest 99%) und das Spiel so gut wie möglich zu verbessern,
damit es dem Original treu bleibt (z.B.: Drop-Raten von Gegenständen).

# CroqueMotel

Auch wenn es noch keine Alpha-Version gibt, läuft die Entwicklung von
CroqueMotel gut.

Wir erstellten eine erste Liste der verschiedenen Funktionen, und von dort aus
konnte die Entwicklung beginnen. Das Wiki ist ziemlich vollständig und die
entschlüsselte Version des Spiels ermöglicht es uns, viele Details darüber zu
sammeln, wie das Spiel funktioniert.

Im Moment ist die Hauptarchitektur des Projekts fertig (Datenbank, Webserver)
und wir arbeiten daran, das Hotel zu rendern.

Der nächste Schritt ist die Unterstützung der ersten Interaktionen zum Aufbau
des Hotels, dann die gesamte Kundenverwaltung.

# Abschließende Worte

Vielen Dank an alle, die dazu beigetragen haben! [Du kannst uns Nachrichten für
Dezember schicke](https://gitlab.com/eternaltwin/etwin/-/issues/35).
