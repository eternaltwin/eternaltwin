# 2021-11 : Ce mois-ci sur Eternaltwin n°2

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de la Motion Twin.

# Renommage du groupe GitLab et des paquets PHP

Comme annoncé dans l'édition précédente de "Ce mois-ci sur Eternaltwin", nous
sommes en train de corriger l'orthographe du projet "Eternaltwin", en un seul
mot et sans tiret. Dans le cadre de ce processus, nous avons appliqué deux
changements qui peuvent impacter les développeurs.

Le groupe GitLab avec tous nos projets a été renommé. La nouvelle URL est
<https://gitlab.com/eternaltwin/>.

Nos paquets PHP communs ont été renommés et mis à jour. Les nouveaux noms sont
[eternaltwin/oauth-client](https://packagist.org/packages/eternaltwin/oauth-client)
et [eternaltwin/etwin](https://packagist.org/packages/eternaltwin/etwin).

# Eternaltwin

Le travail pour passer le code du forum en Rust continue. La dernière
fonctionnalité manquante est l'édition des messages.

Un nouveau contributeur nous a rejoint : Nayr aide à corriger de vieux
problèmes de traductions. En particulier, il a aidé à retirer les traductions
"esperanto" car elles étaient obsolètes.

# Eternal Kingdom

![Eternal Kingdom - Alpha 2](./kingdom.gif)

L'Alpha 2 d'EternalKingdom est désormais disponible !
Vous pouvez lire les détails [ici](https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/road-map/alpha-2).
Le projet n'en est qu'à ses débuts ; il n'y a pas encore de comptes utilisateur
ou de limite de tours.
Vous pouvez [test la version actuelle](https://kingdom.eternaltwin.org/). Vous
pouvez modifier l'URL des pages "Capitale" et "Gestion" pour changer d'id
(entre 43 et 63).

Note aux joueurs non-francophones : les traductions anglaises et espagnoles
feront partie de la prochaine version.

# Neoparc

[Jouer à Neoparc, la nouvelle version de Dinoparc](https://neoparc.eternaltwin.org/)

Vous y retrouverez les anciennes fonctionnalités de Dinoparc (Dinoz, Tournois,
Île de Jazz, Centre de Fusion, Potion Sombre, Arène de l'Hermite, Bazar, etc.),
mais également des nouveautés et événements qui seront incorporés au fur et à
mesure au cours des prochains mois. Le jeu a été amélioré dans son ensemble
pour que jouer soit plus agréable.

Il ne reste qu'à implémenter les guerres de clans pour que tout le contenu
original ait été repris (en mieux !). Ensuite, de vraies nouveautés issues de la
communauté Neoparcienne feront surface une par une, quelque part en 2022 !

Nous venons tout juste de clôturer l'événement d'Halloween 2021. Il s'agissait
d'une chasse aux voleurs sur tout le territoire de Dinoland. Plus de 50
joueurs ont pris part à cette compétition !

À compter du 1er décembre 2021, **un événement de Noël spécial** sera en place
partout sur Dinoland. De nombreux cadeaux seront remportés et il y en aura même
quelques nouveautés permanentes à découvrir pendant l'événement !

La communauté est très active sur [le serveur Discord](https://discord.gg/ERc3svy)
et nous adorons quand de nouveaux joueurs (n'ayant jamais joué au jeu original)
nous rejoignent. N'attendez-plus ! Votre premier Dinoz vous attend et a
soif d'aventures !

Point important : vos anciens Dinoz ne sont pas perdus ! Lors de votre
connexion sur Neoparc (après avoir créé un compte sur Eternaltwin), vous avez
la possibilité d'importer à l'identique (Race, Apparence, Level, Compétences)
l'ensemble des Dinoz de votre compte "dinoparc.com" depuis l'onglet "Mon Compte".

Amicalement, la communauté de Neoparc !

# Emush

![Emush - v0.4.1](./emush.png)

À occasion de la sortie de la version 0.4.1, l'équipe de eMush est fière de vous
annoncer le lancement de son cinquième vaisseau en alpha fermée. Du temps a
passé depuis le dernier vaisseau et de nombreux changements et nouveauté sont
arrivés.

Parmi les changements majeurs, vous allez découvrir le prototype de l'interface
graphique. L'infirmerie et le laboratoire possèdent désormais une interface
visuelle qui permet d'interagir avec les équipements, l'étagère, les portes, les
autres joueurs et vous déplacer dans la pièce. Ce chantier est encore en cours,
et nous avons toujours besoin d'un nouveau développeur pour travailler sur la
partie Phaser ou de volontaire pour créer les scènes Tiled.

Du coté de l'interface, des tooltips ont maintenant été ajouté lorsque vous
survolez les boutons et les icônes, permettant de mieux savoir ce que vous vous
apprêtez à faire. Du côté des interactions rajoutées, la plus importante est
l'implémentation des maladies. Pour l'instant seuls les effets de l'intoxication
alimentaire ont été ajoutés, faites donc attention à ce que vous mangez.

La gestion des actions secrètes et discrètes, ainsi que l'implémentation des
caméras, permet de renforcer le cœur de jeu de Mush. Jouer la contamination de
façon discrète est donc désormais possible.

Le fonctionnement des modificateurs de variable et de pourcentage a été revu,
permettant de compléter les effets des outils, de certains status et des
maladies, tout en ouvrant la possibilité d'ajouter rapidement les projets,
recherches et compétences.

Quelques actions ont aussi été rajoutées.

PS : pour les précédents testeurs, pas d’inquiétude, les risques d'incendie
ont été réduits !

# MyHordes

_grésillements de radio_

Bonjour tout le monde et merci d'utiliser notre système radiophonique sans fil !
Je suis Connor et aujourd'hui je vais résumer la situation de MyHordes.
(dernière mise à jour du jeu : 1.0-beta10, 2021-11-08)

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

**- De quelles ressources nous disposons ?**

Nous avons des informations sur la logique du jeu et la plupart des ressources
originales grâce aux wikis des multiples versions de Hordes et de nombreuses
données du jeu recueillies par nous et la communauté. Nous ne possédons pas le
code d'origine du jeu.

**- Pouvons-nous jouer au jeu ?**

Oui. Vous pouvez trouver une version de développement actuelle à l'adresse
<https://myhordes.eu/>. Le jeu est actuellement en phase de beta ouverte.

Il est entièrement jouable et la plupart des fonctionnalités de Hordes et de
ses variantes fonctionnent, mais certains contenus et fonctions des jeux
originaux sont toujours manquants ou peuvent être légèrement différents.

**- Que fait-on maintenant ?**

Nous travaillons sur de multiples fonctionnalités : réorganisation des
forums, optimisation des performances et finalisation de l'intégration de
MyHordes dans Etwin.

**- Ce qui reste à faire ?**

Nous devons corriger les bugs restants, implémenter les fonctionnalités
manquantes et optimiser le design (en particulier la partie responsive).
Il reste encore beaucoup de travail de traduction à faire (le jeu est en
cours de développement en allemand et traduit en français, anglais et espagnol.
La traduction française est la plus complète), tout le monde peut participer à
la collecte d'éléments manquants et les traductions sur Gitlab ou sur les
Forums Monde sur le jeu.

Notre plan pour un futur proche est de finir d'ajouter les fonctionnalités
manquantes, corriger tous les bugs (ou au moins 99%) et ajuster du mieux qu'on
peut le jeu pour le rendre fidèle à l'original (par exemple : le taux de
drop des objets).

# CroqueMotel

Bien qu'il n'y ait pas encore d'alpha, le redéveloppement de CroqueMotel avance
bien.

Une première liste des différentes fonctionnalités a été établie, et à partir
de ça, le développement a pu commencer. Le wiki assez complet et la version
désobfusquée du jeu nous permettent de récupérer un maximum d'info sur les
mécaniques.

Actuellement l'architecture de base du projet est en place (base de données,
serveur web) et nous progressons sur le rendu graphique de l'hôtel.

La prochaine étape est l'ajout des premières interactions de constructions,
puis toute la gestion des clients.

# Le mot de la fin

Merci à tous les contributeurs ! Vous pouvez envoyer [vos messages pour
l'édition de décembre](https://gitlab.com/eternaltwin/etwin/-/issues/35).
