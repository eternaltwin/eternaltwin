# 2022-12: This Month In Eternaltwin #15

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

_Biosha_ implemented a tool to monitor the website status and send alerts when
an issue is detected. This tool will be deployed soon.

_Moulins_ updated the Hammerfest archive to return inventories and quest
statuses.

# Eternalfest

[Eternalfest](https://eternalfest.net/) lets you play to both classic and new
levels of the Hammerfest platforming game.

_Rarera_ and _Maxdefolsch_ released a new game on December 25th: [L'ancienne source](https://eternalfest.net/games/eb10aaf1-23dc-4b3f-a543-66a0596459d4).
It contains hundreds of great levels, we highly recommend you try it out.

There was also a lot of work to change how games are managed internally. This
should enable support for some long awaited features such as inventory pages,
full translation support, or precise loader progress. The server side is mostly
complete, and we are now working on the interface. This work also serves as a
prototype for improved support of games at the Eternaltwin level (and replace
the manual configuration).

# eMush

Welcome to this section dedicated to [eMush](https://emush.eternaltwin.org/),
the project to save Mush.

## What's new?

On this nice month of December, eMush admins and customization freaks will
rejoice. Indeed, we worked hard so different Daedalus ships with their own
settings could launch side by side. And best of all, it can be all configured
directly from the admin panel on the website.

![eMush](./emush.png)

Among the other improvements, we improved archives of older ships. Once a game
ends, we clean most entities and only keep information related to your digital
legacy.

Last month we announced the completion of English translations. Well,
administrators may now pick the language when creating a Daedalus.

## Coming next

We'll keep working on database migrations so we can deploy updates without
resetting existing ships.

We'll also work on improvements to user profiles.

## When is the open alpha?

We'll keep updating your through TMIET, but the best way to follow progress is
to join [the Eternaltwin discord](https://discord.gg/SMJavjs3gk).

eMush is an open source project, you can also follow our progress on GitLab.
In particular, [progress on the **Spores for All!** update](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Thank you for your attention, see you later aboard the Daedalus!

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello everyone, I hope all is well for you during this cold season _scrambled_

![MyHordes - Wireless technology ad](./myhordes.png)

I hope you did not eat too many Christmas chocolates because apparently they
are _scrambled_

I also think you would like to compare your _scrambled_ So we developed a unique
ranking system for all Distinctions.

Preparations for the 15th season are underway, we opened up a survey requesting
participants for the closed beta, available into the frequency 102.4 MHz
(World Forums), until the 8th of January 2023! _radio crackle_

In addition, we optimised several aspects of our website to being more
convenient to your mobile-Anthrax GPS systems. _radio crackle_

mmmmh, I think there is _scrambled_ but what is the _scrambled_ BEN! Tell that
crow to get off the _scrambled_

# Closing words

Happy new year everyone!

You can send [your messages for January](https://gitlab.com/eternaltwin/etwin/-/issues/57).

This article was edited by: Breut, Demurgos, Dylan57, Fer, Patate404, and Schroedinger.
