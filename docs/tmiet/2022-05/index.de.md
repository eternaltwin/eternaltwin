# 2022-05: Dieser Monat in Eternaltwin #8

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele
und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Nach einer kurzen Unterbrechung im April ist
[das Eternaltwin-Forum](https://eternaltwin.org/forum) wieder aktiviert.

Eternaltwin ist jetzt eine offizielle Open Source Organisation auf GitLab. GitLab ist die
Code-Kollaborationsplattform, die wir für alle Eternaltwin-Projekte nutzen. Mit unserem neuen
Status erhalten wir vollen Zugriff auf alle GitLab-Funktionen.

Im Juni werden wir an der Verbesserung der GitLab-Berechtigungen arbeiten. Momentan haben die meisten
Mitwirkenden vollen Zugriff auf _alle_ Spiele. Wir werden das so ändern, dass
Berechtigungen für _bestimmte_ Spiele gewährt werden. Dies wird es den
Projektleitern erlauben die Arbeiten an den einzelnen Projekten besser verwalten zu können.

Schließlich sind wir dabei, den vor einigen Monaten erwähnten neuen Server zu konfigurieren.
Dieser wird den Projektleitern die volle Kontrolle darüber geben, neue Versionen
ihrer Spiele online zu stellen, ohne sich mit den Eternaltwin-Administratoren abstimmen zu müssen.

# MyHordes

_Radio-Knistern_

_langanhaltende Stille_

_Schlurfen_

Huff Huff... hallo zusammen... Ben hier, mal wieder. Tut mir leid, ich bin ein
bisschen außer Atem, aber Connor lässt mich Kartons voller Papier herumschieben.
Er sagt, sie sind für eine Art Abstimmung oder so...

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Apropos Papier: Wir haben ein brandneues High-Tech-Sortiersystem für persönliche
Nachrichten. Endlich werden all die zufälligen Junk-Mails mal richtig sortiert.
Wir haben sogar einen eigenen Posteingang für Fanpost von unserem Radiosender
eingerichtet, und er quillt bereits über vor Post von euch _Radio-Knistern_
Bürgern. Ich frage mich nur, warum Connor es als "Beschwerden und Todesdrohungen"
beschriftet hat.

Wie auch immer, genug davon. Lasst uns zu den Nachrichten kommen! Wir haben
gehört, dass einige Leute wieder mit Geistern herumspielen wollen und sich um
einige seltsame Gebäude in ihrer Stadt versammeln. Was ist nur mit den Seelen
hier los? Haltet euch fest! Die Scharlatane... Ich meine, die Schamanen werden
in einer besonderen Stadt wieder auftauchen!

_Radio-Knistern_ ...stern kam ein Bericht an, dass die Leute anfangen, mit
Landesflaggen herumzufuchteln. Ich habe Chile, Österreich, Japan und sogar
seltsame Flaggen von einem erfundenen Ort gesehen, den die Leute anscheinend
"Frankreich" nennen. Es ist seltsam... _Radio-Knistern_

Zu guter Letzt noch eine öffentliche Ankündigung. Einige mutige Seelen bereiten
sich darauf vor, sich in die Aussenwelt von Hordes zu begeben, um Informationen
über alle Arten von seltsamen Phänomenen aufzuzeichnen. Wie viele Gegenstände
sind im Sand versteckt? Wie groß ist die Gefahr, an dem übel riechenden Wasser
zu sterben, das man oft in Ruinen findet? Wann hat unser Schamane das letzte
Mal seine Unterwäsche gewechselt? *Radio-Knistern* Wenn ihr helfen wollt,
schaut bitte auf [diesen Beitrag auf unserer Radiosender-Tafel](https://myhordes.eu/jx/forum/9/42008).

# eMush

Wir sind wieder da und haben ein paar Neuigkeiten über [eMush](https://emush.eternaltwin.org/)!
Der Fortschritt war in letzter Zeit etwas langsamer, aber keine Sorge, wir sind immer noch dabei.

Bei der grafischen Schnittstelle sind große Fortschritte zu verzeichnen. Wir haben jetzt 18 von
26 Räumen der Daedelus! Wir haben viel an der Wegfindung gearbeitet und daran, wie wir
die verschiedenen Sprites darstellen können. Außerdem ist es jetzt schneller.

Die Version 0.4.3 mit einer vollständigen grafischen Benutzeroberfläche ist bereits in Arbeit. Wir müssen nur
noch die fehlenden Räume (Geschütztürme, Maschinenraum, Deck) und Sitz-/Liegeanimationen für die Charaktere hinzufügen.

Ich hoffe, dass ich mich nächsten Monat mit der eMush Version 0.4.3 zurückmelden werde, mit einer
Alpha-Release, damit ihr sehen könnt, was neu ist.

# Kube

Die Spieler _Booti_, _Moulins_ und _TheToto_ haben die Arbeit an Kube wieder aufgenommen!

## Bisherige Arbeit

Im Jahr 2020 hat _Moulins_ ein Backup der Kube-Welt erstellt. [Ein Forumsthread wurde
im Kube-Forum erstellt.](http://kube.muxxu.com/tid/forum#!view/492|thread/65361326)
Im Laufe des Jahres 2021 wurde das Backup aktualisiert, und es enthält nun
die persönlichen Foren der einzelnen Zonen.

## Neuimplementierung des Weltgenerators

_Booti_ hat vor kurzem begonnen, den Algorithmus zur Welterzeugung neu zu implementieren. Zum Glück
befand sich der Code in der SWF-Datei des Spiels.

Kube hatte auch einen seit langem ungelösten Fehler mit fehlenden fliegenden Inseln
("îles volantes") und gemischten Biomen. Es stellte sich heraus, dass dies durch neue Flash-Versionen verursacht wurde.

Die Neuimplementierung behebt diese Fehler.

## Neuer Spiel-Client

_TheToto_ entwickelt einen neuen Spiel-Client, der ohne Flash gespielt werden kann. Es ist noch früh,
aber es ist bereits möglich, Kubes zu platzieren oder zu entfernen und Daten zu laden. Die Daten können
vom offiziellen Server, dem bereits erwähnten Backup oder von Kub3dit
Blaupausen geladen werden.

[Der Code ist hier verfügbar.](https://gitlab.com/eternaltwin/kube/kube-client-html5)

## Demonstration

Fliegende Insel ("île volante"), die von _Bootis_ Code erzeugt und durch
_TheToto_'s Spiel-Client sichtbar ist.

![Kube](./kube.png)

Tritt dem "Kube"-Kanal auf [dem Eternaltwin-Discord-Server](https://discord.gg/ERc3svy) bei,
wenn du über Kube diskutieren willst. 😺

# Eternal DinoRPG

Hallo liebe [DinoRPG](https://dinorpg.eternaltwin.org/)-Meister.

Wir haben die Version 0.2.2 veröffentlicht. Diese Version bringt einige grafische Verbesserungen, um
die gesamten Dinoz überall dort anzuzeigen, wo sie erscheinen sollen.

Wir arbeiten an den Leveln. Damit kannst du sehen, wie sich deine Dinoz (na ja, vorerst nur Moueffe) sich entwickeln und durch das Dinoland reisen.

# Eternal Kingdom

Die Implementierung der 4. Alpha geht weiter! Die Seite **Weltauswahl** ist
fertig. Eine Stadt wird uns nun zugewiesen, wenn wir einer Welt beitreten. Und diese Stadt
wird zu einer Barbarenstadt, wenn wir sterben.

![EternalKingdom - Weltauswahl](./kingdom-select.png)

Wir konzentrieren uns nun auf die Implementierung der Hauptelemente der **Weltkartenseite**, einschließlich
die Anzeige der Knotenpunkte (Hauptstadt, Barbaren und Ressourcenpunkt), das Seitenmenü
das die verfügbaren Aktionen für den Spieler anzeigt und die Weltprotokolle.

![EternalKingdom - Weltkartenseite](./kingdom-world.png)

# Abschließende Worte

Du kannst deine [Nachrichten für Juni](https://gitlab.com/eternaltwin/etwin/-/issues/48) senden.

Dieser Artikel wurde bearbeitet von: Bibni, Biosha, Brainbox, Breut, Demurgos,
MisterSimple, Patate404, Schroedinger und TheToto.
