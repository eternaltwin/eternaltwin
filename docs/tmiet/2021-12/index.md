# 2021-12: This Month In Eternaltwin #3

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin application

You can now [download the version 0.5.6](https://eternaltwin.org/docs/desktop)
of the Eternaltwin application. It lets you play to Flash games.

- Update links : MyHordes, Eternal DinoRPG.
- Windows users can now download the app as a `.zip` file to avoid installation.
- Fix connection issues with the English version of Hammerfest.

Make sure to download the latest version!

# Eternaltwin

The server migration to the Rust language is nearing its end.

The forum now has [a German section](https://eternaltwin.org/forum/sections/86b23bed-27e8-4a19-b4d6-22a9fcd9142d).

[Eternal DinoRPG](https://dinorpg.eternaltwin.org/) is now available online, and
Epopotamo should follow suit soon. Given the increasing number of games, we are
planning to move a better server.

# Emush

The [Emush](https://emush.eternaltwin.org/) project had some progress. Mainly,
the issues found during the last Alpha were (mostly) fixed. It brings us the
deployment of the latest version (0.4.2) at the start of this month, with
fixes and completing the illness effects. Some testers are looking to get
rid of the these new Deadalus ships.

The next version will focus on improving the graphical user interface,
with new rooms and other features.

# MyHordes

_radio crackle_

Hello folks, and thank you for using our radio-phonic wireless system! I'm
Connor, and today I'm sending you a little message regarding
[MyHordes](https://myhordes.eu/).

![MyHordes - Wireless technology ad](./myhordes.png)

As you know, Crows gather around cities at the end of the year. For lack of heat
they will still devour your livers during this winter period. But still they are
not greedy: from December 20th to January 7th you will be able to steal from
your friends with the Santa Claus outfit. Who knows, maybe you will be able to
endure his foul smell? Also take the time to thoroughly search the post
centers, maybe you will find lost Christmas packages? Oh and by the way, be
careful because the sand is getting hard, you could hurt yourself! And beware of
your livers... _crackle_ sugar should not be abused.

That's all for now, have a happy holiday with your fellow citizens, and your
zombies fri... _scrambled_

# Eternal DinoRPG

**The Eternal DinoRPG alpha version 0.1.0 is now available!**

You can find it here: [https://dinorpg.eternaltwin.org/](https://dinorpg.eternaltwin.org/).

The website works without Flash, however some animations may not render properly.
It is advised to use [the Eternaltwin application](https://eternaltwin.org/docs/desktop)
for now: Eternal DinoRPG is at the top of the list.

You can sign-in, buy a few dinoz and move them randomly across a few locations.
A less visible but working feature is that you can display all the in-game
statuses, your inventory, as well as all the skills.

We have Dinoz comparator so you, the DinorRPG community, can help us with
sorting the 7000+ graphical elements composing a dinoz. Feel free to buy some
dinoz, try out everything!

The database will be reset a few times over the development period.

We are already working on the next version to let you import your DinoRPG
account, and add a few improvements such as a language picker. Then, the version
after that will fully get rid of Flash!

See you soon for more news!

# Neoparc

A Christmas event is taking place since December 1st in [Neoparc](https://neoparc.eternaltwin.org/).
During this event, each Dinoz master can collect Gift Tickets every day by
digging in the different Dinoland locations. Also, the famous "Santaz" dinoz
race is back.

Many lucky players have already collected a few eggs of this sought-after dinoz
race at the Christmas Wheel of Dinotown. Minor spoiler for our dear readers:
everyone who signs-in on December 25th will get a Santaz Egg as a gift for
this first wonderful year.

This holiday season update is packed with exciting secrets! Please enjoy
discovering them at your pace. Go ahead and share what you find and what you
feel with the rest of the community on [the Discord channel](https://discord.gg/ERc3svy).

It is with pleasure that I wish a wonderful holiday season to all of you!

Your Neodev, Jonathan.

# Directquiz

[Directquiz](https://www.directquiz.org/) gained a few new features over the last
two months.

The first new feature is a typing indicator, as in many modern apps. It is
now available in **Directquiz**!

Next, you can now [submit new questions](https://www.directquiz.org/validationQuestion.php)
without spending your **Direct Dollars**. You can pick from a list of over
300 categories! Go ahead, send questions!

Finally, for this winter season, the website now has a snowy animated theme
to properly celebrate the end of the year.

We also did some internal improvements around config files to prepare a future
migration to the Eternaltwin servers.

# Closing words

You can send [your messages for January](https://gitlab.com/eternaltwin/etwin/-/issues/36).

Merry Christmas and happy end of the year!
