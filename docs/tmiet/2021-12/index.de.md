# 12.2021: Diesen Monat in Eternaltwin #3

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele
und Community von Motion Twin zu erhalten.

# Eternaltwin App

Du kannst jetzt [Version 0.5.6](https://eternaltwin.org/docs/desktop)
der Eternaltwin-App herunterladen. Sie lässt dich Flash-Spiele spielen.

- Aktualisierte Links: MyHordes, Eternal DinoRPG
- Windows-Nutzer können die App jetzt als `.zip` herunterladen, um eine
  Installation zu umgehen.
- Verbindungsprobleme der englischen Hammerfest-Version behoben.

Lade immer die aktuellste Version herunter!

# Eternaltwin

Die Umstellung der Server auf die Sprache Rust nähert sich ihrem Ende.

Das Forum hat nun eine [deutsche Sektion](https://eternaltwin.org/forum/sections/86b23bed-27e8-4a19-b4d6-22a9fcd9142d).

[Eternal DinoRPG](https://dinorpg.eternaltwin.org/) ist nun online verfügbar,
Epopotamo sollte bald folgen. In Anbetracht der steigenden Anzahl von Spielen
planen wir den Umzug auf einen besseren Server.

# Emush

Das [Emush](https://emush.eternaltwin.org/) Projekt hatte etwas Fortschritt.
Größtenteils wurden die gefundenen Probleme der letzten Alpha-Phase behoben.
Dies bringt uns zur Veröffentlichung von Version 0.4.2 diesen Monat mit
Korrekturen und der Vervollständigung des Krankheit-Effekts. Einige Tester
wollen diese neuen Deadalus-Schiffe loswerden.

Die nächste Version wird den Fokus auf das User Interface, neue Räume und
anderes legen.

# MyHordes

_Radioknistern_

Hallo Leute, und vielen Dank, dass ihr unser drahtloses Funksystem nutzt! Ich
bin Connor, und heute sende ich euch eine kleine Nachricht über
[MyHordes](https://myhordes.eu/).

![MyHordes - Werbung für drahtlose Technologie](./myhordes.png)

Wie ihr wisst, sammeln sich am Ende des Jahres die Raben in den Städten. Aus
Mangel an Wärme werden sie weiterhin eure Lebern fressen. Aber sie sind trotzdem
nicht gierig: Vom 20. Dezember bis zum 7. Januar könnt ihr eure Freunde im
Weihnachtsmannkostüm bestehlen. Wer weiß, vielleicht könnt ihr ja auch
seinen üblen Geruch ertragen? Nehmt euch auch die Zeit, die Postämter gründlich
zu durchsuchen. Vielleicht findet ihr ja verlorene Weihnachtspakete?
Ach, und übrigens, seid vorsichtig: Der Sand wird immer härter, ihr könntet euch
verletzen! Und passt auf eure Leber auf... Knisterzucker sollte nicht
missbraucht werden.

Das ist alles für jetzt. Habt fröhliche Feiertage mit euren lieben Einwohnern
und den Zombiefreu... _Rauschen_


# Eternal DinoRPG

**Die Eternal DinoRPG Alpha Version 0.1.0 ist nun verfügbar!**

Du kannst sie hier finden: [https://dinorpg.eternaltwin.org/](https://dinorpg.eternaltwin.org/).

Die Webseite funktioniert ohne Flash, allerdings kann es vorkommen, dass einige
Animationen nicht richtig gerendert werden. Es ist ratsam, [die
Eternaltwin-App](https://eternaltwin.org/docs/desktop) zu benutzen: Eternal
DinoRPG ist ganz oben auf der Liste.

Du kannst dich anmelden, ein paar Dinoz kaufen und sie zu ein paar Orten
schicken. Eine weniger sichtbare, aber funktionierende Funktion ist, dass du
alle Statuswerte, dein Inventar und alle Fertigkeiten anzeigen lassen kannst.

Wir haben einen Dinoz-Komparator: Damit könnt ihr, die DinoRPG-Community, uns
beim Sortieren der über 7000 grafischen Elemente, aus denen ein Dinoz besteht,
helfen. Du kannst gerne ein paar Dinoz kaufen, probiere alles aus!

Die Datenbank wird während des Entwicklungszeitraums einige Male zurückgesetzt
werden.

Wir arbeiten bereits an der nächsten Version, mit der ihr euren DinoRPG-Account
importieren könnt. Ebenso werden wir einige Verbesserungen wie eine
Sprachauswahl hinzufügen. Dann wird die Version nach dieser vollkommen von
Flash befreit sein!

Bis bald mit mehr Informationen!

# Neoparc

Ein Weihnachtsevent findet seit dem 1. Dezember in
[Neoparc](https://neoparc.eternaltwin.org/) statt.
Während dieses Events kann jeder Dinoz-Meister Geschenktickets sammeln. Grabt
einfach in den verschiedenen Dinoland-Orten. Ebenso ist die berühmte
"Santaz"-Rasse zurück.

Viele glückliche Spieler haben bereits ein paar Eier dieser begehrten
Dinoz-Rasse am Weihnachtsrad von Dino-Stadt gesammelt. Kleiner Spoiler für
unsere lieben Leser: Jeder, der sich am 25. Dezember anmeldet, erhält ein
Santaz-Ei als Geschenk für dieses erste wunderbare Jahr.

Dieses Update zur Weihnachtszeit ist vollgepackt mit spannenden Geheimnissen!
Bitte genieße es, sie in deinem eigenen Tempo zu entdecken. Teile deine Funde
und was du darüber denkst mit dem Rest der Community im
[Discord-Channel](https://discord.gg/ERc3svy).

Ich wünsche euch eine wundervolle und besinnliche Weihnachtszeit!

Euer Neodev, Jonathan.

# Directquiz

[Directquiz](https://www.directquiz.org/) hat in den letzten beiden Monaten
einige neue Funktionen bekommen.

Die erste neue Funktion ist eine Eingabeanzeige, wie in vielen modernen Apps.
Sie ist jetzt in **Directquiz** verfügbar!

Als nächstes könnt ihr jetzt [neue Fragen einreichen](https://www.directquiz.org/validationQuestion.php),
ohne eure "Direct Dollars" auszugeben. Ihr könnt aus einer Liste von über 300
Kategorien wählen! Nur zu, sendet neue Fragen!

Und schließlich hat die Webseite für diese Wintersaison jetzt ein verschneites,
animiertes Thema, um das Ende des Jahres gebührend zu feiern.

Wir haben auch einige interne Verbesserungen an den Konfigurationsdateien
vorgenommen, um eine zukünftige Migration auf den Eternaltwin-Server
vorzubereiten.

# Abschließende Worte

Du kannst [deine Nachrichten für Dezember
senden](https://gitlab.com/eternaltwin/etwin/-/issues/35).

Frohe Weihnachten und einen guten Rutsch ins neue Jahr!
