# 2022-04: This Month In Eternaltwin #7

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

In recent articles, we mentioned the [Eternaltwin](https://eternaltwin.org/)
migrated from TypeScript to Rust. During the migration, we had a system providing
compatibility between both languages, so we can move piece by piece. This month
we configured internal requests to go directly to the Rust code.

This mostly worked without any issues... except for the forum.

We found a serious bug in our database client, which causes crashes when visiting
the forum. We are working on a fix. In the meantime, the forum is temporarily
disabled.

In other news, some people are starting work on Frutiparc, Kube, and MyBrute.

# Neoparc

Hello, brave [Dinoz](https://neoparc.eternaltwin.org/) masters!

In order to boost the economy, fill the vaults of the Tree Bank and fight against
the overpopulation of Dinotown, the Sovereigns of Dinoland declared
**the return of the Clan Wars**!

Coming soon... Not yet. Lower those guns...

In order to prepare you as well as possible, it is now possible to register
your Clan for a small price at the city hall of Dinotown (plan a book for the
waiting room, the guy at the reception is a real chatterbox). Once registered,
your clan will be able to accommodate your friends, your assassins and your
innkeepers. It's up to you to decorate your interior, and do good, the other
Clans can always come and spy on you... see how beautiful it is in your home,
say so!

In short, take advantage of this peace in the kingdom, because soon the horns
of war will blast everywhere, day and night! (And no, we can't file a complaint
for nocturnal noise.) This may give the Raid Boss or Daily Wistiti a chance to
escape your soon-to-be-satisfied bloodlust!

![Neoparc](./neoparc.png)

# Eternalfest

On April 1st, we released a new game and a large update on [Eternalfest](https://eternalfest.net/).

The new game is our traditional "April Fools" land: [Eternalfestfest](https://eternalfest.net/games/a7cae442-8ced-4c05-836b-605b0f64bc8f).

[La faille éternelle](https://eternalfest.net/games/a5ac93dd-28f1-4740-b94b-8292a26dbb39)
("The eternal rift") received a very large update. The dimension at the level 20
is now available, which is a labyrinth full of various mechanisms and puzzles
that spans over 50 levels. Are you ready to take on the eternal god of spikes
that awaits you at the end of it? And to end its tyranny (the feared hidden
spikes) once and for all?

We highly recommend you give it a try and (re-)discover one of the best lands
on Eternalfest.

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_ Hello everyone and thank you for using our wireless radio system!
I'm Connor and today I'm sending you a message after _scrambled_ holiday.

![MyHordes - Wireless technology ad](./myhordes.png)

You probably know that lately has been pretty quiet. I hope you were able to
rest and enjoy the Chocolate Crosses. And _scrambled_

Our dear developer friends have implemented the possibility to reset your
MyHordes soul **only once**, for those who want to continue playing on Hordes
and be able to import "at the very end". You will find detailed information on
the Import page. _scrambled_

In addition, our translators are working hard to translate the game, currently
the French translation is completely finished, as for the Spanish and English
translations, they are respectively at 97% and 93%. _scrambled_

Special Shaman cities have been created, allowing the source of disturbances to
be found on the plane of the solar plexus. Know that _scrambled_

By the way, you can now enable the Toxin in your private cities, which was
removed from the original game. Can't wait to see who will be able to _scrambled_

That was all for now, see you the next time ! _radio crackle_

_radio crackle_ What the fuck did Ben do with the cables again...?! _radio crackle_

# Closing words

You can send [your messages for May](https://gitlab.com/eternaltwin/etwin/-/issues/44).

This article was edited by: Demurgos, Dios, Dylan57, Jonathan, MisterSimple,
Patate404, Rikrdo, Schroedinger, Unpuis, and Valedres.
