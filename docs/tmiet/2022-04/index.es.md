# 2022-04: Este mes en Eternaltwin #7

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar los juegos y las comunidades de jugadores de Motion Twin.

# Eternaltwin

En artículos anteriores, mencionamos que [Eternaltwin](https://eternaltwin.org/)
está migrando de TypeScript a Node. Durante la migración, encontramos un sistema
que ofrece compatibilidad entre ambos lenguajes, por lo que podemos ir cambiando
parte por parte. Este mes hemos configurado las solicitudes internas para que
vayan directamente al código en Rust.

Esto funcionó para casi todo sin inconvenientes... excepto para el foro.

Encontramos un error muy grave en el cliente de nuestra base de datos, lo que
ocasiona una caída de la página cuando se visita el foro. Estamos trabajando en
una solución. Mientras tanto, el foro estará inhabilitado temporalmente.

En otras noticias, algunas personas han empezado a trabajar en Frutiparc, Kube,
y ElBruto.

# Neoparc

¡Hola, maestros de Dinoz de [Neoparc](https://neoparc.eternaltwin.org/)!

Con el objetivo de impulsar la economía, llenar las arcas de los bancos y luchar
contra la sobrepoblación de Dinovilla, el Soberano de Dinoland ha declarado
**El Regreso de las Guerras de Clanes**!

Próximamente... Ya no más. Bajen sus armas...

Para prepararlos lo mejor posible, ahora pueden registrar sus clanes por un bajo
costo en la Ciudad de Dinovilla (toma un libro de la sala de espera, el chico de
la recepción es todo un parlanchín). Una vez registrado, tu clan estará
habilitado para alojar apropiadamente a tus amigos, tus asesinos y tus huéspedes.
Depende de ti decorar su interior, y hazlo bien, los otros Clanes siempre vienen
a espiar... ¡para ver lo hermoso de tu hogar!

Por ahora, aprovecha de esta paz restante en el reino, ¡porque pronto los cuernos
de guerra rugirán en todo el reino, de día y de noche! (Y no, no podemos hacer
una denuncia por el ruido en la noche.) Esto podría darle al Jefe de la
Incursión o al Wistiti Diario una oportunidad de escapar de tu insaciable Sed
de sangre!

![Neoparc](./neoparc.png)

# Eternalfest

El 1ro de Abril, publicamos un nuevo juego y una actualización mayor para [Eternalfest](https://eternalfest.net/).

El nuevo juego es nuestra región tradicional de April Fools: [Eternalfestfest](https://eternalfest.net/games/a7cae442-8ced-4c05-836b-605b0f64bc8f).

[La faille éternelle](https://eternalfest.net/games/a5ac93dd-28f1-4740-b94b-8292a26dbb39)
("La grieta eterna") recibió una actualización muy grande. La paralela del nivel
20 ahora está disponile, que es un laberinto lleno de varios mecanismos y
rompecabezas que tiene más de 50 niveles. ¿Estás listo para enfrentarte al Dios
eterno de las púas que te espera al final de esta y acabar con su tiranía (las
temidas púas ocultas) de una vez por todas?

Recomendamos enormemente darle una oportunidad y(re-)descubrir una de las mejores
tierras de Eternalfest.

# MyHordes

Reproducir a [MyHordes](https://myhordes.eu/).

_estática de radio_ ¡Hola a todos y gracias por usar nuestro sistema de radio
inalámbrico! Soy Connor y hoy te mando un mensaje después de las vacaciones de
_cortado_

![MyHordes - Anuncio de tecnología inalámbrica](./myhordes.es.png)

Probablemente sepas que últimamente ha estado bastante tranquilo. Espero que
hayas podido descansar y disfrutar de las Cruces de Chocolate. Y _cortado_

Nuestros queridos amigos desarrolladores han implementado la posibilidad de
restablecer su alma MyHordes solo una vez, para aquellos que quieran seguir
jugando en Hordes y poder importar "al final". Encontrará información detallada
en la página Importar. _cortado_

Además, nuestros traductores están trabajando arduamente para traducir el juego,
actualmente la traducción al francés está completamente terminada, en cuanto a
las traducciones al español y al inglés, están respectivamente al 97 % y al 93 %.
_cortado_

Se han creado pueblos especiales de chamanes, lo que permite encontrar la fuente
de perturbaciones en el plano del plexo solar. Sepa que _cortado_

Por cierto, ahora puedes activar la toxina en tus pueblos privados, que se
eliminó del juego original. No puedo esperar a ver quién será capaz de _cortado_

Eso fue todo por ahora, ¡hasta la próxima! _estática de radio_

_estática de radio_ ¿Qué diablos hizo Ben con los cables otra vez...? _estática de radio_

# Palabras finales

Pueden enviar sus [mensajes para Mayo](https://gitlab.com/eternaltwin/etwin/-/issues/44).

Este artículo fue editado por: Demurgos, Dios, Dylan57, Jonathan, MisterSimple,
Patate404, Rikrdo, Schroedinger, Unpuis y Valedres.
