# 2022-04: Diesen Monat in Eternaltwin #7

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die
Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

In den letzten Artikeln haben wir erwähnt, dass [Eternaltwin](https://eternaltwin.org/)
von TypeScript zu Node migriert wurde. Während der Migration hatten wir ein System benutzt, das
Kompatibilität zwischen beiden Sprachen gewährt, so dass wir Stück für Stück umziehen können.
Diesen Monat haben wir interne Anfragen so konfiguriert, dass sie direkt an den Rust-Code gehen.

Das hat größtenteils ohne Probleme funktioniert... außer im Forum.

Wir haben einen schwerwiegenden Fehler in unserem Datenbank-Client gefunden, der
zu Abstürzen beim Besuch des Forums führt. Wir arbeiten an einer Lösung. In der
Zwischenzeit ist das Forum vorübergehend deaktiviert.

Weitere Neuigkeiten sind, dass einige Leute mit der Arbeit an Frutiparc, Kube
und MyBrute beginnen.

# Neoparc

Hello tapfere [Dinoz](https://neoparc.eternaltwin.org/)-Meister!

Um die Wirtschaft anzukurbeln, die Tresore der Baumbank zu füllen und gegen die
Überbevölkerung von Dinotown zu kämpfen, haben die Fürsten von Dinoland
**die Rückkehr der Clankriege** angekündigt!

Demnächst... Das war's. Runter mit den Waffen...

Um euch so gut wie möglich vorzubereiten, ist es jetzt möglich, euren Clan
im Rathaus von Dinotown registrieren zu lassen (nehmt ein Buch für das
Wartezimmer mit, der Typ am Empfang ist eine echte Quasselstrippe). Einmal registriert,
kann dein Clan deine Freunde, deine Meuchelmörder und deine
Gastwirte aufnehmen. Es liegt an dir, deine Einrichtung zu dekorieren, und mach es gut, die anderen
Clans können jederzeit kommen und dich ausspionieren... um zu sehen, wie schön es in deinem Haus ist.

Kurzum: Nutzt den Frieden im Reich, denn bald werden die Hörner des Krieges überall ertönen, Tag und Nacht! (Und nein, wir können keine Beschwerden
wegen nächtlichen Lärms berücksichtigen.) Das könnte dem Raid Boss oder Daily Wistiti die Möglichkeit geben
eurem bald befriedigten Blutdurst zu entkommen!

![Neoparc](./neoparc.png)

# Eternalfest

Am 1. April haben wir ein neues Spiel und ein großes Update für [Eternalfest] (https://eternalfest.net/) veröffentlicht.

Das neue Spiel ist unser traditionelles "Aprilscherz"-Land: [Eternalfest](https://eternalfest.net/games/a7cae442-8ced-4c05-836b-605b0f64bc8f).

[La Faille éternelle](https://eternalfest.net/games/a5ac93dd-28f1-4740-b94b-8292a26dbb39)
("Der ewige Riss") erhielt ein sehr großes Update. Die Dimension auf Stufe 20
ist nun verfügbar, die ein Labyrinth voller verschiedener Mechanismen und Rätsel ist
das sich über 50 Ebenen erstreckt. Bist du bereit, es mit dem ewigen Gott der Stacheln aufzunehmen
der am Ende des Labyrinths auf dich wartet? Und seine Tyrannei (die gefürchteten versteckten
Stacheln) ein für alle Mal zu beenden?

Wir empfehlen dir dringend, es auszuprobieren und eines der besten Länder auf dem Eternalfest (wieder) zu entdecken.

# MyHordes

Spiele hier [MyHordes](https://myhordes.eu/).

_Radioknistern_ Hallo zusammen und danke, dass ihr unser drahtloses Funksystem benutzt!
Ich bin Connor und heute sende ich euch eine Nachricht nach dem _rauschen_ Urlaub.

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Ihr wisst wahrscheinlich, dass es in letzter Zeit ziemlich ruhig war. Ich hoffe, ihr konntet euch
ausruhen und die Ostereier genießen. Und _rauschen_

Unsere lieben Entwicklerfreunde haben die Möglichkeit eingeführt, die
MyHordes-Seele **nur einmal** zurückzusetzen, für diejenigen, die mit Hordes weiterspielen wollen
und "ganz am Ende" importieren können. Detaillierte Informationen findet ihr auf
der Import-Seite. _rauschen_

Außerdem arbeiten unsere Übersetzer hart daran, das Spiel zu übersetzen.
Die französische Übersetzung ist vollständig abgeschlossen, die spanische und englische
Übersetzungen sind bei 97% bzw. 93%. _rauschen_

Es wurden spezielle Schamanenstädte geschaffen, die es ermöglichen, die Quelle von Störungen
auf der Ebene des Solarplexus zu finden. Wisse, dass _rauschen_

Übrigens könnt ihr jetzt das Toxin in euren privaten Städten aktivieren. Im
Originalspiel wurde dies entfernt. Ich kann nicht warten, zu sehen, wer in der Lage sein wird _rauschen_

Das war alles für jetzt, bis zum nächsten Mal! _Radioknistern_

_Radioknistern_ Was zum Teufel hat Ben wieder mit den Kabeln gemacht...?! _Radioknistern_

Abschließende Worte

Du kannst [deine Nachrichten für Mai senden]. (https://gitlab.com/eternaltwin/etwin/-/issues/44).

Dieser Artikel wurde bearbeitet von: Demurgos, Dios, Dylan57, Jonathan, MisterSimple,
Patate404, Rikrdo, Schroedinger, Unpuis und Valedres.
