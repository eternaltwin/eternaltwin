# 2022-02 : Ce mois-ci sur Eternaltwin n°5

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

Dans le cadre du travail pour archiver et importer les données de DinoRPG,
Eternaltwin peut désormais automatiquement créer des sessions utilisateur sur
Twinoid et les jeux Twinoid. Il s'agit d'une étape importante pour archiver
complètement Twinoid.

# eMush

Le mois de février a été consacré à l'amélioration de l'interface graphique
d'[eMush](https://emush.eternaltwin.org/).

Parmi les nouveautés, l'ajout de plusieurs éléments comme les effets d'incendie
et d'équipements cassés ou encore le surlignage des équipements sélectionnés ou
survolés. Un gros défi portant sur l'ordre d'affichage des différents éléments
d'une pièce a aussi été surmonté, mais il reste encore à résoudre des problèmes
de recherche de chemin.

Toutes ces avancées permettent d'ajouter une par une des pièces dans le Daedalus,
de là à promettre un Daedalus complet bientôt ? Vous verrez bien dans la
prochaine édition.

On est toujours à la recherche de développeurs (front et back), de plus bien que
les derniers ajouts graphiques fonctionnent d'un point de vue technique, il
serait bénéfique qu'une personne avec des notions de graphisme nous aide pour
finaliser ces aspects.

[Bonus : vidéo des effets d'incendie.](https://eternaltwin.org/assets/docs/tmie./emush.mp4)

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_grésillements de radio_

Bonjour ? Est-ce que ce truc est allumé ? _brouillé_ J'espère que vous
m'entendez tous sur notre merveilleux système radiophonique sans fil _brouillé_.
Je suis Ben, et je remplace Connor aujourd'hui, qui se sent étrangement sans
énergie malgré avoir mangé un steak savoureux plus tôt... Je me demande ce qui
se passe ? _brouillé_

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

Pour ceux d'entre vous qui avaient peur d'être seuls le jour de la Saint-Valentin,
nous avons ajouté un tout nouveau système de liste d'amis. C'est encore un peu
rudimentaire pour le moment, mais nous y ajouterons des fonctionnalités
supplémentaires au fur et à mesure que nous avancerons.

Il y a eu d'étranges observations de grands groupes de personnes avec...
_bruit de papier_ des masques de chaman ? Il semble qu'ils prévoient de se
rassembler dans une ville le **27 février**. Plusieurs chamans professionnels
dans une même ville... Qui a déjà entendu parler d'une telle chose ?
_brouillé_

En dehors de cela, nous avons été occupés à mettre à niveau la technologie de
notre serveur vers la dernière version. Je sais que cela ne semble pas
révolutionnaire pour tous les non-techniciens, mais une base technique à jour
garantit une expérience de jeu fluide et stable pour tout le monde _brouillé_
au moins après avoir résolu tous les accrocs qui sont venus avec la mise à niveau.

Et sur ce, je vous dis au revoir à tous et vous souhaite une nuit sans incident !
_brouillé_ Alors, comment je peux désactiver cela ? Peut-être en tirant sur
ce câ _grrrrrrrrz_

# Eternalfest

[Eternalfest](https://eternalfest.net/) travaille sur le nouveau système de
jeux. Le but final est de complètement gérer les objets et quêtes. La première
étape sera de mettre à jour le système de permissions pour permettre aux
joueurs normaux de beta-tester des niveaux.

Le loader a été mis à jour. Cette nouvelle version a une meilleure gestion
de rapports d'erreurs pour aider les créateurs à comprendre les crashs. Elle
devrait être publiée bientôt, avec un nouveau compilateur de jour qui
améliore aussi ses messages d'erreurs.

# Neoparc

[Neoparc](https://neoparc.eternaltwin.org/) is focusing on internal fixes and
improvements. The most import change is the change of the database system
to improve performance and reliability. We are moving from ElasticSearch to
PostgreSQL. We successfully transferred part of our data this month: messages
and bazar listings. Dinoz and accounts still use the old system.

# Le mot de la fin

Vous pouvez envoyer [vos messages pour
l'édition de mars](https://gitlab.com/eternaltwin/etwin/-/issues/40).

Cet article a été édité par : Brainbox, Breut, Demurgos, MisterSimple, Patate404 et Schroedinger.
