# 2022-07: Este mes en Eternaltwin #10

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

Hemos habilitado el nuevo servidor y su sistema de despliegue. Los Líderes de Proyectos ahora tienen la capacidad de enviar actualizaciones por su propia cuenta. Neoparc es el primer proyecto en usar este servidor nuevo para su versión "beta". Estaremos en contacto con otros proyectos para ayudarlos a migrar al sistema nuevo.

# Eteneralfest

En el 26 de julio de 2022, el juego más grande de [Eternalfest](https://eternalfest.net/) tuvo su actualización final. **Hackfest** ahora está completo luego de 10 años de trabajo.

Este juego tiene más de 1600 niveles, misiones y logros. ¡Regresa a Hackfest y supera los retos en el Valhalla y el Laberinto Final, y enfrenta al poderoso Eternal para salvar el mundo una vez más!

# Eternal DinoRGP

El equipo de [Eternal DinoRPG](https://dinorpg.eternaltwin.org/) está relativamente callado en Discord respecto al desarrollo recientemente. Pero no se preocupen, que todavía estamos trabajando activamente en el proyecto. El enfoque actual está centrado en migrar a un nuevo administrador de bases de datos (de sequelize a TypeORM, para los curiosos). Este cambio es necesario para dar al equipo la habilidad de actualizar la arquitectura de la base de datos y esquemas sin borrar todo. Esto es una cantidad de trabajo enorme, lo cual también nos brinda la oportunidad de limpiar nuestro código base y reducir la deuda de código. Nos acercamos al final de este trabajo y estamos satisfechos con el progreso y las nuevas ventajas y beneficios que otorga.

Además, Swagger está siendo integrado para los curiosos que quieran jugar con nuestra API de manera directa. Por último, pero no menos importante, todas las librerías de nuestros proyectos están en proceso de ser actualizadas.

Jolu y Biosha se tomarán unas vacaciones de verano (no juntos ;D). ¡Volverán al inicio del año escolar para finalizar 0.4.0!

_Unas breves palabras de Biosha, el Líder del proyecto:_

> Desde mi perspectiva personal, ha pasado poco más de un año desde que
> tomé el liderazgo del proyecto. Estoy muy contento con todo el trabajo
> que se ha hecho.
> Hace un año, sólo podías conectarte a EternalTwin y comprar un dinoz.
> Hoy, puedes comprar dinoz, verlo, moverlo, ganar experiencia y niveles,
> aprender habilidades, conseguir estados, comprar objetos en las tiendas,
> publicar noticias, ver el ranking, entre otras acciones.
> Hemos aprendido mucho a lo largo de este proyecto, y todavía estamos
> aprendiendo.
> Un año atrás, teníamos como objetivo publicar nuestra primera versión
> en línea. ¡En unos meses, y gracias a Demurgos, la versión 0.1.0 fue
> publicada en el 11 de diciembre de 2021! Desde entonces, sucedieron
> 114 commits y 25k líneas de código fueron cambiadas.
> El camino por delante todavía es largo hasta una versión cercana al
> juego original, pero todavía tenemos la esperanza de que nos
> estamos acercando cada día.

Muchísimas gracias a todos por la ayuda y el apoyo. ¡Felices vacaciones de verano!

# Neoparc

Saludos, [maestros Dinoz](http://neoparc.eternaltwin.org/). Empecemos esta edición de EMEET con el trabajo actual. Como muchos de ustedes ya saben, estamos preparando un nuevo sistema de "guerras de clanes" ajustado a la cantidad de jugadores activos de Neoparc. Este sistema será ligeramente alterado comparado a lo que era en Dinoparc, pero se mantendrá igual de divertido y competitivo, como podrán recordar del pasado. No habrá cambio alguno a los Dinoz oscuros y totems, ¡pero si habrá otros cambios! Además, trabajamos en el centro de misiones de Dinovilla, ¡el lugar perfecto para obtener monedas extra y objetos por tus tareas diarias! El ritmo es un poco más lento durante el verano, pero todavía estamos verdaderamente entusiasmados con el proyecto.

Desde un lado más técnico, Neoparc pronto se mudará a un servidor más poderoso y rico en funciones. No tendrá impacto directo en ustedes, pero hará que sea más fácil a la hora de desplegar parches y arreglos de bugs. Ya hemos migrado la versión beta de Neoparc, mientras que la versión principal será migrada en agosto.

Además, hemos logrado conseguir una reunión de una hora con **warp**, ¡el creador original de Dinoparc de la era de Motion-Twin!. Comenzamos introduciendo a Eternaltwin, los diferentes proyectos, y Neoparc. Luego le presentamos Neoparc (la demo actual) y nos concentramos en las nuevas características como las incursiones semanales, la "Goupignon", los dinoz "cereza", la técnica de granjeo, el Bazar renovado, y los objetos nuevos. Warp parecía muy conmovido al ver que nuestra comunidad logró preservar su legado, y confía muy profundamente en nuestro proyecto. Le pedimos que contacte a sus antiguos compañeros de trabajo de Motion-Twin para que actualicen la página web de Dinoparc para que redireccione a Neoparc. No hay ninguna promesa hecha, pero es bueno que hayamos podido preguntarle de manera directa.

En general, ha habido pocas nuevas funciones este mes, mientras que hubo muchas mejoras importantes y contactos en el fondo.

Les deseo a todos que tengan un bello día en Dinoland.

Hasta la próxima, Jonathan.

# eMush

Bienvenidos a la sección de **[eMush](https://emush.eternaltwin.org/)** de _Este Mes En Eternaltwin_ para julio de 2022, el proyecto de preservación de Mush.

## ¿Qué hay de nuevo?

Las últimas noticias que publicamos fueron en mayo de 2022. La versión 0.4.2 del juego se veía algo así:

![0.4.2](./emush-0.4.2.png)

Actualmente, la versión 0.4.4 (todavía en desarrollo) se ve algo más así:

![0.4.4](./emush-0.4.4.png)

Como pueden ver, estos últimos dos veces tuvieron **muchas mejoras**, la principal siendo la **interfaz isométrica 2D** de la versión 0.4.3. Fue extensivamente probada por nuestros testeadores alfa en junio.

Hemos pasado julio arreglando bugs encontrados durante la versión alfa. La mayoría de los problemas de juegos ya fueron arreglados. Los errores gráficos lo serán pronto.

La próxima **gran actualización** del juego está viendo muchísimo progreso: **enfermedades**. Pronto (re)descubrirán algunos **efectos "divertidos"**...

![Rabias espaciales](./emush-disease.png)

**Enfermedades físicas** ya están casi completas. Nuestra meta es implementar **enfermedades mentales**, por lo que la actualización estará lista para **octubre**.

## Próximos pasos

Proseguiremos trabajando con las **habilidades**

Esto también significa que implementaremos casi todas las acciones del juego original. Ahora mismo, **40% de las acciones se encuentran disponibles** para todos los personajes... incluyendo al increíble Discurso Aburrido.

![Discurso Aburrido](./emush-speech.png)

(...sí, es real: hemos arreglado el bug así que ahora recibes apropiadamente 3MP 😳)

## Roadmap

Como noticia, todavía nos faltan algunas funciones importantes en eMush:

- 🚧 Enfermedades y heridas
- ❌ Habilidades
- ❌ Investigación
- ❌ Proyectos
- ❌ La comunicación terminal
- ❌ Cazadores y pilotaje
- ❌ Planetas y viaje
- ❌ Expediciones

Sin embargo, las siguientes funciones ya se encuentran disponibles (en la versión alfa cerrada):

- ✅ Unirse a Daedalus con otros 15 jugadores, con vista isométrica
- ✅ Ser Mush, conseguir esporas, infectar humanos
- ✅ Golpear y matar a los Mushs (o humanos)
- ✅ Temblores duraderos, incendios, fallas de los motores... y puedes curarte a ti mismo, extinguir los incendios, arreglar los problemas... (o ser asesinado por NERON una vez que no haya humanos)

Más o menos como el juego original, ¿no? 🙂

## ¡Genial! ¿Cómo se juega?

Por ahora, el juego sólo está disponible para las sesiones alfa cerradas. Las realizamos en cada actualización grande.

Les haremos saber cuándo será la próxima sesión. Por favor, únanse al [Discord de Eternaltwin](https://discord.gg/ERc3svy) para tener las últimas novedades respecto al proyecto.

¡Muchísimas gracias a todos por su apoyo y ayuda! ¡Esperamos verlos luego en Daedalus!

# Palabras finales

Pueden enviarnos sus [mensajes para agosto](https://gitlab.com/eternaltwin/etwin/-/issues/52)

Este artículo fue editado por: Biosha, Demurgos, Evian, Fer, Jonathan, MisterSimple, Patate404, SylvanSH, and Schroedinger.
