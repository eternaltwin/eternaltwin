# 2022-01: Dieser Monat in Eternaltwin #4

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die
Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Wir haben die Migration aller [Eternaltwin](https://eternaltwin.org/)
Dienste auf Rust abgeschlossen.

Diese Migration war das Hauptthema des Jahres 2021, und wir sollten nun die Früchte ernten:
Zuverlässigkeit und Leistung.

## Release v0.10.0

Wir haben die Version **0.10.0** von [dem Eternaltwin-Paket](https://www.npmjs.com/package/@eternal-twin/cli) veröffentlicht.
Wir haben auch [den PHP-Client](https://packagist.org/packages/eternaltwin/etwin) entsprechend aktualisiert.
Die Clients für andere Plattformen werden nach und nach aktualisiert.

Mit diesem Update werden vor allem einige kleinere interne Probleme behoben. Du kannst [die vollständigen Änderungen in diesem Merge Request nachlesen](https://gitlab.com/eternaltwin/etwin/-/merge_requests/410).

## Nächste Funktionen

Wir werden daran arbeiten, bestehende Probleme zu lösen, Übersetzungen zu korrigieren und
einige Stiländerungen einzufügen.

Die nächste Funktion wird die Behebung von Problemen bei der Kontenverwaltung sein: Meldung
aussagekräftiger Fehler, Probleme mit der Weiterleitung, Zurücksetzen von Passwörtern, etc.

Wir beginnen auch mit der Planung der Umsetzung des gemeinsamen Belohnungssystems
(Titel und Icons).

# Neoparc

Hallo, lieber Dinoz-Meister von [Neoparc](https://neoparc.eternaltwin.org/)!

Zunächst einmal wurden die wöchentlichen Schlachtzüge in Bezug auf die Zeiten und die Werte der Bosse neu ausbalanciert.

Die meisten der fehlenden Fähigkeiten wurden dem Spiel hinzugefügt.

7 verschiedene neue Fähigkeiten stehen zur Verfügung, so dass jedes theoretische Element für das klassische Dinoz ausgewogen ist. Jetzt ist es an dir, sie zu entdecken!

Schließlich gab es noch einige Änderungen am Basar. Ihr könnt nun Gegenstände aus der geheimen Höhle über den Basar tauschen.

Viel Spaß beim Spielen!

# Eternal Kingdom

[Eternal Kingdom](https://kingdom.eternaltwin.org/) lässt dich jetzt mit deinem
Eternaltwin-Konto spielen! Viel Spaß und versuche, die Rangliste zu erklimmen!

## Wichtigste Merkmale

Wir haben jetzt ein großartiges Logo (done by **Seidanoob#5281**).

Es gibt jetzt ein Protokoll für deine Stadt: Du musst dir nicht mehr merken, was du getan hast.

Die aktuellen Seiten wurden fast vollständig ins Englische und Spanische übersetzt.
Vielen Dank an alle.

Du kannst nun deine Generäle rekrutieren. 💯

Die Fußzeile der Webseite wurde aktualisiert und an Eternaltwin angepasst. 💃

Einige neue Seiten beginnen zu erscheinen, auch wenn sie noch nicht ganz fertig sind (Rangliste,
Weltauswahl, ...).

## Wichtigste Verbesserungen

Du musst die Seite nach Benutzeraktionen nicht mehr aktualisieren. Du kannst
auf der Seite für die Gebäudeauswahl bleiben und die Schaltflächen werden
automatisch aktualisiert. Die Bauoption wird aktiviert, sobald ein Baumeister verfügbar ist
(oder deaktiviert, wenn du deinen letzten Baumeister verlierst).

Als Spieler/Tester, bitte teile dein Feedback im [Eternal Kingdom
Forum](https://eternaltwin.org/forum/sections/407e8a9e-8c5a-4177-b13c-9a7d746c7d24) mit.
Oder direkt [auf Discord](https://discord.gg/ERc3svy) im **#kingdom**-Kanal.

Wenn du bei der Entwicklung helfen willst, suchen wir einen BACKEND-Entwickler (PHP/Symfony
Server) oder Frontend-Entwickler (Web: HTML/JS/CSS).

Danke für eure Unterstützung 😄

# MyHordes

_Radioknacksen_

Hallo Leute und danke, dass ihr unser drahtloses Tonfunksystem benutzt! Ich bin
Connor und heute sende ich euch eine kleine Nachricht bezüglich
[MyHordes](https://myhordes.eu/).

![MyHordes - Werbung für drahtlose Technologie](./myhordes.de.png)

Wie ihr sicherlich wisst, war es in letzter Zeit eher ruhig. Ich hoffe, ihr
konntet euch ausruhen. Und _rauschen_

Zu einem anderen Thema wollte ich ankündigen, dass ihr die Spieleseite in Kürze
ganz normal über Suchmaschinen finden solltet. In der Hoffnung, dass keine
Zombies zur gleichen Zeit kommen... Auch _rauschen_

Die Stadtratsversammlungen sind in den Städten in vollem Gange. Das Spiel wurde
global optimiert und ist auf allen Plattformen schneller. Es ist noch nicht
alles perfekt, aber die Entwickler machen Fortschritte in ihrem eigenen Tempo.
In diesem Zusammenhang sende ich euch diesen kleinen Hinweis, dessen Echtheit
ich euch aber nicht beweisen kann, da er schon unzählige Male kopiert wurde.

---

_Brainbox_: Hallo zusammen.

_Brainbox_: Der erste Punkt auf der Tagesordnung ist die Wahl neuer Spielinhalte für Saison 15. Irgendwelche Ideen?

_Shaitan_: Neue seltene Auszeichnungen!

_Adri_: Weniger Bürger pro Stadt, in kürzerer Zeit?

_Brainbox_: Also... hat niemand irgendwelche guten Ideen?

_Dylan57_: Ich schlage vor, das komplette Heldensystem zu überarbeiten.

_Adri_: Ist er da?

_Shaitan_: Naja, so bekannt ist er nicht.

_Nayr_: *eh eh*

_Dylan57_: Habt ihr das gehört?

_Allen steht die Erschöpfung ins Gesicht geschrieben._

_Brainbox_: Nun, da ihr alle keine Hilfe seid, werde ich eben in die Ideenbox schauen.

_Shaitan_: Oh nein!

_Dylan57_: Ernsthaft?

_Adri_: Trottel.

_Brainbox_: Wir fangen sofort mit dem zufälligen Ziehen an.

_Brainbox_: Jeder von euch kommt und schnappt sich einen der Ideenzettel aus meiner Hand.

_Shaitan_: Warum machen wir nicht zur Abwechslung eine Umfrage?

_Dylan57_: Klar Einstein, mit 34 beschissenen Ideen pro Sekunde...

_...Niemand weiß, was mit den Zetteln geschieht. Aber sie wurden gezogen..._

---

Das war alles für jetzt, bis zum nächsten Mal! _Radioknacksen_

# Eternal DinoRPG

Die Arbeit an [eDinoRPG](https://dinorpg.eternaltwin.org/) geht weiter.

Wir haben unser Ziel für das Ende des Jahres erreicht, indem wir den DinoRPG "scraper" eingeführt haben,
ein Werkzeug zum Archivieren von Daten von der offiziellen Website. Damit können wir Dinoz-Fähigkeiten,
geopferte Dinoz und alle anderen Daten, die in der API fehlen, archivieren.
Wir arbeiten jetzt daran, es auf dem Eternaltwin-Server zu aktivieren.

In der Zwischenzeit bereiten wir bereits die Version 0.2.0 vor!

Sobald der Import von Konten möglich ist, werden wir auch Folgendes einrichten: Einen Ladebildschirm,
Fehler-Pop-Ups, eine Profilseite, eine Sprachauswahl, ein System zum Verschieben des Dinoz.

Wir arbeiten auch an Bestenlisten und epischen Belohnungen.

Bis wir alles veröffentlichen, gibt es hier eine kleine Vorschau:

![Bewegungsvorschau zu Eternal DinoRPG](./edinorpg.gif)

# ePopotamo

Die Alphaversion des Scrabble-ähnlichen Wortspiels [ePopotamo](https://epopotamo.eternaltwin.org/) ist der jüngste Neuzugang in der Liste der Eternaltwin-Spiele!

Wir haben jetzt offiziell geöffnet, du kannst es hier ausprobieren: [https://epopotamo.eternaltwin.org/](https://epopotamo.eternaltwin.org/).

Im Moment ist es eine Alpha-Version. Das heißt, du kannst es ausprobieren und
Probleme melden. Die Grundlagen sind vorhanden: Ein einzelnes Spiel, Buchstaben setzen, Wörter überprüfen,
Optionen, Spielstände, etc. Mit der Zeit werden wir das Spiel verbessern, bis es vollständig
fertig ist.

Auch hier gilt: Bitte melde alle Probleme, die du findest!

# Abschließende Worte

Wir haben 2021 einige Spiele veröffentlicht und hoffen, 2022 noch mehr zu veröffentlichen.
Vielen Dank an alle für eure Hilfe!

Du kannst [deine Nachrichten für Februar](https://gitlab.com/eternaltwin/etwin/-/issues/39) senden.

Dieser Artikel wurde bearbeitet von: Bibni, Biosha, Demurgos, Dylan57, Fabianh,
Jonathan, MisterSimple, Nadawoo, Patate404 und Schroedinger.
