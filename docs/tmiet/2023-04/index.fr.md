# 2023-04 : Ce mois-ci sur Eternaltwin n°19

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux et communautés de joueurs de Motion Twin.

# Eternaltwin

Eternaltwin a besoin de développeurs ! Les différents jeux avancent à leur rythme mais le site principal servant à tout unifier a du mal à avancer. Si vous êtes intéressez, contacter sur Discord Patate404#8620, Demurgos#8218 ou Biosha#2584.

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

Bonjour à tous,

Nous vous remercions pour les propositions pour le concours d'art de MyHordes, cependant nous avons remarqué que nous n'avons pas beaucoup de propositions, peut-être en raison d'un délai assez court, pour cette raison nous avons décidé de vous donner un peu plus de temps. La nouvelle date de fin est le 14 mai 2023 !

Le Duel des Huit, 2ème édition approche. Des citoyens de tous les horizons se rassembleront le 15 juin pour batailler gaiement, chacun défendant son métier de cœur.

![MyHordes - DDH](./MH.png)

Cet événement est ouvert à tous !

Pour participer, contactez votre maire (sur le Forum Monde ou par MP). Il a jusqu'au 20 mai (dernier délai) pour regrouper sous sa bannière une fière équipe de 10 joueurs.

- [éclaireurs](https://myhordes.eu/jx/forum/1563/68343)
- [fouineurs](https://myhordes.eu/jx/forum/1563/69642)
- [ermites](https://myhordes.eu/jx/forum/1563/69317)
- [gardiens](https://myhordes.eu/jx/forum/1563/68124)
- [techniciens](https://myhordes.eu/jx/forum/1563/68386)
- [apprivoiseurs](https://myhordes.eu/jx/forum/1563/68433)
- [habitants](https://myhordes.eu/jx/forum/1563/70131)
- [chamans](https://myhordes.eu/jx/forum/1563/68821)

⚠​ Règle fair-play : ne postulez que pour un seul métier à la fois. Pour les règles détaillées et pour poser vos questions : le topic officiel de l'événement.

À gagner : la couronne à huit pointes et moulte gloire.

Les perdants repartent avec un soupçon de gloire et moulte fun (dans la limite des stocks disponibles)

En vous souhaitant une douce mort,

Les orgas : Renya, Arendil et -HS-

# eMush

Bienvenue à tous dans cette section de consacrés à
[🍄 eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

Pas de gazette ce mois-ci, l'équipe prend une pause 🏖

On se retrouve bientôt pour la mise à jour introduisant les hunters 👀

🎮 Jouer à [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk) (remonter un bug, discuter avec les développeurs...)

Merci de nous avoir lu, et à bientôt sur le Daedalus !

# Eternal DinoRPG

Bonjour tout le monde et merci de votre attention pour cette partie dédiée à [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

Salutations Maîtres Dinoz !

Pas de gazette ce mois-ci l'équipe avance autant que possible sur la refonte de l'architecture. Ca prend du temps et rien n'est visible.

L'équipe recherche toujours un nouveau nom pour le jeu. Ne vous retenez pas !

À bientôt. L'équipe EternalDino.

# Alphabounce, Frutiparc et KadoKado

Blue112 a réimplémenté plusieurs de ces mini jeux sur son site. Pour le moment aucuns score n'est sauvegardé et il n'y a pas d'interface reliant le tout, mais les jeux sont jouables !

Ca se passe sur son [site perso](https://tmp.blue112.eu/games/) et devrait être intégré à Eternaltwin prochainement.

# Closing words

Vous pouvez envoyer [vos messages pour l'édition de Mai](https://gitlab.com/eternaltwin/etwin/-/issues/61).

Cet article a été édité par : Biosha, Demurgos, Dylan57, Evian, Fer, Patate404 et Schroedinger.
