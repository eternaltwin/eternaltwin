# 2023-04: Este Mes En Eternaltwin #19

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar
los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

¡EternalTwin necesita desarrolladores! Todos los juegos avanzan a su propio ritmo, pero el sitio web principal utilizado para unificarlo todo lo está pasando mal. Si estás interesado, comunícate en Discord con Patate404#8620, Demurgos#8218 o Biosha#2584.

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

¡Hola a todos!

Le agradecemos las presentaciones para el concurso de arte de MyHordes, sin embargo hemos notado que no tenemos muchas presentaciones, tal vez debido a una fecha límite bastante corta, por esta razón decidimos darles un poco más de tiempo. La nueva fecha de finalización es el 14 de mayo de 2023, [toda la información está disponible en este tema](https://myhordes.eu/jx/forum/9/68041). ¡A sus lápices!

# eMush

Bienvenidos a esta sección dedicada a [🍄 eMush](https://emush.eternaltwin.org/),
el proyecto dedicado a salvar Mush.

No hay gaceta este mes, el equipo se está tomando un descanso 🏖️

Nos vemos pronto para la actualización que introducirá a los cazadores 👀

🎮 Juega [eMush](https://emush.eternaltwin.org)

💬 [Discordia](https://discord.gg/SMJavjs3gk)

¡Gracias por leernos, y nos vemos pronto en el Daedalus!

# Eternal DinoRPG

Hola a todos y gracias por su atención a esta parte dedicada a [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

¡Saludos, Maestros Dinoz!

Tampoco hay boletín este mes, el equipo está trabajando duro en la refactorización del código.

El equipo todavía está buscando un nuevo nombre para el juego. ¡No se detengan!

Nos vemos pronto. El equipo de EternoDino.

# Alphabounce, Frutiparc y KadoKado

Blue112 ha vuelto a implementar varios de estos minijuegos en su sitio. Por el momento, no se guarda ningún puntaje y no hay una interfaz que vincule todo, ¡pero los juegos son jugables!

Está en su [sitio personal](https://tmp.blue112.eu/games/) y debería integrarse a Eternaltwin pronto.

# Palabras finales

Pueden enviarnos [sus mensajes de mayo](https://gitlab.com/eternaltwin/etwin/-/issues/61).

Este artículo ha sido editado por: Biosha, Demurgos, Dylan57, Evian, Fer, Patate404 y Schroedinger.
