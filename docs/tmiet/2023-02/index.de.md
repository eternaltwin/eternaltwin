# 2023-02: This Month In Eternaltwin #17

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Eternaltwin braucht Entwickler! Alle Spiele schreiten in ihrem eigenen Rhythmus voran, aber die Haupt-Website, die alles vereinheitlichen soll, hat eine schwere Zeit. Wenn du interessiert bist, kontaktiere Patate404#8620, Demurgos#8218 oder Biosha#2584 auf Discord.

# eMush

Willkommen in diesem Bereich, der [🍄 eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

![eMush - Spores for All](./emush.png)

Mit seinem *Spores for All!-Update** ist eMush in die **offene Alpha** gestartet!
👉 [🍄 eMush](https://emush.eternaltwin.org/) 👈

Die wichtigsten Neuerungen des "Spores for All!-Update" sind:

🇬🇧 **Englische Sprachunterstützung**: NERON kann dich jetzt in der Sprache von Shaskespeare beleidigen! 🇬🇧

🗃 **Ranglisten- und Endgame-Seiten**! Jetzt kannst du der Welt zeigen, wie du bei dem Versuch, die menschliche Rasse zu retten, (erbärmlich) gestorben bist. 🏅

💾 Admin-Pane- und Datenbankmigrationen: Wir können jetzt viel einfacher neue Funktionen hinzufügen, ohne dass eure Exploits entfernt werden müssen! 😭 🎉

Dies ist ein wichtiger Meilenstein für eMush. Wir sind sehr zufrieden damit, wie weit wir gekommen sind, und wir hoffen, dass wir euch in Zukunft häufiger Änderungen bieten können.

Denkt daran, dass es sich noch um eine unvollständige Alpha-Version handelt und dass die Daedalus nicht gegen schwarze Löcher gefeit ist, die ohne Vorwarnung zwischen zwei Codezeilen auftauchen könnten...

In diesem Sinne: Viel Spaß und bis bald bei Forschung und Projekten!

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

Hallo zusammen, ich hoffe, dass es euch in dieser kalten und windigen Jahreszeit gut geht _rauschen_

![MyHordes - Wireless technology ad](./myhordes.png)

Saison 15 von MyHordes wurde soeben veröffentlicht!

Wie vereinbart, handelt es sich bei dieser Saison nur um eine Übergangssaison, die viel Identisches von den Hordes-Gameplay-Daten enthalten wird, die von Skool bereitgestellt werden.

Aber nicht nur! Wir haben auch verschiedene Änderungen und Verbesserungen vorgenommen.
Ich lade dich ein, das Spiel zu spielen, um die komplette Liste der Änderungen zu entdecken.

**Und vergesst nicht, dass der Tod in MyHordes nichts bedeutet.

# Eternal DinoRPG

Willkommen in diesem Bereich, der [Eternal DinoRPG](https://dinorpg.eternaltwin.org/) gewidmet ist.

Seid gegrüßt, Dinoz-Meister!
Nach Version 0.5.0 hat sich ein Teil des Teams auf den Import von Original-Dinorpg-Accounts konzentriert. Wir wollen auch nicht, dass jemandes Dinoz verschwinden! Das Team hofft, nächsten Monat gute Fortschritte in diesem Bereich zu machen.

Der andere Teil des Teams arbeitete an spielbezogenen Inhalten und veröffentlichte die Version 0.5.1:

- ausgewogenere Monster, die etwas weniger gemein zu deinem Dinoz sind
- der alte Joe hat Gerüchte über Dinoland gesammelt und kann dir sagen, wie du helfen kannst
- schließlich sollten die Dinoz-Fähigkeiten, die sich auf maximale Lebenspunkte und die Elemente auswirken, jetzt berücksichtigt werden

Außerdem hat mir jemand in letzter Minute ins Ohr geflüstert, dass für das Spiel ein neuer Name gesucht wird. Haltet euch nicht zurück!
Danke schön! Das EternalDino-Team.

# DinoCard
Dinocard ist zurück! Hier ist ein kleiner Vorgeschmack.
![Dinocard - Sneak peak](./dinocard.png)

Wir haben damit begonnen, das Spiel wieder zum Leben zu erwecken!

Die Website ist noch nicht verfügbar, aber wir brauchen dich, um [den Namen der zukünftigen Version des Spiels zu wählen](https://forms.gle/eZrtjfrcfUVgfAxu9).

Weitere Neuigkeiten im März 😄

# Abschließende Worte

Du kannst deine [Nachrichten für März](https://gitlab.com/eternaltwin/etwin/-/issues/59) senden.

Dieser Artikel wurde bearbeitet von: Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Jahaa, Patate404, Schroedinger und Zenoo.
