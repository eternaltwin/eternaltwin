# 2023-03: Dieser Monat in Eternaltwin #18

[Eternaltwin](https://eternaltwin.org) ist ein Projekt mit dem Ziel, die Spiele und Spielergemeinschaften von Motion Twin zu erhalten.

# Eternaltwin

Eternaltwin braucht Entwickler! Alle Spiele schreiten in ihrem eigenen Rhythmus voran, aber die Haupt-Website, die alles vereinheitlichen soll, hat eine schwere Zeit. Wenn du interessiert bist, kontaktiere Patate404#8620, Demurgos#8218 oder Biosha#2584 auf Discord.

Das Update 0.12 steht kurz bevor und konzentriert sich auf die Freigabe der Pakete für Entwickler.

# DinoCard

Die erste Version der Website ist jetzt online. Es ist bereits möglich, ein eigenes Deck zu erstellen und es gegen andere Spieler zu testen.

In diesen ersten Monaten der Entwicklung haben wir die Kampfschleife implementiert und insgesamt 96 Karten (71 Dinoz und 25 Beschwörungen) eingebaut. Es ist bereits möglich, Kämpfe mit fast allen Spezialfähigkeiten der Karten (25 / 28) zu testen.

Der nächste Schritt des Projekts besteht nun darin, das Innenleben des Effektsystems zu implementieren, das für die Implementierung fast aller anderen Karten im Spiel verwendet werden wird. Wir haben auch darüber gesprochen, wie man den Spielern Belohnungen geben kann. Ihr könnt es auf dinocard.net oder im zugehörigen Discord-Thread nachlesen.

Hier sind einige Statistiken über die Karten im Spiel.

![DinoCard - Cards stats](./dinocard.png)

# Popotamo

Nach einer (sehr) langen Unterbrechung wurde die Entwicklung in den letzten Märztagen unter dem Einfluss von @Aredhel wieder aufgenommen. Seine erste Aktion war es, den größten und schwierigsten Fehler zu beheben: einige legitime Wörter wurden im Falle von Mauerwerk abgelehnt und verhinderten die Validierung des Zuges. Der Patch wartet darauf, auf die ePopotamo-Website hochgeladen zu werden.

# MyHordes

Spiele es hier: [MyHordes](https://myhordes.eu/).

Hallo zusammen!

Eine kleine Ankündigung, um euch mitzuteilen, dass ein Kunstwettbewerb eröffnet ist. Er endet am 30. April. [Alle Informationen zu diesem Thema sind hier verfügbar.](https://myhordes.eu/jx/forum/9/68041). An die Stifte, fertig, los!

# eMush

Willkommen in diesem Bereich, der [🍄 eMush](https://emush.eternaltwin.org/) gewidmet ist, dem Projekt zur Rettung von Mush.

## Was ist neu?

Letzten Monat haben wir die Veröffentlichung des "Spores for All!-Update" in der offenen Alpha-Phase angekündigt.

Mehr als 150 von euch haben sich einem Schiff angeschlossen!

Wir haben den März damit verbracht, das Spiel dank eurer zahlreichen Rückmeldungen zu stabilisieren: Das gesamte eMush-Team dankt euch sehr! ❤

![eMush Spore Update for All](./emush1.png)

Außerdem hatten wir diesen Monat die Gelegenheit, mit Blackmagic, einem der Entwickler von Mush, zu sprechen, gefolgt von einer Reihe von Fragen und Antworten an die Community.

Es war eine Gelegenheit, über die Entscheidungen, die bei Mush getroffen wurden, zu sprechen, was für uns sehr bereichernd war.

Wir sind aus diesem Interview mit vielen guten Ratschlägen und einer Menge Motivation hervorgegangen, um euch ein großartiges Mush-Remake zu bieten.

## Demnächst

Während dieses arbeitsreichen Monats und nach eurem Feedback haben wir auch mit der Arbeit an der nächsten Version von eMush begonnen, die Jäger einführen wird.

Wir können die Startschiffe mit Geschütztürmen bereits in der Entwicklungsversion eliminieren:

![eMush Coming soon](./emush2.png)

Wir beabsichtigen, euch dieses Update sehr bald anzubieten und fügen regelmäßig neue Funktionen (Patrouillen, Trümmer) hinzu.

## Super! Wie können wir spielen?

🎮 Spiele [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk)

Danke fürs Lesen und bis bald auf der Daedalus!

# Eternal DinoRPG

Willkommen in diesem Bereich, der [Eternal DinoRPG](https://dinorpg.eternaltwin.org/) gewidmet ist.

Seid gegrüßt, Dinoz-Meister!

Version 0.6.0 ist bereit, online gestellt zu werden. Leider sind noch keine Importe verfügbar, aber wir tun unser Bestes, um dies so bald wie möglich zu erreichen.

Es gibt keine großen Neuigkeiten, aber eine große Überarbeitung des Codes ist im Gange, nachdem wir uns aus dem Projekt zurückziehen konnten.

Das Team ist immer noch auf der Suche nach einem neuen Namen für das Spiel. Haltet euch nicht zurück!

Wir sehen uns bald wieder. Das EternalDino-Team.

# Abschließende Worte

Du kannst deine [Nachrichten für April](https://gitlab.com/eternaltwin/etwin/-/issues/60) senden.

Dieser Artikel wurde bearbeitet von: Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Patate404, Schroedinger und Zenoo.
