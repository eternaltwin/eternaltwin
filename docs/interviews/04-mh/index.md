# MyHordes dev team

## **Eternaltwin:** Hello everyone! Thank you for taking a few moments to answer these questions. Most of the readers know you, but can you introduce yourself in a few words?

**Nayr:** I'm Nayr, 27 years old, from the middle of France, and I'm an Oracle and support of the core team on Myhordes, I joined the project during the alpha and I've been more and more invested until becoming Oracle, for those who don't know the role, we're doing the link between players and admins. I've been a Hordes player for a while, can't remember from which season exactly as I had a previous account which was deleted but on my Hordes account, my oldest towns are from season 6. Apart from that I'm a big fan of cars and video games in general and I work as an IT technician.

**Dylan57:** I'm Connor, but you can call me Dylan too, i'm 25 years old, i live in the Northeast of France, close to the German border, i'm one of the lead of the MyHordes project.
I joined Eternaltwin on April 3, 2020, shortly after MotionTwin's first announcement that indicated a potential shutdown and removal of any paid aspects of Twinoid, then joined Brainbox on May 7, 2020 for "development support" of the game and the administration of the game. I have been playing Hordes since Season 3, before the integration of Twinoid, a game I knew thanks to my older brother.
I enjoy reading crime books, as well as "you are the hero" books, I am a good client of single player games with a great story. My passion is to take everything apart and understand how things work, even if it's not easy sometimes; and this goes well with my job: maintenance technician, so yes I am not a developer and I sort of learned on the job with the other MyHordes developers.

**Adri:** Adri, 25 years old from the South (Dordogne, and now living in Normandy), I joined ET then the MH team right after the first shutdown announcement. I've played Hordes since right after the armageddon. I love coding and rhythm games (e.g. osu!mania). I got my master's degree in software engineering in late 2021 and I've worked full-time ever since, but I still try to contribute to the project on any side. This is always a great learning experience due to the approach of this project.

**ChehTan:** ChehTan, also known as Ludofloria, living in the NorthEast too.
I've been one of the first to join Brainbox on his adventure to create MyHordes. I joined the ET project when it was announced and their discord created. It's where I heard about MyHordes.
I'm a Network & Systems Administrator, with a developer background.
I have been a Hordes player since its beginning (season 0 baby), but wasn't a regular player back then.
I give some of my worksparetime to MyHordes, but not as much as I want, mainly due to a heavily loaded IRL currently.

## **Eternaltwin:** We know Brain is focused on MH every week end, but what about you? How do you organize your personnal life, professional life and MyHordes life?

**ChehTan:** Well, since my personnal life gives me almost no time for anything else than my family and my house, it's my professional life that I need to split between MH and my official work.
So basically, I work for my employer, when I have some « free » time there I work on MH-related stuff, and then at the end of day, back to family x]

**Nayr:** When I had my previous job, I had a lot of free time (I was answering calls and when there was no calls, I had nothing to do + was on remote work 3 days a week) so it allowed me to spend quite some time on MH. However I quit in September because I couldn't stand that job anymore and with the new job I have, I'm always busy.
As for my personal life, I play a lot of video games (mainly on Series X but I also have a PS5 and a Switch), watch YouTube/movies/series, and visit my family on some weekends. I only visit MH on my job breaks and from time to time else. I'm way less active than before on the game but I still try to check it daily. I also play MH less than before, I'm more active on MH forums than the game itself these days. That's life I guess.

**Adri:** It's simple : Whenever I know I'll have a few hours of free time, whether it's during my remote job or at home, I'll decide on whether i'll simply relax, cook, reach my goals in some games, or code for an active project. More often than not, one of these is kinda urgent so I'll prioritize. I don't have any set timeframes as it's easy for me to get sidetracked anyway, and I can have periods where I'll be super focused on some thing. Kinda goes back and forth.

**Dylan57:** I spend my evenings and weekend evenings administering the site, I help when I can during these periods; but on weekends I visit my family and 1 week out of 5 I am on-call at my job so I am not available much for development, especially given my limited skills.

_ChehTan: You made your first hotfix this week, don't be shy 😁_

_BrainBox: You underestimate yourself though, put in a lot of work implementing many of the Season 16 changes surrounding constructions 😄_

## **Eternaltwin:** So you (the core team) work together since almost 4 years. How do you usually split the work to do? Did each of you have a specialty or a feature attributed?

**ChehTan:** Well, I believe each task attribution is « take a task, do it, commit » xD
There's no special attribution. Altough, since I'm not familiar with all the React we recently added into MH, I let **Brainbox** handle all the issue we face with it 😄

**Nayr:** As I don't have any skills in development, I do stuff around that. I usually open issues on Gitlab, give Brainbox feedback, report issues, post informations from the team on the forum, help players on the help forum and the Discord Eternaltwin server, sometimes translate and post changelogs after an update.
Before we got the source code, I did a huge work of gathering data from Hordes with a team of players, I can't mention them all but I can't thank them enough for their help. I also do a lot of translation and as I'm good in spelling, I usually fix typos when we do official announcements for example.

**Adri:** Aside from all the admin stuff, in which I occasionally take part, I'll follow the development and either try to perform fixes if something I did has issues, or jump on a feature when it seems doable by me. From time to time I'll work on UI details to polish the game. I don't really start projects that will take multiple days to achieve anymore, but maybe someday this will be a thing again.

**Dylan57:** This happens quite naturally I would say for me: currently I manage everything related to Animaction, events, moderation and the election of new crows when I can and this since the beginning, I have made some big additions in following the advice of other developers for S16 but generally I work on translations and a little design + 'small bug' correction when I can, also i make discussions around new features to be implemented for future seasons with oracles and crows, so basically this takes some weight off other developers and allows them to focus on code changes without really worrying about all the "administrative part". It's quite funny because some people within the MH community didn't take me for a developer since I rarely make commits, and in general what I do is not complete/functional 😄

## **Eternaltwin:** So season 16 has been release a month ago. It's the first true season of MyHordes, season 15 "only" bring back to the original Hordes with minor changes except QoL ones. What are you thought about this release?

**Nayr:** I think it's exciting because this is the first season with major changes and it required a lot of work for the team which can be proud.

**Dylan57:** For being short, in 3 words for me: exhausting but satisfying

Season 16 brings lots of new features, some of which should have been done during the original Season 14, I'm talking about the change in blueprints for explorable ruins among other things.  Then comes the biggest modification on which several of us worked during months and years: the new construction site tree with the new construction sites.  It was a risky move that we decided to take to mark the occasion: overall the changes to the constructions sites were welcome, but it could have been massively badly perceived, the same for the changes linked to the Watch. Also the Season 16 comes with a massive rework of the internal structure of the code, done by Brainbox: it allowed us to do ~90% of the new content for Season 16 hidden from the public for avoiding spoil.

I can't say if future seasons will brings that much new feature like the Season 16 did, but let's keep this as surprise 🙂

**Brainbox:** I totally agree with Dylan, we took a risk in fundamentally changing many aspects of the game, and as far as I can see, it paid off. I think after so many years of virtually no changes to the game, it was time to shake things up a bit 😄

**ChehTan:** I didn't answer to this one 😁
I'm thrilled by the release of S16. It was a real challenge to work secretly on it and from what I heard, this new season was a success (minus the whole bug we introduced 😬)

## **Eternaltwin:** How did you prepared the season's content ? Is there a spreadsheet, internal forum, Discord's channels or a lots of paperwork? Did you get help from external people?

**ChehTan:** The whole process is currently an internal reflection among the (former) Oracle and the core team.
We have, among other things, a dedicated Discord to handle upcoming seasons, bugs from the current one, etc...
I kinda want to keep the rest of system secret. Where's the fun when there's a mystery?
But my friends here might want to give you more infos 😁

**Dylan57:** I'll not explains all details on how we work internally but we have the concept of 42th floor : we have a private forum in MH where oracles/crows and admins can talk, this is where we talk about the content of future seasons, official changelogs for seasons, informations about the code / etc.

For the Season 16, we took a part of what has been talked between Hordes developers and Oracles between Season 11 to Season 14: basically MotionTwin had some projects that they left in their drawers and was debated with oracles. So we had general ideas like "adjust blueprints of explorable ruins" but not the details, we did the rest 🙂

Also, due to the massive changes of the Season 16, Guizmo (french oracle) made a big sheet document for talking about the new construction site and others things, allowing us to work on it and after doing the changes on the code according to the doc.

For the new construction site tree, Guizmo had help of some trusty external players that play different style of the game for drawing a "prototype", and when we started to work on it then everyone except admins/oracles and crows had access to the document that has been edited by us.
Except this we do not had help from external people (except Docteur, which is an "outside dev" but do not have access of 42th floor), i personally take a look frequently on the MyHordes idea box for see if some ideas can be added into a Season, if one jumps in my eye we will discuss about it in 42th floor for the balancing / etc
For example, the Technician repair's item action added on Season 15 comes from the Idea box 🙂

On a side note, with Season 16 we was supposed to add a special thing about changes that comes from the idea box, like DeadCells does. But we kinda forgot so well 😅

_Adri: honestly there was enough work to do as-is lol_

**Adri:** I don't have much to say about the last two questions.

**Nayr:** Same for me, I didn't work much on S16, mainly gave my opinion.

**BrainBox:** Dylan pretty much described it perfectly 😄

## **Eternaltwin:** Did all the mechanics and secret from S16 has been found? Or is there still surprises?

**ChehTan:** I believe some secret still needs to be found, but I don't know what players discovered already 🤣

**BrainBox:** Yeah, pretty sure there are some secrets still out there 😄

## **Eternaltwin:** What did you think of the french race (even if it's not a race) for the new wonders ? Did you expect something like that?

**ChehTan:** No matter what we expect, people will always find a way to surprise us 😁

**Nayr:** The Blue Gold Thermal baths was an highly requested wonder, so we knew there would probably be a race to build it. It was still really cool to see the great reactions when it was unveiled.

**Brainbox _(out of nowhere)_**
_By the way, in case you're wondering what I'll be doing during the christmas break..._

![Good vacations](./leak.png)

## **Eternaltwin:** And now, are you working on the S17 or Quality of Life and bugfix for S16?

**Brainbox:** While we do have stuff planned already for S17, v2.1.0 will be a QoL update for S16 😉
And ofc we always work on bugfixes; usually, we have at least two separate code branches that are being worked on simultaneously, one for the current version of the game (e.g. 2.0.x), which receives small improvements and ofc bugfixes, and one for the coming version of the game (e.g. 2.1), where we work on larger stuff

**Dylan57:** I started a draft of what we can do for Season 17, ofc this is not written in the rock but we have a pretty idea of what we must do for the next season.
However we do not work on it yet, since we fixes bugs of the Season 16 and improves a little the game every week 🙂
And meanwhile we work on 2.1.0 which is the mid-season 16 update, bringing major improvements or QoL features 😉

## **Eternaltwin:** How do you test the game during its development? Do you have private server with few selected players? Or do you test change one by one?

**Dylan57:** We can test it ourselves on local server, but we also use the online staging server for this purpose.
We launched the staging server for the public beta of Season 15, and after the release of Season 15 we decided to use this server for try new features ourselves among admins/crows and oracles, that allowed to find few bugs before Season 16 🙂
so technically speaking, yes some selected players can try new features : admins, crows and oracles 🙂
For example, we already can try the first feature of 2.1.0 that has been implemented on it 🙂 which I can provide you an easter-egg.

_Usually I do 4096, sometimes 3072, but on rare occasions I do 2847... What i am ?_

## **Eternaltwin:** The extension of the explorable ruin is a really great idea and well welcomed, it increase difficulty but reward the challengers. Did you expect the players to successfully clean a full explorable ruin in pandemonium?

**Dylan57:** It's quite funny, because there is a whole story behind this change: the e-ruins with floors change was made 1 year ago (in October 2022) ; at the start it was supposed to be a private city option, however we left the idea aside because it was deemed not very useful and above all it lacked balancing.
At the start of the year, some people had searched the code and discovered the code for multiple floors e-ruins (without balancing) and had made a topic on the World Forum to find out about it and said that will be a Season 15 feature ; at that time we said that this is not a feature of the game, that it would be abandoned.

When we started working on Season 16, a point that came up often for some time was that the e-ruins on MyHordes are very simple: this is because the website is fast and particularly optimized on the map / explorable ruin aspects as well as our maze algorithm which is different from that of Hordes, so it was easy to explore the whole e-ruin without really risking anything.
And there came the idea of bringing back the e-ruin with floors but with balancing: the goal was simple; make the exploration of the ruin more complex but reward the exploration of the upper floor a little for avoid the punitive aspect ; hence why there are now 15 doors which are randomly distributed over the 2 floors instead of 10 on a single floor, in addition to the loss of oxygen when using the stairs (even if some people said that is kinda too much, we did not changed it).
Afterwards the oxygen levels have not changed, and by being minimally prepared and with a not too complex maze, it is still possible to search the 15 doors and come out without injury even in Hardcore town, in addition of Scavenger's oxygen bonus then you have more time for explore it. We didn't design the floors to make them impossible, so it's good if it's feasible and we knew it 😉

_On a side note : did you noticed that the floor of bunker are a basement while the floor of hospital and hotel is an upper floor ?_

## **Eternaltwin:** Twinoid and all their games come to an end few months ago. Most how us grow up with theses games. You are a part of the people enjoying so much the game that you made a reborn of it. How do you feel about the closing ?

**Nayr:** I was prepared to the closing but still, when it actually closed, it felt weird, like we indeed lost something. It's definitely a part of my childhood and I'm of course a little sad it's gone. However, I'm really glad we got enough time to develop MyHordes.

**Brainbox:** I feel similarly, since Twinoid was kept open way longer than anyone actually expected, it lessened the blow a bit. But of course it is saddening still. At the same time, I am happy that so many of the "old" people came over to Eternaltwin and MyHordes.

**Dylan57:** To tell the truth on Twinoid it had been years since we heard about MotionTwin's developers before their announcement in March 2020, it didn't surprise me when they made their first announcement, but it's sure still a bit saddening when everything was really closed. It also feels good in another way, to have a little recognition from MotionTwin, they didn't have to set up a redirect to Eternaltwin but they did, and I think that's very cool, even if it's a bit of a shame to have these games disappear from the portfolio on their website.

## **Eternaltwin:** 2024 is here, what are the big next steps for MH ? Can we except season 17 and 18 in the same year?

**Brainbox:** As for our season plans, we can definitely promise S17. Since we're all working in our free time, it is hard to estimate when things will finish though 😂

**Dylan57:** For the seasons scheduling, there will be Season 17 for 2024 for sure, with perhaps a surprise? In any case, we are quite far from reaching Season 666!

_Joke aside: Deepnight said in Hordes world forums with an announce tag that he will come back at Season 666._

_ChehTan: We already have some plan to reach Season 666 as soon as possible. One of them is going through a wormhole_

## **Eternaltwin:** What was your funniest bug you encountered during MH development?

**Nayr:** Forum page trying to merge with login page was pretty funny.

![Merging Bug](./bug1.png)

Also once we came back to beta for some reason despite being in S15

![Season Bug](./bug2.png)

**Brainbox:** Ah yes, the last one happened because I pushed a hotfix to the server that I didn't properly register, so the game assumed that I had pushed a beta version xD
If I may, I'd like to highlight something that was not a bug, but during the alpha, we had the betapropine item that supplied 26 AP. People were developing Opti strategies around playing with that, which I found very funny. It's still in the game by the way, and can be enabled for private towns 😂

**Adri:** There was one very important bug that got brain and me to hop on vocal in order to fix this, was my fault for not paying enough attention. Not funny but definitely a highlight for me and probably the only time the vocal was ever used as well lol.

**Dylan57:** It's not a bug that is very disturbing but it struck me a little and i laught a lot about it when we called it a "Bugthesda" somehow 😄
When we set up the floors explorable ruins for Season 16, we tested everything... except being ejected due to lack of oxygen with objects at upstairs. It didn't missed: there was a piece of code that we forgot, which ensured that objects were placed on the ground floor even if you were upstairs, thus causing cases where the objects disappeared to the void because they were in an inaccessible area.

Otherwise, another bug that I find to be a running gag; it's a bug that we fixed in alpha v0.3, that we refixed in alpha v0.6 and that we had to correct again in beta.6 : the gazette was displayed as if we were looking in a mirror when you click on "return" on mobile. A small evolution of the bug is that each time it was something different and a bit worse.

Ah otherwise there's also a noticable bug that we found during the alpha: I don't remember how, but part of the code for changing the password somehow completely reset the soul progression MH: an english player informed us about the bug, and I, who didn't have a local server installed yet, reproduced the bug on my current public soul to check.
Brainbox was able to correct the problem, but however for the player's progress as well as mine could not be restored.

It's a bit complex to find THE PROBLEM among the 3k problems we had 😅
Funny thing, when it was the start of alpha: this betapropin provided 999AP and no limit per day. Afterwards, it was 26AP with no limit per day, and then during beta phase it was 26AP with once per day limit

## **Eternaltwin:** Thanks for your participation in this interview, do you have anything else to add?

**Dylan57:** BEWARE THE CHICKEN

**Nayr:** A big thanks to all the people helping on MH, and to the growing base of players for their continued support.

_Dylan57: Yeah, this is better than my version 😄_

_BrainBox: I fully support Nayr's sentence 🙂_

_Dylan posted a gif saying "The chicken is not amused"_

**BrainBox:** Damnit the update will be cursed again!

_Few minutes later, the MH update failed_
