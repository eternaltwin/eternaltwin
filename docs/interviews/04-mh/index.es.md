# Equipo de desarrollo de MyHordes

## **Eternaltwin:** ¡Hola a todos! Gracias por tomarse unos minutos para responder estas preguntas. La mayoría de los lectores los conocen, pero ¿podrían presentarse en pocas palabras?

**Nayr:** Soy Nayr, 27 años, soy del centro de Francia, y soy un Oráculo y apoyo del equipo central de Myhordes. Me uní al proyecto durante la fase alfa y he estado cada vez más y más involucrado hasta convertirme en Oráculo, para aquellos que no conocen el rol, estamos haciendo el vínculo entre jugadores y administradores. He sido jugador de Hordes por un tiempo, no recuerdo exactamente de qué temporada ya que tenía una cuenta anterior que fue eliminada pero, en mi cuenta de Hordes, mis ciudades más antiguas son de la temporada 6. Aparte de eso, soy un gran aficionado a los autos y los videojuegos en general y trabajo como técnico informático.

**Dylan57:** Soy Connor, pero también pueden llamarme Dylan, tengo 25 años, vivo en el noreste de Francia, cerca de la frontera alemana, soy uno de los líderes del proyecto MyHordes.
Me uní a Eternaltwin el 3 de abril de 2020, poco después del primer anuncio de MotionTwin que indicaba un posible cierre y eliminación de cualquier aspecto pago de Twinoid, luego me uní a Brainbox el 7 de mayo de 2020 para el "apoyo al desarrollo" del juego y la administración del juego. Llevo jugando Hordes desde la Temporada 3, antes de la integración de Twinoid, un juego que conocí gracias a mi hermano mayor.
Disfruto leyendo libros sobre crímenes, así como libros sobre "tú eres el héroe", soy un buen cliente de juegos para un jugador con una gran historia. Mi pasión es desarmar todo y entender cómo funcionan las cosas, aunque a veces no sea fácil; y esto va bien con mi trabajo: técnico de mantenimiento, así que sí, no soy desarrollador y aprendí durante el trabajo con los otros desarrolladores de MyHordes.

**Adri:** Adri, 25 años, del sur (Dordoña y ahora viviendo en Normandía), me uní a ET y luego al equipo de MH justo después del primer anuncio de cierre. He jugado a Hordes desde inmediatamente después del armagedón. Me encantan los juegos de codificación y ritmo (por ejemplo, osu!mania). Obtuve mi maestría en ingeniería de software a fines de 2021 y he trabajado a tiempo completo desde entonces, pero todavía trato de contribuir al proyecto desde cualquier lado. Esta es siempre una gran experiencia de aprendizaje debido al enfoque de este proyecto.

**ChehTan:** ChehTan, también conocido como Ludofloria, también viviendo en el noreste.
He sido uno de los primeros en unirme a Brainbox en su aventura para crear MyHordes. Me uní al proyecto ET cuando se anunció y su Discord se creó. Allí es donde oí hablar de MyHordes.
Soy administrador de redes y sistemas, con experiencia en desarrollo.
He sido jugador de Hordes desde sus inicios (temporada 0, bebé), pero no era un jugador habitual en aquel entonces.
Le doy parte de mi tiempo libre de trabajo a MyHordes, pero no tanto como quisiera, principalmente debido a una vida real muy cargada.

## **Eternaltwin:** Sabemos que Brain se centra en MH todos los fines de semana, pero ¿ustedes? ¿Cómo organizan su vida personal, su vida profesional y su vida en MyHordes?

**ChehTan:** Bueno, dado que mi vida personal casi no me deja tiempo para nada más que mi familia y mi casa, es mi vida profesional la que necesito dividir entre MH y mi trabajo oficial.
Básicamente, trabajo para mi empleador, cuando tengo algo de tiempo “libre” allí trabajo en cosas relacionadas con MH y luego, al final del día, vuelvo a mi familia x]

**Nayr:** Cuando tenía mi trabajo anterior, tenía mucho tiempo libre (respondía llamadas y cuando no había llamadas, no tenía nada que hacer + estaba en trabajo remoto 3 días a la semana), así que me permitió que dedique bastante tiempo a MH. Sin embargo renuncié en septiembre porque ya no aguantaba más ese trabajo y con el nuevo trabajo que tengo siempre estoy ocupado.
En cuanto a mi vida personal, juego muchos videojuegos (principalmente en la Serie X, pero también tengo una PS5 y una Switch), veo YouTube/películas/series y visito a mi familia algunos fines de semana. Solo visito MH en mis descansos laborales y de vez en cuando. Estoy mucho menos activo que antes en el juego, pero sigo intentando comprobarlo a diario. También juego menos a MH que antes, estoy más activo en los foros de MH que en el juego en sí estos días. Así es la vida, supongo.

**Adri:** Es simple: cada vez que sé que tendré algunas horas de tiempo libre, ya sea durante mi trabajo remoto o en casa, decidiré si simplemente me relajaré, cocinaré, alcanzaré metas en algunos juegos, o codificaré para un proyecto activo. La mayoría de las veces, uno de estos es un poco urgente, así que lo priorizaré. No tengo plazos establecidos porque de todos modos es fácil para mí desviarme y puedo tener períodos en los que estaré súper concentrado en algo. Un poco va y viene.

**Dylan57:** Paso las tardes y los fines de semana administrando el sitio, ayudo cuando puedo durante estos períodos; pero los fines de semana visito a mi familia y 1 de cada 5 semanas estoy de guardia en mi trabajo, por lo que no estoy muy disponible para el desarrollo, especialmente dadas mis habilidades limitadas.

_ChehTan: Hiciste tu primera revisión esta semana, no seas tímido 😁_

_BrainBox: Sin embargo, te subestimas, trabajaste mucho para implementar muchos de los cambios de la Temporada 16 en torno a las construcciones 😄_

## **Eternaltwin:** Entonces ustedes (el equipo central) trabajan juntos desde hace casi 4 años. ¿Cómo suelen dividir el trabajo a realizar? ¿Cada uno de ustedes tenía una especialidad o una característica atribuida?

**ChehTan:** Bueno, creo que cada atribución de tarea es "toma una tarea, hazla, comprométete" xD
No hay ninguna atribución especial. Sin embargo, como no estoy familiarizado con todo el React que agregamos recientemente a MH, dejo que **Brainbox** se encargue de todos los problemas que enfrentamos 😄

**Nayr:** Como no tengo ninguna habilidad en desarrollo, hago cosas alrededor de eso. Normalmente abro problemas en Gitlab, doy retroalimentación a Brainbox, informo problemas, publico información del equipo en el foro, ayudo a los jugadores en el foro de ayuda y en el servidor Discord de Eternaltwin, a veces traduzco y publico registros de cambios después de una actualización.
Antes de obtener el código fuente, hice un gran trabajo recopilando datos de Hordes con un equipo de jugadores, no puedo mencionarlos a todos, pero no puedo agradecerles lo suficiente por su ayuda. También traduzco mucho y como tengo buena ortografía, suelo corregir errores tipográficos cuando hacemos anuncios oficiales, por ejemplo.

**Adri:** Aparte de todas las tareas administrativas, en las que ocasionalmente participo, sigo el desarrollo e intento realizar correcciones si algo que hice tiene problemas, o trabajaré en una característica cuando me parezca realizable por mí. De vez en cuando trabajaré en los detalles de la interfaz de usuario para pulir el juego. Realmente ya no empiezo proyectos que me llevarán varios días lograr, pero tal vez algún día esto vuelva a existir.

**Dylan57:** Esto sucede con bastante naturalidad. Yo diría por mí: actualmente administro todo lo relacionado con la Animacción, los eventos, la moderación y la elección de nuevos cuervos cuando puedo y esto desde el principio, he hecho algunas adiciones importantes siguiendo el consejo de otros desarrolladores para S16, pero generalmente trabajo en traducciones y un poco de diseño + corrección de 'pequeños errores' cuando puedo, también hago discusiones sobre nuevas características que se implementarán para futuras temporadas con oráculos y cuervos, así que básicamente esto quita algo de peso a otros desarrolladores y les permite centrarse en los cambios de código sin preocuparse realmente por toda la "parte administrativa". Es bastante gracioso porque algunas personas dentro de la comunidad de MH no me tomaron por un desarrollador ya que rara vez hago commits y, en general, lo que hago no es completo/funcional 😄

## **Eternaltwin:** Entonces, la temporada 16 se lanzó hace un mes. Es la primera temporada verdadera de MyHordes, la temporada 15 "sólo" trae de vuelta a las Hordas originales con cambios menores, excepto los de calidad de vida. ¿Qué opinan de este lanzamiento?

**Nayr:** Creo que es emocionante porque esta es la primera temporada con cambios importantes y requirió mucho trabajo para el equipo del cual podemos estar orgullosos.

**Dylan57:** Para ser breve, en 3 palabras para mí: agotador pero satisfactorio.

La Temporada 16 trae muchas características nuevas, algunas de las cuales deberían haberse realizado durante la Temporada 14 original, me refiero al cambio en los planos de las ruinas explorables, entre otras cosas. Luego viene la modificación más grande en la que varios de nosotros trabajamos durante meses y años: el nuevo árbol de obras con las nuevas obras. Fue un movimiento arriesgado que decidimos tomar para conmemorar la ocasión: en general, los cambios en las obras fueron bienvenidos, pero podrían haber sido muy mal percibidos, lo mismo para los cambios relacionados con la Vigilancia. Además, la Temporada 16 viene con una reelaboración masiva de la estructura interna del código, realizada por Brainbox: nos permitió hacer ~90% del contenido nuevo para la Temporada 16 oculto al público para evitar spoilers.

No puedo decir si las temporadas futuras traerán tantas características nuevas como lo hizo la Temporada 16, pero dejemos esto como una sorpresa 🙂

**Brainbox:** Estoy totalmente de acuerdo con Dylan, nos arriesgamos a cambiar fundamentalmente muchos aspectos del juego y, hasta donde puedo ver, valió la pena. Creo que después de tantos años sin prácticamente cambios en el juego, era hora de cambiar un poco las cosas 😄

**ChehTan:** No respondí a esto 😁
Estoy emocionado por el lanzamiento del S16. Fue un verdadero desafío trabajar en secreto en ello y, por lo que escuché, esta nueva temporada fue un éxito (menos el bug que introdujimos 😬)

## **Eternaltwin:** ¿Cómo prepararon el contenido de la temporada? ¿Hay una hoja de cálculo, un foro interno, canales de Discord o mucho papeleo? ¿Recibieron ayuda de personas externas?

**ChehTan:** Todo el proceso es actualmente una reflexión interna entre el (antiguo) Oráculo y el equipo central.
Tenemos, entre otras cosas, un Discord dedicado para manejar las próximas temporadas, errores de la actual, etc...
Quiero mantener el resto del sistema en secreto. ¿Dónde está la diversión cuando hay un misterio?
Pero mis amigos aquí quizás quieran darte más información 😁

**Dylan57:** No explicaré todos los detalles sobre cómo trabajamos internamente, pero tenemos el concepto del piso 42: tenemos un foro privado en MH donde los oráculos/cuervos y los administradores pueden hablar, aquí es donde hablamos sobre contenido de temporadas futuras, registros de cambios oficiales de temporadas, información sobre el código, etc.

Para la Temporada 16, tomamos una parte de lo que se habló entre los desarrolladores de Hordes y Oráculos entre la Temporada 11 y la 14: básicamente MotionTwin tenía algunos proyectos que dejaron en sus cajones y se debatieron con los oráculos. Entonces teníamos ideas generales como "ajustar planos de ruinas explorables" pero no los detalles, hicimos el resto 🙂

Además, debido a los cambios masivos de la Temporada 16, Guizmo (oráculo francés) hizo un gran documento para hablar sobre el nuevo sitio de construcción y otras cosas, permitiéndonos trabajar en ello y después de hacer los cambios en el código de acuerdo con el doc.

Para el nuevo árbol del sitio de construcción, Guizmo contó con la ayuda de algunos jugadores externos confiables que juegan diferentes estilos de juego para dibujar un "prototipo", y cuando comenzamos a trabajar en él, todos, excepto los administradores/oráculos y los cuervos, tuvieron acceso al documento que había sido editado por nosotros.
Excepto esto, no contamos con ayuda de personas externas (excepto Docteur, que es un "desarrollador externo" pero no tiene acceso al piso 42), personalmente miro con frecuencia el cuadro de ideas de MyHordes para ver si se pueden agregar algunas ideas en una Temporada, si uno salta a mis ojos lo discutiremos en el piso 42 para el equilibrio / etcétera.
Por ejemplo, la acción del elemento de reparación del Técnico agregada en la Temporada 15 proviene del cuadro de ideas 🙂

Como nota al margen, con la Temporada 16 se suponía que íbamos a agregar algo especial sobre los cambios que provienen del cuadro de ideas, como lo hace DeadCells. Pero como que nos olvidamos, así que bueno 😅

_Adri: honestamente había suficiente trabajo para hacer tal como está jajaja_

**Adri:** No tengo mucho que decir sobre las dos últimas preguntas.

**Nayr:** Lo mismo para mí, no trabajé mucho en S16, principalmente di mi opinión.

**BrainBox:** Dylan lo describió prácticamente perfectamente 😄

## **Eternaltwin:** ¿Se han encontrado todas las mecánicas y secretos de la T16? ¿O todavía hay sorpresas?

**ChehTan:** Creo que aún queda algún secreto por encontrar, pero no sé qué descubrieron ya los jugadores 🤣

**BrainBox:** Sí, estoy bastante seguro de que todavía quedan algunos secretos por ahí 😄

## **Eternaltwin:** ¿Qué te pareció la carrera francesa (incluso aunque no sea una carrera) por las nuevas maravillas? ¿Esperabas algo así?

**ChehTan:** No importa lo que esperemos, la gente siempre encontrará una manera de sorprendernos 😁

**Nayr:** Los baños termales Blue Gold eran una maravilla muy solicitada, por lo que sabíamos que probablemente habría una carrera para construirlos. Fue genial ver las grandes reacciones cuando se dio a conocer.

**Brainbox _(de la nada)_**
_Por cierto, por si te preguntas qué haré durante las vacaciones de Navidad..._

![Good vacations](./leak.png)

## **Eternaltwin:** Y ahora, ¿están trabajando en la T17 o en la calidad de vida y corrección de errores para la T16?

**Brainbox:** Si bien ya tenemos cosas planeadas para la T17, la versión 2.1.0 será una actualización de calidad de vida para la T16 😉
Y, por supuesto, siempre trabajamos para corregir errores; por lo general, tenemos al menos dos ramas de código separadas en las que se trabaja simultáneamente, una para la versión actual del juego (por ejemplo, 2.0.x), que recibe pequeñas mejoras y correcciones de errores, y otra para la próxima versión del juego ( por ejemplo, 2.1), donde trabajamos en cosas más grandes

**Dylan57:** Comencé un borrador de lo que podemos hacer para la Temporada 17, por supuesto, esto no está escrito en roca, pero tenemos una idea bastante clara de lo que debemos hacer para la próxima temporada.
Sin embargo, todavía no trabajamos en ello, ya que corregimos errores de la Temporada 16 y mejoramos un poco el juego cada semana 🙂
Y mientras tanto trabajamos en 2.1.0, que es la actualización de mitad de Temporada 16, que trae importantes mejoras o características de calidad de vida 😉

## **Eternaltwin:** ¿Cómo prueban el juego durante su desarrollo? ¿Tienen un servidor privado con pocos jugadores seleccionados? ¿O prueban los cambios uno por uno?

**Dylan57:** Podemos probarlo nosotros mismos en el servidor local, pero también utilizamos el servidor de prueba en línea para este propósito.
Lanzamos el servidor de prueba para la beta pública de la Temporada 15, y después del lanzamiento de la Temporada 15 decidimos usar este servidor para probar nuevas características entre administradores/cuervos y oráculos, que permitieron encontrar algunos errores antes de la Temporada 16 🙂
Técnicamente hablando, sí, algunos jugadores seleccionados pueden probar nuevas funciones: administradores, cuervos y oráculos 🙂
Por ejemplo, ya podemos probar la primera característica de 2.1.0 que se implementó en él 🙂 y puedo brindarles un huevo de pascua como pista.

_Normalmente hago 4096, a veces 3072, pero en raras ocasiones hago 2847... ¿Qué soy?_

## **Eternaltwin:** La extensión de las ruinas explorables es una idea realmente genial y bien recibida, aumenta la dificultad pero recompensa a los retadores. ¿Esperaban que los jugadores limpiaran con éxito una ruina completamente explorable en pandemonio?

**Dylan57:** Es bastante gracioso, porque hay toda una historia detrás de este cambio: el cambio de ruinas explorables con pisos se realizó hace 1 año (en octubre de 2022); al principio se suponía que sería una opción de ciudad privada, pero dejamos de lado la idea porque la consideramos poco útil y sobre todo carecía de equilibrio.
A principios de año, algunas personas buscaron el código y descubrieron el código para ruinas explorables de múltiples pisos (sin equilibrio) y crearon un tema en el Foro Mundial para averiguarlo y dijeron que será una característica de la Temporada 15. ; En ese momento dijimos que esto no era una característica del juego, que sería abandonado.

Cuando empezamos a trabajar en la Temporada 16, un punto que surgió a menudo durante algún tiempo fue que las ruinas explorables en MyHordes son muy simples: esto se debe a que el sitio web es rápido y está particularmente optimizado en los aspectos del mapa/ruinas explorables, así como en nuestro algoritmo de laberinto que es diferente al de Hordes, por lo que fue fácil explorar toda la ruina sin arriesgar nada.
Y surgió la idea de traer de vuelta la ruina explorable con pisos pero con equilibrio: el objetivo era simple; hacer más compleja la exploración de las ruinas pero premiar un poco la exploración del piso superior para evitar el aspecto punitivo; por eso ahora hay 15 puertas distribuidas aleatoriamente en los 2 pisos en lugar de 10 en un solo piso, además de la pérdida de oxígeno al usar las escaleras (incluso aunque algunas personas dijeron que es demasiado, no lo cambiamos).
Después los niveles de oxígeno no han cambiado, y al estar mínimamente preparado y con un laberinto no demasiado complejo, aún es posible buscar las 15 puertas y salir sin lastimarse incluso en pueblo Hardcore, además que con el bono de oxígeno de Scavenger tienes más tiempo para explorarlo. No diseñamos los pisos para hacerlos imposibles, así que es bueno si es factible y lo sabíamos 😉

_En una nota al margen: ¿notaron que el piso del búnker es un sótano mientras que el piso del hospital y del hotel es un piso superior?_

## **Eternaltwin:** Twinoid y todos sus juegos llegaron a su fin hace unos meses. La mayoría de nosotros crecimos con estos juegos. Son parte de la gente que disfrutaron tanto de los juegos que los hicieron renacer. ¿Cómo se sienten con el cierre?

**Nayr:** Estaba preparado para el cierre, pero aún así, cuando realmente cerró, me sentí raro, como si realmente hubiésemos perdido algo. Definitivamente es parte de mi infancia y, por supuesto, estoy un poco triste porque ya no está. Sin embargo, estoy muy contento de que hayamos tenido suficiente tiempo para desarrollar MyHordes.

**Brainbox:** Me siento similar, dado que Twinoid se mantuvo abierto mucho más tiempo de lo que nadie esperaba, disminuyó un poco el golpe. Pero, por supuesto, sigue siendo triste. Al mismo tiempo, estoy feliz de que tantas de las personas "viejas" hayan venido a Eternaltwin y MyHordes.

**Dylan57:** A decir verdad sobre Twinoid, habían pasado años desde que supimos de los desarrolladores de MotionTwin antes de su anuncio en marzo de 2020, no me sorprendió cuando hicieron su primer anuncio, pero seguro que todavía es un poco triste cuando todo estaba realmente cerrado. También se siente bien en otro sentido, tener un poco de reconocimiento por parte de MotionTwin, no tenían que configurar una redirección a Eternaltwin pero lo hicieron, y creo que eso es genial, incluso aunque sea una pena que estos juegos tengan que desaparecer del portafolio de su sitio web.

## **Eternaltwin:** 2024 ya está aquí, ¿cuáles son los próximos grandes pasos para MH? ¿Podemos esperar las Temporadas 17 y 18 en el mismo año?

**Brainbox:** En cuanto a nuestros planes de temporada, definitivamente podemos prometer la T17. Sin embargo, como todos trabajamos en nuestro tiempo libre, es difícil estimar cuándo terminarán las cosas 😂

**Dylan57:** Para la programación de temporadas, seguramente habrá una Temporada 17 para 2024, ¿quizás con una sorpresa? En cualquier caso, ¡estamos bastante lejos de llegar a la Temporada 666!

_Broma aparte: Deepnight dijo en los foros mundiales de Hordas con una etiqueta de anuncio que regresará en la Temporada 666._

_ChehTan: Ya tenemos algún plan para llegar a la Temporada 666 lo antes posible. Uno de ellos está atravesando un agujero de gusano_

## **Eternaltwin:** ¿Cuál fue el error más divertido que encontraron durante el desarrollo de MH?

**Nayr:** La página del foro que intentó fusionarse con la página de inicio de sesión fue bastante divertido.

![Merging Bug](./bug1.png)

También una vez volvimos a la versión beta por alguna razón a pesar de estar en T15

![Season Bug](./bug2.png)

**Brainbox:** Ah, sí, el último sucedió porque envié una revisión al servidor que no registré correctamente, por lo que el juego asumió que había enviado una versión beta xD
Si se me permite, me gustaría resaltar algo que no fue un error, pero durante la alfa, teníamos el objeto betapropina que suministraba 26 PA. La gente estaba desarrollando estrategias óptimas para jugar con eso, lo cual me pareció muy divertido. Por cierto, todavía está en el juego y se puede habilitar para ciudades privadas 😂

**Adri:** Hubo un error muy importante que hizo que Brain y yo usáramos el canal de voz para solucionarlo. Fue culpa mía por no prestar suficiente atención. No es gracioso, pero definitivamente es lo más destacado para mí y probablemente la única vez que también se usó el canal de voz jajaja.

**Dylan57:** No es un error que sea muy perturbador, pero me impactó un poco y me reí mucho cuando lo llamamos "Bugthesda" de alguna manera 😄
Cuando configuramos las ruinas explorables del piso para la Temporada 16, probamos todo... excepto ser expulsado debido a la falta de oxígeno con objetos en el piso de arriba. No falló: había un fragmento de código que olvidamos, que aseguraba que los objetos se colocaran en la planta baja incluso si estabas arriba, provocando casos en los que los objetos desaparecían en el vacío porque estaban en un área inaccesible.

De lo contrario, otro error que considero un chiste recurrente; es un error que arreglamos en alpha v0.3, que volvimos a arreglar en alpha v0.6 y que tuvimos que corregir nuevamente en beta.6: la gaceta se mostraba como si nos miráramos en un espejo cuando hacíamos clic en "volver" en móvil. Una pequeña evolución del fallo es que cada vez era algo diferente y un poco peor.

Ah, de lo contrario, también hay un error notable que encontramos durante la alfa: no recuerdo cómo, pero parte del código para cambiar la contraseña de alguna manera restablece completamente la progresión del alma MH: un jugador inglés nos informó sobre el error, y yo, que aún no tenía un servidor local instalado, reprodujo el error en mi alma pública actual para comprobarlo.
Brainbox pudo corregir el problema, pero no se pudo restaurar el progreso del jugador ni el mío.

Es un poco complejo encontrar EL PROBLEMA entre los 3k problemas que tuvimos 😅
Lo curioso es que cuando era el comienzo de la versión alfa: esta betapropina proporcionaba 999PA y sin límite por día. Después, fueron 26PA sin límite por día, y luego durante la fase beta fueron 26PA con límite de una vez por día.

## **Eternaltwin:** Gracias por su participación en esta entrevista, ¿tienen algo más que agregar?

**Dylan57:** CUIDADO CON EL POLLO

**Nayr:** Muchas gracias a toda la gente que ayuda en MH, y a la creciente base de jugadores por su continuo apoyo.

_Dylan57: Sí, esta es mejor que mi versión 😄_

_BrainBox: Apoyo plenamente la sentencia de Nayr 🙂_

_Dylan publicó un gif que decía "Al pollo no le hace gracia"_

**BrainBox:** ¡Maldita sea, la actualización volverá a estar maldita!

_Unos minutos más tarde, la actualización de MH falló_
