# L'équipe de développement de MyHordes

## **Eternaltwin:** Bonjour tout le monde! Merci d'avoir pris quelques instants pour répondre à ces questions. La plupart des lecteurs vous connaissent, mais pouvez-vous vous présenter en quelques mots ?

**Nayr:** Je m'appelle Nayr, 27 ans, originaire du centre de la France, et je suis Oracle et support de l'équipe de Myhordes, j'ai rejoint le projet lors de l'alpha et je me suis de plus en plus investi jusqu'à devenir Oracle, pour ceux qui ne connaissent pas le rôle, nous faisons le lien entre les joueurs et les admins. Je suis un Hordien depuis un moment, je ne me souviens plus de quelle saison exactement car j'avais un compte précédent qui a été supprimé mais sur mon compte Hordes, mes villes les plus anciennes datent de la saison 6. A part ça, je suis un grand fan d'automobile et de jeux vidéo en général et je travaille comme technicien informatique.

**Dylan57:** Je m'appelle Connor, mais vous pouvez aussi m'appeler Dylan, j'ai 25 ans, j'habite dans le Nord-Est de la France, près de la frontière allemande, je suis l'un des responsables du projet MyHordes.
J'ai rejoint Eternaltwin le 3 avril 2020, peu de temps après la première annonce de MotionTwin qui indiquait un arrêt potentiel et la suppression de tout aspect payant de Twinoid, puis j'ai rejoint Brainbox le 7 mai 2020 pour le "support au développement" du jeu et l'administration du jeu. Je joue à Hordes depuis la saison 3, avant l'intégration de Twinoid, jeu que je connaissais grâce à mon frère aîné.
J'aime lire des livres policiers, ainsi que des livres "dont vous êtes le héros", je suis un bon client des jeux solo avec une histoire fournie. Ma passion est de tout démonter et de comprendre comment les choses fonctionnent, même si ce n'est pas facile parfois ; et cela va bien avec mon métier : technicien de maintenance, donc oui je ne suis pas développeur et j'ai en quelque sorte appris sur le tas avec les autres développeurs MyHordes.

**Adri:** Adri, 25 ans originaire du Sud (Dordogne, et vivant désormais en Normandie), j'ai rejoint ET puis l'équipe MH juste après la première annonce d'arrêt. J'ai joué à Hordes juste après l'Armageddon. J'adore le codage et les jeux de rythme (par exemple osu!mania). J'ai obtenu mon master en génie logiciel fin 2021 et je travaille à temps plein depuis, mais j'essaie toujours de contribuer au projet de quelque côté que ce soit. C'est toujours une excellente expérience d'apprentissage en raison de l'approche de ce projet.

**ChehTan:** ChehTan, également connu sous le nom de Ludofloria, vit également dans le Nord-Est.
J'ai été l'un des premiers à rejoindre Brainbox dans son aventure pour créer MyHordes. J'ai rejoint le projet ET lorsqu'il a été annoncé et que leur discord a été créé. C'est là que j'ai entendu parler de MyHordes.
Je suis administrateur réseaux et systèmes, avec une formation de développeur.
Je suis joueur de Hordes depuis ses débuts (saison 0 bébé), mais je n'étais pas un joueur régulier à l'époque.
Je donne une partie de mon temps libre à MyHordes, mais pas autant que je le souhaiterais, principalement à cause d'un IRL très chargé actuellement.

## **Eternaltwin:** Nous savons que Brainbox se concentre sur MH chaque week-end, mais qu'en est-il de vous ? Comment organisez-vous votre vie personnelle, votre vie professionnelle et votre vie MyHordienne ?

**ChehTan:** Eh bien, comme ma vie personnelle ne me laisse presque pas de temps pour autre chose que ma famille et ma maison, c'est ma vie professionnelle que je dois partager entre MH et mon travail officiel.
Donc en gros, je travaille pour mon employeur, quand j'ai du temps « libre » là-bas, je travaille sur des trucs liés à MH, et puis en fin de journée, je retourne en famille x]

**Nayr:** Quand j'avais mon précédent travail, j'avais beaucoup de temps libre (je répondais aux appels et quand il n'y avait pas d'appels, je n'avais rien à faire + j'étais en télétravail 3 jours par semaine) donc ça me permettait d'y consacrer pas mal de temps sur MH. Cependant, j'ai arrêté en septembre parce que je ne supportais plus ce travail et avec le nouveau travail que j'ai, je suis toujours occupé.
Quant à ma vie personnelle, je joue beaucoup à des jeux vidéo (principalement sur Series X mais j'ai aussi une PS5 et une Switch), je regarde YouTube/films/séries et je rends visite à ma famille certains week-ends. Je ne visite MH que pendant mes pauses au travail et de temps en temps. Je suis beaucoup moins actif qu'avant sur le jeu mais j'essaie quand même de vérifier le jeu quotidiennement. Je joue aussi moins à MH qu'avant, je suis plus actif sur les forums de MH que sur le jeu lui-même ces jours-ci. C'est la vie, je suppose.

**Adri:** C'est simple : chaque fois que je sais que j'aurai quelques heures de temps libre, que ce soit pendant mon travail à distance ou à la maison, je décide si je vais simplement me détendre, cuisiner, atteindre mes objectifs dans certains jeux ou coder pour un projet actif. Le plus souvent, l’une d’entre elles est assez urgente, je vais donc lui donner la priorité. Je n'ai pas de calendrier fixe car il est facile pour moi de me laisser distraire de toute façon, et je peux avoir des périodes où je serai très concentré sur quelque chose. Un peu de va-et-vient.

**Dylan57:** Je passe mes soirées et soirées de week-end à administrer le site, j'aide quand je peux pendant ces périodes ; mais le week-end je rends visite à ma famille et 1 semaine sur 5 je suis d'astreinte à mon travail donc je ne suis pas beaucoup disponible pour développer, surtout compte tenu de mes compétences limitées.

_ChehTan: Tu as fait votre premier hotfix cette semaine, ne sois pas timide 😁_

_BrainBox: Cependant, tu te sous-estimes et tu as consacré beaucoup de travail à la mise en œuvre de nombreux changements de la saison 16 concernant les constructions. 😄_

## **Eternaltwin:** Donc vous (l’équipe principale) travaillez donc ensemble depuis presque 4 ans. Comment répartissez-vous habituellement le travail à effectuer? Est-ce que chacun d'entre vous s'est vu attribuer une spécialité ou une particularité ?

**ChehTan:** Eh bien, je crois que chaque attribution de tâche est « prendre une tâche, la faire, commit » xD
Il n'y a pas d'attribution spéciale. Cependant, comme je ne connais pas tout le React que nous avons récemment ajouté à MH, je laisse **Brainbox** gérer tous les problèmes auxquels nous sommes confrontés. 😄

**Nayr:** Comme je n'ai aucune compétence en développement, je fais des trucs autour de ça. J'ouvre généralement des tickets sur Gitlab, je donne des commentaires à Brainbox, je signale des problèmes, je publie des informations de l'équipe sur le forum, j'aide les joueurs sur le forum d'aide et sur le serveur Discord Eternaltwin, je traduis et publie parfois des listes de modifications après une mise à jour.
Avant d'avoir le code source d'Hordes, j'ai fait un énorme travail de collecte de données sur Hordes avec une équipe de joueurs, je ne peux pas tous les citer mais je ne pourrai jamais assez les remercier pour leur aide. Je fais aussi beaucoup de traduction et comme je suis bon en orthographe, je corrige généralement les fautes de frappe lorsque nous faisons des annonces officielles par exemple.

**Adri:** Mis à part toutes les tâches d'administration, auxquelles je participe occasionnellement, je suis le développement et j'essaie soit d'effectuer des correctifs si quelque chose que j'ai fait présente des problèmes, soit de sauter sur une fonctionnalité lorsqu'elle me semble réalisable. De temps en temps, je travaillerai sur les détails de l'interface utilisateur pour peaufiner le jeu. Je ne démarre plus vraiment des projets qui prendront plusieurs jours à réaliser, mais peut-être qu'un jour, ce sera à nouveau une chose.

**Dylan57:** Cela se fait assez naturellement je dirais pour moi : actuellement je gère tout ce qui touche à l'Animaction, aux événements, à la modération et à l'élection des nouveaux corbeaux quand je peux et ce depuis le début, j'ai fait de gros ajouts en suivant les conseils des autres développeurs pour la Saison 16 mais en ce moment je travaille sur les traductions et un peu de design + correction de 'petit bug' quand je peux, aussi je crée des discussions autour des nouvelles fonctionnalités à implémenter pour les saisons futures avec les oracles et corbeaux, donc en gros cela enlève un peu de poids aux autres développeurs et permet leur permettre de se concentrer sur les changements de code sans vraiment se soucier de toute la « partie administrative ». C'est assez drôle car certaines personnes au sein de la communauté MH ne m'ont pas pris pour un développeur puisque je fais rarement des commits, et en général ce que je fais n'est pas complet/fonctionnel 😄

## **Eternaltwin:** La saison 16 est donc sortie il y a un mois. C'est la première vraie saison de MyHordes, la saison 15 ramène "seulement" au Hordes original avec des changements mineurs sauf ceux d'amélioration de qualité. Que pensez-vous de cette sortie ?

**Nayr:** Je pense que c'est excitant car c'est la première saison avec des changements majeurs et cela a demandé beaucoup de travail à l'équipe qui peut en être fière.

**Dylan57:** Pour faire court, en 3 mots pour moi : épuisant mais satisfaisant

La saison 16 apporte beaucoup de nouvelles fonctionnalités, dont certaines auraient dû être réalisées lors de la saison 14 d'origine, je parle entre autres du changement des plans des ruines explorables. Vient ensuite la plus grosse modification sur laquelle plusieurs d'entre nous ont travaillé pendant des mois et des années : la nouvelle arborescence des chantiers avec les nouveaux chantiers. C'est un pari risqué que nous avons décidé de prendre pour marquer le coup : globalement les changements sur les chantiers étaient les bienvenus, mais cela aurait pu être massivement mal perçu, de même pour les changements liés à la Veille. De plus, la saison 16 s'accompagne d'une refonte massive de la structure interne du code, réalisée par Brainbox : cela nous a permis de cacher environ 90 % du nouveau contenu de la saison 16 au public pour éviter de gâcher les surprises.

Je ne peux pas dire si les saisons à venir apporteront autant de nouveautés que la saison 16, mais gardons cela comme une surprise. 🙂

**Brainbox:** Je suis tout à fait d'accord avec Dylan, nous avons pris le risque de changer fondamentalement de nombreux aspects du jeu, et d'après ce que je peux voir, cela a payé. Je pense qu'après tant d'années sans pratiquement aucun changement dans le jeu, il était temps de faire bouger un peu les choses. 😄

**ChehTan:** Je n'ai pas répondu à celui-ci 😁
Je suis ravi de la sortie de la Saison 16. C'était un vrai challenge de travailler dessus en secret et d'après ce que j'ai entendu, cette nouvelle saison a été une réussite (sauf tout les bugs qu'on a introduit 😬)

## **Eternaltwin:** Comment avez-vous préparé le contenu de la saison ? Y a-t-il une feuille de calcul, un forum interne, des serveurs Discord ou beaucoup de paperasse? Avez-vous bénéficié de l'aide de personnes extérieures ?

**ChehTan:** L'ensemble du processus est actuellement une réflexion interne entre les Oracles et l'équipe principale.
Nous avons entre autres un Discord dédié pour gérer les saisons à venir, les bugs de celle en cours, etc...
Je veux en quelque sorte garder le reste du système secret. Où est le plaisir quand il y a pas de mystère ?
Mais mes amis ici voudront peut-être vous donner plus d'informations 😁

**Dylan57:** Je ne vais pas vous expliquer tous les détails sur notre fonctionnement en interne mais nous avons le concept du 42ème étage : nous avons un forum privé dans MH où les oracles/corbeaux et les admins peuvent parler, c'est là que nous parlons du contenu des saisons à venir, changelogs officiels pour les saisons, informations sur le code / etc.

Pour la saison 16, nous avons repris une partie de ce qui a été discuté entre les développeurs de Hordes et les Oracles entre la saison 11 et la saison 14 : en gros, MotionTwin avait quelques projets qu'ils laissaient dans leurs tiroirs et qui étaient débattus avec les oracles. Nous avions donc des idées générales comme "ajuster les plans des ruines explorables" mais pas les détails, nous avons fait le reste. 🙂

Aussi, en raison des changements massifs de la saison 16, Guizmo (oracle français) a réalisé un grand document pour parler des nouveaux chantiers et d'autres choses, nous permettant de travailler dessus et après de faire les modifications sur le code selon le doc.

Pour la nouvelle arborescence des chantiers, Guizmo a eu l'aide de joueurs externes de confiance qui jouent à différents styles de jeu pour dessiner un "prototype", et lorsque nous avons commencé à travailler dessus, tout le monde sauf les administrateurs/oracles et les corbeaux ont eu accès au document qui a ensuite été édité par nos soins.
Hormis cela, nous n'avons pas eu d'aide de personnes extérieures (sauf Docteur, qui est un "dev extérieur" mais n'a pas accès au 42ème étage) pour le développement de la Saison 16, je regarde personnellement fréquemment la boite à idées MyHordes pour voir si des idées peuvent être ajoutées dans une Saison, si ça me saute aux yeux on en discutera au 42ème étage pour l'équilibrage / etc
Par exemple, l'action d'objet de réparation pour le technicien, qui a été ajoutée lors de la saison 15 provient de la boîte à idées. 🙂

Par ailleurs, avec la saison 16, nous étions censés ajouter une chose spéciale concernant les changements provenant de la boîte à idées, comme le fait DeadCells. Mais on a un peu oublié 😅

_Adri: honnêtement, il y avait assez de travail à faire tel quel lol_

**Adri:** Je n'ai pas grand chose à dire sur les deux dernières questions.

**Nayr:** Pareil pour moi, je n'ai pas beaucoup travaillé sur S16, j'ai surtout donné mon avis.

**BrainBox:** Dylan l'a parfaitement décrit 😄

## **Eternaltwin:** Tous les mécanismes et secrets de S16 ont-ils été découverts? Ou y a-t-il encore des surprises?

**ChehTan:** Je pense qu'il reste encore des secrets à découvrir, mais je ne sais pas ce que les joueurs ont déjà découvert. 🤣

**BrainBox:** Ouais, je suis presque sûr qu'il y a encore des secrets qu'ils doivent découvrir 😄

## **Eternaltwin:** Qu'avez-vous pensé de la course française (même si ce n'est pas une course) pour la nouvelle merveille ? Vous attendiez-vous à quelque chose comme ça ?

**ChehTan:** Peu importe ce à quoi nous nous attendons, les gens trouveront toujours un moyen de nous surprendre 😁

**Nayr:** Les Thermes de l'Or Bleu était une merveille très demandée, nous savions donc qu’il y aurait probablement une course pour la construire. C'était quand même vraiment cool de voir les grandes réactions lors de son dévoilement.

**Brainbox _(sorti de nul part)_**
_Au fait, au cas où vous vous demanderiez ce que je ferai pendant les vacances de Noël..._

![Good vacations](./leak.png)

## **Eternaltwin:** Et maintenant, travaillez-vous sur la S17 ou sur la qualité et la correction de bugs pour la S16?

**Brainbox:** Bien que nous ayons déjà prévu des choses pour S17, la v2.1.0 sera une mise à jour de qualité pour la S16. 😉
Et bien sûr, nous travaillons toujours sur des corrections de bugs ; généralement, nous avons au moins deux branches de code distinctes sur lesquelles nous travaillons simultanément, une pour la version actuelle du jeu (par exemple 2.0.x), qui reçoit de petites améliorations et souvent des corrections de bugs, et une pour la prochaine version du jeu (par exemple 2.1), où nous travaillons sur des choses plus importantes

**Dylan57:** J'ai commencé une ébauche de ce que nous pouvons faire pour la saison 17, bien sûr, ce n'est pas écrit dans le marbre mais nous avons une jolie idée de ce que nous devons faire pour la saison prochaine.
Cependant nous n'y travaillons pas encore, puisque nous corrigeons des bugs de la saison 16 et améliorons un peu le jeu chaque semaine 🙂
Et en parallèle nous travaillons sur la 2.1.0 qui est la mise à jour de mi-saison 16, apportant des améliorations majeures de performances ou des fonctionnalités pratiques hors-jeu. 😉

## **Eternaltwin:** Comment testez-vous le jeu pendant son développement ? Avez-vous un serveur privé avec peu de joueurs sélectionnés ? Ou testez-vous les changements un par un ?

**Dylan57:** Nous pouvons le tester nous-mêmes sur un serveur local, mais nous utilisons également le serveur staging à cet effet.
Nous avions lancé le serveur staging pour la version bêta publique de la saison 15, et après la sortie de la saison 15, nous avons décidé d'utiliser ce serveur pour essayer nous-mêmes de nouvelles fonctionnalités parmi les administrateurs/corbeaux et oracles, ce qui a permis de trouver quelques bugs avant la saison 16. 🙂
donc techniquement parlant, oui certains joueurs sélectionnés peuvent essayer de nouvelles fonctionnalités : admins, corbeaux et oracles 🙂
Par exemple, nous pouvons déjà essayer la première fonctionnalité de la version 2.1.0 qui y a été implémentée 🙂 dont je peux vous fournir un indice.

_Habituellement je fais 4096, parfois 3072, mais en de rares occasions je fais 2847... Qu'est-ce que je suis ?_

## **Eternaltwin:** L'extension de la ruine explorable est une très bonne idée et bien accueillie, elle augmente la difficulté mais récompense les challengers. Vous attendiez-vous à ce que les joueurs réussissent à fouiller une ruine entièrement dans une ville Pandémonium ?

**Dylan57:** C'est assez drôle, car il y a toute une histoire derrière ce changement : le changement des ruines avec étages a été réalisé il y a 1 an (en octobre 2022) ; au départ, c'était censé être une option de ville privée, mais nous avons laissé l'idée de côté car elle était jugée peu utile et surtout elle manquait d'équilibre.
Au début de l'année, certaines personnes avaient cherché dans le code et découvert le code des ruines à plusieurs étages (sans équilibrage) et avaient créé un sujet sur le Forum Monde pour en savoir plus et avaient déclaré que ce serait une fonctionnalité de la saison 15. ; à l'époque, nous avions dit que ce n'était pas une fonctionnalité du jeu, que ce serait abandonné.

Lorsque nous avons commencé à travailler sur la saison 16, un point qui revenait depuis quelques temps était que les ruines sur MyHordes sont plus simples : c'est parce que le site est rapide et particulièrement optimisé sur les aspects carte/ruines ainsi que notre algorithme de labyrinthe qui est différent de celui de Hordes, il était donc facile d'explorer toute la ruine sans vraiment rien risquer.
Et c'est là qu'est venue l'idée de ramener la ruine avec des étages mais avec un équilibrage : le but était simple ; rendre l'exploration de la ruine plus complexe mais récompenser un peu l'exploration de l'étage pour éviter l'aspect punitif ; d'où pourquoi il y a maintenant 15 portes qui sont réparties aléatoirement sur les 2 étages au lieu de 10 sur un seul étage, en plus de la perte d'oxygène lors de l'utilisation des escaliers (même si certains ont dit que c'était un peu trop, nous n'avons pas changé cela).
Après les niveaux d'oxygène n'ont pas changé, et en étant préparé un minimum et avec un labyrinthe pas trop complexe, il est toujours possible de fouiller les 15 portes et d'en sortir sans blessure même en ville Pandémonium, en plus du bonus d'oxygène du Fouineur, alors vous avez plus de temps de l'explorer. Nous n'avons pas conçu les étages pour les rendre impossibles, donc c'est bien si c'est réalisable et nous le savions 😉

_En passant : avez-vous remarqué que l'étage du bunker est un sous-sol tandis que l'étage de l'hôpital et de l'hôtel est un étage supérieur ?_

## **Eternaltwin:** Twinoid et tous ses jeux ont pris fin il y a quelques mois. La plupart de nous ont grandis avec ces jeux. Vous faites partie de ceux qui apprécient tellement le jeu que vous en avez fait renaître. Que pensez-vous de la clôture ?

**Nayr:** J'étais prêt pour la fermeture, mais quand même, quand cela a fermé, c'était bizarre, comme si nous avions effectivement perdu quelque chose. Cela fait définitivement partie de mon enfance et je suis bien sûr un peu triste que ce soit terminé. Cependant, je suis vraiment content que nous ayons eu suffisamment de temps pour développer MyHordes.

**Brainbox:** Je ressens la même chose, puisque Twinoid est resté ouvert bien plus longtemps que prévu, cela a un peu atténué le coup. Mais bien sûr, c’est quand même triste. En même temps, je suis heureux qu'un si grand nombre de "vieux" se soient tournés vers Eternaltwin et MyHordes.

**Dylan57:** A vrai dire sur Twinoid cela faisait des années qu'on n'avait pas entendu parler des développeurs de MotionTwin avant leur annonce en mars 2020, ça ne m'a pas surpris quand ils ont fait leur première annonce, mais c'est sûr que c'est triste quand tout était vraiment fermé. Ça fait du bien aussi d'une autre manière, d'avoir un peu de reconnaissance de la part de MotionTwin, ils n'avaient pas besoin de mettre en place une redirection vers Eternaltwin mais ils l'ont fait, et je trouve ça très cool, même si c'est un peu dommage d'avoir ces jeux qui ont disparu du portfolio sur leur site Internet.

## **Eternaltwin:** 2024 est là, quelles sont les grandes prochaines étapes pour MH ? Peut-on avoir les saisons 17 et 18 la même année ?

**Brainbox:** Pour nos projets de saison, nous pouvons certainement promettre la S17. Comme nous travaillons tous pendant notre temps libre, il est difficile d'estimer quand les choses se termineront. 😂

**Dylan57:** Pour le planning des saisons, il y aura bien sûr la saison 17 pour 2024, avec peut-être une surprise ? En tout cas, nous sommes assez loin d'atteindre la saison 666 !

_Blague à part : Deepnight avait déclaré sur les forums mondiaux d'Hordes avec une balise d'annonce qu'il reviendrait à la saison 666._

_ChehTan: Nous avons déjà des plans pour atteindre la saison 666 le plus rapidement possible. L'un d'eux traverse un trou de ver_

## **Eternaltwin:** Quel a été le bug le plus drôle que vous ayez rencontré lors du développement de MH ?

**Nayr:** La page du forum essayant de fusionner avec la page de connexion était plutôt amusante.

![Merging Bug](./bug1.png)

De plus, une fois nous sommes revenus à la version bêta pour une raison quelconque, même si nous étions en S15

![Season Bug](./bug2.png)

**Brainbox:** Ah oui, cela s'est produit parce que j'ai publié un correctif sur le serveur que je n'avais pas correctement enregistré, donc le jeu a supposé que j'avais publié une version bêta xD
Si vous me le permettez, j'aimerais souligner quelque chose qui n'était pas un bug, mais pendant l'alpha, nous avions l'objet betapropine qui fournissait 26 PA. Les gens développaient des stratégies Opti pour jouer avec ça, ce que j'ai trouvé très drôle. D'ailleurs, c'est toujours dans le jeu et peut être activé pour les villes privées. 😂

**Adri:** Il y avait un bug très important qui a poussé Brainbox et moi à se connecter en vocal pour résoudre ce problème, c'était de ma faute si je n'y prêtais pas suffisamment attention. Pas drôle mais c'est certainement un moment fort pour moi et probablement la seule fois où le vocal a été utilisé aussi lol.

**Dylan57:** Ce n'est pas un bug très dérangeant mais ça m'a un peu frappé et j'en ai beaucoup ri quand on l'appelait un "Bugthesda" en quelque sorte 😄
Lorsque nous avons mis en place les étages des ruines explorables pour la saison 16, nous avons tout testé... sauf être éjecté par manque d'oxygène avec des objets à l'étage. Cela n'a pas manqué : il y avait un morceau de code que nous avions oublié, qui...garantissait que les objets étaient placés au rez-de-chaussée même si vous étiez à l'étage, provoquant ainsi des cas où les objets disparaissaient dans le vide car ils se trouvaient dans une zone inaccessible.

Sinon, un autre bug que je trouve être un running gag ; c'est un bug que nous avons corrigé en alpha v0.3, que nous avons recorrigé en alpha v0.6 et que nous avons dû encore corriger en beta.6 : la gazette s'affichait comme si on se regardait dans un miroir lorsqu'on cliquait sur "retour" sur mobile. Une petite évolution du bug est qu'à chaque fois c'était quelque chose de différent et de pire.

Ah sinon il y a aussi un bug notable que nous avons trouvé lors de l'alpha : je ne me souviens plus comment, mais une partie du code pour changer le mot de passe réinitialisait en quelque sorte complètement la progression de l'âme MH : un joueur anglais nous a informé du bug, et moi, qui n'avait pas encore de serveur local installé, a reproduit le bug sur mon âme publique actuelle pour vérifier.
Brainbox a pu corriger le problème, mais cependant la progression du joueur ainsi que la mienne n'ont pas pu être restaurées.

C'est un peu complexe de trouver LE PROBLEME parmi les 3 mille problèmes qu'on a eus 😅
Chose amusante, au début de l'alpha : cette betapropine fournissait 999 PA et il y'avait aucune limite par jour. Par la suite, c'était 26 PA sans limite par jour, puis pendant la phase bêta, c'était 26 PA avec une limite d'une fois par jour.

## **Eternaltwin:** Merci pour votre participation à cette interview, avez-vous autre chose à ajouter ?

**Dylan57:** ATTENTION AU POULET

**Nayr:** Un grand merci a toutes les personnes qui aident sur MH et au nombre de joueurs qui augmentent pour leur soutien continu.

_Dylan57: Ouais, c'est mieux que ma version 😄_

_BrainBox: Je soutiens pleinement la phrase de Nayr 🙂_

_Dylan a posté un gif disant "Le poulet n'est pas amusé"_

**BrainBox:** Bon sang, la mise à jour sera encore maudite !

_Quelques minutes plus tard, la mise à jour de MH a échoué_
