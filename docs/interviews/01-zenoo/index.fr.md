# Zenoo

**Salut Zenoo ! Merci de prendre quelques instants pour répondre à ces questions. Tu as fait une entrée en grande pompe durant cet été 2022 en prenant en main LaBrute. Peux-tu te présenter en quelques mots ?**

_Hello ! Je suis Zenoo, un ancien joueur Hordes de la grande époque. Je suis développeur à temps plein, avec d'autres projets personnels en plus, dont LaBrute plus récemment._

**Ton premier message montrant une maquette de ta version de LaBrute date du 30 Juillet et l'ouverture de la première version 1.0.0 du 10 Août. Comment as-tu fait pour sortir aussi rapidement un jeu fonctionnel ? As-tu un secret à partager ?**

_En regardant l'historique Git, j'ai réellement commencé à bosser sur ce projet en début/mi Août, mais les 2-3 premières semaines ont été consacrées à la décryptions/récupération d'assets depuis les fichiers sources de la MT (à l'époque pas encore disponible comme ils le sont aujourd'hui)._

_Donc effectivement j'ai commencé à réellement coder le projet fin Août. La v1 est sortie peu après, mais il faut rappeler que cette v1 était la première version fonctionnelle, mais qu'il lui manquait beaucoup de choses. La visualisation des combats n'existait pas, par exemple, les joueurs avaient uniquement une vue textuelle de ce qui se passait dans les combats._

_Quant à la rapidité de production, j'essaie d'être efficace dans ma méthodologie, et j'organise strictement mon développement. Toutes les tâches sont regroupées dans un Workflow (disponible sur mon [Github](https://github.com/Zenoo/labrute/issues), si jamais ça vous tente de venir donner un coup de main), ce qui me permet de faire abstraction de tout ce qu'il me reste à faire quand je me lance sur une tâche, et de m'y concentrer pleinement._

**En effet on peut remarquer que tu travaille sur le projet par sessions assez intences. ![Interview - Git Graph de Zenoo](./interview_zenoo.png)**

**LaBrute commence à être stable et attire de plus en plus de joueurs. Peux-tu nous donner des stats sur le nombre de brutes créées ?**

_Effectivement, j'ai tendance à carburer quand je me lance sur quelque chose, c'est probablement dû à la satisfaction quand j'arrive à surmonter un obstacle. Avec le temps les obstacles deviennent de plus en plus grands, ce qui me fait arrêter et reprendre plus tard, une fois la hype remontée et de nouvelles idées trouvées._

_Au niveau des stats, je ne peux malheureusement pas en fournir sur la totalité du développement, vu qu'il y a eu plusieurs remises à zéro. Depuis le dernier reset (30 Janvier), il y a eu un total de 11925 brutes créées._

**Ah oui en effet quasiment 12k brutes en même pas un mois c'est vraiment impressionnant ! Tu disais toute à l'heure être un ancien joueur de Hordes. Il me semble que tu es le créateur de Zavatar, un site de demande de création d'avatar ?**

_Ouhla, ça date tout ça ! J'ai effectivement créé la V2 de Zavatars avec Tsha (une personne avec un talent fou pour le dessin), qui gérait déjà Zavatars V1 plusieurs années auparavant avant son abandon._

_Le site fonctionne toujours depuis son ouverture en avril 2016, bien que très calme en ce moment. A l'inverse du développement continu sur LaBrute, ce projet a été développé en une seule fois, quasiment. Je n'ai pas le souvenir d'avoir eu à faire une mise à jour pour fixer un bug, c'était une autre époque, avec des développement bien plus simples qu'aujourd'hui 😄._

_D'ailleurs, il est possible que Zavatars fasse son apparition sur MyHordes un jour, qui sait ?_

_... Et pas que Zavatars !_

**Très bonne nouvelle pour Zavatars ! Et du coup, en temps que Hordiens pourquoi avoir choisis de développer LaBrute plutôt que d'aider sur MyHordes ? Les technologies utilisées ne te correspondaient pas ?**

_C'est ça, MyHordes est développé en PHP, un langage que je préfère ne plus toucher, j'en ai trop utilisé il y a une dizaine d'années, je le trouve bien trop arriéré par rapport à ce qu'on a à disposition aujourd'hui._

**A quel pourcentage d'avancement pense-tu en être pour LaBrute ? As-tu un objectif final pour dire "ca y est j'ai terminé" ou compte-tu ajouter des fonctionnalités quitte à refaire la v2 ?**

_C'est difficile d'estimer le degré d'avancement de LaBrute, tout simplement parce que c'est un projet en développement continu, qui tient compte des retours et suggestions des utilisateurs. Sans compter les idées de développement qui arrivent au jour le jour, je dirais que je suis actuellement à 30% d'avancement._

_Je n'ai pas réellement d'objectif final, à part de réussir à avoir au minimum toutes les fonctionnalités qui existaient auparavant, et aucun bug. Par rapport à la V2, cette nouvelle version est en réalité un mix de la V1/V2, en essayant de garder le meilleur des 2. Je compte aussi rajouter des fonctionnalités qui n'étaient ni dans la V1 ni dans la V2._

**Imaginons une minute que tu juge ta version de LaBrute terminée. Tu as pu y ajouté toutes les fonctionnalités de la v1 et v2 intéressantes. Quelle fonctionnalité ou mode de jeux vas-tu rajouter ? Ou peut-être rien et passer à un autre jeux à speedrun le développement ?**

_Une fois la V1 et V2 implémentées, je pense rajouter un système d'achievements, des quêtes, des améliorations sur le système de récompense des tournois, et sur le sacrifice des brutes. Une fois tout ça fait, si DinoRPG n'est pas encore fini, j'ai bien envie de m'y pencher. Ce genre de jeu avec des auto-battles et un système de leveling journalier est très satisfaisant pour moi._

_J'aurais bien refait Hordes par la suite, mais vu qu'il est déjà géré, ça n'arrivera pas. Un autre jeu qui sort de l'univers MT qui avait retenu mon attention il y a 10-15 ans était Zepirates, qui a le même principe de leveling journalier. Si je ne trouve rien d'autre entre temps, je pourrais peut-être me lancer dessus._

**Que de bonnes nouvelles ! Quel a été ton plus gros challenge sur le développement de LaBrute pour le moment ? Et quel est la fonctionnalité qui te fait le plus peur, qui te semble le plus dur à intégrer ?**

_Le plus gros problème que j'ai rencontré jusque là a été la mise en place des assets des brutes et les afficher correctement dans le moteur de combats. Créer les sprites des brutes avec des apparences personnalisées est un travail monumental._

_C'est d'ailleurs aussi ma réponse à la deuxième question. Finir l'implémentation des sprites des brutes avec des couleurs/formes customisées, et l'implémentation de l'affichage des armes et du bouclier va m'obliger à créer des centaines si ce n'est des milliers de fichiers sources, chacun comprenant la logique d'affichage des couleurs/formes sur chaque image de chaque animation._

_**Bref, un travail énorme.**_

**En effet, nous avons le même soucis sur DinoRPG et y allons petit à petit car chaque dinoz en affichage statique (hors combat) représente déjà une centaine de fichier. Dernière question : peux-tu dire quel sera le contenu de la prochaine grosse mise à jours de LaBrute ?**

_Ça sera probablement l'implémentation des différents rangs de brute, après avoir gagné un tournoi._

**Super ! Merci pour toutes tes réponses ! As-tu quelque chose à ajouter 🙂 ?**

_N'hésitez pas à passer sur le Discord pour partager vos retours et me notifier de bugs que vous rencontrez, ça aide le projet à avancer._
