# eMush équipe de développement

## **Eternaltwin:** Bonjour à vous, merci de vous prêter à cette exercice d'interview. Pouvez vous vous présenter rapidement ?

**Breut:** Bonjour,  je suis Breut (Sylvain Breton, 31 ans IRL). J'ai découvert les jeux Twinoid avec Horde aux alentours de 2013 et me suis rapidement mis à jouer à Mush. Lorsque le projet ET a vu le jour, je me suis assez rapidement impliqué dans la récolte des données de Mush, puis au bout de 6 mois, j'ai fini par franchir le pas et commencer à coder pour eMush. J'adore coder, mais je ne suis pas développeur de formation. J'utilisais python pour faire de la recherche et je ne pensais pas pouvoir me montrer utile au projet. Au final, je suis maintenant l'un des trois développeurs principaux de eMush.

**Sami:** Bonjour je suis Sami, 22 ans et j'aidais ce projet car Mush a marqué mon enfance, j'ai cherché pour un projet déjà existant et il y en avait déjà un alors j'ai atterri dessus, j'ai débuté l'informatique assez tôt dans ma vie ce qui fait de l'informatique une de mes passions et je voulais y trouver une utilité même si ça m'a pris du temps pour oser le faire ici. Dans la vraie vie je suis développeur Java dans une entreprise de conseil, ce qui m'éloigne pas mal du PHP mais reste dans mon domaine car j'y fais du back end et pour le moment je reste en retrait car j'ai du mal à ne pas revenir dans mes réflexes Java quand je me mets à PHP (et sans Webstorm c'est encore pire haha). Mon travail a pris le dessus sur mes projets perso pour m'entraîner pour celui ci donc j'ai dû délaisser le projet pour le moment.

**Zasfree:** Bonjour, je suis zasfree. J'ai découvert les jeux Twinoid en 2011, et plus tard Mush en 2013. J'ai vraiment commencé à m'attacher à ce jeu en 2015.

Depuis que j'ai commencé à jouer à Mush, j'ai voulu comprendre les mécaniques du jeu le mieux possible afin d'établir les records de survie les plus impressionnants. Lorsque le projet Emush a commencé, même si je ne suis pas développeur, je suis devenu l'un des trois premiers responsables du projet. Je me chargeais principalement de la collecte de données sur le jeu original, de la sélection des joueurs pour les vaisseaux alpha d'Emush, et du lancement de vaisseaux tests dans Mush pour recueillir autant d'informations que possible. J'étais également en charge des annonces du projet.

Aujourd'hui, l'alpha est ouverte à tous, Mush est fermé, et Evian est beaucoup plus performant que moi pour les annonces XD. Je m'occupe donc de mettre à disposition des informations sur le jeu original à partir de captures d'écran ou de vidéos que j'ai prises quand le jeu était encore ouvert, afin d'aider les programmeurs lorsqu'ils en ont besoin ou posent des questions.

**Gowoons:** Yo moi je suis gowoons, étudiante en école d'ingé. J'ai découvert mush en 2020 (oui tard lol, le projet emush existait déjà je crois) parce que j'étais confinée avec un de mes meilleurs amis qui adore le jeu depuis longtemps (joker). J'ai tout de suite accroché et adoré. Je fais des études dans l'informatique, notamment en devops et en cyber. J'ai commencé à participer au projet il y a 1-2 ans, même si le dev est pas ma spécialité ça me plaît bien. Je participe quand j'ai du temps, il y a eu une longue période où je n'ai rien fait, et j'essaie de reprendre un peu maintenant, ce qui est parfois difficile parce que malheureusement mes cours/stages doivent prendre la priorité sur emush :calim:

**Evian:** Salut, je suis Evian. Dans la vraie vie, j'ai 25 ans et je suis ingénieur en science des données en alternance.
J'ai découvert les jeux de Motion Twin avec Minitroopers au collège, puis j'ai joué à peu près à tous les jeux de Twinoid, jusqu'à ce que Mush sorte en 2013 et que je lui sacrifie mon âme (anecdote inutile donc indispensable, à l'époque j'étais le 2ème joueur qui avait le plus joué Chun). 😛

J'avais rejoint une alpha d'eMush début 2022 et j'avais été impressionné par les progrès réalisés.
Ayant appris à programmer durant mon alternance au même moment, ça m'a donné envie de franchir le pas. Je suis aujourd'hui l'un des développeurs principaux de eMush.

**A7:** Moi c'est A7 (ou Atar7, c'est égal), il me semble avoir découvert les jeux MT avec Hordes, puis Twinoid ensuite (ça date tellement...). De profession, je suis issu de l'industrie graphique avec une curiosité pour le développement et la technologie. J'ai intégré le projet Eternaltwin d'abord pour travailler sur le site principal, puis le projet eMush qui m'a semblé être un challenge intéressant.

## **Eternaltwin:** Comment faites vous pour gérer eMush et votre vie pro/perso ? Est-ce que eMush rythme vos journées ou c'est plutot ce qui comble les temps morts ?

**Breut:** En trois ans, il y a eu beaucoup de changements dans ma vie pro et ma réponse n'est donc pas la même aujourd'hui que l'année dernière. En particulier, il y a deux ans, je pouvais régulièrement et efficacement travailler sur eMush car j'étais en sous service (pas assez d'heures de cours disponibles pour que je les fasse), aujourd'hui, c'est plutôt pendant les vacances scolaires. Je m'efforce cependant de résoudre régulièrement les bugs qui nous sont reportés pour que la communauté qui n'a désormais plus accès à Mush sente le projet avancer malgré notre baisse de disponibilité.

**Evian:** J'ai des tendances mono-maniaques, donc eMush occupe une (très) grande partie de mes soirées et de mes week-ends actuellement, voir un peu plus si on compte le temps que je passe à corriger des bugs quand je suis en cours (oui je sais, c'est pas bien 😔).

Donc effectivement on peut dire que ça rythme mes journées.

Il peut m'arriver d'être moins actif lorsque mon temps libre diminue, la plupart du temps car je dois réviser plus longtemps pour compenser le temps que j'ai passé à coder pour eMush en cours. Hmm... Il y a peut être un changement de stratégie à effectuer... 🤔

**A7:** Il y a eu des périodes d'occupations variées dans ma vie ces dernières années. Par principe, je considère toujours eMush comme une seconde priorité, ma vie professionnelle doit passer avant. Par chance j'ai parfois beaucoup de temps libre, que je remplis avec des projets persos tels que eMush.

## **Eternaltwin:** Comment vous répartissez les taches entre vous ? Est-ce que certains ont des facilité sur des fonctionnalité spécifique et ne font que ca ou est-ce plutot chacun prend ce qu'il veut ?

**Breut:** Alors pour commencer, je vais donner 2 termes qu'on risque d'utiliser pour répondre à cette question. On peut découper le projet en deux grandes parties. Le front c'est ce que vous voyez sur votre page internet, comment s'arrangent les différents éléments, le Daedalus représenté en vue isométrique et le bouton, très tentant, pour frapper Chao qui s'est allongé. Le back c'est toutes les opérations sur le serveur de eMush, c'est là qu'on modifie la santé de Chao parce que vous n'avez pas pu vous retenir d'appuyer sur ce bouton.
Moi, j'ai commencé sur la partie back de eMush, puis j'ai fini par faire un énorme pan du front, le Daedalus en vue isométrique. Pour ce dernier point, on peut dire que c'est ma spécialité. Pour le back on travaille souvent ensemble, à chaque fois qu'un dev fait une modification, son code doit être relu par un autre dev, ça implique de comprendre ce qui a été fait. Mais le projet devient de plus en plus gros avec les maladies, les hunters, les explorations... je dois avouer que je maîtrise de moins en moins le projet dans son ensemble.

**Evian:** Breut a bien résumé notre fonctionnement. Vous pouvez aussi voir le back comme le moteur du jeu et le front comme l'affichage.

Il y a en effet quelques pôles d'expertise : en ce moment, c'est moi qui développe la plupart des nouvelles fonctionnalités côté back comme les explorations, pendant que Breut consolide et nettoie les fondations du moteur. 
J'ai également une très forte pensée pour A7 sans qui le jeu serait juste une page blanche avec du texte (allez, elle serait peut être bleue). 😱 

D'un autre côté, nous avons besoin d'être polyvalents si nous voulons faire avancer le projet : si je veux que les explorations fonctionnent vraiment, je dois aussi coder le bouton pour les lancer et la page pour visualiser les événements, même si ce n'est pas ma spécialité (mais heureusement que A7 vient corriger derrière moi).

Nous avons tous dû mettre la main dans le cambouis de toutes les parties de eMush pour faire des mises à jour régulièrement, et il n'est pas si rare que plusieurs développeurs collaborent sur une seule fonctionnalité pendant quelques jours.

Enfin, je peux évoquer les contributeurs plus ponctuels, qui n'en sont pas moins importants.
Lorsqu'on regarde bien, ces derniers ont développé des fonctionnalités qu'on ne priorise pas mais qui restent importantes. Je pense par exemple à notre système d'alertes interne, qui nous aide énormément à déboguer et à intervenir en cas de problèmes, ou à la minimap du vaisseau.

**Zasfree:** Actuellement, ma participation à eMush est flexible. Je surveille le projet plusieurs fois par jour, mais en raison du faible nombre de questions, ça n'occupe pas la majeure partie de mes journées. Je métiens un équilibre en consacrant du temps spécifique au projet tout en préservant ma vie personnelle et professionnelle. Bien que les interactions soient rares, je reste disponible et actif pour répondre aux besoins de la communauté.

Breut et Evian ont bien expliqué notre mode de fonctionnement. Pour ma part, après les fêtes, je prévois de m'impliquer davantage en tant que testeur en installant le jeu en local. Cela me permettra de tester plus facilement le jeu sans gêner les joueurs actuels. Mon objectif est d'identifier les problèmes potentiels et en contribuant en proposant des solutions. J'aspire à offrir une meilleure expérience de jeu à la communauté des joueurs. 

**A7:** Breut et Evian ont en effet très bien résumé le processus. Chacun trouve ses spécialités et le projet avance grâce à une équipe pluridisciplinaire. Personnellement je serais bien incapable de toucher au back-end, même avec toute la bonne volonté du monde.
Evian dit que sans moi le jeu ne serait qu'une page de texte blanche; ma réponse est que sans Breut et lui, le jeu ne serait qu'une page d'accueil sans suite 🙂

_Evian_ Et Simpkin surtout, il ne faut pas oublier **Simpkin**! Sans Simpkin y a pas de projet, eMush au papier crayon.

## **Eternaltwin:** Aujourd'hui eMush est toujours en phase d'alpha ouverte. Quelles seront les prochaines features à être implémentées ? Est-ce que vous avez déjà fixer une étape mettant fin à cette alpha pour un passage en beta ?

**Breut:** Pour rappel, la dernière mise à jour importante a ajouté les explorations, il reste encore du contenu à ajouter, comme implémenter les effets de tout les secteurs et des équipements. La prochaine étape risque d'être liée aux projets-skills-recherches, normalement, nous avons travaillé en amont pour que leur implémentation soit très facile, mais il y en a tellement qu'on va forcément tomber sur des cas à part qui vont nous donner du fil à retordre. Une fois que cette étape sera faite, on ne sera pas loin d'avoir toutes les features du jeu de base. Je pense qu'on pourra passer en beta après ça. Il restera alors beaucoup de travail de finition : bug, petites choses pas encore implémentées, amélioration de l'interface, outils d'admin...

**Gowoons:** Schrödinger aussi dans la finition 😜 très attendu celui là 

**A7:** Il faudra aussi commencer à travailler sur les comptes utilisateurs, et toutes les autres fonctionnalités qui ne font pas partie du jeu en lui-même. Il y aura encore un peu de travail avant d'envisager une beta bien propre.

**Evian:** Nous sommes toujours en train de développer les explorations, et en effet, la suite concernera toutes les "améliorations" du Daedalus (projets, recherches et compétences), selon ce qui est le plus demandé par les joueurs.

Beaucoup de détails évoqués par gowoons et A7 restent à régler avant une béta (et il y en a d'autres). D'une part car j'ai tendance à privilégier la rapidité de publication des mises à jour à la précision (quitte à corriger les bugs rapidement) afin de garder un sentiment de progression régulier. De plus, les 20% de détails restants ont tendance à prendre 80% du temps...

D'autre part car eMush est un jeu très complexe : il m'arrive souvent d'écrire plus de code de test que de code implémentant une fonctionnalité, ce qui n'empêche pas d'oublier des cas particuliers ou certains comportements inattendus. Je pense que nous pouvons progresser là-dessus, par exemple en impliquant plus zasfree dans l'écriture des fonctionnalités à implémenter.

## **Eternaltwin:** Quelle est votre plus grande hate par rapport aux travaux restant ?

**Breut:** C'est un peu présomptueux, mais j'ai vraiment hâte d'ajouter ma touche personnelle à eMush. C'est un sujet encore en discussion, mais j'adorerais faire un petit ajout comme un nouvel artefact ou une nouvelle compétence. Une idée beaucoup moins polémique que je souhaite ajouter, c'est un système d'apparence d'équipement (qui n'a jamais rêvé de taguer une tête de mort sur son patrouilleur). Je ronge mon frein car je n'ai pas le temps en ce moment mais ça fait 1 mois que je souhaite implémenter ça.

**Evian:** J'ai hâte que le jeu d'enquête reprenne ses droits, car c'est ce que je préférais sur Mush et que j'aime beaucoup lire les histoires qui y sont liées. De plus, j'aimerais m'investir de plus en plus dans la communication autour du jeu au fur et à mesure que le projet avance. Nous n'aurons jamais le même nombre de joueurs qu'en 2013, mais je pense qu'il est possible d'attirer beaucoup plus de joueurs, rien que parmi ceux ayant déjà joué à Mush. Si ça se passe bien, j'ai des ambitions beaucoup plus ambitieuses (et polémiques) que Breut pour la suite. 😛 

## **Eternaltwin:** Est ce que vous avez une idée du nombre joueurs quotidien ou total sur cette alpha ?

**Evian** Il y a eu plusieurs remises à zéro durant le développement donc il est impossible de fournir des stats exactes. Ceci dit, au moment où je vous parle, il y a quatre vaisseaux en cours et 255 joueurs ont lancé au moins une partie.

## **Eternaltwin:** Motion Twin a fermé tout ses jeux depuis un peu plus d'un mois. Êtiez-vous mentallement prêt à cet arrêt ? Comment l'avez-vous vécu ?

**Evian:** Pour être honnête, cela faisait déjà au moins deux ans que je ne jouais plus vraiment sur Twinoid, mais je lisais encore les forums par habitude, tout en communiquant dessus pour eMush. J'ai donc moins été touché par d'autres, mais effectivement, les quelques jours après la fermeture, ça faisait bizarre : il m'arrivait régulièrement de tenter d'aller sur Twinoid et de tomber sur la page de fermeture.

**Breut:** Mon objectif était de pouvoir proposer au joueurs une alpha ouverte avant la fermeture totale de Mush. Même si le jeu est loin d'être complet, je pense que le pari est réussit, depuis la fermeture de twinoid on a vu une augmentation de nos joueurs et des rapports de bugs. Concrètement , pour moi, ça s'est traduit par un travail additionnel  de bugfix, c'est tombé au même moment qu'une période chargée au travail, c'était un peu compliqué de cumuler tout ça.

## **Eternaltwin:** Comment envisagez vous l'année pour emush ? Des plans en particulier, une feature surprise?

**Evian:** J'envisage 2024 comme une continuité de 2023.

En effet, il y a un an jour pour jour, eMush était encore en alpha fermée : on ne pouvait lancer qu'un seul vaisseau à la fois, il n'y avait pas de pages de fin de partie, pas de hunters, pas de planètes, pas d'explorations... La page d’accueil laissait à désirer etc... 😅

Je suis content de ce que nous avons accompli et très motivé à continuer de faire des mises à jour régulièrement, ce qui reste le plus important, surtout pour un projet bénévole comme celui-ci.

J'ai aussi beaucoup appris en 2023, ce qui m'a permis d'être beaucoup plus à l'aise sur le code d'eMush. J'ai beaucoup d'idées à tester pour livrer des mises à jour plus fiables et dans la communication autour du jeu.

En terme de fonctionnalités, ma priorité reste qu'eMush ait les mêmes fonctionnalités que Mush, et avec moins de bugs, le plus rapidement possible : nous n'allons donc pas nous éparpiller.

Nous allons donc finir les explorations, puis nous attaquerons aux recherches, projets ou aux compétences.

N'hésitez pas à nous dire sur le forum ou Discord ce que vous préférez !
Merci pour l'interview !

**Breut:** Pour moi 2024 va être plus chargé que 2023 IRL, cela veut dire que je serais surtout là pour épauler Evian pour résoudre des bugs et apporter quelques modifications techniques pour que l'ajout des compétences/projet/recherche se fasse avec le plus de fluidité possible.
Comme l'a dit Evian le plus urgent reste d'implémenter toutes les fonctionnalité de Mush avant de se lancer dans les features surprises, même si j'ai quelques idées que je testerais si j'ai le temps.
