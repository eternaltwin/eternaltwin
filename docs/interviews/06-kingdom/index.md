# eKingdom development team

## Hello, thank you for taking part in this interview. Can you briefly introduce yourself?

**Bibni:** It's Bibni, I discovered Motion Twin games a long time ago (via PiouzPiouz, Hammerfest then Hyperliner). Their games and the people I met on Muxxu/Twinoid gave me a passion for video game development, which I now practice professionally with Unreal Engine. As part of the EternalTwin project, I've been working on EternalKingdom for 3 years now and Dinocard Rebirth for a whole year.

By the way, I don't know if we'll be able to put in a word for ✨ **Glaurung** ✨ who hasn't given us any news for nearly 3 years 🥺 

**Thanhatos:** Thanhatos, "Legendary Barbarian" after the title on the late Muxxu, I discovered Motion Twin games around 2010/11 and was immediately hooked on Kingdom. At the time, I played a lot of role-playing and strategy games, with varying degrees of role-playing.
Kingdom caught my eye because it brought together my personal vice for strategy 😈 , the joy of zany role-play exchanges 🤪 , and resource management, as well as having the good taste to renew games and players 😋 . All in one game! In the EternalTwin project, I take care of EternalKingdom as scribe and rules advisor.

**Talsi:** Hello! I'm Talsi, and I discovered Twinoid games with Hordes, recommended to me by friends. Then I tried out some of the other MT games, including Kingdom, which I particularly liked. I do quite a bit of development in my job, even if it's not my "core business", and I still have a few personal development projects in my spare time, which may one day come to fruition, or at least have taught me a lot. EternalKingdom is an opportunity to use some of my skills for a nice project with nice people, to learn new tricks, and to revive a game I've played a lot.

**Drange:** Drange, "budding explorer" was my name, or "tester of silly things", and I joined Kingdom a few days after its launch in early 2010. I then discovered that an IRL friend was playing it, and we exchanged impressions. My second game was a revelation when I met Glaurung and Logortho. With lots of questions, testing began following discussions with Musaran. From questioning to investigation, I ended up getting to the bottom of a lot of things (even if I was still a dunce when it came to wheat management). I soon realized that destabilization is an excellent breeding ground for those who want to seize opportunities.

## Why did you choose Kingdom as your project? Was it your game of choice? The only Twinoid game you played?

**Thanhatos:** I played Snake quickly, tried out a couple of other games, took a few (too) quick turns on Hordes (which I'd like to play more seriously in the future, by the way), but Kingdom was clearly my great favorite. It combined several forms of play into one, can be played solo or with others, seriously or not at all, and each game is different from the last.

**Talsi:** Kingdom is clearly my game of choice, the one I spent the most time on! I really like the balance between strategy/tactics and diplomacy, which can change completely from one game to the next. It's also a game where the games are long enough (on average about a month) to build something, and short enough to renew themselves and allow you to explore other strategies and meet new players. Or to meet up with old acquaintances in totally different situations, as the mortal enemies of one game can become the indispensable allies of another.

**When Muxxu came out, I first got hooked on Intrusion and then Snake, which I played a lot. I've also tried a lot of MT games, even lesser-known ones like FormulaWan, CaféJeux, CroqueMonster and HyperLiner, which is definitely my favorite game, where I spent 4 years being blown up by players with the Kortex and where I met a lot of friends from that time. I'd like to take this opportunity to make a dedication to Lifaen35 and Terminator35, Sodimel, Dingonikolas and all the other members of HyPeRaCtIfS 😄 . Getting back to Kingdom. I'd chosen this project out of curiosity, remembering the match-3 principle found in a few MT games and wanting to try my hand at this challenge because I love developing multiplayer games. I discovered a game that was much deeper than I thought and it made me want to stick with it and reproduce the original experience.

**Drange:** This is my favorite game, where simple concepts have produced a complex and rich environment. Kingdom is a UFO, with its open maps where players arrive, grow and then die, without any hegemony being established. Messaging is 80% of the game while being outside the game, incredible. I've played other games, but more to wait for a general to move than anything else. I'm in IT but on the database side. The developments I do are not useful for the project, or only marginally so.

I serve as an archive for certain aspects of the game.

## Today, eKingdom is still in the open alpha phase. What are your plans for the future? Are there any plans to go into beta?

**Bibni:** At the moment, we still have three major systems to put in place before we can move on to a beta phase. Battles (alpha 6), trade and bankruptcy management (alpha 7) and vassalization (alpha 8). Alpha 9 will logically be the last, with missing systems such as towers and inactivity. A beta will be released once all the major systems are in place, and the pantheon and statistics will be added.

**Talsi:** Bibni made a great road map that he summed up very well 😄 The main objective is to have a playable game again, using the mechanisms of the old Kingdom. Secondly, we want to improve the game's ergonomics to make it more enjoyable to play, especially on mobile. We've already made a few changes in this direction, such as removing a certain number of popups that required the entire page to be reloaded each time the "ok" button was clicked, or redesigning the map display to include a zoom function. But there are still many ergonomic aspects that need to be improved. Finally, we'll see what we can do about the gameplay, but that's a tricky one - we need to make the game even better without changing its character! In any case, we still have a lot of work to do before we can really ask ourselves these kinds of questions!

**Thanhatos:** Today, the project's super dev' team has put together a roadmap for the development aspects, which shows that we're moving forward with a vision that's not too chaotic. 😄 As far as I'm concerned, it's time to get busy writing/rewriting the rules to simplify the approach according to the different levels of player involvement to come (remember the tutorial that drove us to bankruptcy and starvation in the 1st game? Yeah, well, we're going to try and take it a bit easier. :D). I can't wait until we get to the beta version to bring this project to life, and put glitter and catapults back into our daily lives!

## **Eternalwin:** How many players are in the Kingdom games? Were you expecting this many people?

**Bibni:** There are currently just over 350 players who have joined since the update in early November. There are also around 40 players on the early battle version. This has allowed us to work out some major bugs. Thanks again to everyone for your support and feedback on the forum or discord!

**Talsi:** I was very pleasantly surprised by the number of players who came to test the game. Kingdom had been losing players for years, with a vague revival of interest when MT made the muxxu token options free, only to recover once the craze for these options, which actually break the game, had passed. It's great to see so many players interested in Kingdom!

Personally it's a surprise, I didn't realize how busy the game could still be.

## **Eternalwin:** Bibni, you're lead on both eKingdom and DinoCard, how do you prioritize these two projects?

**Bibni:** Trick question 😅 Generally speaking, I focus mainly on Kingdom.

Occasionally, I'll put some effort into Dinocard when there's an urgent need, such as recently with data recovery.

But it's certain that Kingdom development will finish before Dinocard.
The lack of volunteer developers and the time I can allocate mean I can't make as much progress on both projects.

## **Eternalwin:** How did you cope with the closure of twinoid?

**Talsi:** Personally, I had the impression that Twinoid had been dying for years. Kingdom hadn't been updated in more than 10 years, even though certain problems had long been pointed out by the community and could have been corrected (I'm thinking in particular of the big piles of barbarian units, called "bouses" by the commu', dropped just before dying by ill-intentioned players with the aim of rotting the maps). It had also been years since anything had been done about multi-accounts in-game, and not much had been done to moderate the forums.  Finally, the switch to open-bar on muxxu tokens, making any player potentially immortal, completely broke the gameplay (in fact, I've always found token options on Kingdom to be game-breaking, but they were relatively little used before). Allowing the community to remake the game was, in my view, the best decision MT could have made, since they didn't want to reinvest time/people/pennies in the game. From that point on, closing the game was quite logical. I didn't have a bad time of it myself: we were warned well in advance and were able to recover almost everything we needed (we're still missing the formula for the revolt ^^).

## **Eternalwin:** Do you have any teasers, news or plans for readers?

**Talsi:** Teasing, I don't know 🙂 We'll try to make the game as good as we can! Contributions are welcome, as are tests and feedback, or just drop in for a quick hello, it's always a great pleasure 🙂 Anyway, come on in!