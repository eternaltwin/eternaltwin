# Brainbox

**Merci de prendre de ton temps pour la gazette d’Eternaltwin. BrainBox est un nom dont beaucoup de personnes ont entendus parler, peux-tu te présenter pour les autres ?**

_Bien sûr. Je m'appelle Brainbox, j'ai 32 ans, je viens d'Allemagne et je suis le chef de projet de MyHordes. Je joue à DieVerdammten (version allemande de Hordes) depuis la saison 2 et j'ai commencé à développer des applications externes pour ce jeu avec la saison 3._

**Hordes (et ses autres versions) était l'un des jeux les plus emblématiques du Motion Twin. Quand as-tu commencé le développement de MyHordes ?**

_J'ai commencé à la fin du mois de novembre 2019, bien avant la création d'Eternaltwin. L'idée est née lorsque je discutais de la fin du support de Flash Player avec un autre joueur de DV. Nous avons terminé notre discussion sur l'idée qu'il serait formidable que quelqu'un refasse DieVerdammten sans Flash. Le lendemain, je me suis dit "Hé, pourquoi attendre que quelqu'un le fasse alors que je pourrais le faire moi-même" et je me suis mis au travail._

**Le 11 février 2023, 3 ans et demi plus tard, le jeu est enfin prêt pour son début avec la saison 15. C'est un travail impressionnant. Qu'en penses-tu ?**

_Cela a en effet représenté beaucoup de travail. Avec le recul, je suis moi-même impressionné par le fait que nous ayons réussi à le mener à bien. On voit souvent ce genre de projets Open Source: les développeurs partent faire autre chose, les mises à jour sont de plus en plus lentes et, à la fin, le projet meurt. Mais Hordes a une communauté très active, et les retours constants - bons et mauvais - nous ont permis de rester motivés._

_Dans un sens, je suis heureux que nous ayons finalement réussi. D'un autre côté, nous avons tous des tonnes d'idées pour de futures améliorations et du contenu, des choses que nous voulons améliorer, etc. La fin de la bêta n'est donc que le début de nouveaux développements._

**Pour l'instant, quel est votre plus grand défi accompli sur ce projet ? Et quelle est la chose dont vous êtes le plus fier ?**

_Le plus grand défi a été d'arriver à régler tous les détails ; même pendant la bêta, nous n'arrêtions pas de trouver des éléments différents de ceux de Hordes. C'est un jeu très complexe, mais je ne m'attendais pas à ce qu'il soit AUSSI complexe au départ._

_La réalisation dont je suis le plus fier va un peu dans le même sens ; c'est que nous avons réussi à ajouter de nombreuses choses qui avaient été supprimées de Hordes à un moment ou à un autre ; comme la profession de chaman par exemple, ou les goules automatiques. Toutes ces choses sont maintenant de retour dans MyHordes, sélectionnables comme options pour les villes privées ou événementielles._

**La publication du code source de la Motion Twin vous a-t-elle aidé ou les gains ont-ils été anecdotiques ?**

_Cela a été d'une grande aide. Nous avons pu obtenir de nombreuses valeurs, comme les chances de trouver les objets, pour lesquelles nous n'avions auparavant que des estimations de la communauté. De plus, nous avons pu obtenir de nombreux textes originaux que nous aurions dû extraire des jeux originaux._

**Il vous manque encore des données de gameplay ? J'ai entendu dire que la propagation des zombies n'est pas encore équilibrée ? Est-ce que vous pensez que MT va nous donner plus de code source ?**

_Nous avons des données sur la propagation des zombies dans le code source que MT nous a donné, mais c'est étrange. Nous avons réussi à faire fonctionner le code tel quel, sans aucune modification, et il produit des résultats qui ne ressemblent en rien à la propagation des zombies que nous connaissons. Nous pensons qu'il s'agit soit d'un nouvel algorithme de propagation qui n'a jamais été activé, soit d'une sorte d'expérience. Donc oui, l'algorithme réel de propagation des zombies n'est toujours pas disponible pour nous._

_Il y a aussi quelques petites choses qui manquent, par exemple en ce qui concerne les taux de chances des objets dans les ruines explorables._

_Cependant, pour autant que je me souvienne, Skool a mentionné quelque part que c'était tout le code qu'ils pouvaient partager. J'ai donc peu d'espoir que le reste du code soit mis à notre disposition._

_Je suppose que nous devrons faire de notre mieux avec ce que nous avons._

**Je pense que nous sommes tous d'accord pour dire que c'est un succès. Ton travail sur MH est impressionnant. Comment as-tu fait pour livrer une mise à jour tous les lundis ? En regardant le graphique git, nous pouvons voir que vous travaillez sur le projet presque tous les week-ends ?**
![Interview - Git Graph of BrainBox](./interview_brain.png)

_Je passe généralement du temps avec mes amis ou ma famille le soir, donc le week-end, je peux travailler pendant la journée. Il y a bien sûr quelques exceptions, comme les vacances. Mais même dans ce cas, la plupart du temps, j'arrive à travailler au moins un jour par week-end, ce qui est suffisant pour sortir une mise à jour avec des corrections importantes._

**Vous êtes aussi développeur web à plein temps ? Vous n'en avez jamais marre de faire du code toute la semaine et les week-ends ?**

_Pas vraiment, je suppose que je réponds au stéréotype de l'Allemand obsédé par le travail. J'adore coder et j'aime le faire, avec ou sans salaire._

**Ahah c'est bon à entendre. Et est-ce que MH vous a aidé à progresser dans vos compétences pour votre travail ?**

_Pour être parfaitement honnête, MyHordes m'a aidé à obtenir ce travail en premier lieu. Lorsque j'ai postulé, je l'ai cité comme référence. J'ai travaillé comme chercheur universitaire auparavant, principalement en C++ et OpenGL. Bien que j'aie fait beaucoup de projets web à côté, aucun d'entre eux n'était vraiment approprié pour être utilisé comme référence. Je pense que si je n'avais pas eu MyHordes pour montrer mes compétences en développement web, il aurait été plus difficile d'atteindre mon poste actuel._

_Mais en ce qui concerne les compétences, c'est en fait l'inverse. Mon travail me donne de nombreuses occasions d'apprendre et d'expérimenter de nouvelles choses que je peux ensuite intégrer dans MyHordes._

**Wow, c'est une sacrée histoire ! Et pour l'avenir de MH, avez-vous une idée du calendrier des saisons ou seront-elles publiées quand elles seront prêtes ? Maintenant que la saison 15 est en ligne, nous attendons déjà la saison 16 et ses nouvelles fonctionnalités !**

_Nous prévoyons de suivre un calendrier approximatif, même si les intervalles ne sont pas encore fixés. Bien sûr, nous ne pouvons sortir une nouvelle saison que lorsque son développement est terminé, et comme nous sommes tous bénévoles, il est un peu difficile de faire des estimations précises._

_Mais rassurez-vous, il y aura d'autres saisons, et nous les publierons aussi régulièrement que possible._

**C'est rassurant d'entendre des choses comme ça. Je pense avoir demandé tout ce que j'ai dans mes manches, est-ce que tu veux ajouter quelque chose ? Peut-être un indice sur une future fonctionnalité ?**

_Je voudrais donner un coup de chapeau au reste de l'équipe: Dylan, Nayr, Ludofloria et Adri, ainsi que tous les autres qui ont contribué à MyHordes ! Je n'aurais pas pu le faire sans eux !_

_Et comme un indice... eh bien, disons que nous avons trouvé des traces de contenu inutilisé ou des idées futures pour Hordes qui n'ont jamais été mises en œuvre comme...._

**Hmmm il semble que cette interview soit terminée parce qu'un zombie a mangé Brainbox. Peut-être qu'ils ne voulaient pas que Brain divulgue leurs plans...**
