use clap::Parser;
use xtask::cmd::Args;

#[tokio::main]
async fn main() {
  let args: Args = Args::parse();

  let res = xtask::cmd::run(&args).await;

  match res {
    Ok(_) => std::process::exit(0),
    Err(_) => res.unwrap(),
  }
}
