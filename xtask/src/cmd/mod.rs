use clap::Parser;
use std::collections::BTreeMap;
use std::path::Path;
use std::path::PathBuf;

mod codegen;
mod release;
mod structure;

pub struct CliContext<'a, Args, Out = std::io::Stdout> {
  pub args: &'a Args,
  pub env: &'a BTreeMap<String, String>,
  pub working_dir: &'a Path,
  pub out: Out,
}

impl<'a, Args, Out> CliContext<'a, Args, Out> {
  pub fn map_out<F, R>(self, mut f: F) -> CliContext<'a, Args, R>
  where
    F: FnMut(Out) -> R,
  {
    CliContext {
      args: self.args,
      env: self.env,
      working_dir: self.working_dir,
      out: f(self.out),
    }
  }
}

#[derive(Debug, Parser)]
#[clap(author = "Eternaltwin", name = "eternaltwin-xtask", version)]
pub struct Args {
  #[clap(subcommand)]
  command: CliCommand,
}

#[derive(Debug, Parser)]
pub enum CliCommand {
  /// Run the `codegen` subcommand
  #[clap(name = "codegen")]
  Codegen(codegen::Args),
  /// Run the `dns` subcommand
  #[clap(name = "dns")]
  Dns(crate::dns::Args),
  /// Run the `docs` subcommand
  #[clap(name = "docs")]
  Docs(crate::docs::Args),
  /// Generate precompiled packages
  #[clap(name = "precompile")]
  Precompile(crate::precompile::Args),
  /// Publish the CLI and all its dependencies
  #[clap(name = "publish")]
  Publish(crate::publish::Args),
  /// Create a new release
  #[clap(name = "release")]
  Release(release::Args),
  /// Analyze the project structure
  #[clap(name = "backend")]
  Structure(structure::Args),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("codegen error")]
  Codegen(#[from] codegen::Error),
  #[error("dns error")]
  Dns(#[from] crate::dns::Error),
  #[error("docs error")]
  Docs(#[from] crate::docs::Error),
  #[error("precompile error")]
  Precompile(#[from] crate::precompile::Error),
  #[error("codegen error")]
  Publish(#[from] crate::publish::Error),
  #[error("publish error")]
  Release(#[from] release::Error),
  #[error("structure error")]
  Structure(#[from] structure::Error),
}

pub async fn run(args: &Args) -> Result<(), Error> {
  let env: BTreeMap<String, String> = std::env::vars().collect();
  let env = &env;
  let working_dir: PathBuf = std::env::current_dir().expect("failed to retrieve current working directory");
  let working_dir = working_dir.as_path();
  let out = std::io::stdout();
  match &args.command {
    CliCommand::Codegen(ref args) => {
      codegen::run(CliContext {
        args,
        env,
        working_dir,
        out,
      })
      .await?
    }
    CliCommand::Dns(ref args) => crate::dns::run(args)?,
    CliCommand::Docs(ref args) => crate::docs::run(args)?,
    CliCommand::Precompile(ref args) => crate::precompile::run(args)?,
    CliCommand::Publish(ref args) => crate::publish::run(args).await?,
    CliCommand::Release(ref args) => release::run(args).await?,
    CliCommand::Structure(ref args) => {
      structure::run(CliContext {
        args,
        env,
        working_dir,
        out,
      })
      .await?
    }
  }
  Ok(())
}
