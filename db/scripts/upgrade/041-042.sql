-- `FALSE`: not accepted, `TRUE`: accepted
-- If there is no row for a given point in time, it means that the user never replied (we assume `FALSE`)
CREATE TABLE tos_answer
(
  "period"  PERIOD_LOWER NOT NULL,
  "user_id" USER_ID      NOT NULL,
  "answer"  BOOLEAN      NOT NULL DEFAULT FALSE,
  PRIMARY KEY (period, user_id),
  EXCLUDE USING gist (user_id WITH =, period WITH &&),
  CONSTRAINT tos_answer__users__fk FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
