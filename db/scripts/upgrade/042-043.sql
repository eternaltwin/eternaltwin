alter table users
  add column enabled period_multirange null;

update users set enabled = period_multirange(period);

alter table users
  alter column enabled set not null,
  add constraint users__enabled_check check (lower(enabled) is not distinct from lower(period) and upper(enabled) is not distinct from upper(period));

CREATE DOMAIN user_event_id AS UUID;
CREATE TYPE user_event_kind AS ENUM ('Enabled', 'Disabled', 'DisplayNameChanged', 'UsernameChanged');

create table user_event (
  -- event id, primary key (uuidv4, should be replaced by a v7 id before it's publicly exposed)
  "user_event_id4" user_event_id primary key not null,
  -- time when the event occurred
  "time" instant not null,
  -- user for which the event happened
  "target" user_id not null,
  -- user who performed the action that caused the event
  "actor" user_id null,
  -- otlp span for the action that caused the event
  "span" opentelemetry_trace_parent_bytes null,
  -- event payload kind (event type)
  "payload_kind" user_event_kind not null,
  -- event payload version, for payload data migration
  "payload_version" u8 not null,
  -- encrypted event payload
  "payload" bytea not null,
  constraint user_event__target__fk foreign key (target) references users(user_id) on delete cascade on update cascade,
  constraint user_event__actor__fk foreign key (actor) references users(user_id) on delete set null on update cascade
);
