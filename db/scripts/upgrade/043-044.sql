alter type user_event_kind rename to user_event_kind_old;
create type user_event_kind as enum ('UserEnabled', 'UserDisabled');
alter table user_event alter column "payload_kind" type "user_event_kind" USING payload_kind::text::user_event_kind;
drop type user_event_kind_old;
