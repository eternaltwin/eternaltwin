ALTER TABLE hammerfest_profiles
  ALTER COLUMN best_level TYPE i32 USING best_level::integer;

CREATE TABLE hammerfest_evni_user
(
  period             PERIOD_LOWER       NOT NULL,
  retrieved_at       INSTANT_SET        NOT NULL,
  hammerfest_server  HAMMERFEST_SERVER  NOT NULL,
  hammerfest_user_id HAMMERFEST_USER_ID NOT NULL,
--
  is_evni            BOOLEAN            NOT NULL CHECK (is_evni),
  PRIMARY KEY (period, hammerfest_server, hammerfest_user_id),
  EXCLUDE USING gist (hammerfest_server WITH =, hammerfest_user_id WITH =, period WITH &&)
);

ALTER TABLE task
  ADD COLUMN starvation I32 NOT NULL DEFAULT 0;
