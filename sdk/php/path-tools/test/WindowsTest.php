<?php declare(strict_types=1);

use Eternaltwin\PathTools\Windows;
use PHPUnit\Framework\TestCase;

final class WindowsTest extends TestCase {
  public function testJoin(): void {
    $this->assertEquals("x/b/c.js", Windows::join(".", "x/b", "..", "/b/c.js"));
    $this->assertEquals(".", Windows::join(""));
    $this->assertEquals("/x/b/c.js", Windows::join("/.", "x/b", "..", "/b/c.js"));
    $this->assertEquals("/bar", Windows::join("/foo", "../../../bar"));
    $this->assertEquals("../../bar", Windows::join("foo", "../../../bar"));
    $this->assertEquals("../../bar", Windows::join("foo/", "../../../bar"));
    $this->assertEquals("../bar", Windows::join("foo/x", "../../../bar"));
    $this->assertEquals("foo/x/bar", Windows::join("foo/x", "./bar"));
    $this->assertEquals("foo/x/bar", Windows::join("foo/x/", "./bar"));
    $this->assertEquals("foo/x/bar", Windows::join("foo/x/", ".", "bar"));
    $this->assertEquals("./", Windows::join("./", "./"));
    $this->assertEquals("./", Windows::join(".", "./"));
    $this->assertEquals(".", Windows::join(".", ".", "."));
    $this->assertEquals(".", Windows::join(".", "./", "."));
    $this->assertEquals(".", Windows::join(".", "/./", "."));
    $this->assertEquals(".", Windows::join(".", "/////./", "."));
    $this->assertEquals(".", Windows::join("."));
    $this->assertEquals(".", Windows::join("", "."));
    $this->assertEquals("foo", Windows::join("", "foo"));
    $this->assertEquals("foo/bar", Windows::join("foo", "/bar"));
    $this->assertEquals("/foo", Windows::join("", "/foo"));
    $this->assertEquals("/foo", Windows::join("", "", "/foo"));
    $this->assertEquals("foo", Windows::join("", "", "foo"));
    $this->assertEquals("foo", Windows::join("foo", ""));
    $this->assertEquals("foo/", Windows::join("foo/", ""));
    $this->assertEquals("foo/bar", Windows::join("foo", "", "/bar"));
    $this->assertEquals("../foo", Windows::join("./", "..", "/foo"));
    $this->assertEquals("../../foo", Windows::join("./", "..", "..", "/foo"));
    $this->assertEquals("../../foo", Windows::join(".", "..", "..", "/foo"));
    $this->assertEquals("../../foo", Windows::join("", "..", "..", "/foo"));
    $this->assertEquals("/", Windows::join("/"));
    $this->assertEquals("/", Windows::join("/", "."));
    $this->assertEquals("/", Windows::join("/", ".."));
    $this->assertEquals("/", Windows::join("/", "..", ".."));
    $this->assertEquals(".", Windows::join(""));
    $this->assertEquals(".", Windows::join("", ""));
    $this->assertEquals(" /foo", Windows::join(" /foo"));
    $this->assertEquals(" /foo", Windows::join(" ", "foo"));
    $this->assertEquals(" ", Windows::join(" ", "."));
    $this->assertEquals(" /", Windows::join(" ", "/"));
    $this->assertEquals(" ", Windows::join(" ", ""));
    $this->assertEquals("/foo", Windows::join("/", "foo"));
    $this->assertEquals("/foo", Windows::join("/", "/foo"));
    $this->assertEquals("/foo", Windows::join("/", "//foo"));
    $this->assertEquals("/foo", Windows::join("/", "", "/foo"));
    $this->assertEquals("/foo", Windows::join("", "/", "foo"));
    $this->assertEquals("/foo", Windows::join("", "/", "/foo"));
    // Windows-specific tests
    $this->assertEquals("//foo/bar", Windows::join("//foo/bar"));
    $this->assertEquals("//foo/bar", Windows::join("\\/foo/bar"));
    $this->assertEquals("//foo/bar", Windows::join("\\\\foo/bar"));
    $this->assertEquals("//foo/bar", Windows::join("//foo", "bar"));
    $this->assertEquals("//foo/bar", Windows::join("//foo/", "bar"));
    $this->assertEquals("//foo/bar", Windows::join("//foo", "/bar"));
    $this->assertEquals("//foo/bar", Windows::join("//foo", "", "bar"));
    $this->assertEquals("//foo/bar", Windows::join("//foo/", "", "bar"));
    $this->assertEquals("//foo/bar", Windows::join("//foo/", "", "/bar"));
    $this->assertEquals("//foo/bar", Windows::join("", "//foo", "bar"));
    $this->assertEquals("//foo/bar", Windows::join("", "//foo/", "bar"));
    $this->assertEquals("//foo/bar", Windows::join("", "//foo/", "/bar"));
    $this->assertEquals("/foo/bar", Windows::join("\\", "foo/bar"));
    $this->assertEquals("/foo/bar", Windows::join("\\", "/foo/bar"));
    $this->assertEquals("/foo/bar", Windows::join("", "/", "/foo/bar"));
    $this->assertEquals("/foo/bar", Windows::join("//", "foo/bar"));
    $this->assertEquals("/foo/bar", Windows::join("//", "/foo/bar"));
    $this->assertEquals("/foo/bar", Windows::join("\\\\", "/", "/foo/bar"));
    $this->assertEquals("/", Windows::join("//"));
    $this->assertEquals("//foo/", Windows::join("//foo"));
    $this->assertEquals("//foo/", Windows::join("//foo/"));
    $this->assertEquals("//foo/", Windows::join("//foo", "/"));
    $this->assertEquals("//foo/", Windows::join("//foo", "", "/"));
    $this->assertEquals("/foo/bar", Windows::join("///foo/bar"));
    $this->assertEquals("/foo/bar", Windows::join("////foo", "bar"));
    $this->assertEquals("/foo/bar", Windows::join("\\\\\\/foo/bar"));
    $this->assertEquals("c:", Windows::join("c:"));
    $this->assertEquals("c:.", Windows::join("c:."));
    $this->assertEquals("c:", Windows::join("c:", ""));
    $this->assertEquals("c:", Windows::join("", "c:"));
    $this->assertEquals("c:./", Windows::join("c:.", "/"));
    $this->assertEquals("c:./file", Windows::join("c:.", "file"));
    $this->assertEquals("c:/", Windows::join("c:", "/"));
    $this->assertEquals("c:/file", Windows::join("c:", "file"));
  }

  public function testRelative(): void {
    $this->assertEquals("d:/games", Windows::relative("c:/blah\\blah", "d:/games"));
    $this->assertEquals("..", Windows::relative("c:/aaaa/bbbb", "c:/aaaa"));
    $this->assertEquals("../../cccc", Windows::relative("c:/aaaa/bbbb", "c:/cccc"));
    $this->assertEquals(".", Windows::relative("c:/aaaa/bbbb", "c:/aaaa/bbbb"));
    $this->assertEquals("../cccc", Windows::relative("c:/aaaa/bbbb", "c:/aaaa/cccc"));
    $this->assertEquals("./cccc", Windows::relative("c:/aaaa/", "c:/aaaa/cccc"));
    $this->assertEquals("./aaaa/bbbb", Windows::relative("c:/", "c:\\aaaa\\bbbb"));
    $this->assertEquals("d:/", Windows::relative("c:/aaaa/bbbb", "d:\\"));
    $this->assertEquals("../../aaaa/bbbb", Windows::relative("c:/AaAa/bbbb", "c:/aaaa/bbbb"));
    $this->assertEquals("../aaaa/cccc", Windows::relative("c:/aaaaa/", "c:/aaaa/cccc"));
    $this->assertEquals("../../../..", Windows::relative("C:\\foo\\bar\\baz\\quux", "C:\\"));
    $this->assertEquals("./bar/package.json", Windows::relative("C:\\foo\\test", "C:\\foo\\test\\bar\\package.json"));
    $this->assertEquals("../baz", Windows::relative("C:\\foo\\bar\\baz-quux", "C:\\foo\\bar\\baz"));
    $this->assertEquals("../baz-quux", Windows::relative("C:\\foo\\bar\\baz", "C:\\foo\\bar\\baz-quux"));
    $this->assertEquals("./baz", Windows::relative("\\\\foo\\bar", "\\\\foo\\bar\\baz"));
    $this->assertEquals("..", Windows::relative("\\\\foo\\bar\\baz", "\\\\foo\\bar"));
    $this->assertEquals("../baz", Windows::relative("\\\\foo\\bar\\baz-quux", "\\\\foo\\bar\\baz"));
    $this->assertEquals("../baz-quux", Windows::relative("\\\\foo\\bar\\baz", "\\\\foo\\bar\\baz-quux"));
    $this->assertEquals("../baz", Windows::relative("C:\\baz-quux", "C:\\baz"));
    $this->assertEquals("../baz-quux", Windows::relative("C:\\baz", "C:\\baz-quux"));
    $this->assertEquals("../baz", Windows::relative("\\\\foo\\baz-quux", "\\\\foo\\baz"));
    $this->assertEquals("../baz-quux", Windows::relative("\\\\foo\\baz", "\\\\foo\\baz-quux"));
    $this->assertEquals("//foo/bar/baz", Windows::relative("C:\\baz", "\\\\foo\\bar\\baz"));
    $this->assertEquals("C:/baz", Windows::relative("\\\\foo\\bar\\baz", "C:\\baz"));
  }

  public function testIsAbsolute(): void {
    $this->assertTrue(Windows::isAbsolute("/"));
    $this->assertTrue(Windows::isAbsolute("/foo"));
    $this->assertTrue(Windows::isAbsolute("/foo/bar"));
    $this->assertTrue(Windows::isAbsolute("/."));
    $this->assertFalse(Windows::isAbsolute(""));
    $this->assertFalse(Windows::isAbsolute("."));
    $this->assertTrue(Windows::isAbsolute("\\"));
    $this->assertFalse(Windows::isAbsolute("foo"));
    $this->assertFalse(Windows::isAbsolute("foo/bar"));
    $this->assertTrue(Windows::isAbsolute("C:/"));
    $this->assertTrue(Windows::isAbsolute("C:\\"));
    $this->assertTrue(Windows::isAbsolute("C:\\Windows"));
  }
}
