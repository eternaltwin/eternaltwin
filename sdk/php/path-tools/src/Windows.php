<?php declare(strict_types=1);

namespace Eternaltwin\PathTools ;

use Eternaltwin\PathTools\PathTools;

final class Windows extends PathTools {
  public const SEPARATOR = "/";
  private const UPPER_A = 0x41;
  private const UPPER_Z = 0x5a;
  private const LOWER_A = 0x61;
  private const LOWER_Z = 0x7a;

  public static function join(string ...$paths): string {
    return self::normalize(self::concat(...$paths));
  }

  public static function relative(string $from, string $to): string {
    if (!self::isAbsolute($from) || !self::isAbsolute($to)) {
      throw new \Error("Arguements to `PathTools::relative` must be absolute, got: " . $from . " and " . $to);
    }
    $from = self::normalize($from);
    $to = self::normalize($to);
    if ($from == $to) {
      return ".";
    }
    $fromPrefix = WindowsPrefix::get($from);
    $toPrefix = WindowsPrefix::get($to);
    if ($toPrefix->prefix !== $fromPrefix->prefix) {
      return $to;
    }
    $fromParts = explode(self::SEPARATOR, substr($from, strlen($fromPrefix->prefix)));
    $toParts = explode(self::SEPARATOR, substr($to, strlen($toPrefix->prefix)));
    $fromParts = array_values(array_filter($fromParts, function($p) { return !empty($p); }));
    $toParts = array_values(array_filter($toParts, function($p) { return !empty($p); }));
    $common = 0;
    $fromLen = count($fromParts);
    $toLen = count($toParts);
    while ($common < $fromLen && $common < $toLen && $fromParts[$common] === $toParts[$common]) {
      $common += 1;
    }
    $parentCount = $fromLen - $common;
    $prefix = $parentCount > 0 ? str_repeat("../", $parentCount) : "./";
    if ($common === $toLen) {
      $prefix = substr($prefix, 0, strlen($prefix) - 1);
    }
    return $prefix . implode(self::SEPARATOR, array_slice($toParts, $common));
  }

  public static function concat(string ...$paths): string {
    if (empty($paths)) {
      return ".";
    }
    $result = [];
    $first = null;
    foreach ($paths as $path) {
      if (!empty($path)) {
        if (empty($result)) {
          $first = $path;
        }
        $result[] = $path;
      }
    }
    if (empty($result)) {
      return ".";
    }
    $joined = implode(self::SEPARATOR, $result);
    if (!(strlen($first) >= 2 && self::isSeparator($first[0]) && self::isSeparator($first[1]))) {
      $slashEnd = 0;
      $l = strlen($joined);
      while ($slashEnd < $l && self::isSeparator($joined[$slashEnd])) {
        $slashEnd += 1;
      }
      if ($slashEnd >= 2) {
        $joined = self::SEPARATOR . substr($joined, $slashEnd, null);
      }
    }
    return $joined;
  }

  public static function normalize(string $path): string {
    if (empty($path)) {
      return ".";
    }
    $prefix = WindowsPrefix::get($path);
    $tail = $prefix !== null ? substr($path, strlen($prefix->prefix), null) : $path;

    $isAbsolute = $prefix !== null;
    $trailingSeparator = str_ends_with($path, self::SEPARATOR) || str_ends_with($path, "\\");
    $normalized = self::normalizeInner($tail, self::SEPARATOR, !$isAbsolute, function($chr) { return self::isSeparator($chr); });
    if (empty($normalized)) {
      if ($isAbsolute) {
        return $prefix->normalized();
      } elseif ($trailingSeparator) {
        return "./";
      } else {
        return ".";
      }
    }
    if ($isAbsolute) {
      $normalized = $prefix->normalized() . $normalized;
    }
    if ($trailingSeparator) {
      $normalized = $normalized . self::SEPARATOR;
    }
    return $normalized;
  }

  public static function isAbsolute(string $path): bool {
    return WindowsPrefix::get($path) !== null;
  }

  public static function isSeparator(string $chr): bool {
    return $chr === "/" || $chr === "\\";
  }

  public static function isAsciiLetter(string $chr): bool {
    $code = ord($chr);
    return (self::UPPER_A <= $code && $code <= self::UPPER_Z) || (self::LOWER_A <= $code && $code <= self::LOWER_Z);
  }
}
