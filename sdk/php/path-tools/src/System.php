<?php declare(strict_types=1);

namespace Eternaltwin\PathTools;

use Eternaltwin\PathTools\PathTools;

final class System extends PathTools {
  public static function join(string ...$paths): string {
    if (PHP_OS_FAMILY === "Windows") {
      return Windows::join(...$paths);
    } else {
      return Posix::join(...$paths);
    }
  }

  public static function isAbsolute(string $path): bool {
    if (PHP_OS_FAMILY === "Windows") {
      return Windows::isAbsolute($path);
    } else {
      return Posix::isAbsolute($path);
    }
  }

  public static function relative(string $from, string $to): string {
    if (PHP_OS_FAMILY === "Windows") {
      return Windows::relative($from, $to);
    } else {
      return Posix::relative($from, $to);
    }
  }
}
