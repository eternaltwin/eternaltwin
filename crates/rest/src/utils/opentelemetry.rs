use opentelemetry::logs::AnyValue;
use opentelemetry::{Array, StringValue, Value};
use opentelemetry::{Key, KeyValue};
use std::collections::HashMap;
use std::time::{Duration, SystemTime};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum KeyValueListFromProtobufError {
  #[error("failed to read protobuf key-value list due to missing value at index {0}")]
  MissingValue(usize),
  #[error("failed to read protobuf key-value list due to invalid value at index {1}")]
  InvalidValue(#[source] ValueFromProtobufError, usize),
}

pub(crate) fn kv_list_from_pb(
  pb_kv_list: Vec<opentelemetry_proto::tonic::common::v1::KeyValue>,
) -> Result<Vec<KeyValue>, KeyValueListFromProtobufError> {
  let mut kv_list: Vec<KeyValue> = Vec::with_capacity(pb_kv_list.len());
  for (i, pb_kv) in pb_kv_list.into_iter().enumerate() {
    let key = pb_kv.key;
    let value = pb_kv.value.ok_or(KeyValueListFromProtobufError::MissingValue(i))?;
    let value = value_from_pb(value).map_err(|e| KeyValueListFromProtobufError::InvalidValue(e, i))?;
    kv_list.push(KeyValue::new(key, value));
  }
  Ok(kv_list)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("failed to read protobuf value")]
pub enum ValueFromProtobufError {
  #[error("missing internal `value` field")]
  Empty,
  #[error("invalid array value")]
  Array(#[from] ArrayFromProtobufError),
  #[error("invalid composite value")]
  Composite,
}

pub(crate) fn value_from_pb(
  pb_value: opentelemetry_proto::tonic::common::v1::AnyValue,
) -> Result<Value, ValueFromProtobufError> {
  let pb_value = match pb_value.value {
    Some(pbv) => pbv,
    None => return Err(ValueFromProtobufError::Empty),
  };

  use opentelemetry_proto::tonic::common::v1::any_value::Value as PbValue;
  match pb_value {
    PbValue::StringValue(v) => Ok(Value::String(StringValue::from(v))),
    PbValue::BoolValue(v) => Ok(Value::Bool(v)),
    PbValue::IntValue(v) => Ok(Value::I64(v)),
    PbValue::DoubleValue(v) => Ok(Value::F64(v)),
    PbValue::ArrayValue(v) => Ok(Value::Array(array_from_pb(v)?)),
    PbValue::KvlistValue(..) | PbValue::BytesValue(..) => Err(ValueFromProtobufError::Composite),
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("failed to read protobuf AnyValue")]
pub enum AnyValueFromProtobufError {
  #[error("missing internal `value` field")]
  Empty,
  #[error("composite value size is too large")]
  Size,
  #[error("invalid array value at index {0}")]
  ArrayInvalid(#[source] Box<Self>, u64),
  #[error("empty kv list value at index {0}")]
  KvListEmpty(u64),
  #[error("invalid kv list value at index {0}")]
  KvListInvalid(#[source] Box<Self>, u64),
}

pub(crate) fn any_value_from_pb(
  pb_value: opentelemetry_proto::tonic::common::v1::AnyValue,
) -> Result<AnyValue, AnyValueFromProtobufError> {
  let pb_value = match pb_value.value {
    Some(pbv) => pbv,
    None => return Err(AnyValueFromProtobufError::Empty),
  };

  use opentelemetry_proto::tonic::common::v1::any_value::Value as PbValue;
  match pb_value {
    PbValue::StringValue(v) => Ok(AnyValue::String(StringValue::from(v))),
    PbValue::BoolValue(v) => Ok(AnyValue::Boolean(v)),
    PbValue::IntValue(v) => Ok(AnyValue::Int(v)),
    PbValue::DoubleValue(v) => Ok(AnyValue::Double(v)),
    PbValue::ArrayValue(v) => Ok(AnyValue::ListAny(Box::new({
      v.values
        .into_iter()
        .enumerate()
        .map(|(i, v)| {
          u64::try_from(i)
            .map_err(|_| AnyValueFromProtobufError::Size)
            .and_then(|i| any_value_from_pb(v).map_err(|e| AnyValueFromProtobufError::ArrayInvalid(Box::new(e), i)))
        })
        .collect::<Result<Vec<_>, AnyValueFromProtobufError>>()?
    }))),
    PbValue::KvlistValue(v) => Ok(AnyValue::Map(Box::new({
      v.values
        .into_iter()
        .enumerate()
        .map(|(i, kv)| -> Result<(Key, AnyValue), AnyValueFromProtobufError> {
          let i = u64::try_from(i).map_err(|_| AnyValueFromProtobufError::Size)?;
          let v = kv.value.ok_or(AnyValueFromProtobufError::KvListEmpty(i))?;
          any_value_from_pb(v)
            .map(|v| (Key::new(kv.key), v))
            .map_err(|e| AnyValueFromProtobufError::KvListInvalid(Box::new(e), i))
        })
        .collect::<Result<HashMap<_, _>, AnyValueFromProtobufError>>()?
    }))),
    PbValue::BytesValue(v) => Ok(AnyValue::Bytes(Box::new(v))),
  }

  // let value = value_from_pb(pb_value)?;
  // Ok(match value {
  //   Value::Bool(x) => ,
  //   Value::I64(x) => AnyValue::Int(x),
  //   Value::F64(x) => AnyValue::Double(x),
  //   Value::String(s) => AnyValue::String(s),
  //   Value::Array(a) => AnyValue::ListAny(Box::new({
  //     let vec = match a {
  //       Array::String()
  //     }
  //   })),
  // })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum ArrayFromProtobufError {
  #[error("failed to read array value")]
  ArrayValue(#[from] PrimitiveValueFromProtobufError),
  #[error("invalid array due to mixed primitive value types")]
  MixedArray,
}

/// Decode a protobuf array
///
/// If the array is empty, it defaults to be typed as `Array::String`
fn array_from_pb(
  pb_array: opentelemetry_proto::tonic::common::v1::ArrayValue,
) -> Result<Array, ArrayFromProtobufError> {
  let size = pb_array.values.len();
  let mut items = pb_array.values.into_iter();
  match items.next() {
    None => {
      // default to a string array if the array is empty
      Ok(Array::String(Vec::new()))
    }
    Some(first) => {
      let first = primitive_value_from_pb(first)?;
      match first {
        PrimitiveValue::String(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::String(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::String(result))
        }
        PrimitiveValue::Bool(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::Bool(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::Bool(result))
        }
        PrimitiveValue::I64(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::I64(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::I64(result))
        }
        PrimitiveValue::F64(v) => {
          let mut result = Vec::with_capacity(size);
          result.push(v);
          for v in items {
            match primitive_value_from_pb(v)? {
              PrimitiveValue::F64(v) => result.push(v),
              _ => return Err(ArrayFromProtobufError::MixedArray),
            }
          }
          Ok(Array::F64(result))
        }
      }
    }
  }
}

pub enum PrimitiveValue {
  String(StringValue),
  Bool(bool),
  I64(i64),
  F64(f64),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("non-primitive protobuf value forbidden inside array")]
pub struct PrimitiveValueFromProtobufError;

fn primitive_value_from_pb(
  pb_value: opentelemetry_proto::tonic::common::v1::AnyValue,
) -> Result<PrimitiveValue, PrimitiveValueFromProtobufError> {
  let pb_value = match pb_value.value {
    Some(pbv) => pbv,
    None => return Err(PrimitiveValueFromProtobufError),
  };

  use opentelemetry_proto::tonic::common::v1::any_value::Value as PbValue;
  match pb_value {
    PbValue::StringValue(v) => Ok(PrimitiveValue::String(StringValue::from(v))),
    PbValue::BoolValue(v) => Ok(PrimitiveValue::Bool(v)),
    PbValue::IntValue(v) => Ok(PrimitiveValue::I64(v)),
    PbValue::DoubleValue(v) => Ok(PrimitiveValue::F64(v)),
    PbValue::ArrayValue(..) | PbValue::KvlistValue(..) | PbValue::BytesValue(..) => {
      Err(PrimitiveValueFromProtobufError)
    }
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("overflow when computing system time from UNIX nanosecond timestamp")]
pub struct SystemTimeFromUnixNanoError;

pub(crate) fn system_time_from_nanos(nanos: u64) -> Result<SystemTime, SystemTimeFromUnixNanoError> {
  SystemTime::UNIX_EPOCH
    .checked_add(Duration::from_nanos(nanos))
    .ok_or(SystemTimeFromUnixNanoError)
}
