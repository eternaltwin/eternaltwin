use axum::routing::get;
use axum::{Json, Router};
use eternaltwin_core::core::Instant;
use serde_json::{json, Value as JsonValue};

pub fn router() -> Router<()> {
  Router::new().route("/releases", get(get_releases))
}

async fn get_releases() -> Json<JsonValue> {
  Json(json!({
    "latest": {
      "version": "0.6.6",
      "time": Instant::ymd_hms(2023, 2, 10, 23, 0, 0),
    },
  }))
}
