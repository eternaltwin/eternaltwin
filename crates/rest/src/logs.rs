use crate::extract::Extractor;
use crate::utils::opentelemetry::{
  any_value_from_pb, kv_list_from_pb, system_time_from_nanos, AnyValueFromProtobufError, KeyValueListFromProtobufError,
  SystemTimeFromUnixNanoError,
};
use crate::utils::prost013::Prost013;
use crate::EternaltwinSystem;
use axum::http::{header, HeaderMap, HeaderName, HeaderValue, StatusCode};
use axum::response::{IntoResponse, Response};
use axum::routing::post;
use axum::{Extension, Router};
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use opentelemetry::logs::{LogRecord as _, Severity};
use opentelemetry::trace::{SpanContext, SpanId, TraceId};
use opentelemetry::{InstrumentationLibrary, InstrumentationLibraryBuilder, Key, StringValue, Value};
use opentelemetry_proto::tonic::collector::trace::v1::ExportTraceServiceResponse;
use opentelemetry_proto::tonic::logs::v1::{ResourceLogs, ScopeLogs};
use opentelemetry_sdk::logs::{LogData, LogRecord, TraceContext};
use opentelemetry_sdk::Resource;
use std::sync::LazyLock;

pub fn router() -> Router<()> {
  Router::new().route("/", post(create_logs))
}

#[derive(Debug, thiserror::Error)]
enum CreateLogsError {
  #[error("forbidden: {0}")]
  Forbidden(&'static str),
  #[error("failed to read provided input data, maybe due to invalid or unsupported format")]
  Read(#[from] LogDataListFromProtobufResourceLogsError),
  #[error("invalid span, all span resources must have attributes service.name={0:?}, deployment.environment={1:?}")]
  InvalidSpan(String, String),
  #[error("internal error")]
  Internal(#[from] WeakError),
}

impl IntoResponse for CreateLogsError {
  fn into_response(self) -> Response {
    let (http_status, pb_status) = match &self {
      Self::Forbidden(..) => (StatusCode::FORBIDDEN, tonic::Code::PermissionDenied),
      Self::InvalidSpan(..) | Self::Read(..) => (StatusCode::UNPROCESSABLE_ENTITY, tonic::Code::InvalidArgument),
      Self::Internal(..) => (StatusCode::INTERNAL_SERVER_ERROR, tonic::Code::Internal),
    };
    let grpc_status = tonic::Status::new(pb_status, DisplayErrorChain(&self).to_string());

    let mut headers = HeaderMap::new();
    headers.insert(
      header::CONTENT_TYPE,
      header::HeaderValue::from_static("application/grpc"),
    );
    // tonic uses an old `http` version, so we need to handle compat here to emit the proper headers for the status
    let mut legacy_headers = HeaderMap::new();
    grpc_status
      .add_header(&mut legacy_headers)
      .expect("adding grpc headers always succeeds");
    for (key, value) in legacy_headers.iter() {
      headers.insert(
        HeaderName::from_bytes(key.as_str().as_bytes()).expect("converting to a header name succeeds"),
        HeaderValue::from_bytes(value.as_bytes()).expect("converting to a header value succeeds"),
      );
    }
    (http_status, headers).into_response()
  }
}

// todo: assert media type is `"application/x-protobuf"` before parsing
// todo: support JSON format
async fn create_logs(
  Extension(api): Extension<EternaltwinSystem>,
  acx: Extractor<AuthContext>,
  Prost013(req): Prost013<opentelemetry_proto::tonic::collector::logs::v1::ExportLogsServiceRequest>,
) -> Result<Prost013<ExportTraceServiceResponse>, CreateLogsError> {
  let acx = match acx.value() {
    AuthContext::OauthClient(acx) => acx,
    _ => return Err(CreateLogsError::Forbidden("must provide OAuth client authentication")),
  };
  let client_key = match acx.client.key.as_ref() {
    Some(key) => key,
    None => {
      return Err(CreateLogsError::Forbidden(
        "oauth client must have a register human-readable key such as `game_channel@clients`",
      ))
    }
  };
  let (app, channel) = client_key.parts();
  let channel = channel.unwrap_or("production");

  for resource_logs in req.resource_logs.into_iter() {
    let (resource, log_list) = log_data_list_from_protobuf_resource_logs(resource_logs)?;
    if !check_resource_permission(app, channel, &resource) {
      return Err(CreateLogsError::InvalidSpan(app.to_string(), channel.to_string()));
    }
    api.opentelemetry_backend.write_log_batch(&resource, &log_list);
  }

  // See <https://opentelemetry.io/docs/specs/otlp/#full-success-1>
  let res = ExportTraceServiceResponse { partial_success: None };
  Ok(Prost013(res))
}

fn check_resource_permission(app: &str, channel: &str, resource: &Resource) -> bool {
  let Some(service_name) = get_resource_str(resource, Key::from_static_str("service.name")) else {
    return false;
  };
  let Some(deployment_environement) = get_resource_str(resource, Key::from_static_str("deployment.environment")) else {
    return false;
  };
  service_name.as_str() == app && deployment_environement.as_str() == channel
}

fn get_resource_str(resource: &Resource, key: Key) -> Option<StringValue> {
  match resource.get(key) {
    Some(Value::String(s)) => Some(s),
    _ => None,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum LogDataListFromProtobufResourceLogsError {
  #[error("missing `resource`")]
  MissingResource,
  #[error("invalid `resource.attributes`")]
  InvalidResourceAttributes(#[source] KeyValueListFromProtobufError),
  #[error("invalid `scope_spans` item at index {1}")]
  InvalidScopeLog(#[source] ScopeLogFromProtobufScopeLogsError, usize),
}

fn log_data_list_from_protobuf_resource_logs(
  resource_logs: ResourceLogs,
) -> Result<(Resource, Vec<LogData>), LogDataListFromProtobufResourceLogsError> {
  let mut result: Vec<LogData> = Vec::new();
  let resource: Resource = {
    let pb_resource = resource_logs
      .resource
      .ok_or(LogDataListFromProtobufResourceLogsError::MissingResource)?;
    let attrs = kv_list_from_pb(pb_resource.attributes)
      .map_err(LogDataListFromProtobufResourceLogsError::InvalidResourceAttributes)?;
    if resource_logs.schema_url.is_empty() {
      Resource::new(attrs)
    } else {
      Resource::from_schema_url(attrs, resource_logs.schema_url)
    }
  };
  for (scope_log_i, scope_logs) in resource_logs.scope_logs.into_iter().enumerate() {
    let span_data = log_data_list_from_protobuf_log_spans(scope_logs)
      .map_err(|e| LogDataListFromProtobufResourceLogsError::InvalidScopeLog(e, scope_log_i))?;
    result.extend(span_data.into_iter())
  }

  Ok((resource, result))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum ScopeLogFromProtobufScopeLogsError {
  #[error("missing `scope`")]
  MissingScope,
  #[error("invalid `scope.attributes`")]
  InvalidScopeAttributes(#[source] KeyValueListFromProtobufError),
  #[error("invalid `log_records` item at index {1}")]
  InvalidLog(#[source] LogRecordFromProtobufError, usize),
}

fn log_data_list_from_protobuf_log_spans(
  scope_logs: ScopeLogs,
) -> Result<Vec<LogData>, ScopeLogFromProtobufScopeLogsError> {
  let mut result: Vec<LogData> = Vec::new();
  let instrumentation_lib: InstrumentationLibrary = {
    let pb_instrumentation_scope = scope_logs
      .scope
      .ok_or(ScopeLogFromProtobufScopeLogsError::MissingScope)?;

    let name: String = pb_instrumentation_scope.name;
    let version: Option<String> = if pb_instrumentation_scope.version.is_empty() {
      None
    } else {
      Some(pb_instrumentation_scope.version)
    };
    let schema_url: Option<String> = if scope_logs.schema_url.is_empty() {
      None
    } else {
      Some(scope_logs.schema_url)
    };
    let attrs = kv_list_from_pb(pb_instrumentation_scope.attributes)
      .map_err(ScopeLogFromProtobufScopeLogsError::InvalidScopeAttributes)?;
    let attributes = if attrs.is_empty() { None } else { Some(attrs) };
    let builder: InstrumentationLibraryBuilder = InstrumentationLibrary::builder(name);
    let builder = match version {
      Some(v) => builder.with_version(v),
      None => builder,
    };
    let builder = match schema_url {
      Some(su) => builder.with_schema_url(su),
      None => builder,
    };
    let builder = match attributes {
      Some(a) => builder.with_attributes(a),
      None => builder,
    };
    builder.build()
  };

  for (log_i, log) in scope_logs.log_records.into_iter().enumerate() {
    let log: LogRecord = log_from_protobuf(instrumentation_lib.clone(), log)
      .map_err(|e| ScopeLogFromProtobufScopeLogsError::InvalidLog(e, log_i))?;
    result.push(LogData {
      record: log,
      instrumentation: instrumentation_lib.clone(),
    });
  }
  Ok(result)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("`SpanId` must be a byte array of size exactly 0 or 8, input has size {0}")]
pub struct SpanIdFromSliceError(usize);

fn span_id_from_slice(span_id: &[u8]) -> Result<SpanId, SpanIdFromSliceError> {
  Ok(if span_id.is_empty() {
    SpanId::INVALID
  } else {
    SpanId::from_bytes(<[u8; 8]>::try_from(span_id).map_err(|_| SpanIdFromSliceError(span_id.len()))?)
  })
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("`TraceId` must be a byte array of size exactly 0 or 16, input has size {0}")]
pub struct TraceIdFromSliceError(usize);

fn trace_id_from_slice(trace_id: &[u8]) -> Result<TraceId, TraceIdFromSliceError> {
  Ok(if trace_id.is_empty() {
    TraceId::INVALID
  } else {
    TraceId::from_bytes(<[u8; 16]>::try_from(trace_id).map_err(|_| TraceIdFromSliceError(trace_id.len()))?)
  })
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum LogRecordFromProtobufError {
  #[error("invalid `time_unix_nano`")]
  TimestampUnixNano(#[source] SystemTimeFromUnixNanoError),
  #[error("invalid `observed_time_unix_nano`")]
  ObservedTimestampUnixNano(#[source] SystemTimeFromUnixNanoError),
  #[error("invalid `severity`")]
  Severity(#[source] SeverityFromI32Error),
  #[error("invalid `severity_text`")]
  SeverityText,
  #[error("invalid `trace_id`")]
  Body(#[source] AnyValueFromProtobufError),
  #[error("invalid `attributes`")]
  Attributes(#[source] KeyValueListFromProtobufError),
  #[error("invalid `trace_id`")]
  TraceId(#[source] TraceIdFromSliceError),
  #[error("invalid `span_id`")]
  SpanId(#[source] SpanIdFromSliceError),
}

fn log_from_protobuf(
  _instrumentation_library: InstrumentationLibrary,
  pb_log_record: opentelemetry_proto::tonic::logs::v1::LogRecord,
) -> Result<LogRecord, LogRecordFromProtobufError> {
  let mut record = LogRecord::default();
  record.timestamp = if pb_log_record.time_unix_nano != 0 {
    Some(system_time_from_nanos(pb_log_record.time_unix_nano).map_err(LogRecordFromProtobufError::TimestampUnixNano)?)
  } else {
    None
  };
  record.observed_timestamp = if pb_log_record.observed_time_unix_nano != 0 {
    Some(
      system_time_from_nanos(pb_log_record.observed_time_unix_nano)
        .map_err(LogRecordFromProtobufError::ObservedTimestampUnixNano)?,
    )
  } else {
    None
  };
  let severity = severity_from_i32(pb_log_record.severity_number).map_err(LogRecordFromProtobufError::Severity)?;
  let severity_text = if !pb_log_record.severity_text.is_empty() {
    Some(pb_log_record.severity_text)
  } else {
    None
  };
  match (severity, severity_text) {
    (Some(severity), Some(severity_text)) => {
      if severity.name() != severity_text {
        return Err(LogRecordFromProtobufError::SeverityText);
      }
      record.severity_number = Some(severity);
      record.severity_text = Some(severity.name());
    }
    (Some(severity), None) => {
      record.severity_number = Some(severity);
    }
    (None, Some(_)) => {
      return Err(LogRecordFromProtobufError::SeverityText);
    }
    (None, None) => {}
  }
  record.body = pb_log_record
    .body
    .map(any_value_from_pb)
    .transpose()
    .map_err(LogRecordFromProtobufError::Body)?;

  record.add_attributes(
    kv_list_from_pb(pb_log_record.attributes)
      .map_err(LogRecordFromProtobufError::Attributes)?
      .into_iter()
      .map(|kv| (kv.key, kv.value)),
  );
  // record.dropped_attributes, etc.

  static DEFAULT_TRACE_CX: LazyLock<TraceContext> = LazyLock::new(|| TraceContext::from(&SpanContext::NONE));

  record.trace_context = {
    let mut cx = TraceContext::clone(&*DEFAULT_TRACE_CX);
    cx.trace_id = trace_id_from_slice(&pb_log_record.trace_id).map_err(LogRecordFromProtobufError::TraceId)?;
    cx.span_id = span_id_from_slice(&pb_log_record.span_id).map_err(LogRecordFromProtobufError::SpanId)?;
    if cx != *DEFAULT_TRACE_CX {
      Some(cx)
    } else {
      None
    }
  };

  Ok(record)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("unexpected severity value")]
pub struct SeverityFromI32Error;

fn severity_from_i32(value: i32) -> Result<Option<Severity>, SeverityFromI32Error> {
  Ok(match value {
    0 => None,
    1 => Some(Severity::Trace),
    2 => Some(Severity::Trace2),
    3 => Some(Severity::Trace3),
    4 => Some(Severity::Trace4),
    5 => Some(Severity::Debug),
    6 => Some(Severity::Debug2),
    7 => Some(Severity::Debug3),
    8 => Some(Severity::Debug4),
    9 => Some(Severity::Info),
    10 => Some(Severity::Info2),
    11 => Some(Severity::Info3),
    12 => Some(Severity::Info4),
    13 => Some(Severity::Warn),
    14 => Some(Severity::Warn2),
    15 => Some(Severity::Warn3),
    16 => Some(Severity::Warn4),
    17 => Some(Severity::Error),
    18 => Some(Severity::Error2),
    19 => Some(Severity::Error3),
    20 => Some(Severity::Error4),
    21 => Some(Severity::Fatal),
    22 => Some(Severity::Fatal2),
    23 => Some(Severity::Fatal3),
    24 => Some(Severity::Fatal4),
    _ => return Err(SeverityFromI32Error),
  })
}
