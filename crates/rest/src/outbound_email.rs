use crate::extract::Extractor;
use crate::{EternaltwinSystem, Serialize};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::post;
use axum::{Extension, Json, Router};
use compact_str::CompactString;
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::core::{Duration, IdempotencyKey, Instant, Listing};
use eternaltwin_core::email::EmailAddress;
use eternaltwin_core::mailer::store::{OutboundEmail, OutboundEmailRequest};
use eternaltwin_core::types::WeakError;
use eternaltwin_serde_tools::Deserialize;
use eternaltwin_services::mailer::{GetOutboundEmailRequests, GetOutboundEmails, SendMarktwinEmail};
use serde_json::json;
use serde_json::Value as JsonValue;
use thiserror::Error;

pub fn router() -> Router<()> {
  Router::new().route("/", post(create_outbound_email).get(get_outbound_emails))
}

#[derive(Debug, Error)]
pub enum CreateOutboundEmailError {
  #[error("forbidden")]
  Forbidden,
  #[error("timeout during request processing")]
  Timeout,
  #[error("internal error")]
  Internal(#[from] WeakError),
}

impl From<eternaltwin_services::mailer::SendMarktwinEmailError> for CreateOutboundEmailError {
  fn from(value: eternaltwin_services::mailer::SendMarktwinEmailError) -> Self {
    use eternaltwin_services::mailer::SendMarktwinEmailError::*;
    match value {
      Forbidden => Self::Forbidden,
      Other(e) => Self::Internal(e),
      SpawnJob(e) => Self::Internal(e),
      JoinJob(e) => Self::Internal(e),
      SendEmail(e) => Self::Internal(WeakError::wrap(e)),
    }
  }
}

impl IntoResponse for CreateOutboundEmailError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Timeout => StatusCode::GATEWAY_TIMEOUT,
      Self::Internal(_) => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct CreateOutboundEmailBody {
  pub deadline: Instant,
  pub idempotency_key: IdempotencyKey,
  pub recipient: EmailAddress,
  pub subject: CompactString,
  pub body: CompactString,
}

async fn create_outbound_email(
  Extension(api): Extension<EternaltwinSystem>,
  acx: Extractor<AuthContext>,
  Json(body): Json<CreateOutboundEmailBody>,
) -> Result<Json<OutboundEmail<JsonValue>>, CreateOutboundEmailError> {
  let fut = async move {
    api
      .mailer
      .send_marktwin_email(
        &acx.value(),
        &SendMarktwinEmail {
          deadline: body.deadline,
          idempotency_key: body.idempotency_key,
          recipient: body.recipient,
        },
      )
      .await
  };
  let timeout = api.clock.sleep(Duration::from_seconds(10));
  let res = tokio::select! {
    res = fut => res.map_err(CreateOutboundEmailError::from),
    () = timeout => Err(CreateOutboundEmailError::Timeout),
  };

  Ok(Json(res?))
}

#[derive(Debug, Error)]
pub enum GetOutboundEmailsError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal(#[from] WeakError),
}

impl From<eternaltwin_services::mailer::GetOutboundEmailsError> for GetOutboundEmailsError {
  fn from(value: eternaltwin_services::mailer::GetOutboundEmailsError) -> Self {
    use eternaltwin_services::mailer::GetOutboundEmailsError::*;
    match value {
      Forbidden => Self::Forbidden,
      Other(e) => Self::Internal(e),
    }
  }
}

impl IntoResponse for GetOutboundEmailsError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal(e) => {
        dbg!(&e);
        StatusCode::INTERNAL_SERVER_ERROR
      }
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

pub async fn get_outbound_emails(
  Extension(api): Extension<EternaltwinSystem>,
  acx: Extractor<AuthContext>,
) -> Result<Json<Listing<OutboundEmail<JsonValue>>>, GetOutboundEmailsError> {
  Ok(Json(
    api
      .mailer
      .get_outbound_emails(
        &acx.value(),
        &GetOutboundEmails {
          time: None,
          offset: 0,
          limit: 100,
        },
      )
      .await
      .map_err(WeakError::wrap)?,
  ))
}

#[derive(Debug, Error)]
pub enum GetOutboundEmailRequestsError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal(WeakError),
}

impl From<eternaltwin_services::mailer::GetOutboundEmailRequestsError> for GetOutboundEmailRequestsError {
  fn from(value: eternaltwin_services::mailer::GetOutboundEmailRequestsError) -> Self {
    use eternaltwin_services::mailer::GetOutboundEmailRequestsError::*;
    match value {
      Forbidden => Self::Forbidden,
      Other(e) => Self::Internal(e),
    }
  }
}

impl IntoResponse for GetOutboundEmailRequestsError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal(e) => {
        dbg!(&e);
        StatusCode::INTERNAL_SERVER_ERROR
      }
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[allow(unused)]
pub async fn get_outbound_email_requests(
  Extension(api): Extension<EternaltwinSystem>,
  acx: Extractor<AuthContext>,
) -> Result<Json<Listing<OutboundEmailRequest>>, GetOutboundEmailRequestsError> {
  Ok(Json(
    api
      .mailer
      .get_outbound_email_requests(
        &acx.value(),
        &GetOutboundEmailRequests {
          time: None,
          offset: 0,
          limit: 100,
        },
      )
      .await?,
  ))
}
