use crate::EternaltwinSystem;
use axum::extract::{Extension, Path};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::get;
use axum::{Json, Router};
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::twinoid::store::{GetRewardsError, GetScoresError};
use eternaltwin_core::twinoid::{
  EtwinTwinoidUser, GetTwinoidRewardsOptions, GetTwinoidScoresOptions, GetTwinoidUserOptions, TwinoidScore,
  TwinoidSiteId, TwinoidStatsAndRewards, TwinoidUserId,
};
use serde::Serialize;
use serde_json::json;

pub fn router() -> Router<()> {
  Router::new()
    .route("/users/:user_id", get(get_user))
    .route("/users/:user_id/scores", get(get_score))
    .route("/users/:user_id/rewards/:site_id", get(get_rewards))
}

#[derive(Copy, Clone, Debug, Serialize)]
#[serde(tag = "error")]
enum GetTwinoidUserError {
  TwinoidUserNotFound,
  InternalServerError,
}

impl IntoResponse for GetTwinoidUserError {
  fn into_response(self) -> Response {
    let status = match self {
      Self::TwinoidUserNotFound => StatusCode::NOT_FOUND,
      Self::InternalServerError => StatusCode::INTERNAL_SERVER_ERROR,
    };
    let (status, body) = match serde_json::to_value(self) {
      Ok(body) => (status, Json(body)),
      Err(_) => (
        StatusCode::INTERNAL_SERVER_ERROR,
        Json(json!({"error": "InternalServerError"})),
      ),
    };
    (status, body).into_response()
  }
}

async fn get_user(
  Extension(api): Extension<EternaltwinSystem>,
  Path(id): Path<TwinoidUserId>,
) -> Result<Json<EtwinTwinoidUser>, GetTwinoidUserError> {
  use eternaltwin_services::twinoid::GetUserError;

  let acx = AuthContext::guest();

  match api
    .twinoid
    .get_user(&acx, &GetTwinoidUserOptions { id, time: None })
    .await
  {
    Ok(result) => Ok(Json(result)),
    Err(GetUserError::NotFound) => Err(GetTwinoidUserError::TwinoidUserNotFound),
    Err(GetUserError::Other(_)) => Err(GetTwinoidUserError::InternalServerError),
    Err(GetUserError::GetEtwinLinkedBy(_)) => Err(GetTwinoidUserError::InternalServerError),
    Err(GetUserError::GetLinkedEtwin(_)) => Err(GetTwinoidUserError::InternalServerError),
  }
}

#[derive(Copy, Clone, Debug, Serialize)]
#[serde(tag = "error")]
enum GetTwinoidScoresError {
  InternalServerError,
}

impl IntoResponse for GetTwinoidScoresError {
  fn into_response(self) -> Response {
    let status = match self {
      Self::InternalServerError => StatusCode::INTERNAL_SERVER_ERROR,
    };
    let (status, body) = match serde_json::to_value(self) {
      Ok(body) => (status, Json(body)),
      Err(_) => (
        StatusCode::INTERNAL_SERVER_ERROR,
        Json(json!({"error": "InternalServerError"})),
      ),
    };
    (status, body).into_response()
  }
}

async fn get_score(
  Extension(api): Extension<EternaltwinSystem>,
  Path(id): Path<TwinoidUserId>,
) -> Result<Json<Vec<TwinoidScore>>, GetTwinoidScoresError> {
  match api.twinoid.get_scores(&GetTwinoidScoresOptions { id }).await {
    Ok(result) => Ok(Json(result)),
    Err(GetScoresError::Other(_)) => Err(GetTwinoidScoresError::InternalServerError),
  }
}

#[derive(Copy, Clone, Debug, Serialize)]
#[serde(tag = "error")]
enum GetTwinoidRewardsError {
  InternalServerError,
}

impl IntoResponse for GetTwinoidRewardsError {
  fn into_response(self) -> Response {
    let status = match self {
      Self::InternalServerError => StatusCode::INTERNAL_SERVER_ERROR,
    };
    let (status, body) = match serde_json::to_value(self) {
      Ok(body) => (status, Json(body)),
      Err(_) => (
        StatusCode::INTERNAL_SERVER_ERROR,
        Json(json!({"error": "InternalServerError"})),
      ),
    };
    (status, body).into_response()
  }
}

async fn get_rewards(
  Extension(api): Extension<EternaltwinSystem>,
  Path((user_id, site_id)): Path<(TwinoidUserId, TwinoidSiteId)>,
) -> Result<Json<TwinoidStatsAndRewards>, GetTwinoidRewardsError> {
  match api
    .twinoid
    .get_rewards(&GetTwinoidRewardsOptions { user_id, site_id })
    .await
  {
    Ok(result) => Ok(Json(result)),
    Err(GetRewardsError::Other(_)) => Err(GetTwinoidRewardsError::InternalServerError),
  }
}
