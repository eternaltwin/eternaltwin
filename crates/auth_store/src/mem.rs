use async_trait::async_trait;
use eternaltwin_core::auth::{
  AuthStore, CreateSessionOptions, CreateValidatedEmailVerificationOptions, RawDeleteAllSessions, RawSession, SessionId,
};
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::Instant;
use eternaltwin_core::types::WeakError;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::HashMap;
use std::sync::RwLock;

struct StoreState {
  sessions: HashMap<SessionId, RawSession>,
}

impl StoreState {
  fn new() -> Self {
    Self {
      sessions: HashMap::new(),
    }
  }

  pub(crate) fn create_session(
    &mut self,
    now: Instant,
    uuid_generator: &(impl UuidGenerator + ?Sized),
    options: &CreateSessionOptions,
  ) -> Result<RawSession, WeakError> {
    let session_id = SessionId::from_uuid(uuid_generator.next());
    let session = RawSession {
      id: session_id,
      user: options.user,
      ctime: now,
      atime: now,
    };
    self.sessions.insert(session_id, session.clone());
    Ok(session)
  }

  pub(crate) fn get_and_touch_session(
    &mut self,
    now: Instant,
    session_id: SessionId,
  ) -> Result<Option<RawSession>, WeakError> {
    let session = self.sessions.get_mut(&session_id);
    match session {
      None => Ok(None),
      Some(session) => {
        session.atime = now;
        Ok(Some(session.clone()))
      }
    }
  }

  pub(crate) fn delete_all_sessions(&mut self, cmd: &RawDeleteAllSessions) {
    self.sessions.retain(|_sid, session| session.user == cmd.user);
  }
}

pub struct MemAuthStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  uuid_generator: TyUuidGenerator,
  state: RwLock<StoreState>,
}

impl<TyClock, TyUuidGenerator> MemAuthStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(clock: TyClock, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      uuid_generator,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock, TyUuidGenerator> AuthStore for MemAuthStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_validated_email_verification(
    &self,
    _options: &CreateValidatedEmailVerificationOptions,
  ) -> Result<(), WeakError> {
    eprintln!("Warning: PgAuthStore#create_validated_email_verification is a no-op stub");
    Ok(())
  }

  async fn create_session(&self, options: &CreateSessionOptions) -> Result<RawSession, WeakError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    state.create_session(now, self.uuid_generator.uuid_generator(), options)
  }

  async fn get_and_touch_session(&self, session: SessionId) -> Result<Option<RawSession>, WeakError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    state.get_and_touch_session(now, session)
  }

  async fn delete_all_sessions(&self, cmd: &RawDeleteAllSessions) -> Result<(), WeakError> {
    let mut state = self.state.write().unwrap();
    state.delete_all_sessions(cmd);
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemAuthStore;
  use crate::test::TestApi;
  use eternaltwin_core::auth::AuthStore;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_user_store::mem::MemUserStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<dyn AuthStore>, Arc<VirtualClock>, Arc<dyn UserStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let auth_store: Arc<dyn AuthStore> = Arc::new(MemAuthStore::new(Arc::clone(&clock), uuid_generator));
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(Arc::clone(&clock), Uuid4Generator));

    TestApi {
      auth_store,
      clock,
      user_store,
    }
  }

  test_dinoparc_store!(|| make_test_api());
}
