use eternaltwin_core::api::SyncRef;
use eternaltwin_core::auth::{AuthStore, AuthStoreRef, CreateSessionOptions, RawSession};
use eternaltwin_core::clock::VirtualClock;
use eternaltwin_core::core::{Duration, Instant};
use eternaltwin_core::user::{CreateUserOptions, UserStore, UserStoreRef};

#[macro_export]
macro_rules! test_dinoparc_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_create_session);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyAuthStore, TyClock, TyUserStore>
where
  TyAuthStore: AuthStoreRef,
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  pub(crate) auth_store: TyAuthStore,
  pub(crate) clock: TyClock,
  pub(crate) user_store: TyUserStore,
}

pub(crate) async fn test_create_session<TyAuthStore, TyClock, TyUserStore>(
  api: TestApi<TyAuthStore, TyClock, TyUserStore>,
) where
  TyAuthStore: AuthStoreRef,
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let user = api
    .user_store
    .user_store()
    .create_user(&CreateUserOptions {
      display_name: "Alice".parse().unwrap(),
      username: Some("alice".parse().unwrap()),
      email: None,
      password: None,
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .auth_store
    .auth_store()
    .create_session(&CreateSessionOptions { user: user.id.into() })
    .await
    .unwrap();
  let expected = RawSession {
    id: actual.id,
    user: user.id.into(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
    atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
  };
  assert_eq!(actual, expected);
}
