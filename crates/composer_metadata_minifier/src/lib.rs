pub use serde_json;
use serde_json::map::Entry;

pub fn expand(
  versions: &[serde_json::Map<String, serde_json::Value>],
) -> Vec<serde_json::Map<String, serde_json::Value>> {
  let mut expanded = Vec::new();
  let mut expanded_version: Option<serde_json::Map<String, serde_json::Value>> = None;
  for version_data in versions {
    let expanded_version: &mut serde_json::Map<String, serde_json::Value> = match expanded_version.as_mut() {
      Some(ev) => ev,
      None => {
        expanded_version = Some(version_data.clone());
        expanded.push(version_data.clone());
        continue;
      }
    };
    for (key, val) in version_data {
      match val {
        serde_json::Value::String(ref val) if val == "__unset" => {
          expanded_version.remove(key);
        }
        v => {
          expanded_version.insert(key.clone(), v.clone());
        }
      }
    }
    expanded.push(expanded_version.clone());
  }
  expanded
}

pub fn minify(
  versions: &[serde_json::Map<String, serde_json::Value>],
) -> Vec<serde_json::Map<String, serde_json::Value>> {
  let mut minified_versions: Vec<serde_json::Map<String, serde_json::Value>> = Vec::new();

  let mut last_known_version_data = None;

  for version in versions {
    let last_known_version_data: &mut serde_json::Map<String, serde_json::Value> =
      match last_known_version_data.as_mut() {
        Some(ev) => ev,
        None => {
          last_known_version_data = Some(version.clone());
          minified_versions.push(version.clone());
          continue;
        }
      };

    let mut minified_version = serde_json::Map::new();

    // add any changes from the previous version
    for (key, val) in version {
      match last_known_version_data.entry(key.clone()) {
        Entry::Vacant(e) => {
          minified_version.insert(key.clone(), val.clone());
          e.insert(val.clone());
        }
        Entry::Occupied(mut e) => {
          if e.get() != val {
            minified_version.insert(key.clone(), val.clone());
            e.insert(val.clone());
          }
        }
      }
    }

    // store any deletions from the previous version for keys missing in current one
    last_known_version_data.retain(|key, _val| {
      if version.contains_key(key) {
        true
      } else {
        minified_version.insert(key.clone(), serde_json::Value::String(String::from("__unset")));
        false
      }
    });

    minified_versions.push(minified_version);
  }

  minified_versions
}

#[cfg(test)]
mod test {
  use super::*;
  use serde_json::json;

  fn to_object(val: serde_json::Value) -> Option<serde_json::Map<String, serde_json::Value>> {
    if let serde_json::Value::Object(v) = val {
      Some(v)
    } else {
      None
    }
  }

  #[test]
  fn test_minify_expand() {
    let package1 = to_object(json!({
      "name": "foo/bar",
      "version": "2.0.0",
      "version_normalized": "2.0.0.0",
      "type": "library",
      "scripts": {
        "foo": "bar",
      },
      "license": ["MIT"],
    }))
    .unwrap();
    let package2 = to_object(json!({
      "name": "foo/bar",
      "version": "1.2.0",
      "version_normalized": "1.2.0.0",
      "type": "library",
      "license": ["GPL"],
      "homepage": "https://example.org",
    }))
    .unwrap();
    let package3 = to_object(json!({
      "name": "foo/bar",
      "version": "1.0.0",
      "version_normalized": "1.0.0.0",
      "type": "library",
      "license": ["GPL"],
    }))
    .unwrap();

    let minified = vec![
      to_object(json!({
        "name": "foo/bar",
        "version": "2.0.0",
        "version_normalized": "2.0.0.0",
        "type": "library",
        "scripts": {
          "foo": "bar",
        },
        "license": ["MIT"],
      }))
      .unwrap(),
      to_object(json!({
        "version": "1.2.0",
        "version_normalized": "1.2.0.0",
        "license": ["GPL"],
        "homepage": "https://example.org",
        "scripts": "__unset",
      }))
      .unwrap(),
      to_object(json!({
        "version": "1.0.0",
        "version_normalized": "1.0.0.0",
        "homepage": "__unset",
      }))
      .unwrap(),
    ];

    let source = vec![package1, package2, package3];

    assert_eq!(minified, minify(&source));
    assert_eq!(source, expand(&minified));
  }
}
