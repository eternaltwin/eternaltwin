use async_trait::async_trait;
use eternaltwin_core::core::LocaleId;
use eternaltwin_core::email::{EmailContent, EmailFormatter, EmailSubject, VerifyRegistrationEmail};
use eternaltwin_core::forum::MarktwinText;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};

pub struct JsonEmailFormatter;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct JsonBody<T> {
  pub locale: LocaleId,
  pub data: T,
}

#[async_trait]
impl EmailFormatter for JsonEmailFormatter {
  async fn verify_registration_email(
    &self,
    locale: LocaleId,
    data: &VerifyRegistrationEmail,
  ) -> Result<EmailContent, WeakError> {
    let mut body = serde_json::to_string_pretty(&JsonBody { locale, data }).map_err(WeakError::wrap)?;
    body.push('\n');
    Ok(EmailContent {
      subject: "verifyRegistrationEmail".parse().unwrap(),
      body_text: body.parse().unwrap(),
      body_html: None,
    })
  }

  async fn marktwin(&self, subject: EmailSubject, data: &MarktwinText) -> Result<EmailContent, WeakError> {
    let mut body = serde_json::to_string_pretty(&JsonBody {
      locale: LocaleId::FrFr,
      data,
    })
    .map_err(WeakError::wrap)?;
    body.push('\n');
    Ok(EmailContent {
      subject,
      body_text: body.parse().unwrap(),
      body_html: None,
    })
  }
}

#[cfg(test)]
mod test {
  use crate::json::JsonEmailFormatter;
  use eternaltwin_core::core::LocaleId;
  use eternaltwin_core::email::{EmailContent, EmailFormatter, VerifyRegistrationEmail};

  #[tokio::test]
  async fn verify_registration_en() {
    let formatter = JsonEmailFormatter;

    let actual = formatter
      .verify_registration_email(
        LocaleId::EnUs,
        &VerifyRegistrationEmail {
          token: "abcdef".to_string(),
        },
      )
      .await
      .unwrap();

    let expected = EmailContent {
      subject: "verifyRegistrationEmail".parse().unwrap(),
      body_text: r#"{
  "locale": "en-US",
  "data": {
    "token": "abcdef"
  }
}
"#
      .parse()
      .unwrap(),
      body_html: None,
    };

    assert_eq!(actual, expected);
  }
  #[tokio::test]
  async fn verify_registration_fr() {
    let formatter = JsonEmailFormatter;

    let actual = formatter
      .verify_registration_email(
        LocaleId::FrFr,
        &VerifyRegistrationEmail {
          token: "abcdef".to_string(),
        },
      )
      .await
      .unwrap();

    let expected = EmailContent {
      subject: "verifyRegistrationEmail".parse().unwrap(),
      body_text: r#"{
  "locale": "fr-FR",
  "data": {
    "token": "abcdef"
  }
}
"#
      .parse()
      .unwrap(),
      body_html: None,
    };

    assert_eq!(actual, expected);
  }
}
