pub mod backend;
pub mod human;
pub mod hyper_client;
pub mod jsonl;

use core::fmt;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use opentelemetry::trace::{SpanId, TraceId};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct IdGenerator<TyUuidGeneratorRef> {
  uuid_generator: TyUuidGeneratorRef,
}

impl<TyUuidGeneratorRef> fmt::Debug for IdGenerator<TyUuidGeneratorRef> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.debug_struct(core::any::type_name::<Self>()).finish_non_exhaustive()
  }
}

impl<TyUuidGeneratorRef> IdGenerator<TyUuidGeneratorRef> {
  pub fn new(uuid_generator: TyUuidGeneratorRef) -> Self {
    Self { uuid_generator }
  }
}

impl<TyUuidGeneratorRef> opentelemetry_sdk::trace::IdGenerator for IdGenerator<TyUuidGeneratorRef>
where
  TyUuidGeneratorRef: UuidGeneratorRef,
{
  fn new_trace_id(&self) -> TraceId {
    TraceId::from(self.uuid_generator.uuid_generator().next().as_u128())
  }

  fn new_span_id(&self) -> SpanId {
    SpanId::from(self.uuid_generator.uuid_generator().next().as_u64_pair().0)
  }
}
