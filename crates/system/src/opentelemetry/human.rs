use compact_str::CompactString;
use core::fmt;
use eternaltwin_core::core::{Duration, Instant};
use eternaltwin_core::types::WeakError;
use futures_util::future::BoxFuture;
use once_cell::sync::Lazy;
use opentelemetry::logs::{AnyValue, LogError, LogResult, Severity};
use opentelemetry::trace::{Event, SpanId, Status, TraceError, TraceId};
use opentelemetry::{ExportError, InstrumentationLibrary, Key};
use opentelemetry_sdk::export::logs::{LogBatch, LogExporter};
use opentelemetry_sdk::export::trace::{ExportResult, SpanData, SpanExporter};
use opentelemetry_sdk::logs::LogRecord;
use opentelemetry_sdk::Resource;
use opentelemetry_semantic_conventions::attribute::SERVICE_NAME;
use opentelemetry_semantic_conventions::trace as otel_keys;
use std::sync::{Arc, Mutex};
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("write failed")]
struct JsonlWriteError(#[source] WeakError);

impl ExportError for JsonlWriteError {
  fn exporter_name(&self) -> &'static str {
    HumanOtelExporter::<()>::NAME
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("flush failed")]
struct JsonlFlushError(#[source] WeakError);

impl ExportError for JsonlFlushError {
  fn exporter_name(&self) -> &'static str {
    HumanOtelExporter::<()>::NAME
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("exporter was shut down")]
struct ShutExporterError;

impl ExportError for ShutExporterError {
  fn exporter_name(&self) -> &'static str {
    HumanOtelExporter::<()>::NAME
  }
}

pub struct HumanOtelExporter<W: ?Sized> {
  writer: Option<Arc<Mutex<W>>>,
  resource: Resource,
}

impl<W: ?Sized> HumanOtelExporter<W> {
  pub const NAME: &'static str = "HumanOtelExporter";

  pub fn new(writer: Arc<Mutex<W>>) -> Self {
    Self {
      writer: Some(writer),
      resource: Resource::empty(),
    }
  }
}

impl<W: ?Sized> fmt::Debug for HumanOtelExporter<W> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.debug_struct(std::any::type_name::<Self>())
      .field("writer", if self.writer.is_some() { &"Some(..)" } else { &"None" })
      .field("resource", &self.resource)
      .finish_non_exhaustive()
  }
}

impl<W: ?Sized> SpanExporter for HumanOtelExporter<W>
where
  W: std::io::Write + Send + Sync,
{
  fn export(&mut self, batch: Vec<SpanData>) -> BoxFuture<'static, ExportResult> {
    let mut res: ExportResult = Ok(());
    if let Some(writer) = self.writer.as_deref() {
      let writer = writer
        .lock()
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::WouldBlock, err.to_string()))
        .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
      match writer {
        Ok(mut writer) => {
          let writer = &mut *writer;
          for span in batch {
            let span = HumanSpan(&self.resource, &span);
            let write_res = writeln!(writer, "{span}")
              .map_err(|e| TraceError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
            if let Err(e) = write_res {
              res = Err(e);
              break;
            }
          }
        }
        Err(e) => res = Err(e),
      }
    } else {
      res = Err(TraceError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn shutdown(&mut self) {
    self.writer = None;
  }

  fn force_flush(&mut self) -> BoxFuture<'static, ExportResult> {
    let res = if let Some(writer) = self.writer.as_mut() {
      writer
        .lock()
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::WouldBlock, err.to_string()))
        .and_then(|mut w| w.flush())
        .map_err(|e| TraceError::ExportFailed(Box::new(JsonlFlushError(WeakError::wrap(e)))))
    } else {
      Err(TraceError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn set_resource(&mut self, resource: &Resource) {
    self.resource = resource.clone();
  }
}

pub enum SpanCategory {
  Other,
  Db,
  Http,
}

impl SpanCategory {
  pub const fn as_str(&self) -> &'static str {
    match self {
      Self::Other => "?",
      Self::Db => "DB",
      Self::Http => "HTTP",
    }
  }
}

impl SpanCategory {
  pub fn from_span(span: &SpanData) -> Self {
    let mut has_http_route = false;
    let mut has_http_req_method = false;
    let mut has_db_system = false;

    for attr in &span.attributes {
      if attr.key.as_str() == otel_keys::HTTP_ROUTE {
        has_http_route = true;
      }
      if attr.key.as_str() == otel_keys::HTTP_REQUEST_METHOD {
        has_http_req_method = true;
      }
      #[allow(deprecated)] // Node uses this deprecated value
      if attr.key.as_str() == opentelemetry_semantic_conventions::attribute::HTTP_METHOD {
        has_http_req_method = true;
      }
      if attr.key.as_str() == otel_keys::DB_SYSTEM {
        has_db_system = true;
      }
    }
    if has_http_route && has_http_req_method {
      Self::Http
    } else if has_db_system {
      Self::Db
    } else {
      Self::Other
    }
  }
}

// write last 3 bytes
pub struct ShortTraceId(pub TraceId);

impl fmt::Display for ShortTraceId {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let bytes = self.0.to_bytes();
    let short = u32::from_be_bytes([bytes[12], bytes[13], bytes[14], bytes[15]]);
    f.write_fmt(format_args!("{:06x}", short & 0x00ffffff))
  }
}

pub struct ShortSpanId(pub SpanId);

// write last 3 bytes
impl fmt::Display for ShortSpanId {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let bytes = self.0.to_bytes();
    let short = u32::from_be_bytes([bytes[4], bytes[5], bytes[6], bytes[7]]);
    f.write_fmt(format_args!("{:06x}", short & 0x00ffffff))
  }
}

pub struct SpanHeader {
  pub service_name: CompactString,
  pub trace_id: TraceId,
  pub parent_span_id: SpanId,
  pub span_id: SpanId,
  pub duration: Duration,
  pub category: SpanCategory,
  pub is_error: bool,
}

impl SpanHeader {
  pub fn from_span(resource: &Resource, span: &SpanData) -> Self {
    Self {
      service_name: resource
        .get(Key::from_static_str(SERVICE_NAME))
        .map(|v| CompactString::new(v.as_str()))
        .unwrap_or(CompactString::new("unknown")),
      trace_id: span.span_context.trace_id(),
      parent_span_id: span.parent_span_id,
      span_id: span.span_context.span_id(),
      duration: Instant::from_system(span.end_time) - Instant::from_system(span.start_time),
      category: SpanCategory::from_span(span),
      is_error: matches!(span.status, Status::Error { .. }),
    }
  }
}

impl fmt::Display for SpanHeader {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let error_mark = if self.is_error { '!' } else { ' ' };
    // the prefix `S` means `Span`
    write!(
      f,
      "S{error_mark}{}/{}.{} [{:>12}.{:<6}] {:>14.9}s",
      ShortTraceId(self.trace_id),
      ShortSpanId(self.parent_span_id),
      ShortSpanId(self.span_id),
      self.service_name.as_str(),
      self.category.as_str(),
      self.duration.seconds_f64(),
    )
  }
}

pub struct EventHeader {
  pub service_name: CompactString,
  pub trace_id: TraceId,
  pub span_id: SpanId,
  pub time: Instant,
  pub is_error: bool,
}

impl EventHeader {
  pub fn from_span(resource: &Resource, span: &SpanData, ev: &Event) -> Self {
    Self {
      service_name: resource
        .get(Key::from_static_str(SERVICE_NAME))
        .map(|v| CompactString::new(v.as_str()))
        .unwrap_or(CompactString::new("unknown")),
      trace_id: span.span_context.trace_id(),
      span_id: span.span_context.span_id(),
      time: Instant::from_system(ev.timestamp),
      is_error: matches!(span.status, Status::Error { .. }),
    }
  }
}

impl fmt::Display for EventHeader {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let error_mark = if self.is_error { '!' } else { ' ' };
    // the prefix `E` means `Event`
    write!(
      f,
      "E{error_mark}{}/{}        [{:>12}.event ] {:>15} ",
      ShortTraceId(self.trace_id),
      ShortSpanId(self.span_id),
      self.service_name.as_str(),
      self.time.into_chrono().format("%H:%M:%S%.3f"),
    )
  }
}

struct HumanSpan<'s>(&'s Resource, &'s SpanData);

impl fmt::Display for HumanSpan<'_> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    for ev in &self.1.events.events {
      let header = EventHeader::from_span(self.0, self.1, ev);
      write!(f, "{}{}", header, ev.name)?;
      for attr in &ev.attributes {
        let v = attr.value.as_str();
        if v.len() <= 50 {
          write!(f, " {}={:?}", attr.key.as_str(), v)?;
        } else {
          write!(f, " {}=<size={}>", attr.key.as_str(), v)?;
        }
      }
      writeln!(f)?;
    }

    let header = SpanHeader::from_span(self.0, self.1);
    match header.category {
      SpanCategory::Http => {
        write!(f, "{header} {}", HttpHumanSpan(self.1))
      }
      SpanCategory::Db => {
        write!(f, "{header} {}", OtherHumanSpan(self.1))
      }
      SpanCategory::Other => {
        write!(f, "{header} {}", OtherHumanSpan(self.1))
      }
    }
  }
}

struct HttpHumanSpan<'a>(&'a SpanData);

impl fmt::Display for HttpHumanSpan<'_> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    use std::fmt::Write;

    let span: &SpanData = self.0;
    let mut method: Option<String> = None;
    let mut path: Option<String> = None;
    let mut status: Option<String> = None;
    let mut other_attrs = String::new();
    for attr in &span.attributes {
      if attr.key.as_str() == otel_keys::HTTP_REQUEST_METHOD {
        method = Some(attr.value.to_string());
        continue;
      }
      #[allow(deprecated)] // Node uses `http.method`
      if attr.key.as_str() == opentelemetry_semantic_conventions::attribute::HTTP_METHOD {
        method = Some(attr.value.to_string());
        continue;
      }
      if attr.key.as_str() == otel_keys::URL_PATH {
        path = Some(attr.value.to_string());
        continue;
      }
      #[allow(deprecated)] // Node uses `http.method` instead of `url.path`/`url.query`
      if attr.key.as_str() == opentelemetry_semantic_conventions::attribute::HTTP_TARGET {
        static BASE_URL: Lazy<Url> = Lazy::new(|| Url::parse("https://localhost/").expect("base URL is valid"));
        let url = Url::options().base_url(Some(&*BASE_URL)).parse(&attr.value.as_str());
        if let Ok(url) = url {
          path = Some(url.path().to_string());
        }
        continue;
      }
      if attr.key.as_str() == otel_keys::HTTP_RESPONSE_STATUS_CODE {
        status = Some(attr.value.to_string());
        continue;
      }
      #[allow(deprecated)] // Node uses `http.status_code`
      if attr.key.as_str() == opentelemetry_semantic_conventions::attribute::HTTP_STATUS_CODE {
        status = Some(attr.value.to_string());
        continue;
      }
      let v = attr.value.as_str();
      if v.len() <= 50 {
        write!(other_attrs, " {}={:?}", attr.key.as_str(), v)?;
      } else {
        write!(other_attrs, " {}=<size={}>", attr.key.as_str(), v.len())?;
      }
    }

    let method = method.as_deref().unwrap_or("?");
    let path = path.as_deref().unwrap_or("?");
    let status = status.as_deref().unwrap_or("?");

    write!(f, "{status} {method} {path}")?;
    if let Status::Error { description } = &span.status {
      write!(f, " {description}")?;
    }
    write!(f, "{other_attrs}")?;
    Ok(())
  }
}

struct OtherHumanSpan<'a>(&'a SpanData);

impl fmt::Display for OtherHumanSpan<'_> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    use std::fmt::Write;

    let span: &SpanData = self.0;
    let mut other_attrs = String::new();
    for attr in &span.attributes {
      let v = attr.value.as_str();
      if v.len() <= 50 {
        write!(other_attrs, " {}={:?}", attr.key.as_str(), v)?;
      } else {
        write!(other_attrs, " {}=<size={}>", attr.key.as_str(), v.len())?;
      }
    }
    if span.dropped_attributes_count > 0 {
      write!(other_attrs, " (+{} dropped attributes)", span.dropped_attributes_count)?;
    }

    write!(f, "{}", span.name.as_ref())?;
    if let Status::Error { description } = &span.status {
      write!(f, " {description}")?;
    }
    write!(f, "{other_attrs}")?;
    for link in &span.links.links {
      write!(
        f,
        " [{}/{}]",
        ShortTraceId(link.span_context.trace_id()),
        ShortSpanId(link.span_context.span_id()),
      )?;
    }
    if span.links.dropped_count > 0 {
      write!(f, " (+{} dropped links)", span.links.dropped_count)?;
    }
    Ok(())
  }
}

impl<W: ?Sized> LogExporter for HumanOtelExporter<W>
where
  W: std::io::Write + Send + Sync,
{
  fn export<'life0, 'life1, 'async_trait>(
    &'life0 mut self,
    batch: LogBatch<'life1>,
  ) -> BoxFuture<'async_trait, LogResult<()>>
  where
    'life0: 'async_trait,
    'life1: 'async_trait,
    Self: 'async_trait,
  {
    let mut res: LogResult<()> = Ok(());
    if let Some(writer) = self.writer.as_deref() {
      let writer = writer
        .lock()
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::WouldBlock, err.to_string()))
        .map_err(|e| LogError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
      match writer {
        Ok(mut writer) => {
          let writer = &mut *writer;
          for (record, instrumentation_lib) in batch.iter() {
            let log = HumanLog(&self.resource, instrumentation_lib, record);
            let write_res = writeln!(writer, "{log}")
              .map_err(|e| LogError::ExportFailed(Box::new(JsonlWriteError(WeakError::wrap(e)))));
            if let Err(e) = write_res {
              res = Err(e);
              break;
            }
          }
        }
        Err(e) => res = Err(e),
      }
    } else {
      res = Err(LogError::ExportFailed(Box::new(ShutExporterError)))
    };
    Box::pin(core::future::ready(res))
  }

  fn shutdown(&mut self) {
    self.writer.take();
  }

  fn set_resource(&mut self, resource: &Resource) {
    self.resource = resource.clone();
  }
}

struct HumanLog<'l>(&'l Resource, &'l InstrumentationLibrary, &'l LogRecord);

impl fmt::Display for HumanLog<'_> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let (resource, _instr_lib, log) = (self.0, self.1, self.2);
    let header = LogHeader::from_log(resource, log);
    let body = match &log.body {
      Some(AnyValue::String(s)) => format!(" {s}"),
      Some(v) => format!(" {v:?}"),
      _ => String::from(""),
    };
    write!(f, "{header}{body}")
  }
}

pub struct LogHeader {
  pub service_name: CompactString,
  pub trace_id: TraceId,
  pub span_id: SpanId,
  pub severity: Severity,
  // pub time: Option<Instant>,
  // pub category: SpanCategory,
  pub is_error: bool,
}

impl LogHeader {
  pub fn from_log(resource: &Resource, log: &LogRecord) -> Self {
    let (trace_id, span_id) = log
      .trace_context
      .as_ref()
      .map(|tcx| (tcx.trace_id, tcx.span_id))
      .unwrap_or((TraceId::INVALID, SpanId::INVALID));
    Self {
      service_name: resource
        .get(Key::from_static_str(SERVICE_NAME))
        .map(|v| CompactString::new(v.as_str()))
        .unwrap_or(CompactString::new("unknown")),
      trace_id,
      span_id,
      severity: log.severity_number.unwrap_or(Severity::Debug),
      // time: log.timestamp.map(Instant::from_system),
      // category: SpanCategory::from_span(log),
      is_error: log.severity_number >= Some(Severity::Warn),
    }
  }
}

impl fmt::Display for LogHeader {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let error_mark = if self.is_error { '!' } else { ' ' };
    // the prefix `L` means `Log`
    write!(
      f,
      "L{error_mark}{}/{}        [{:>12}.{:<6}]                ",
      ShortTraceId(self.trace_id),
      ShortSpanId(self.span_id),
      self.service_name.as_str(),
      self.severity.name(),
      // self.category.as_str(),
      // self.duration.seconds_f64(),
    )
  }
}
