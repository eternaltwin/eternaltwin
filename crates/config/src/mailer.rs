use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct MailerConfig<TyMeta> {
  pub host: ConfigValue<String, TyMeta>,
  pub username: ConfigValue<String, TyMeta>,
  pub password: ConfigValue<String, TyMeta>,
  pub sender: ConfigValue<String, TyMeta>,
  pub headers: ConfigValue<Vec<MailerHeader>, TyMeta>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct MailerHeader {
  pub name: String,
  pub value: String,
}

impl<TyMeta> MailerConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: MailerConfigPatch, meta: TyMeta) -> Self {
    if let SimplePatch::Set(host) = config.host {
      self.host = ConfigValue::new_meta(host, meta.clone());
    }
    if let SimplePatch::Set(username) = config.username {
      self.username = ConfigValue::new_meta(username, meta.clone());
    }
    if let SimplePatch::Set(password) = config.password {
      self.password = ConfigValue::new_meta(password, meta.clone());
    }
    if let SimplePatch::Set(sender) = config.sender {
      self.sender = ConfigValue::new_meta(sender, meta.clone());
    }
    if let SimplePatch::Set(headers) = config.headers {
      match headers {
        None => {
          // TODO: propagate source instead of clearing
          self.headers.value.clear()
        }
        Some(headers) => {
          self.headers = ConfigValue::new_meta(headers, meta.clone());
        }
      }
    }
    self
  }
}

impl MailerConfig<ConfigSource> {
  pub(crate) fn default_for_profile(_profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    Self {
      host: ConfigValue::new_meta("localhost".to_string(), meta.clone()),
      username: ConfigValue::new_meta("eternaltwin_mailer".to_string(), meta.clone()),
      password: ConfigValue::new_meta("dev".to_string(), meta.clone()),
      sender: ConfigValue::new_meta("support@eternaltwin.localhost".to_string(), meta.clone()),
      headers: ConfigValue::new_meta(Vec::new(), meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct MailerConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub host: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub username: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub password: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub sender: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub headers: SimplePatch<Option<Vec<MailerHeader>>>,
}
