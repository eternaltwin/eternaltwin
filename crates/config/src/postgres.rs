use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PostgresConfig<TyMeta> {
  pub host: ConfigValue<String, TyMeta>,
  pub port: ConfigValue<u16, TyMeta>,
  pub name: ConfigValue<String, TyMeta>,
  pub user: ConfigValue<String, TyMeta>,
  pub password: ConfigValue<String, TyMeta>,
  pub admin_user: ConfigValue<String, TyMeta>,
  pub admin_password: ConfigValue<String, TyMeta>,
  pub max_connections: ConfigValue<u32, TyMeta>,
}

impl<TyMeta> PostgresConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: PostgresConfigPatch, meta: TyMeta) -> Self {
    if let SimplePatch::Set(host) = config.host {
      self.host = ConfigValue::new_meta(host, meta.clone());
    }
    if let SimplePatch::Set(port) = config.port {
      self.port = ConfigValue::new_meta(port, meta.clone());
    }
    if let SimplePatch::Set(name) = config.name {
      self.name = ConfigValue::new_meta(name, meta.clone());
    }
    if let SimplePatch::Set(user) = config.user {
      self.user = ConfigValue::new_meta(user.clone(), meta.clone());
      self.admin_user = ConfigValue::new_meta(user, meta.clone());
    }
    if let SimplePatch::Set(password) = config.password {
      self.password = ConfigValue::new_meta(password.clone(), meta.clone());
      self.admin_password = ConfigValue::new_meta(password, meta.clone());
    }
    if let SimplePatch::Set(admin_user) = config.admin_user {
      self.admin_user = ConfigValue::new_meta(admin_user, meta.clone());
    }
    if let SimplePatch::Set(admin_password) = config.admin_password {
      self.admin_password = ConfigValue::new_meta(admin_password, meta.clone());
    }
    if let SimplePatch::Set(max_connections) = config.max_connections {
      self.max_connections = ConfigValue::new_meta(max_connections, meta.clone());
    }
    self
  }
}

impl PostgresConfig<ConfigSource> {
  pub(crate) fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    use BuiltinEternaltwinConfigProfile::*;
    Self {
      host: ConfigValue::new_meta("localhost".to_string(), meta.clone()),
      port: ConfigValue::new_meta(5432, meta.clone()),
      name: ConfigValue::new_meta(
        match profile {
          Production => "eternaltwin.production".to_string(),
          Dev | Sdk | Test => "eternaltwin.dev".to_string(),
        },
        meta.clone(),
      ),
      user: ConfigValue::new_meta(
        match profile {
          Production => "eternaltwin.production.main".to_string(),
          Dev | Sdk | Test => "eternaltwin.dev.main".to_string(),
        },
        meta.clone(),
      ),
      password: ConfigValue::new_meta("dev".to_string(), meta.clone()),
      admin_user: ConfigValue::new_meta(
        match profile {
          Production => "eternaltwin.production.admin".to_string(),
          Dev | Sdk | Test => "eternaltwin.dev.main".to_string(),
        },
        meta.clone(),
      ),
      admin_password: ConfigValue::new_meta("dev".to_string(), meta.clone()),
      max_connections: ConfigValue::new_meta(50, meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PostgresConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub host: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub port: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub name: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub user: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub password: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub admin_user: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub admin_password: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub max_connections: SimplePatch<u32>,
}
