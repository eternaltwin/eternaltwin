use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::core::Duration;
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[non_exhaustive]
pub struct ScryptConfig<TyMeta> {
  pub max_time: ConfigValue<Duration, TyMeta>,
  pub max_mem_frac: ConfigValue<f64, TyMeta>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct StructuralScryptConfig<TyMeta> {
  max_time: ConfigValue<Duration, TyMeta>,
  max_mem_frac: ConfigValue<[u8; 8], TyMeta>,
}

impl<TyMeta> PartialEq for ScryptConfig<TyMeta>
where
  TyMeta: PartialEq,
{
  fn eq(&self, other: &Self) -> bool {
    self.structural() == other.structural()
  }
}

impl<TyMeta> Eq for ScryptConfig<TyMeta> where TyMeta: Eq {}

impl<TyMeta> PartialOrd for ScryptConfig<TyMeta>
where
  TyMeta: PartialOrd,
{
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    self.structural().partial_cmp(&other.structural())
  }
}

impl<TyMeta> Ord for ScryptConfig<TyMeta>
where
  TyMeta: Ord,
{
  fn cmp(&self, other: &Self) -> Ordering {
    self.structural().cmp(&other.structural())
  }
}

impl<TyMeta> ScryptConfig<TyMeta> {
  fn structural(&self) -> StructuralScryptConfig<&TyMeta> {
    StructuralScryptConfig {
      max_time: ConfigValue {
        value: self.max_time.value,
        meta: &self.max_time.meta,
      },
      max_mem_frac: ConfigValue {
        value: self.max_mem_frac.value.to_be_bytes(),
        meta: &self.max_mem_frac.meta,
      },
    }
  }
}

impl<TyMeta> ScryptConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: ScryptConfigPatch, meta: TyMeta) -> Self {
    if let SimplePatch::Set(max_time) = config.max_time {
      self.max_time = ConfigValue::new_meta(max_time, meta.clone());
    }
    if let SimplePatch::Set(max_mem_frac) = config.max_mem_frac {
      self.max_mem_frac = ConfigValue::new_meta(max_mem_frac, meta.clone());
    }
    self
  }
}

impl ScryptConfig<ConfigSource> {
  pub(crate) fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    use BuiltinEternaltwinConfigProfile::*;
    Self {
      max_time: ConfigValue::new_meta(
        match profile {
          Production => Duration::from_millis(500),
          Dev | Sdk | Test => Duration::from_millis(100),
        },
        meta.clone(),
      ),
      max_mem_frac: ConfigValue::new_meta(
        match profile {
          Production => 0.1,
          Dev | Sdk | Test => 0.05,
        },
        meta.clone(),
      ),
    }
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[non_exhaustive]
pub struct ScryptConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub max_time: SimplePatch<Duration>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub max_mem_frac: SimplePatch<f64>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct StructuralScryptConfigPatch {
  max_time: SimplePatch<Duration>,
  max_mem_frac: SimplePatch<[u8; 8]>,
}

impl PartialEq for ScryptConfigPatch {
  fn eq(&self, other: &Self) -> bool {
    self.structural() == other.structural()
  }
}

impl Eq for ScryptConfigPatch {}

impl PartialOrd for ScryptConfigPatch {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

impl Ord for ScryptConfigPatch {
  fn cmp(&self, other: &Self) -> Ordering {
    self.structural().cmp(&other.structural())
  }
}

impl ScryptConfigPatch {
  fn structural(&self) -> StructuralScryptConfigPatch {
    StructuralScryptConfigPatch {
      max_time: self.max_time,
      max_mem_frac: self.max_mem_frac.map(f64::to_be_bytes),
    }
  }
}
