use crate::{
  BuiltinEternaltwinConfigProfile, ConfigField, ConfigFormat, ConfigMeta, ConfigValue, DebugConfigValue,
  EternaltwinConfigProfile, LoadConfigSource,
};
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ConfigRoot<TyMeta = LoadConfigSource> {
  Data {
    data: ConfigValue<String, TyMeta>,
    format: ConfigValue<ConfigFormat, TyMeta>,
  },
  Url {
    url: ConfigValue<String, TyMeta>,
    format: ConfigValue<ConfigFormat, TyMeta>,
  },
  Search {
    patterns: ConfigValue<Vec<String>, TyMeta>,
    format: ConfigValue<ConfigFormat, TyMeta>,
  },
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MetaLoadConfig {
  working_dir: DebugConfigValue<PathBuf>,
  profile: ConfigValue<EternaltwinConfigProfile, LoadConfigSource>,
  env_config: ConfigValue<bool, LoadConfigSource>,
  config_roots: DebugConfigValue<Vec<ConfigRoot>>,
}

impl MetaLoadConfig {
  pub fn new(working_directory: PathBuf) -> Self {
    Self {
      working_dir: ConfigValue {
        value: working_directory.clone(),
        meta: ConfigMeta {
          default: working_directory,
          source: LoadConfigSource::Default(BuiltinEternaltwinConfigProfile::Dev),
        },
      },
      profile: ConfigValue {
        value: Profile::DEFAULT_VALUE,
        meta: LoadConfigSource::Default(BuiltinEternaltwinConfigProfile::Dev),
      },
      env_config: ConfigValue {
        value: EnvConfig::DEFAULT_VALUE,
        meta: LoadConfigSource::Default(BuiltinEternaltwinConfigProfile::Dev),
      },
      config_roots: ConfigValue {
        value: ConfigRoots::default_value(&EternaltwinConfigProfile::Dev),
        meta: ConfigMeta {
          default: ConfigRoots::default_value(&EternaltwinConfigProfile::Dev),
          source: LoadConfigSource::Default(BuiltinEternaltwinConfigProfile::Dev),
        },
      },
    }
  }

  pub fn get<'a, F, V>(&'a self, field: &F) -> V
  where
    F: ConfigField<&'a Self, V>,
  {
    field.get(self)
  }

  pub fn arg_env_config(mut self, env_config: bool) -> Self {
    self.env_config.value = env_config;
    self.env_config.meta = LoadConfigSource::Args;
    self
  }

  pub fn arg_profile(mut self, profile: EternaltwinConfigProfile) -> Self {
    self.profile.value = profile;
    self.profile.meta = LoadConfigSource::Args;
    self.on_profile_change();
    self
  }

  pub fn env_profile(mut self, profile: EternaltwinConfigProfile) -> Self {
    assert!(self.env_config.value);
    self.profile.value = profile;
    self.profile.meta = LoadConfigSource::Env;
    self.on_profile_change();
    self
  }

  pub fn refresh_env_config_from_profile(mut self) -> Self {
    if matches!(self.profile.value, EternaltwinConfigProfile::Sdk)
      && !matches!(self.env_config.meta, LoadConfigSource::Args)
      && !matches!(self.profile.meta, LoadConfigSource::Env)
    {
      self.env_config.value = false;
      self.env_config.meta = LoadConfigSource::Default(BuiltinEternaltwinConfigProfile::Sdk);
    }
    self
  }

  fn on_profile_change(&mut self) {
    let base_profile = self.profile.value.as_builtin_or_dev();
    if matches!(self.working_dir.meta.source, LoadConfigSource::Default(_)) {
      self.working_dir.meta.source = LoadConfigSource::Default(base_profile);
    }
    if matches!(self.env_config.meta, LoadConfigSource::Default(_)) {
      self.env_config.meta = LoadConfigSource::Default(base_profile);
    }
    if matches!(self.config_roots.meta.source, LoadConfigSource::Default(_)) {
      self.config_roots.meta.source = LoadConfigSource::Default(base_profile);
      self.config_roots.meta.default = ConfigRoots::default_value(&self.profile.value);
      self.config_roots.value = ConfigRoots::default_value(&self.profile.value);
    }
  }

  pub fn set_config_roots(mut self, roots: Vec<ConfigRoot>, source: LoadConfigSource) -> Self {
    self.config_roots.value = roots;
    self.config_roots.meta.source = source;
    self
  }
}

pub struct Profile;
impl<'a> ConfigField<&'a MetaLoadConfig, DebugConfigValue<&'a EternaltwinConfigProfile>> for Profile {
  fn path(&self) -> &[&str] {
    &["profile"]
  }

  fn get(&self, container: &'a MetaLoadConfig) -> DebugConfigValue<&'a EternaltwinConfigProfile> {
    DebugConfigValue {
      value: &container.profile.value,
      meta: ConfigMeta {
        default: &Self::DEFAULT_VALUE,
        source: container.profile.meta,
      },
    }
  }
}
impl Profile {
  pub const DEFAULT_VALUE: EternaltwinConfigProfile = EternaltwinConfigProfile::Dev;
}

pub struct EnvConfig;
impl<'a> ConfigField<&'a MetaLoadConfig, DebugConfigValue<bool>> for EnvConfig {
  fn path(&self) -> &[&str] {
    &["env_config"]
  }

  fn get(&self, container: &'a MetaLoadConfig) -> DebugConfigValue<bool> {
    let mut val = DebugConfigValue {
      value: container.env_config.value,
      meta: ConfigMeta {
        default: Self::DEFAULT_VALUE,
        source: container.env_config.meta,
      },
    };
    if matches!(val.meta.source, LoadConfigSource::Default(_)) {
      val.meta.default = val.value;
    }
    val
  }
}
impl EnvConfig {
  pub const DEFAULT_VALUE: bool = true;
}

pub struct WorkingDir;
impl<'a> ConfigField<&'a MetaLoadConfig, DebugConfigValue<&'a PathBuf>> for WorkingDir {
  fn path(&self) -> &[&str] {
    &["working_dir"]
  }

  fn get(&self, container: &'a MetaLoadConfig) -> DebugConfigValue<&'a PathBuf> {
    DebugConfigValue {
      value: &container.working_dir.value,
      meta: ConfigMeta {
        default: &container.working_dir.meta.default,
        source: container.working_dir.meta.source,
      },
    }
  }
}

pub struct ConfigRoots;
impl<'a> ConfigField<&'a MetaLoadConfig, DebugConfigValue<&'a [ConfigRoot]>> for ConfigRoots {
  fn path(&self) -> &[&str] {
    &["config_roots"]
  }

  fn get(&self, container: &'a MetaLoadConfig) -> DebugConfigValue<&'a [ConfigRoot]> {
    DebugConfigValue {
      value: &container.config_roots.value,
      meta: ConfigMeta {
        default: &container.config_roots.meta.default,
        source: container.config_roots.meta.source,
      },
    }
  }
}

impl ConfigRoots {
  pub fn default_value(profile: &EternaltwinConfigProfile) -> Vec<ConfigRoot> {
    if matches!(profile, EternaltwinConfigProfile::Sdk) {
      Vec::new()
    } else {
      let profile_str = profile.as_str();
      vec![ConfigRoot::Search {
        patterns: ConfigValue {
          value: vec![
            format!("./eternaltwin.{profile_str}.local.toml"),
            format!("./eternaltwin.{profile_str}.local.json"),
            format!("./eternaltwin.{profile_str}.toml"),
            format!("./eternaltwin.{profile_str}.json"),
            "./eternaltwin.local.toml".to_string(),
            "./eternaltwin.local.json".to_string(),
            "./eternaltwin.toml".to_string(),
            "./eternaltwin.json".to_string(),
          ],
          meta: LoadConfigSource::Default(profile.as_builtin_or_dev()),
        },
        format: ConfigValue {
          value: ConfigFormat::Auto,
          meta: LoadConfigSource::Default(profile.as_builtin_or_dev()),
        },
      }]
    }
  }
}
