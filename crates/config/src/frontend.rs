use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct FrontendConfig<TyMeta> {
  pub port: ConfigValue<u16, TyMeta>,
  pub uri: ConfigValue<Url, TyMeta>,
  pub forum_posts_per_page: ConfigValue<u16, TyMeta>,
  pub forum_threads_per_page: ConfigValue<u16, TyMeta>,
}

impl<TyMeta> FrontendConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: FrontendConfigPatch, meta: TyMeta) -> Self {
    if let SimplePatch::Set(port) = config.port {
      self.port = ConfigValue::new_meta(port, meta.clone());
      self.uri = ConfigValue::new_meta(
        {
          let mut u = Url::parse("http://localhost/").expect("computed `frontend.uri` is valid");
          u.set_port(Some(self.port.value)).expect("setting the port succeeds");
          u
        },
        meta.clone(),
      );
    }
    if let SimplePatch::Set(uri) = config.uri {
      self.uri = ConfigValue::new_meta(uri, meta.clone());
    }
    if let SimplePatch::Set(forum_posts_per_page) = config.forum_posts_per_page {
      self.forum_posts_per_page = ConfigValue::new_meta(forum_posts_per_page, meta.clone());
    }
    if let SimplePatch::Set(forum_threads_per_page) = config.forum_threads_per_page {
      self.forum_threads_per_page = ConfigValue::new_meta(forum_threads_per_page, meta.clone());
    }
    self
  }
}

impl FrontendConfig<ConfigSource> {
  pub(crate) fn default_for_profile(_profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    Self {
      port: ConfigValue::new_meta(50321, meta.clone()),
      uri: ConfigValue::new_meta(
        Url::parse("http://localhost:50321/").expect("default `frontend.uri` is valid"),
        meta.clone(),
      ),
      forum_posts_per_page: ConfigValue::new_meta(10, meta.clone()),
      forum_threads_per_page: ConfigValue::new_meta(20, meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct FrontendConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub port: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub uri: SimplePatch<Url>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub forum_posts_per_page: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub forum_threads_per_page: SimplePatch<u16>,
}
