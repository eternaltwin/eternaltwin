pub mod app;
pub mod forum_section;
pub mod user;

use self::app::AppConfig;
use self::forum_section::ForumSectionConfig;
use self::user::UserConfig;
use crate::seed::app::PartialAppConfig;
use crate::seed::forum_section::PartialForumSectionConfig;
use crate::seed::user::PartialUserConfig;
use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::core::LocaleId;
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::user::{UserDisplayName, Username};
use serde::{Deserialize, Serialize};
use std::collections::btree_map::Entry;
use std::collections::BTreeMap;
use std::str::FromStr;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct SeedConfig<TyMeta> {
  pub user: ConfigValue<BTreeMap<String, UserConfig<TyMeta>>, TyMeta>,
  pub app: ConfigValue<BTreeMap<String, AppConfig<TyMeta>>, TyMeta>,
  pub forum_section: ConfigValue<BTreeMap<String, ForumSectionConfig<TyMeta>>, TyMeta>,
}

impl<TyMeta> SeedConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: SeedConfigPatch, meta: TyMeta) -> Self {
    if let SimplePatch::Set(patch) = config.user {
      match patch {
        None => self.user.value.clear(),
        Some(patch) => {
          for (key, patch) in patch {
            match patch {
              None => drop(self.user.value.remove(&key)),
              Some(patch) => {
                match self.user.value.entry(key) {
                  Entry::Vacant(e) => {
                    let c = UserConfig::default_for_key(e.key(), meta.clone());
                    e.insert(c).patch_ref(patch, meta.clone())
                  }
                  Entry::Occupied(mut e) => e.get_mut().patch_ref(patch, meta.clone()),
                };
              }
            }
          }
        }
      }
    }
    if let SimplePatch::Set(patch) = config.app {
      match patch {
        None => self.app.value.clear(),
        Some(patch) => {
          for (key, patch) in patch {
            match patch {
              None => drop(self.app.value.remove(&key)),
              Some(patch) => {
                match self.app.value.entry(key) {
                  Entry::Vacant(e) => {
                    let c = AppConfig::default_for_key(e.key(), meta.clone());
                    e.insert(c).patch_ref(patch, meta.clone())
                  }
                  Entry::Occupied(mut e) => e.get_mut().patch_ref(patch, meta.clone()),
                };
              }
            }
          }
        }
      }
    }
    if let SimplePatch::Set(patch) = config.forum_section {
      match patch {
        None => self.forum_section.value.clear(),
        Some(patch) => {
          for (key, patch) in patch {
            match patch {
              None => drop(self.forum_section.value.remove(&key)),
              Some(patch) => {
                match self.forum_section.value.entry(key) {
                  Entry::Vacant(e) => {
                    let c = ForumSectionConfig::default_for_key(e.key(), meta.clone());
                    e.insert(c).patch_ref(patch, meta.clone())
                  }
                  Entry::Occupied(mut e) => e.get_mut().patch_ref(patch, meta.clone()),
                };
              }
            }
          }
        }
      }
    }
    self
  }
}

impl SeedConfig<ConfigSource> {
  pub(crate) fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    use BuiltinEternaltwinConfigProfile::*;
    Self {
      user: ConfigValue::new_meta(
        match profile {
          Production | Test => BTreeMap::new(),
          Dev | Sdk => [
            (
              "alice".to_string(),
              UserConfig {
                id: ConfigValue::new_meta(None, meta.clone()),
                display_name: ConfigValue::new_meta(
                  UserDisplayName::from_str("Alice").expect("default seed user display_name is valid"),
                  meta.clone(),
                ),
                username: ConfigValue::new_meta(
                  Some(Username::from_str("alice").expect("default seed user username is valid")),
                  meta.clone(),
                ),
                password: ConfigValue::new_meta(Some("aaaaaaaaaa".to_string()), meta.clone()),
                is_administrator: ConfigValue::new_meta(true, meta.clone()),
              },
            ),
            (
              "bob".to_string(),
              UserConfig {
                id: ConfigValue::new_meta(None, meta.clone()),
                display_name: ConfigValue::new_meta(
                  UserDisplayName::from_str("Bob").expect("default seed user display_name is valid"),
                  meta.clone(),
                ),
                username: ConfigValue::new_meta(
                  Some(Username::from_str("bob").expect("default seed user username is valid")),
                  meta.clone(),
                ),
                password: ConfigValue::new_meta(Some("bbbbbbbbbb".to_string()), meta.clone()),
                is_administrator: ConfigValue::new_meta(false, meta.clone()),
              },
            ),
            (
              "charlie".to_string(),
              UserConfig {
                id: ConfigValue::new_meta(None, meta.clone()),
                display_name: ConfigValue::new_meta(
                  UserDisplayName::from_str("Charlie").expect("default seed user display_name is valid"),
                  meta.clone(),
                ),
                username: ConfigValue::new_meta(
                  Some(Username::from_str("charlie").expect("default seed user username is valid")),
                  meta.clone(),
                ),
                password: ConfigValue::new_meta(Some("cccccccccc".to_string()), meta.clone()),
                is_administrator: ConfigValue::new_meta(false, meta.clone()),
              },
            ),
            (
              "dan".to_string(),
              UserConfig {
                id: ConfigValue::new_meta(None, meta.clone()),
                display_name: ConfigValue::new_meta(
                  UserDisplayName::from_str("Dan").expect("default seed user display_name is valid"),
                  meta.clone(),
                ),
                username: ConfigValue::new_meta(
                  Some(Username::from_str("dan").expect("default seed user username is valid")),
                  meta.clone(),
                ),
                password: ConfigValue::new_meta(Some("dddddddddd".to_string()), meta.clone()),
                is_administrator: ConfigValue::new_meta(false, meta.clone()),
              },
            ),
            (
              "eve".to_string(),
              UserConfig {
                id: ConfigValue::new_meta(None, meta.clone()),
                display_name: ConfigValue::new_meta(
                  UserDisplayName::from_str("Eve").expect("default seed user display_name is valid"),
                  meta.clone(),
                ),
                username: ConfigValue::new_meta(
                  Some(Username::from_str("eve").expect("default seed user username is valid")),
                  meta.clone(),
                ),
                password: ConfigValue::new_meta(Some("eeeeeeeeee".to_string()), meta.clone()),
                is_administrator: ConfigValue::new_meta(false, meta.clone()),
              },
            ),
            (
              "frank".to_string(),
              UserConfig {
                id: ConfigValue::new_meta(None, meta.clone()),
                display_name: ConfigValue::new_meta(
                  UserDisplayName::from_str("Frank").expect("default seed user display_name is valid"),
                  meta.clone(),
                ),
                username: ConfigValue::new_meta(
                  Some(Username::from_str("frank").expect("default seed user username is valid")),
                  meta.clone(),
                ),
                password: ConfigValue::new_meta(Some("ffffffffff".to_string()), meta.clone()),
                is_administrator: ConfigValue::new_meta(false, meta.clone()),
              },
            ),
          ]
          .into_iter()
          .collect(),
        },
        meta.clone(),
      ),
      app: ConfigValue::new_meta(
        match profile {
          Production | Test => BTreeMap::new(),
          Dev | Sdk => [
            (
              "brute_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("LaBrute".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://localhost:3000/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://localhost:3000/oauth/callback").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
            (
              "emush_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("eMush".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://emush.localhost/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://emush.localhost/oauth/callback").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
            (
              "eternalfest_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("Eternalfest".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://localhost:50313/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://localhost:50313/oauth/callback").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
            (
              "kadokadeo_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("Kadokadeo".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://kadokadeo.localhost/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://kadokadeo.localhost/oauth/callback").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
            (
              "kingdom_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("Kingdom".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://localhost:8000/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://localhost:8000/oauth/callback").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
            (
              "myhordes_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("MyHordes".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://myhordes.localhost/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://myhordes.localhost/twinoid").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
            (
              "neoparc_dev".to_string(),
              AppConfig {
                display_name: ConfigValue::new_meta("NeoParc".to_string(), meta.clone()),
                uri: ConfigValue::new_meta(Url::parse("http://localhost:8880/").unwrap(), meta.clone()),
                oauth_callback: ConfigValue::new_meta(
                  Url::parse("http://localhost:8880/api/account/callback").unwrap(),
                  meta.clone(),
                ),
                secret: ConfigValue::new_meta("dev".to_string(), meta.clone()),
              },
            ),
          ]
          .into_iter()
          .collect(),
        },
        meta.clone(),
      ),
      forum_section: ConfigValue::new_meta(
        match profile {
          Production | Test => BTreeMap::new(),
          Dev | Sdk => [
            (
              "main_en".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("Main Forum (en-US)".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("en-US").unwrap()), meta.clone()),
              },
            ),
            (
              "main_fr".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("Forum Général (fr-FR)".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "main_es".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("Foro principal (es-SP)".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("es-SP").unwrap()), meta.clone()),
              },
            ),
            (
              "main_de".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("Hauptforum (de-DE)".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("de-DE").unwrap()), meta.clone()),
              },
            ),
            (
              "main_eo".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("Ĉefa forumo (eo)".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("eo").unwrap()), meta.clone()),
              },
            ),
            (
              "eternalfest_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Eternalfest] Le Panthéon".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "emush_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[eMush] Neron is watching you".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "drpg_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[DinoRPG] Jurassic Park".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "myhordes_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Myhordes] Le Saloon".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "kadokadeo_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Kadokadeo] Café des palabres".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "kingdom_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Kingdom] La foire du trône".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "na_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Naturalchimie] Le laboratoire".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "sq_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Studioquiz] Le bar à questions".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "ts_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Teacher Story] La salle des profs".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
            (
              "popotamo_main".to_string(),
              ForumSectionConfig {
                display_name: ConfigValue::new_meta("[Popotamo] Le mot le plus long".to_string(), meta.clone()),
                locale: ConfigValue::new_meta(Some(LocaleId::from_str("fr-FR").unwrap()), meta.clone()),
              },
            ),
          ]
          .into_iter()
          .collect(),
        },
        meta.clone(),
      ),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct SeedConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub user: SimplePatch<Option<BTreeMap<String, Option<PartialUserConfig>>>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub app: SimplePatch<Option<BTreeMap<String, Option<PartialAppConfig>>>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub forum_section: SimplePatch<Option<BTreeMap<String, Option<PartialForumSectionConfig>>>>,
}
