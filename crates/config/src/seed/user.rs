use crate::ConfigValue;
use eternaltwin_core::patch::SimplePatch;
use eternaltwin_core::user::{UserDisplayName, UserId, Username};
use serde::{Deserialize, Serialize};
use std::str::FromStr;

/// Seed user are handled the following way.
/// First, a reference is selected to detect if we perform an insert or update:
/// 1. If `id` is provided, the id is used.
/// 2. If `username` is provided, the username is used.
/// 3. Otherwise, the key is used to match on the username.
///
/// If a user matches the ref, it is updated. Otherwise, it is inserted.
/// If a conflict occurs (e.g. `username` already used), an error is raised.
///
/// Seed users are processed in order, sorted by their key.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct UserConfig<TyMeta> {
  pub id: ConfigValue<Option<UserId>, TyMeta>,
  pub display_name: ConfigValue<UserDisplayName, TyMeta>,
  pub username: ConfigValue<Option<Username>, TyMeta>,
  pub password: ConfigValue<Option<String>, TyMeta>,
  pub is_administrator: ConfigValue<bool, TyMeta>,
}

impl<TyMeta> UserConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch_ref(&mut self, config: PartialUserConfig, meta: TyMeta) {
    if let SimplePatch::Set(id) = config.id {
      self.id = ConfigValue::new_meta(id, meta.clone());
    }
    if let SimplePatch::Set(display_name) = config.display_name {
      self.display_name = ConfigValue::new_meta(display_name, meta.clone());
    }
    if let SimplePatch::Set(username) = config.username {
      self.username = ConfigValue::new_meta(username, meta.clone());
    }
    if let SimplePatch::Set(password) = config.password {
      self.password = ConfigValue::new_meta(password, meta.clone());
    }
    if let SimplePatch::Set(is_administrator) = config.is_administrator {
      self.is_administrator = ConfigValue::new_meta(is_administrator, meta.clone());
    }
  }

  pub(crate) fn default_for_key(key: &str, meta: TyMeta) -> Self {
    Self {
      id: ConfigValue::new_meta(None, meta.clone()),
      display_name: ConfigValue::new_meta(
        UserDisplayName::from_str(key).expect("seed user key should be a valid user display name"),
        meta.clone(),
      ),
      username: ConfigValue::new_meta(None, meta.clone()),
      password: ConfigValue::new_meta(None, meta.clone()),
      is_administrator: ConfigValue::new_meta(false, meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PartialUserConfig {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub id: SimplePatch<Option<UserId>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub display_name: SimplePatch<UserDisplayName>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub username: SimplePatch<Option<Username>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub password: SimplePatch<Option<String>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub is_administrator: SimplePatch<bool>,
}
