use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, UserDot};
use eternaltwin_core::link::store::{LinkStore, LinkStoreRef};
use eternaltwin_core::link::{EtwinLink, GetLinkOptions, VersionedEtwinLink, VersionedRawLink};
use eternaltwin_core::twinoid::store::get_scores::RawGetScoresError;
use eternaltwin_core::twinoid::store::{self, GetRewardsError, GetScoresError, RawGetRewardsError, TwinoidStoreRef};
use eternaltwin_core::twinoid::{
  ArchivedTwinoidUser, EtwinTwinoidUser, GetTwinoidRewardsOptions, GetTwinoidScoresOptions, GetTwinoidUserOptions,
  TwinoidReward, TwinoidScore, TwinoidSiteIdRef, TwinoidStat, TwinoidStatsAndRewards, TwinoidStore, TwinoidUserIdRef,
};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{GetShortUserOptions, RawGetShortUserError, ShortUser, UserRef, UserStore, UserStoreRef};
use std::sync::Arc;
use thiserror::Error;

pub struct TwinoidService<TyClock, TyLinkStore, TyTwinoidStore, TyUserStore>
where
  TyClock: ClockRef,
  TyLinkStore: LinkStoreRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  clock: TyClock,
  link_store: TyLinkStore,
  twinoid_store: TyTwinoidStore,
  user_store: TyUserStore,
}

pub type DynTwinoidService =
  TwinoidService<Arc<dyn Clock>, Arc<dyn LinkStore>, Arc<dyn TwinoidStore>, Arc<dyn UserStore>>;

#[derive(Debug, Error)]
pub enum GetUserError {
  #[error("twinoid user not found")]
  NotFound,
  #[error("failed to find user who created the etwin link")]
  GetEtwinLinkedBy(#[source] RawGetShortUserError),
  #[error("failed to find linked etwin user")]
  GetLinkedEtwin(#[source] RawGetShortUserError),
  #[error(transparent)]
  Other(WeakError),
}

impl<TyClock, TyLinkStore, TyTwinoidStore, TyUserStore>
  TwinoidService<TyClock, TyLinkStore, TyTwinoidStore, TyUserStore>
where
  TyClock: ClockRef,
  TyLinkStore: LinkStoreRef,
  TyTwinoidStore: TwinoidStoreRef,
  TyUserStore: UserStoreRef,
{
  pub fn new(clock: TyClock, link_store: TyLinkStore, twinoid_store: TyTwinoidStore, user_store: TyUserStore) -> Self {
    Self {
      clock,
      link_store,
      twinoid_store,
      user_store,
    }
  }

  pub async fn get_user(
    &self,
    _acx: &AuthContext,
    options: &GetTwinoidUserOptions,
  ) -> Result<EtwinTwinoidUser, GetUserError> {
    let time: Instant = options.time.unwrap_or_else(|| self.clock.clock().now());
    let user: ArchivedTwinoidUser = self
      .twinoid_store
      .twinoid_store()
      .get_user(store::GetUser {
        user: TwinoidUserIdRef { id: options.id },
        time,
      })
      .await
      .map_err(|e| match e {
        store::GetUserError::NotFound => GetUserError::NotFound,
        store::GetUserError::Other(e) => GetUserError::Other(e),
      })?;
    let etwin_link: VersionedRawLink<TwinoidUserIdRef> = {
      let options: GetLinkOptions<TwinoidUserIdRef> = GetLinkOptions {
        remote: TwinoidUserIdRef { id: user.id },
        time: None,
      };
      self
        .link_store
        .link_store()
        .get_link_from_twinoid(options)
        .await
        .map_err(GetUserError::Other)?
    };
    let etwin_link: VersionedEtwinLink = {
      let current = match etwin_link.current {
        None => None,
        Some(l) => {
          let user: ShortUser = self
            .user_store
            .user_store()
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.link.user),
              time: options.time,
              skip_deleted: false,
            })
            .await
            .map_err(GetUserError::GetEtwinLinkedBy)?;
          let etwin: ShortUser = self
            .user_store
            .user_store()
            .get_short_user(&GetShortUserOptions {
              r#ref: UserRef::Id(l.etwin),
              time: options.time,
              skip_deleted: false,
            })
            .await
            .map_err(GetUserError::GetLinkedEtwin)?;
          Some(EtwinLink {
            link: UserDot {
              time: l.link.time,
              user,
            },
            unlink: (),
            etwin,
          })
        }
      };
      VersionedEtwinLink { current, old: vec![] }
    };
    Ok(EtwinTwinoidUser {
      id: user.id,
      archived_at: user.archived_at,
      display_name: user.display_name,
      links: user.links,
      etwin: etwin_link,
    })
  }

  pub async fn get_scores(&self, options: &GetTwinoidScoresOptions) -> Result<Vec<TwinoidScore>, GetScoresError> {
    let archived_scores = self
      .twinoid_store
      .twinoid_store()
      .get_scores(store::GetScores {
        user: TwinoidUserIdRef { id: options.id },
      })
      .await
      .map_err(|e| match e {
        RawGetScoresError::Other(e) => GetScoresError::Other(e),
      })?;

    let scores = archived_scores
      .scores
      .iter()
      .map(|reward| TwinoidScore {
        site: TwinoidSiteIdRef { id: reward.site.id },
        points: reward.points.round(),
      })
      .collect();

    Ok(scores)
  }

  pub async fn get_rewards(
    &self,
    options: &GetTwinoidRewardsOptions,
  ) -> Result<TwinoidStatsAndRewards, GetRewardsError> {
    let archived_rewards = self
      .twinoid_store
      .twinoid_store()
      .get_rewards(store::GetRewards {
        user: TwinoidUserIdRef { id: options.user_id },
        site: TwinoidSiteIdRef { id: options.site_id },
      })
      .await
      .map_err(|e| match e {
        RawGetRewardsError::Other(e) => GetRewardsError::Other(e),
      })?;

    let stats = archived_rewards
      .stats
      .iter()
      .map(|stat| TwinoidStat {
        stat_key: stat.stat_key.clone(),
        score: stat.score,
        rarity: stat.rarity,
      })
      .collect();

    let rewards = archived_rewards
      .rewards
      .iter()
      .map(|reward| TwinoidReward {
        achievement_key: reward.achievement_key.clone(),
        points: reward.points.round(),
      })
      .collect();

    Ok(TwinoidStatsAndRewards { stats, rewards })
  }
}
