use eternaltwin_core::core::{FinitePeriod, Instant};
use eternaltwin_core::oauth::OauthClientRef;
use eternaltwin_core::types::WeakError;
use url::Url;

mod generated;

pub trait ErrorInfo {
  const CODE: &'static str;
  const MESSAGE: &'static str;
  fn extra(&self) -> serde_json::Value;
}

/// Weakly typed error info
#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct AnyErrorInfo {
  pub code: String,
  pub message: String,
  pub extra: serde_json::Value,
}

impl AnyErrorInfo {
  pub fn upcast<TyErrorInfo: ErrorInfo>(err: TyErrorInfo) -> Self {
    Self {
      code: String::from(TyErrorInfo::CODE),
      message: String::from(TyErrorInfo::MESSAGE),
      extra: err.extra(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct InternalError(pub WeakError);

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct MissingAuthenticationError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct MissingOauthClientAuthenticationError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct MissingClientIdError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct InvalidClientIdError;

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct RedirectUriMismatchError {
  pub registered: Url,
  pub provided: String,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct OauthClientNotFoundError {
  pub oauth_client: OauthClientRef,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct MissingResponseTypeError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct InvalidResponseTypeError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct UnsupportedResponseTypeError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct InvalidScopeError;

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct OauthCodeMissingError;

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct OauthCodeFormatError(pub WeakError);

#[derive(Debug, Clone, Copy, PartialEq, Eq, serde::Serialize)]
pub struct OauthCodeTimeError {
  pub now: Instant,
  pub period: FinitePeriod,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize)]
pub struct WrongOauthClientError {
  pub registered: String,
}
