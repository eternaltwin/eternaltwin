use crate::job::error_sleep;
use async_trait::async_trait;
use eternaltwin_core::hammerfest::{
  GetActiveSession, GetActiveSessionError, HammerfestClient, HammerfestClientCreateSessionError, HammerfestClientRef,
  HammerfestCredentials, HammerfestSession, HammerfestStore, HammerfestStoreRef, HammerfestUserRef,
  HammerfestUserUsernameRef, TouchSession,
};
use eternaltwin_core::job::{Sleep, Task, TaskCx, TaskPoll};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ReliableHammerfestAcquireSession {
  Start {
    credentials: HammerfestCredentials,
  },
  Fetch {
    failures: u32,
    timeout: Option<Sleep>,
    credentials: HammerfestCredentials,
  },
  Ready(Result<HammerfestSession, HammerfestClientCreateSessionError>),
}

impl ReliableHammerfestAcquireSession {
  pub fn new(credentials: HammerfestCredentials) -> Self {
    Self::Start { credentials }
  }
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ReliableHammerfestAcquireSession
where
  Cx: TaskCx,
  Cx::Extra: HammerfestClientRef + HammerfestStoreRef,
{
  const NAME: &'static str = "ReliableHammerfestAcquireSession";
  const VERSION: u32 = 1;
  type Output = Result<HammerfestSession, HammerfestClientCreateSessionError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start { credentials } => {
          *self = Self::Fetch {
            credentials: credentials.clone(),
            timeout: None,
            failures: 0,
          }
        }
        Self::Fetch {
          credentials,
          timeout,
          failures,
        } => {
          if let Some(sleep) = timeout {
            match sleep.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(()) => *timeout = None,
            }
          }

          let session = cx
            .extra()
            .hammerfest_store()
            .get_active_session(GetActiveSession {
              user: HammerfestUserRef::Username(HammerfestUserUsernameRef {
                server: credentials.server,
                username: credentials.username.clone(),
              }),
              time: cx.now(),
            })
            .await;
          match session {
            Ok(session) => {
              *self = Self::Ready(Ok(session));
              continue;
            }
            Err(GetActiveSessionError::NotFound) => {
              // Ignore
            }
            Err(GetActiveSessionError::Other(e)) => {
              eprintln!("[ReliableHammerfestAcquireSession] {e:#}");
              *failures += 1;
              *timeout = Some(cx.sleep(error_sleep(*failures)));
              continue;
            }
          }
          match cx.extra().hammerfest_client().create_session(credentials).await {
            Ok(session) => {
              match cx
                .extra()
                .hammerfest_store()
                .touch_session(TouchSession {
                  now: cx.now(),
                  server: session.user.server,
                  key: session.key.clone(),
                  user: Some(session.user.clone()),
                })
                .await
              {
                Ok(()) => *self = Self::Ready(Ok(session)),
                Err(e) => {
                  eprintln!("[ReliableHammerfestAcquireSession] {e:#}");
                  *failures += 1;
                  *timeout = Some(cx.sleep(error_sleep(*failures)));
                  continue;
                }
              }
            }
            Err(e @ HammerfestClientCreateSessionError::WrongCredentials) => *self = Self::Ready(Err(e)),
            Err(e) => {
              eprintln!("[ReliableHammerfestAcquireSession] {e:#}");
              *failures += 1;
              *timeout = Some(cx.sleep(error_sleep(*failures)))
            }
          }
        }
        Self::Ready(result) => {
          return TaskPoll::Ready(result.clone());
        }
      }
    }
  }
}
