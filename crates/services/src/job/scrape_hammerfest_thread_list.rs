use crate::job::scrape_hammerfest_thread::{ScrapeHammerfestThread, ScrapeHammerfestThreadError};
use async_trait::async_trait;
use eternaltwin_core::hammerfest::{
  HammerfestClientRef, HammerfestCredentials, HammerfestForumThreadId, HammerfestStoreRef,
};
use eternaltwin_core::job::{Task, TaskCx, TaskPoll};
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::num::NonZeroU16;
use thiserror::Error;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ScrapeHammerfestThreadList {
  Start {
    credentials: HammerfestCredentials,
    // Thread id + message count
    threads: Vec<(HammerfestForumThreadId, NonZeroU16)>,
  },
  Iter {
    credentials: HammerfestCredentials,
    // Thread id + message count
    threads: Vec<(HammerfestForumThreadId, NonZeroU16)>,
    active_thread: Option<Box<ScrapeHammerfestThread>>,
    active_thread_id: Option<HammerfestForumThreadId>,
  },
  Ready(Result<(), ScrapeHammerfestThreadListError>),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error, Serialize, Deserialize)]
pub enum ScrapeHammerfestThreadListError {
  #[error("error while scraping thread {1:?}")]
  // TODO: Make the thread id non-optional
  Thread(#[source] ScrapeHammerfestThreadError, Option<HammerfestForumThreadId>),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[async_trait]
impl<'cx, Cx> Task<&'cx mut Cx> for ScrapeHammerfestThreadList
where
  Cx: TaskCx,
  Cx::Extra: HammerfestClientRef + HammerfestStoreRef,
{
  const NAME: &'static str = "ScrapeHammerfestThreadList";
  const VERSION: u32 = 1;
  type Output = Result<(), ScrapeHammerfestThreadListError>;

  async fn poll(&mut self, cx: &'cx mut Cx) -> TaskPoll<Self::Output> {
    let mut iter = 0;
    loop {
      iter += 1;
      if iter >= 50 {
        return TaskPoll::Pending;
      }
      match self {
        Self::Start { credentials, threads } => {
          *self = Self::Iter {
            credentials: credentials.clone(),
            threads: threads.clone(),
            active_thread_id: None,
            active_thread: None,
          }
        }
        Self::Iter {
          credentials,
          threads,
          active_thread_id,
          active_thread,
        } => {
          if let Some(inner) = active_thread.as_mut() {
            match inner.poll(cx).await {
              TaskPoll::Pending => return TaskPoll::Pending,
              TaskPoll::Ready(Ok(())) => *active_thread = None,
              TaskPoll::Ready(Err(e)) => {
                *self = Self::Ready(Err(ScrapeHammerfestThreadListError::Thread(e, *active_thread_id)));
                continue;
              }
            }
          } else if let Some((id, msg_count)) = threads.pop() {
            *active_thread_id = Some(id);
            *active_thread = Some(Box::new(ScrapeHammerfestThread::Start {
              credentials: credentials.clone(),
              thread_id: id,
              last_page: {
                // 10 messages per page, and one extra message for the first page
                let replies = msg_count.get().saturating_sub(1);
                let mut pages = replies / 10;
                if replies % 10 != 0 {
                  pages += 1;
                }
                // At least one page, even if there are no replies
                pages = core::cmp::max(1, pages);
                NonZeroU16::new(pages).unwrap()
              },
            }));
          } else {
            *self = Self::Ready(Ok(()))
          }
        }
        Self::Ready(result) => {
          return TaskPoll::Ready(result.clone());
        }
      }
    }
  }
}
