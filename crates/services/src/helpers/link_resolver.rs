use async_trait::async_trait;
use eternaltwin_core::core::{Instant, UserDot};
use eternaltwin_core::dinoparc::{
  DinoparcStore, DinoparcUserIdRef, GetDinoparcUserOptions, RawGetShortDinoparcUserError, ShortDinoparcUser,
};
use eternaltwin_core::hammerfest::{
  GetHammerfestUserOptions, HammerfestStore, HammerfestStoreRef, HammerfestUserIdRef, RawGetShortHammerfestUserError,
  ShortHammerfestUser,
};
use eternaltwin_core::link::{Link, VersionedLink, VersionedLinks, VersionedRawLink, VersionedRawLinks};
use eternaltwin_core::twinoid::store::{GetShortUser, TwinoidStoreRef};
use eternaltwin_core::twinoid::{ShortTwinoidUser, TwinoidStore, TwinoidUserIdRef};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{GetShortUserOptions, RawGetShortUserError, UserRef, UserStore};
use thiserror::Error;

#[derive(Debug, PartialEq, Eq, Error)]
pub enum ResolveLinksError {
  #[error("failed to resolve dinoparc link")]
  Dinoparc(#[from] ResolveDinoparcLinkError),
  #[error("failed to resolve hammerfest link")]
  Hammerfest(#[from] ResolveHammerfestLinkError),
  #[error("failed to resolve twinoid link")]
  Twinoid(#[from] ResolveTwinoidLinkError),
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum ResolveDinoparcLinkError {
  #[error("failed to get `linked_by` user")]
  GetLinkedByUser(#[source] RawGetShortUserError),
  #[error("failed to get linked Dinoparc user")]
  GetLinkedDinoparcUser(#[source] RawGetShortDinoparcUserError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum ResolveHammerfestLinkError {
  #[error("failed to get `linked_by` user")]
  GetLinkedByUser(#[source] RawGetShortUserError),
  #[error("failed to get linked Hammerfest user")]
  GetLinkedHammerfestUser(#[source] RawGetShortHammerfestUserError),
  #[error(transparent)]
  Other(WeakError),
}

#[derive(Debug, PartialEq, Eq, Error)]
pub enum ResolveTwinoidLinkError {
  #[error("failed to get `linked_by` user")]
  GetLinkedByUser(#[source] RawGetShortUserError),
  #[error("failed to get linked Twinoid user")]
  GetLinkedTwinoidUser(#[source] eternaltwin_core::twinoid::store::GetShortUserError),
  #[error(transparent)]
  Other(WeakError),
}

#[async_trait]
pub trait LinkResolver: HammerfestStoreRef + TwinoidStoreRef {
  type DinoparcStore: DinoparcStore + ?Sized;
  type UserStore: UserStore + ?Sized;

  fn dinoparc_store(&self) -> &Self::DinoparcStore;
  fn user_store(&self) -> &Self::UserStore;

  async fn resolve_links(
    &self,
    raw_links: VersionedRawLinks,
    time: Instant,
  ) -> Result<VersionedLinks, ResolveLinksError> {
    Ok(VersionedLinks {
      dinoparc_com: self.resolve_dinoparc_link(raw_links.dinoparc_com, time).await?,
      en_dinoparc_com: self.resolve_dinoparc_link(raw_links.en_dinoparc_com, time).await?,
      hammerfest_es: self.resolve_hammerfest_link(raw_links.hammerfest_es, time).await?,
      hammerfest_fr: self.resolve_hammerfest_link(raw_links.hammerfest_fr, time).await?,
      hfest_net: self.resolve_hammerfest_link(raw_links.hfest_net, time).await?,
      sp_dinoparc_com: self.resolve_dinoparc_link(raw_links.sp_dinoparc_com, time).await?,
      twinoid: self.resolve_twinoid_link(raw_links.twinoid, time).await?,
    })
  }

  async fn resolve_dinoparc_link(
    &self,
    raw: VersionedRawLink<DinoparcUserIdRef>,
    time: Instant,
  ) -> Result<VersionedLink<ShortDinoparcUser>, ResolveDinoparcLinkError> {
    let current = match raw.current {
      Some(current) => {
        let linked_by = self
          .user_store()
          .get_short_user(&GetShortUserOptions {
            r#ref: UserRef::Id(current.link.user),
            time: Some(time),
            skip_deleted: false,
          })
          .await
          .map_err(ResolveDinoparcLinkError::GetLinkedByUser)?;
        let remote = self
          .dinoparc_store()
          .get_short_user(&GetDinoparcUserOptions {
            server: current.remote.server,
            id: current.remote.id,
            time: Some(time),
          })
          .await
          .map_err(ResolveDinoparcLinkError::GetLinkedDinoparcUser)?;
        Some(Link {
          link: UserDot {
            time: current.link.time,
            user: linked_by,
          },
          unlink: (),
          remote,
        })
      }
      None => None,
    };
    Ok(VersionedLink { current, old: vec![] })
  }

  async fn resolve_hammerfest_link(
    &self,
    raw: VersionedRawLink<HammerfestUserIdRef>,
    time: Instant,
  ) -> Result<VersionedLink<ShortHammerfestUser>, ResolveHammerfestLinkError> {
    let current = match raw.current {
      Some(current) => {
        let linked_by = self
          .user_store()
          .get_short_user(&GetShortUserOptions {
            r#ref: UserRef::Id(current.link.user),
            time: Some(time),
            skip_deleted: false,
          })
          .await
          .map_err(ResolveHammerfestLinkError::GetLinkedByUser)?;
        let remote = self
          .hammerfest_store()
          .get_short_user(&GetHammerfestUserOptions {
            server: current.remote.server,
            id: current.remote.id,
            time: Some(time),
          })
          .await
          .map_err(ResolveHammerfestLinkError::GetLinkedHammerfestUser)?;
        Some(Link {
          link: UserDot {
            time: current.link.time,
            user: linked_by,
          },
          unlink: (),
          remote,
        })
      }
      None => None,
    };
    Ok(VersionedLink { current, old: vec![] })
  }

  async fn resolve_twinoid_link(
    &self,
    raw: VersionedRawLink<TwinoidUserIdRef>,
    time: Instant,
  ) -> Result<VersionedLink<ShortTwinoidUser>, ResolveTwinoidLinkError> {
    let current = match raw.current {
      Some(current) => {
        let linked_by = self
          .user_store()
          .get_short_user(&GetShortUserOptions {
            r#ref: UserRef::Id(current.link.user),
            time: Some(time),
            skip_deleted: false,
          })
          .await
          .map_err(ResolveTwinoidLinkError::GetLinkedByUser)?;
        let remote = self
          .twinoid_store()
          .get_short_user(GetShortUser {
            user: current.remote,
            time,
          })
          .await
          .map_err(ResolveTwinoidLinkError::GetLinkedTwinoidUser)?;
        Some(Link {
          link: UserDot {
            time: current.link.time,
            user: linked_by,
          },
          unlink: (),
          remote,
        })
      }
      None => None,
    };
    Ok(VersionedLink { current, old: vec![] })
  }
}
