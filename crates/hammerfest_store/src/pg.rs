use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, PeriodLower, SecretString};
use eternaltwin_core::email::touch_email_address;
use eternaltwin_core::hammerfest::{
  hammerfest_reply_count_to_page_count, GetActiveSession, GetActiveSessionError, GetHammerfestUserOptions,
  HammerfestDate, HammerfestDateTime, HammerfestForumPostId, HammerfestForumRole, HammerfestForumThemeDescription,
  HammerfestForumThemeId, HammerfestForumThemeIdRef, HammerfestForumThemePageResponse, HammerfestForumThemeTitle,
  HammerfestForumThreadIdRef, HammerfestForumThreadKind, HammerfestForumThreadPageResponse, HammerfestForumThreadTitle,
  HammerfestGodchildrenResponse, HammerfestInventoryResponse, HammerfestItemId, HammerfestLadderLevel,
  HammerfestProfileResponse, HammerfestQuestId, HammerfestQuestStatus, HammerfestServer, HammerfestSession,
  HammerfestSessionKey, HammerfestSessionUser, HammerfestShop, HammerfestShopResponse, HammerfestStore,
  HammerfestUserId, HammerfestUserIdRef, HammerfestUserRef, HammerfestUsername, RawGetHammerfestUserError,
  RawGetShortHammerfestUserError, ShortHammerfestUser, StoredHammerfestProfile, StoredHammerfestUser, TouchEvniUser,
  TouchHammerfestEvniUserError, TouchHammerfestGodchildrenError, TouchHammerfestInventoryError,
  TouchHammerfestProfileError, TouchHammerfestSessionError, TouchHammerfestShopError, TouchHammerfestThemePageError,
  TouchHammerfestThreadPageError, TouchHammerfestUserError, TouchSession,
};
use eternaltwin_core::pg_num::PgU32;
use eternaltwin_core::temporal::{ForeignRetrieved, ForeignSnapshot, LatestTemporal};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternaltwin_populate::hammerfest::populate_hammerfest;
use eternaltwin_populate::hammerfest::PopulateHammerfestError;
use eternaltwin_postgres_tools::upsert_archive_query;
use sha3::{Digest, Sha3_256};
use sqlx::postgres::PgQueryResult;
use sqlx::types::Uuid;
use sqlx::{PgPool, Postgres, Transaction};
use std::collections::{BTreeMap, BTreeSet};
use std::convert::TryInto;
use std::num::NonZeroU16;

pub struct PgHammerfestStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  database_secret: SecretString,
  uuid_generator: TyUuidGenerator,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum NewPgHammerfestStoreError {
  #[error("failed to begin transaction")]
  BeginTx(#[source] WeakError),
  #[error("failed to populate dinoparc tables")]
  PopulateHammerfest(#[from] PopulateHammerfestError),
  #[error("failed to commit transaction")]
  CommitTx(#[source] WeakError),
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgHammerfestStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub async fn new(
    clock: TyClock,
    database: TyDatabase,
    database_secret: SecretString,
    uuid_generator: TyUuidGenerator,
  ) -> Result<Self, NewPgHammerfestStoreError> {
    let mut tx = database
      .begin()
      .await
      .map_err(|e| NewPgHammerfestStoreError::CommitTx(WeakError::wrap(e)))?;
    populate_hammerfest(&mut tx).await?;
    tx.commit()
      .await
      .map_err(|e| NewPgHammerfestStoreError::CommitTx(WeakError::wrap(e)))?;
    Ok(Self {
      clock,
      database,
      database_secret,
      uuid_generator,
    })
  }
}

#[derive(Debug, thiserror::Error)]
pub enum InnerTouchHammerfestUserError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_user(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: &ShortHammerfestUser,
) -> Result<(), InnerTouchHammerfestUserError> {
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r"
      INSERT
      INTO hammerfest_users(hammerfest_server, hammerfest_user_id, username, archived_at)
      VALUES (
        $1::HAMMERFEST_SERVER, $2::HAMMERFEST_USER_ID, $3::HAMMERFEST_USERNAME, $4::INSTANT
      )
      ON CONFLICT (hammerfest_server, hammerfest_user_id) DO UPDATE SET username = $3::HAMMERFEST_USERNAME;
      ",
  )
  .bind(user.server)
  .bind(user.id)
  .bind(user.username.as_str())
  .bind(now)
  .execute(&mut **tx)
  .await
  .map_err(|e| InnerTouchHammerfestUserError::Query(WeakError::wrap(e)))?;
  assert_eq!(res.rows_affected(), 1);
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchEvniUserError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_evni_user(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  server: HammerfestServer,
  user_id: HammerfestUserId,
) -> Result<(), TouchEvniUserError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_evni_user(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 is_evni::BOOLEAN),
    )
  ))
  .bind(now)
  .bind(server)
  .bind(user_id)
  .bind(true)
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchEvniUserError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  assert_eq!(res.rows_affected(), 1);

  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum InnerTouchHammerfestSessionError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_session(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  server: HammerfestServer,
  key: HammerfestSessionKey,
  user: Option<HammerfestUserId>,
  db_secret: &SecretString,
) -> Result<(), InnerTouchHammerfestSessionError> {
  let hash = Sha3_256::digest(key.as_str().as_bytes());
  let hash = hash.as_slice();

  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_session(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 _hammerfest_session_key_hash::BYTEA),
      data($4 hammerfest_user_id::HAMMERFEST_USER_ID),
      secret($5, $6 hammerfest_session_key),
      unique(user_session(hammerfest_server, hammerfest_user_id)),
    )
  ))
  .bind(now)
  .bind(server)
  .bind(hash)
  .bind(user)
  .bind(db_secret.as_str())
  .bind(key.as_str())
  .execute(&mut **tx)
  .await
  .map_err(|e| InnerTouchHammerfestSessionError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (first insert), 1 invalidated (user_session)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  // 3 : 1 inserted (data change), 1 invalidated (primary), 1 invalidated (user_session)
  assert!((1..=3u64).contains(&res.rows_affected()));

  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThemeError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_theme(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  server: HammerfestServer,
  id: HammerfestForumThemeId,
  title: &HammerfestForumThemeTitle,
  description: Option<&HammerfestForumThemeDescription>,
  is_public: bool,
) -> Result<(), TouchHammerfestForumThemeError> {
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r"
      INSERT
      INTO hammerfest_forum_themes(hammerfest_server, hammerfest_theme_id, archived_at, title, description, is_public)
      VALUES (
        $1::HAMMERFEST_SERVER, $2::HAMMERFEST_FORUM_THEME_ID, $3::INSTANT, $4::HAMMERFEST_FORUM_THEME_TITLE, $5::HAMMERFEST_FORUM_THEME_DESCRIPTION, $6::BOOLEAN
      )
      ON CONFLICT (hammerfest_server, hammerfest_theme_id)
        DO UPDATE SET
          title = $4::HAMMERFEST_FORUM_THEME_TITLE,
          description = COALESCE(EXCLUDED.description, $5::HAMMERFEST_FORUM_THEME_DESCRIPTION),
          is_public = $6::BOOLEAN;
      ",
  )
    .bind(server)
    .bind(id)
    .bind(now)
    .bind(title)
    .bind(description)
    .bind(is_public)
    .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumThemeError::Query(WeakError::wrap(e)))?;
  assert_eq!(res.rows_affected(), 1);
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThemeCountError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_theme_count(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  theme: HammerfestForumThemeIdRef,
  page_count: NonZeroU16,
) -> Result<(), TouchHammerfestForumThemeCountError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_theme_counts(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_theme_id::HAMMERFEST_FORUM_THEME_ID),
      data($4 page_count::U16),
    )
  ))
  .bind(now)
  .bind(theme.server)
  .bind(theme.id)
  .bind(i32::from(page_count.get()))
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestForumThemeCountError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum ThemePage {
  Sticky,
  Regular(NonZeroU16),
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThemePageCountError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_theme_page_count(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  theme: HammerfestForumThemeIdRef,
  page: ThemePage,
  thread_count: u8,
) -> Result<(), TouchHammerfestForumThemePageCountError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_theme_page_counts(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_theme_id::HAMMERFEST_FORUM_THEME_ID, $4 page::U16),
      data($5 thread_count::U8),
    )
  ))
  .bind(now)
  .bind(theme.server)
  .bind(theme.id)
  .bind(match page {
    ThemePage::Sticky => 0,
    ThemePage::Regular(page) => i32::from(page.get()),
  })
  .bind(i16::from(thread_count))
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestForumThemePageCountError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThreadPageCountError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_thread_page_count(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  thread: HammerfestForumThreadIdRef,
  page: NonZeroU16,
  post_count: u8,
) -> Result<(), TouchHammerfestForumThreadPageCountError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_thread_page_counts(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_thread_id::HAMMERFEST_FORUM_THREAD_ID, $4 page::U16),
      data($5 post_count::U8),
    )
  ))
  .bind(now)
  .bind(thread.server)
  .bind(thread.id)
  .bind(i32::from(page.get()))
  .bind(i16::from(post_count))
  .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumThreadPageCountError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThemePageItemError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_theme_page_item(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  theme: HammerfestForumThemeIdRef,
  page: ThemePage,
  offset: u8,
  thread: HammerfestForumThreadIdRef,
) -> Result<(), TouchHammerfestForumThemePageItemError> {
  assert_eq!(thread.server, theme.server);
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_theme_threads(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_theme_id::HAMMERFEST_FORUM_THEME_ID, $4 page::U16, $5 offset_in_list::U8),
      data($6 hammerfest_thread_id::HAMMERFEST_FORUM_THREAD_ID),
    )
  ))
  .bind(now)
  .bind(theme.server)
  .bind(theme.id)
  .bind(match page {
    ThemePage::Sticky => 0,
    ThemePage::Regular(page) => i32::from(page.get()),
  })
    .bind(i16::from(offset))
  .bind(thread.id)
  .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumThemePageItemError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 2 invalidated (primary + thread_id)
  assert!((1..=3u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThreadError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_thread(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  thread: HammerfestForumThreadIdRef,
) -> Result<(), TouchHammerfestForumThreadError> {
  let res: PgQueryResult = sqlx::query(
    r"
      INSERT
      INTO hammerfest_forum_threads(hammerfest_server, hammerfest_thread_id, archived_at)
      VALUES (
        $1::HAMMERFEST_SERVER, $2::HAMMERFEST_FORUM_THREAD_ID, $3::INSTANT
      )
      ON CONFLICT (hammerfest_server, hammerfest_thread_id) DO NOTHING;
      ",
  )
  .bind(thread.server)
  .bind(thread.id)
  .bind(now)
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestForumThreadError::Query(WeakError::wrap(e)))?;
  assert!((0..=1u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThreadSharedMetaError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_thread_shared_meta(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  thread: HammerfestForumThreadIdRef,
  theme_id: HammerfestForumThemeId,
  title: &HammerfestForumThreadTitle,
  is_closed: bool,
  page_count: NonZeroU16,
) -> Result<(), TouchHammerfestForumThreadSharedMetaError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_thread_shared_meta(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_thread_id::HAMMERFEST_FORUM_THREAD_ID),
      data($4 hammerfest_theme_id::HAMMERFEST_FORUM_THEME_ID, $5 title::HAMMERFEST_FORUM_THREAD_TITLE, $6 is_closed::BOOLEAN, $7 page_count::U32),
    )
  ))
    .bind(now)
    .bind(thread.server)
    .bind(thread.id)
    .bind(theme_id)
    .bind(title.as_str())
    .bind(is_closed)
    .bind(i64::from(page_count.get()))
    .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumThreadSharedMetaError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumThreadThemeMetaError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_forum_thread_theme_meta(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  thread: HammerfestForumThreadIdRef,
  is_sticky: bool,
  latest_post_at: Option<HammerfestDate>,
  author: HammerfestUserId,
  reply_count: u16,
) -> Result<(), TouchHammerfestForumThreadThemeMetaError> {
  assert_eq!(is_sticky, latest_post_at.is_none());
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_thread_theme_meta(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_thread_id::HAMMERFEST_FORUM_THREAD_ID),
      data($4 is_sticky::BOOLEAN, $5 latest_post_at::HAMMERFEST_DATE, $6 author::HAMMERFEST_USER_ID, $7 reply_count::U16),
    )
  ))
    .bind(now)
    .bind(thread.server)
    .bind(thread.id)
    .bind(is_sticky)
    .bind(latest_post_at)
    .bind(author)
    .bind(i64::from(reply_count))
    .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumThreadThemeMetaError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumPostError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_forum_post(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  thread: HammerfestForumThreadIdRef,
  page: NonZeroU16,
  offset: u8,
  author: HammerfestUserId,
  posted_at: HammerfestDateTime,
  remote_html_body: &str,
) -> Result<(), TouchHammerfestForumPostError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_posts(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_thread_id::HAMMERFEST_FORUM_THREAD_ID, $4 page::U16, $5 offset_in_list::U8),
      data($6 author::HAMMERFEST_USER_ID, $7 posted_at::HAMMERFEST_DATE_TIME, $8 remote_html_body::TEXT),
    )
  ))
    .bind(now)
    .bind(thread.server)
    .bind(thread.id)
    .bind(i32::from(page.get()))
    .bind(i16::from(offset))
    .bind(author)
    .bind(posted_at)
    .bind(remote_html_body)
    .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumPostError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumPostIdError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_post_id(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  thread: HammerfestForumThreadIdRef,
  page: NonZeroU16,
  offset: u8,
  post_id: HammerfestForumPostId,
) -> Result<(), TouchHammerfestForumPostIdError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_post_ids(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_thread_id::HAMMERFEST_FORUM_THREAD_ID, $4 page::U16, $5 offset_in_list::U8),
      data($6 hammerfest_post_id::HAMMERFEST_FORUM_POST_ID),
      unique(mid(hammerfest_server, hammerfest_post_id)),
    )
  ))
    .bind(now)
    .bind(thread.server)
    .bind(thread.id)
    .bind(i32::from(page.get()))
    .bind(i16::from(offset))
    .bind(post_id)
    .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestForumPostIdError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (first insert), 1 invalidated (mid)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  // 3 : 1 inserted (data change), 1 invalidated (primary), 1 invalidated (mid)
  assert!((1..=3u64).contains(&res.rows_affected()));

  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfesQuestStatusesError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_quest_statuses(
  tx: &mut Transaction<'_, Postgres>,
  quests: &BTreeMap<HammerfestQuestId, HammerfestQuestStatus>,
  new_id: Uuid,
) -> Result<Uuid, TouchHammerfesQuestStatusesError> {
  let hash = {
    let json = serde_json::to_string(&quests).unwrap();
    let hash = Sha3_256::digest(json.as_bytes());
    hash
  };

  let map_id = {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_quest_status_map_id: Uuid,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH
        input_row(hammerfest_quest_status_map_id, _sha3_256) AS (
          VALUES($1::HAMMERFEST_QUEST_STATUS_MAP_ID, $2::BYTEA)
        ),
        inserted_rows AS (
          INSERT
          INTO hammerfest_quest_status_maps(hammerfest_quest_status_map_id, _sha3_256)
          SELECT * FROM input_row
          ON CONFLICT DO NOTHING
          RETURNING hammerfest_quest_status_map_id
        )
      SELECT hammerfest_quest_status_map_id FROM inserted_rows
      UNION ALL
      SELECT old.hammerfest_quest_status_map_id FROM hammerfest_quest_status_maps AS old INNER JOIN input_row USING(_sha3_256);
      ",
    )
      .bind(new_id)
      .bind(hash.as_slice())
      .fetch_one(&mut **tx)
      .await.map_err(|e| TouchHammerfesQuestStatusesError::Query(WeakError::wrap(e)))?;

    row.hammerfest_quest_status_map_id
  };

  if map_id == new_id {
    // Newly created map: fill its content
    for (qid, status) in quests {
      let res: PgQueryResult = sqlx::query(
        r"
        INSERT
        INTO hammerfest_quest_status_map_items(hammerfest_quest_status_map_id, hammerfest_quest_id, status)
        VALUES ($1::HAMMERFEST_QUEST_STATUS_MAP_ID, $2::HAMMERFEST_QUEST_ID, $3::HAMMERFEST_QUEST_STATUS);
      ",
      )
      .bind(map_id)
      .bind(qid)
      .bind(status)
      .execute(&mut **tx)
      .await
      .map_err(|e| TouchHammerfesQuestStatusesError::Query(WeakError::wrap(e)))?;
      assert_eq!(res.rows_affected(), 1);
    }
  } else {
    // Re-using old id, check for hash collision
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_quest_id: HammerfestQuestId,
      status: HammerfestQuestStatus,
    }

    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT hammerfest_quest_id, status
      FROM hammerfest_quest_status_map_items
      WHERE hammerfest_quest_status_map_id = $1::HAMMERFEST_QUEST_STATUS_MAP_ID;
    ",
    )
    .bind(map_id)
    .fetch_all(&mut **tx)
    .await
    .map_err(|e| TouchHammerfesQuestStatusesError::Query(WeakError::wrap(e)))?;

    let actual: BTreeMap<HammerfestQuestId, HammerfestQuestStatus> =
      rows.iter().map(|r| (r.hammerfest_quest_id, r.status)).collect();

    assert_eq!(actual, *quests);
  }

  Ok(map_id)
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfesUnlockedItemsError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_unlocked_items(
  tx: &mut Transaction<'_, Postgres>,
  items: &BTreeSet<HammerfestItemId>,
  new_id: Uuid,
) -> Result<Uuid, TouchHammerfesUnlockedItemsError> {
  let hash = {
    let json = serde_json::to_string(&items).unwrap();
    let hash = Sha3_256::digest(json.as_bytes());
    hash
  };

  let set_id = {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_unlocked_item_set_id: Uuid,
    }
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH
        input_row(hammerfest_unlocked_item_set_id, _sha3_256) AS (
          VALUES($1::HAMMERFEST_UNLOCKED_ITEM_SET_ID, $2::BYTEA)
        ),
        inserted_rows AS (
          INSERT
          INTO hammerfest_unlocked_item_sets(hammerfest_unlocked_item_set_id, _sha3_256)
          SELECT * FROM input_row
          ON CONFLICT DO NOTHING
          RETURNING hammerfest_unlocked_item_set_id
        )
      SELECT hammerfest_unlocked_item_set_id FROM inserted_rows
      UNION ALL
      SELECT old.hammerfest_unlocked_item_set_id FROM hammerfest_unlocked_item_sets AS old INNER JOIN input_row USING(_sha3_256);
      ",
    )
      .bind(new_id)
      .bind(hash.as_slice())
      .fetch_one(&mut **tx)
    .await.map_err(|e| TouchHammerfesUnlockedItemsError::Query(WeakError::wrap(e)))?;

    row.hammerfest_unlocked_item_set_id
  };

  if set_id == new_id {
    // Newly created map: fill its content
    for iid in items {
      let res: PgQueryResult = sqlx::query(
        r"
        INSERT
        INTO hammerfest_unlocked_item_set_items(hammerfest_unlocked_item_set_id, hammerfest_item_id)
        VALUES ($1::HAMMERFEST_UNLOCKED_ITEM_SET_ID, $2::HAMMERFEST_ITEM_ID);
      ",
      )
      .bind(set_id)
      .bind(iid)
      .execute(&mut **tx)
      .await
      .map_err(|e| TouchHammerfesUnlockedItemsError::Query(WeakError::wrap(e)))?;
      assert_eq!(res.rows_affected(), 1);
    }
  } else {
    // Re-using old id, check for hash collision
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_item_id: HammerfestItemId,
    }

    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT hammerfest_item_id
      FROM hammerfest_unlocked_item_set_items
      WHERE hammerfest_unlocked_item_set_id = $1::HAMMERFEST_UNLOCKED_ITEM_SET_ID;
    ",
    )
    .bind(set_id)
    .fetch_all(&mut **tx)
    .await
    .map_err(|e| TouchHammerfesUnlockedItemsError::Query(WeakError::wrap(e)))?;

    let actual: BTreeSet<HammerfestItemId> = rows.iter().map(|r| r.hammerfest_item_id).collect();

    assert_eq!(actual, *items);
  }

  Ok(set_id)
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfesItemCountsError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_item_counts(
  tx: &mut Transaction<'_, Postgres>,
  items: &BTreeMap<HammerfestItemId, u32>,
  new_id: Uuid,
) -> Result<Uuid, TouchHammerfesItemCountsError> {
  let hash = {
    let json = serde_json::to_string(&items).unwrap();
    let hash = Sha3_256::digest(json.as_bytes());
    hash
  };

  let map_id = {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_item_count_map_id: Uuid,
    }
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH
        input_row(hammerfest_item_count_map_id, _sha3_256) AS (
          VALUES($1::HAMMERFEST_ITEM_COUNT_MAP_ID, $2::BYTEA)
        ),
        inserted_rows AS (
          INSERT
          INTO hammerfest_item_count_maps(hammerfest_item_count_map_id, _sha3_256)
          SELECT * FROM input_row
          ON CONFLICT DO NOTHING
          RETURNING hammerfest_item_count_map_id
        )
      SELECT hammerfest_item_count_map_id FROM inserted_rows
      UNION ALL
      SELECT old.hammerfest_item_count_map_id FROM hammerfest_item_count_maps AS old INNER JOIN input_row USING(_sha3_256);
      ",
    )
      .bind(new_id)
      .bind(hash.as_slice())
      .fetch_one(&mut **tx)
      .await.map_err(|e| TouchHammerfesItemCountsError::Query(WeakError::wrap(e)))?;

    row.hammerfest_item_count_map_id
  };

  if map_id == new_id {
    // Newly created map: fill its content
    for (id, count) in items {
      let res: PgQueryResult = sqlx::query(
        r"
        INSERT
        INTO hammerfest_item_count_map_items(hammerfest_item_count_map_id, hammerfest_item_id, count)
        VALUES ($1::HAMMERFEST_ITEM_COUNT_MAP_ID, $2::HAMMERFEST_ITEM_ID, $3::U32);
      ",
      )
      .bind(map_id)
      .bind(id)
      .bind(PgU32::from(*count))
      .execute(&mut **tx)
      .await
      .map_err(|e| TouchHammerfesItemCountsError::Query(WeakError::wrap(e)))?;
      assert_eq!(res.rows_affected(), 1);
    }
  } else {
    // Re-using old id, check for hash collision
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_item_id: HammerfestItemId,
      count: PgU32,
    }

    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT hammerfest_item_id, count
      FROM hammerfest_item_count_map_items
      WHERE hammerfest_item_count_map_id = $1::HAMMERFEST_ITEM_COUNT_MAP_ID;
    ",
    )
    .bind(map_id)
    .fetch_all(&mut **tx)
    .await
    .map_err(|e| TouchHammerfesItemCountsError::Query(WeakError::wrap(e)))?;

    let actual: BTreeMap<HammerfestItemId, u32> = rows
      .iter()
      .map(|r| (r.hammerfest_item_id, u32::from(r.count)))
      .collect();

    assert_eq!(actual, *items);
  }

  Ok(map_id)
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestSessionUserError {
  #[error("failed to touch hammerfest user")]
  TouchHammerfestUser(#[from] InnerTouchHammerfestUserError),
  #[error("failed to touch hammerfest tokens")]
  TouchHammerfestTokens(#[from] TouchHammerfestTokensError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_session_user(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  session_user: &HammerfestSessionUser,
) -> Result<(), TouchHammerfestSessionUserError> {
  touch_hammerfest_user(tx, now, &session_user.user).await?;
  touch_hammerfest_tokens(tx, now, session_user.user.as_ref(), session_user.tokens).await?;
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum InnerTouchHammerfestProfileError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_profile(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  best_score: u32,
  best_level: i32,
  season_score: u32,
  quests: Uuid,
  unlocked_items: Uuid,
) -> Result<(), InnerTouchHammerfestProfileError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_profiles(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 best_score::U32, $5 best_level::I32, $6 season_score::U32, $7 quest_statuses::HAMMERFEST_QUEST_STATUS_MAP_ID, $8 unlocked_items::HAMMERFEST_UNLOCKED_ITEM_SET_ID),
    )
  ))
    .bind(now)
    .bind(user.server)
    .bind(user.id)
    .bind(PgU32::from(best_score))
    .bind(best_level)
    .bind(PgU32::from(season_score))
    .bind(quests)
    .bind(unlocked_items)
    .execute(&mut **tx)
    .await.map_err(|e| InnerTouchHammerfestProfileError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestEmailError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_email(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  email_hash: Option<Vec<u8>>,
) -> Result<(), TouchHammerfestEmailError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_emails(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 email::EMAIL_ADDRESS_HASH),
      unique(email(hammerfest_server, email)),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(&email_hash)
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestEmailError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (first insert), 1 invalidated (email)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  // 3 : 1 inserted (data change), 1 invalidated (primary), 1 invalidated (email)
  assert!((1..=3u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumBanError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

async fn touch_hammerfest_forum_ban(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  forum_ban: bool,
) -> Result<(), TouchHammerfestForumBanError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_ban(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 forum_ban::BOOLEAN),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(forum_ban)
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestForumBanError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestAchievementsError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_achievements(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  has_carrot: bool,
  ladder_level: HammerfestLadderLevel,
) -> Result<(), TouchHammerfestAchievementsError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_user_achievements(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 has_carrot::BOOLEAN, $5 ladder_level::HAMMERFEST_LADDER_LEVEL),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(has_carrot)
  .bind(ladder_level)
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestAchievementsError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestBestSeasonRankError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_best_season_rank(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  season_rank: Option<u32>,
) -> Result<(), TouchHammerfestBestSeasonRankError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_best_season_ranks(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 best_season_rank::U32?),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(season_rank.map(PgU32::from))
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestBestSeasonRankError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestForumRoleError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_forum_role(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  role: HammerfestForumRole,
) -> Result<(), TouchHammerfestForumRoleError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_forum_roles(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 role::HAMMERFEST_FORUM_ROLE),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(role)
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestForumRoleError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum InnerTouchHammerfestInventoryError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_inventory(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  items: Uuid,
) -> Result<(), InnerTouchHammerfestInventoryError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_inventories(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 item_counts::HAMMERFEST_ITEM_COUNT_MAP_ID),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(items)
  .execute(&mut **tx)
  .await
  .map_err(|e| InnerTouchHammerfestInventoryError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum InnerTouchHammerfestShopError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_shop(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  shop: &HammerfestShop,
) -> Result<(), InnerTouchHammerfestShopError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_shops(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 weekly_tokens::U8, $5 purchased_tokens::U8?, $6 has_quest_bonus::BOOLEAN),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(i32::from(shop.weekly_tokens))
  .bind(shop.purchased_tokens.map(i16::from))
  .bind(shop.has_quest_bonus)
  .execute(&mut **tx)
  .await
  .map_err(|e| InnerTouchHammerfestShopError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestTokensError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_tokens(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: HammerfestUserIdRef,
  tokens: u32,
) -> Result<(), TouchHammerfestTokensError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_tokens(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 tokens::U32),
    )
  ))
  .bind(now)
  .bind(user.server)
  .bind(user.id)
  .bind(PgU32::from(tokens))
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestTokensError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestGodchildListError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_godchild_list(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  godfather: HammerfestUserIdRef,
  godchild_count: u32,
) -> Result<(), TouchHammerfestGodchildListError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_godchild_lists(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID),
      data($4 godchild_count::U32),
    )
  ))
  .bind(now)
  .bind(godfather.server)
  .bind(godfather.id)
  .bind(PgU32::from(godchild_count))
  .execute(&mut **tx)
  .await
  .map_err(|e| TouchHammerfestGodchildListError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 2 : 1 inserted (data change), 1 invalidated (primary)
  assert!((1..=2u64).contains(&res.rows_affected()));
  Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum TouchHammerfestGodchildError {
  #[error("db query failed")]
  Query(#[source] WeakError),
}

#[allow(clippy::too_many_arguments)]
async fn touch_hammerfest_godchild(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  godfather: HammerfestUserIdRef,
  offset_in_list: u32,
  godchild: HammerfestUserIdRef,
  tokens: u32,
) -> Result<(), TouchHammerfestGodchildError> {
  assert_eq!(godchild.server, godfather.server);
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    hammerfest_godchildren(
      time($1 period, retrieved_at),
      primary($2 hammerfest_server::HAMMERFEST_SERVER, $3 hammerfest_user_id::HAMMERFEST_USER_ID, $4 offset_in_list::U32),
      data($5 godchild_id::HAMMERFEST_USER_ID, $6 tokens::U32),
      unique(godchild(hammerfest_server, godchild_id)),
    )
  ))
  .bind(now)
  .bind(godfather.server)
  .bind(godfather.id)
  .bind(PgU32::from(offset_in_list))
  .bind(godchild.id)
  .bind(PgU32::from(tokens))
  .execute(&mut **tx)
    .await.map_err(|e| TouchHammerfestGodchildError::Query(WeakError::wrap(e)))?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  // 3 : 1 inserted (data change), 2 invalidated (primary, unique godchild)
  assert!((1..=3u64).contains(&res.rows_affected()));
  // TODO: Invalidate parent list a godchild is invalidated
  Ok(())
}

fn to_latest_temporal<T>(
  period: Option<PeriodLower>,
  retrieved_latest: Option<Instant>,
  value: Option<T>,
) -> Option<LatestTemporal<T>> {
  match (period, retrieved_latest, value) {
    (Some(period), Some(latest), Some(value)) => Some(LatestTemporal {
      latest: ForeignSnapshot {
        period,
        retrieved: ForeignRetrieved { latest },
        value,
      },
    }),
    (None, None, None) => None,
    _ => unreachable!(),
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> HammerfestStore for PgHammerfestStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn get_short_user(
    &self,
    options: &GetHammerfestUserOptions,
  ) -> Result<ShortHammerfestUser, RawGetShortHammerfestUserError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_server: HammerfestServer,
      hammerfest_user_id: HammerfestUserId,
      username: HammerfestUsername,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT hammerfest_server, hammerfest_user_id, username
      FROM hammerfest_users
      WHERE hammerfest_server = $1::HAMMERFEST_SERVER AND hammerfest_user_id = $2::HAMMERFEST_USER_ID;
    ",
    )
    .bind(options.server)
    .bind(options.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(RawGetShortHammerfestUserError::other)?;

    match row {
      Some(r) => Ok(ShortHammerfestUser {
        server: r.hammerfest_server,
        id: r.hammerfest_user_id,
        username: r.username,
      }),
      None => Err(RawGetShortHammerfestUserError::NotFound),
    }
  }

  async fn get_user(
    &self,
    options: &GetHammerfestUserOptions,
  ) -> Result<Option<StoredHammerfestUser>, RawGetHammerfestUserError> {
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());
    let mut tx = self.database.begin().await.map_err(RawGetHammerfestUserError::other)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_server: HammerfestServer,
      hammerfest_user_id: HammerfestUserId,
      username: HammerfestUsername,
      archived_at: Instant,
      profile_items: Option<Uuid>,
      profile_quests: Option<Uuid>,
      profile_best_score: Option<PgU32>,
      profile_best_level: Option<i32>,
      profile_period: Option<PeriodLower>,
      profile_retrieved_latest: Option<Instant>,
      inventory: Option<Uuid>,
      inventory_period: Option<PeriodLower>,
      inventory_retrieved_latest: Option<Instant>,
      game_completed: Option<bool>,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      WITH latest_inventories AS (
        SELECT * FROM hammerfest_inventories
        WHERE
          hammerfest_server = $1::HAMMERFEST_SERVER
          AND hammerfest_user_id = $2::HAMMERFEST_USER_ID
          AND lower(period) <= $3::INSTANT
        ORDER BY lower(period) DESC
        LIMIT 1
      ),
      latest_profiles AS (
        SELECT * FROM hammerfest_profiles
        WHERE
          hammerfest_server = $1::HAMMERFEST_SERVER
          AND hammerfest_user_id = $2::HAMMERFEST_USER_ID
          AND lower(period) <= $3::INSTANT
        ORDER BY lower(period) DESC
        LIMIT 1
      ),
      latest_achievements AS (
        SELECT * FROM hammerfest_user_achievements
        WHERE
          hammerfest_server = $1::HAMMERFEST_SERVER
          AND hammerfest_user_id = $2::HAMMERFEST_USER_ID
          AND lower(period) <= $3::INSTANT
        ORDER BY lower(period) DESC
        LIMIT 1
      )
      SELECT
        users.hammerfest_server,
        users.hammerfest_user_id,
        users.username,
        users.archived_at,
        profile.period AS profile_period, profile.retrieved_at[CARDINALITY(profile.retrieved_at)] AS profile_retrieved_latest,
        profile.unlocked_items AS profile_items,
        profile.quest_statuses AS profile_quests,
        profile.best_score AS profile_best_score,
        profile.best_level AS profile_best_level,
        inventory.period AS inventory_period, inventory.retrieved_at[CARDINALITY(inventory.retrieved_at)] AS inventory_retrieved_latest,
        inventory.item_counts AS inventory,
        achievements.has_carrot AS game_completed
      FROM hammerfest_users users
      LEFT OUTER JOIN latest_profiles AS profile USING (hammerfest_server, hammerfest_user_id)
      LEFT OUTER JOIN latest_inventories AS inventory USING (hammerfest_server, hammerfest_user_id)
      LEFT OUTER JOIN latest_achievements AS achievements USING (hammerfest_server, hammerfest_user_id)
      WHERE hammerfest_server = $1::HAMMERFEST_SERVER AND hammerfest_user_id = $2::HAMMERFEST_USER_ID;
    ",
    )
    .bind(options.server)
    .bind(options.id)
    .bind(time)
    .fetch_optional(&mut *tx)
    .await.map_err(RawGetHammerfestUserError::other)?;

    let Some(row) = row else {
      return Ok(None);
    };

    let inventory = if let Some(item_map_id) = row.inventory {
      #[derive(Debug, sqlx::FromRow)]
      struct ItemRow {
        item_id: HammerfestItemId,
        count: PgU32,
      }

      // Note: we don't filter out hidden items.
      // language=PostgreSQL
      let items: BTreeMap<_, _> = sqlx::query_as::<_, ItemRow>(
        r"
        SELECT hammerfest_item_id AS item_id, count
        FROM hammerfest_item_count_map_items
        WHERE hammerfest_item_count_map_id = $1::HAMMERFEST_ITEM_COUNT_MAP_ID
        ",
      )
      .bind(item_map_id)
      .fetch_all(&mut *tx)
      .await
      .map_err(RawGetHammerfestUserError::other)?
      .into_iter()
      .map(|r| (r.item_id, u32::from(r.count)))
      .collect();

      Some(items)
    } else {
      None
    };

    let profile = if row.profile_retrieved_latest.is_some() {
      #[derive(Debug, sqlx::FromRow)]
      struct ItemRow {
        item_id: HammerfestItemId,
      }

      #[derive(Debug, sqlx::FromRow)]
      struct QuestRow {
        quest_id: HammerfestQuestId,
        status: HammerfestQuestStatus,
      }

      Some(StoredHammerfestProfile {
        best_score: row.profile_best_score.unwrap().into(),
        best_level: row.profile_best_level.unwrap(),
        // This should always be present, but let's be resilient.
        game_completed: row.game_completed.unwrap_or(false),
        // language=PostgreSQL
        items: sqlx::query_as::<_, ItemRow>(
          r"
            SELECT hammerfest_item_id AS item_id
            FROM hammerfest_unlocked_item_set_items
            WHERE hammerfest_unlocked_item_set_id = $1::HAMMERFEST_UNLOCKED_ITEM_SET_ID
            ",
        )
        .bind(row.profile_items.unwrap())
        .fetch_all(&mut *tx)
        .await
        .map_err(RawGetHammerfestUserError::other)?
        .into_iter()
        .map(|r| r.item_id)
        .collect(),
        // language=PostgreSQL
        quests: sqlx::query_as::<_, QuestRow>(
          r"
            SELECT hammerfest_quest_id AS quest_id, status
            FROM hammerfest_quest_status_map_items
            WHERE hammerfest_quest_status_map_id = $1::HAMMERFEST_QUEST_STATUS_MAP_ID
            ",
        )
        .bind(row.profile_quests.unwrap())
        .fetch_all(&mut *tx)
        .await
        .map_err(RawGetHammerfestUserError::other)?
        .into_iter()
        .map(|r| (r.quest_id, r.status))
        .collect(),
      })
    } else {
      None
    };

    Ok(Some(StoredHammerfestUser {
      server: row.hammerfest_server,
      id: row.hammerfest_user_id,
      username: row.username,
      archived_at: row.archived_at,
      profile: to_latest_temporal(row.profile_period, row.profile_retrieved_latest, profile),
      inventory: to_latest_temporal(row.inventory_period, row.inventory_retrieved_latest, inventory),
    }))
  }

  async fn get_active_session(&self, query: GetActiveSession) -> Result<HammerfestSession, GetActiveSessionError> {
    let (server, user_id, username) = match query.user {
      HammerfestUserRef::Id(r) => (r.server, Some(r.id), None),
      HammerfestUserRef::Username(r) => (r.server, None, Some(r.username)),
    };

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      hammerfest_server: HammerfestServer,
      hammerfest_user_id: HammerfestUserId,
      username: HammerfestUsername,
      ctime: Instant,
      atime: Instant,
      key: HammerfestSessionKey,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT hammerfest_server, hammerfest_user_id, username,
        LOWER(period) AS ctime, retrieved_at[array_upper(retrieved_at, 1)] AS atime,
        pgp_sym_decrypt(hammerfest_session_key, $5::text)::hammerfest_session_key AS key
      FROM hammerfest_session INNER JOIN hammerfest_users USING (hammerfest_server, hammerfest_user_id)
      WHERE period @> $1::INSTANT
        AND hammerfest_server = $2::HAMMERFEST_SERVER
        AND hammerfest_user_id = COALESCE($3::HAMMERFEST_USER_ID, hammerfest_user_id)
        AND lower(username) = lower(COALESCE($4::hammerfest_username, username))
    ",
    )
    .bind(query.time)
    .bind(server)
    .bind(user_id)
    .bind(username)
    .bind(self.database_secret.as_str())
    .fetch_optional(&*self.database)
    .await
    .map_err(|e| GetActiveSessionError::Other(WeakError::wrap(e)))?;

    let row = row.ok_or(GetActiveSessionError::NotFound)?;

    let session = HammerfestSession {
      key: row.key,
      user: ShortHammerfestUser {
        server: row.hammerfest_server,
        id: row.hammerfest_user_id,
        username: row.username,
      },
      ctime: row.ctime,
      atime: row.atime,
    };

    Ok(session)
  }

  async fn touch_session(&self, cmd: TouchSession) -> Result<(), TouchHammerfestSessionError> {
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestSessionError::other)?;
    let user_id = match cmd.user {
      Some(user) => {
        let user_id = user.id;
        touch_hammerfest_user(&mut tx, cmd.now, &user)
          .await
          .map_err(TouchHammerfestSessionError::other)?;
        Some(user_id)
      }
      None => None,
    };
    touch_hammerfest_session(&mut tx, cmd.now, cmd.server, cmd.key, user_id, &self.database_secret)
      .await
      .map_err(TouchHammerfestSessionError::other)?;
    tx.commit().await.map_err(TouchHammerfestSessionError::other)?;
    Ok(())
  }

  async fn touch_short_user(
    &self,
    user: &ShortHammerfestUser,
  ) -> Result<StoredHammerfestUser, TouchHammerfestUserError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(TouchHammerfestUserError::other)?;
    touch_hammerfest_user(&mut tx, now, user)
      .await
      .map_err(TouchHammerfestUserError::other)?;
    tx.commit().await.map_err(TouchHammerfestUserError::other)?;
    Ok(StoredHammerfestUser {
      server: user.server,
      id: user.id,
      username: user.username.clone(),
      archived_at: now,
      profile: None,
      inventory: None,
    })
  }

  async fn touch_evni_user(&self, cmd: TouchEvniUser) -> Result<(), TouchHammerfestEvniUserError> {
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestEvniUserError::other)?;
    touch_evni_user(&mut tx, cmd.now, cmd.user.server, cmd.user.id)
      .await
      .map_err(TouchHammerfestEvniUserError::other)?;
    tx.commit().await.map_err(TouchHammerfestEvniUserError::other)?;
    Ok(())
  }

  async fn touch_shop(&self, response: &HammerfestShopResponse) -> Result<(), TouchHammerfestShopError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(TouchHammerfestShopError::other)?;
    touch_hammerfest_session_user(&mut tx, now, &response.session)
      .await
      .map_err(TouchHammerfestShopError::other)?;
    touch_hammerfest_shop(&mut tx, now, response.session.user.as_ref(), &response.shop)
      .await
      .map_err(TouchHammerfestShopError::other)?;
    tx.commit().await.map_err(TouchHammerfestShopError::other)?;
    Ok(())
  }

  async fn touch_profile(&self, response: &HammerfestProfileResponse) -> Result<(), TouchHammerfestProfileError> {
    let profile = if let Some(profile) = response.profile.as_ref() {
      profile
    } else {
      return Ok(());
    };

    assert_eq!(
      response.session.is_some(),
      profile.email.is_some(),
      "session user presence must match email knowledge presence"
    );
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestProfileError::other)?;
    if let Some(session_user) = response.session.as_ref() {
      touch_hammerfest_session_user(&mut tx, now, session_user)
        .await
        .map_err(TouchHammerfestProfileError::other)?;
    }
    let options = profile;
    touch_hammerfest_user(&mut tx, now, &options.user)
      .await
      .map_err(TouchHammerfestProfileError::other)?;
    let quests = touch_hammerfest_quest_statuses(&mut tx, &options.quests, self.uuid_generator.uuid_generator().next())
      .await
      .map_err(TouchHammerfestProfileError::other)?;
    let unlocked_items =
      touch_hammerfest_unlocked_items(&mut tx, &options.items, self.uuid_generator.uuid_generator().next())
        .await
        .map_err(TouchHammerfestProfileError::other)?;
    touch_hammerfest_profile(
      &mut tx,
      now,
      options.user.as_ref(),
      options.best_score,
      options.best_level,
      options.season_score,
      quests,
      unlocked_items,
    )
    .await
    .map_err(TouchHammerfestProfileError::other)?;
    if let Some(email) = &options.email {
      let email_hash = if let Some(email) = &email {
        let email_hash = touch_email_address(&mut tx, &self.database_secret, email, now)
          .await
          .map_err(TouchHammerfestProfileError::other)?;
        Some(email_hash)
      } else {
        None
      };
      touch_hammerfest_email(&mut tx, now, options.user.as_ref(), email_hash)
        .await
        .map_err(TouchHammerfestProfileError::other)?;
    }
    if let Some(forum_ban) = &options.forum_ban {
      touch_hammerfest_forum_ban(&mut tx, now, options.user.as_ref(), *forum_ban)
        .await
        .map_err(TouchHammerfestProfileError::other)?;
    }
    touch_hammerfest_achievements(
      &mut tx,
      now,
      options.user.as_ref(),
      options.has_carrot,
      options.ladder_level,
    )
    .await
    .map_err(TouchHammerfestProfileError::other)?;

    tx.commit().await.map_err(TouchHammerfestProfileError::other)?;

    Ok(())
  }

  async fn touch_inventory(&self, response: &HammerfestInventoryResponse) -> Result<(), TouchHammerfestInventoryError> {
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestInventoryError::other)?;
    touch_hammerfest_session_user(&mut tx, now, &response.session)
      .await
      .map_err(TouchHammerfestInventoryError::other)?;
    let inventory = &response.inventory;
    let items = touch_hammerfest_item_counts(&mut tx, inventory, self.uuid_generator.uuid_generator().next())
      .await
      .map_err(TouchHammerfestInventoryError::other)?;
    touch_hammerfest_inventory(&mut tx, now, response.session.user.as_ref(), items)
      .await
      .map_err(TouchHammerfestInventoryError::other)?;
    tx.commit().await.map_err(TouchHammerfestInventoryError::other)?;

    Ok(())
  }

  async fn touch_godchildren(
    &self,
    response: &HammerfestGodchildrenResponse,
  ) -> Result<(), TouchHammerfestGodchildrenError> {
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestGodchildrenError::other)?;
    touch_hammerfest_session_user(&mut tx, now, &response.session)
      .await
      .map_err(TouchHammerfestGodchildrenError::other)?;
    touch_hammerfest_godchild_list(
      &mut tx,
      now,
      response.session.user.as_ref(),
      response.godchildren.len().try_into().expect("OverflowOnGodchildCount"),
    )
    .await
    .map_err(TouchHammerfestGodchildrenError::other)?;
    for (offset_in_list, godchild) in response.godchildren.iter().enumerate() {
      touch_hammerfest_user(&mut tx, now, &godchild.user)
        .await
        .map_err(TouchHammerfestGodchildrenError::other)?;
      touch_hammerfest_godchild(
        &mut tx,
        now,
        response.session.user.as_ref(),
        offset_in_list.try_into().expect("OverflowOnGodchildOffest"),
        godchild.user.as_ref(),
        godchild.tokens,
      )
      .await
      .map_err(TouchHammerfestGodchildrenError::other)?;
    }
    tx.commit().await.map_err(TouchHammerfestGodchildrenError::other)?;

    Ok(())
  }

  async fn touch_theme_page(
    &self,
    response: &HammerfestForumThemePageResponse,
  ) -> Result<(), TouchHammerfestThemePageError> {
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
    if let Some(session_user) = response.session.as_ref() {
      touch_hammerfest_session_user(&mut tx, now, session_user)
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
    }
    let options = &response.page;
    touch_hammerfest_forum_theme(
      &mut tx,
      now,
      options.theme.server,
      options.theme.id,
      &options.theme.name,
      None,
      options.theme.is_public,
    )
    .await
    .map_err(TouchHammerfestThemePageError::other)?;
    touch_hammerfest_forum_theme_count(&mut tx, now, options.theme.as_ref(), options.threads.pages)
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
    touch_hammerfest_forum_theme_page_count(
      &mut tx,
      now,
      options.theme.as_ref(),
      ThemePage::Sticky,
      options.sticky.len().try_into().expect("OverflowOnStickyThreadCount"),
    )
    .await
    .map_err(TouchHammerfestThemePageError::other)?;

    for (offset, sticky) in options.sticky.iter().enumerate() {
      assert!(matches!(sticky.kind, HammerfestForumThreadKind::Sticky));
      touch_hammerfest_forum_thread(&mut tx, now, sticky.as_ref())
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_theme_page_item(
        &mut tx,
        now,
        options.theme.as_ref(),
        ThemePage::Sticky,
        offset.try_into().expect("OverflowOnStickyThreadOffset"),
        sticky.as_ref(),
      )
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_thread_shared_meta(
        &mut tx,
        now,
        sticky.as_ref(),
        options.theme.id,
        &sticky.short.name,
        sticky.short.is_closed,
        hammerfest_reply_count_to_page_count(sticky.reply_count),
      )
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_user(&mut tx, now, &sticky.author)
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_role(&mut tx, now, sticky.author.as_ref(), sticky.author_role)
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_thread_theme_meta(
        &mut tx,
        now,
        sticky.as_ref(),
        true,
        None,
        sticky.author.id,
        sticky.reply_count,
      )
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
    }
    let page = ThemePage::Regular(options.threads.page1);
    touch_hammerfest_forum_theme_page_count(
      &mut tx,
      now,
      options.theme.as_ref(),
      page,
      options
        .threads
        .items
        .len()
        .try_into()
        .expect("OverflowOnRegularThreadCount"),
    )
    .await
    .map_err(TouchHammerfestThemePageError::other)?;
    for (offset, thread) in options.threads.items.iter().enumerate() {
      let last_post_date = match thread.kind {
        HammerfestForumThreadKind::Regular {
          latest_post_date: last_post_date,
        } => last_post_date,
        _ => unreachable!(),
      };
      touch_hammerfest_forum_thread(&mut tx, now, thread.as_ref())
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_theme_page_item(
        &mut tx,
        now,
        options.theme.as_ref(),
        page,
        offset.try_into().expect("OverflowOnRegularThreadOffset"),
        thread.as_ref(),
      )
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_thread_shared_meta(
        &mut tx,
        now,
        thread.as_ref(),
        options.theme.id,
        &thread.short.name,
        thread.short.is_closed,
        hammerfest_reply_count_to_page_count(thread.reply_count),
      )
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_user(&mut tx, now, &thread.author)
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_role(&mut tx, now, thread.author.as_ref(), thread.author_role)
        .await
        .map_err(TouchHammerfestThemePageError::other)?;
      touch_hammerfest_forum_thread_theme_meta(
        &mut tx,
        now,
        thread.as_ref(),
        false,
        Some(last_post_date),
        thread.author.id,
        thread.reply_count,
      )
      .await
      .map_err(TouchHammerfestThemePageError::other)?;
    }

    tx.commit().await.map_err(TouchHammerfestThemePageError::other)?;

    Ok(())
  }

  async fn touch_thread_page(
    &self,
    response: &HammerfestForumThreadPageResponse,
  ) -> Result<(), TouchHammerfestThreadPageError> {
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(TouchHammerfestThreadPageError::other)?;
    let mut session_user_is_moderator = false;
    if let Some(session_user) = response.session.as_ref() {
      touch_hammerfest_session_user(&mut tx, now, session_user)
        .await
        .map_err(TouchHammerfestThreadPageError::other)?;
    }
    let options = &response.page;
    touch_hammerfest_forum_theme(
      &mut tx,
      now,
      options.theme.server,
      options.theme.id,
      &options.theme.name,
      None,
      options.theme.is_public,
    )
    .await
    .map_err(TouchHammerfestThreadPageError::other)?;
    touch_hammerfest_forum_thread(&mut tx, now, options.thread.as_ref())
      .await
      .map_err(TouchHammerfestThreadPageError::other)?;
    touch_hammerfest_forum_thread_shared_meta(
      &mut tx,
      now,
      options.thread.as_ref(),
      options.theme.id,
      &options.thread.name,
      options.thread.is_closed,
      options.posts.pages,
    )
    .await
    .map_err(TouchHammerfestThreadPageError::other)?;

    touch_hammerfest_forum_thread_page_count(
      &mut tx,
      now,
      options.thread.as_ref(),
      options.posts.page1,
      options.posts.items.len().try_into().expect("OverflowOnThreadPostCount"),
    )
    .await
    .map_err(TouchHammerfestThreadPageError::other)?;
    for (offset, post) in options.posts.items.iter().enumerate() {
      let offset: u8 = offset.try_into().expect("OverflowOnThreadPostOffset");
      touch_hammerfest_user(&mut tx, now, &post.author.user)
        .await
        .map_err(TouchHammerfestThreadPageError::other)?;
      touch_hammerfest_achievements(
        &mut tx,
        now,
        post.author.user.as_ref(),
        post.author.has_carrot,
        post.author.ladder_level,
      )
      .await
      .map_err(TouchHammerfestThreadPageError::other)?;
      touch_hammerfest_best_season_rank(&mut tx, now, post.author.user.as_ref(), post.author.rank)
        .await
        .map_err(TouchHammerfestThreadPageError::other)?;
      touch_hammerfest_forum_role(&mut tx, now, post.author.user.as_ref(), post.author.role)
        .await
        .map_err(TouchHammerfestThreadPageError::other)?;
      touch_hammerfest_forum_post(
        &mut tx,
        now,
        options.thread.as_ref(),
        options.posts.page1,
        offset,
        post.author.user.id,
        post.ctime,
        &post.content,
      )
      .await
      .map_err(TouchHammerfestThreadPageError::other)?;
      if let Some(mid) = post.id {
        session_user_is_moderator = true;
        touch_hammerfest_forum_post_id(&mut tx, now, options.thread.as_ref(), options.posts.page1, offset, mid)
          .await
          .map_err(TouchHammerfestThreadPageError::other)?;
      }
    }
    if let Some(session_user) = response.session.as_ref() {
      touch_hammerfest_forum_role(
        &mut tx,
        now,
        session_user.user.as_ref(),
        if session_user_is_moderator {
          HammerfestForumRole::Moderator
        } else {
          HammerfestForumRole::None
        },
      )
      .await
      .map_err(TouchHammerfestThreadPageError::other)?;
    }

    tx.commit().await.map_err(TouchHammerfestThreadPageError::other)?;

    Ok(())
  }
}

#[cfg(test)]
mod test {
  use super::PgHammerfestStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::hammerfest::HammerfestStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn HammerfestStore>> {
    let config = eternaltwin_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let database_secret = SecretString::new("dev_secret".to_string());
    let hammerfest_store: Arc<dyn HammerfestStore> = Arc::new(
      PgHammerfestStore::new(
        Arc::clone(&clock),
        Arc::clone(&database),
        database_secret,
        uuid_generator,
      )
      .await
      .unwrap(),
    );

    TestApi {
      clock,
      hammerfest_store,
    }
  }

  test_hammerfest_store!(
    #[serial]
    || make_test_api().await
  );

  test_hammerfest_store_pg!(
    #[serial]
    || make_test_api().await
  );
}
