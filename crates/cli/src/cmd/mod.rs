use std::collections::BTreeMap;
use std::path::Path;

pub mod backend;
pub mod config;
pub mod db;
pub mod dump;
pub mod job;
#[cfg(feature = "start")]
pub mod start;
pub mod tidsave;
pub mod version;

pub struct CliContext<'a, Args, Out = std::io::Stdout> {
  pub args: &'a Args,
  pub env: &'a BTreeMap<String, String>,
  pub working_dir: &'a Path,
  pub out: Out,
}

impl<'a, Args, Out> CliContext<'a, Args, Out> {
  pub fn map_out<F, R>(self, mut f: F) -> CliContext<'a, Args, R>
  where
    F: FnMut(Out) -> R,
  {
    CliContext {
      args: self.args,
      env: self.env,
      working_dir: self.working_dir,
      out: f(self.out),
    }
  }
}
