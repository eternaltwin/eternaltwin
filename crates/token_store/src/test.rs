use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::VirtualClock;
use eternaltwin_core::core::{Duration, Instant};
use eternaltwin_core::dinoparc::{
  DinoparcServer, DinoparcStore, DinoparcStoreRef, DinoparcUserIdRef, ShortDinoparcUser, StoredDinoparcSession,
};
use eternaltwin_core::token::TokenStore;
use eternaltwin_core::token::TokenStoreRef;

#[macro_export]
macro_rules! test_token_store {
  ($(#[$meta:meta])* || $api:expr) => {
    // register_test!($(#[$meta])*, $api, test_touch_twinoid_oauth);
    // register_test!($(#[$meta])*, $api, test_touch_twinoid_oauth_twice);
    // register_test!($(#[$meta])*, $api, test_revoke_twinoid_access_token);
    // register_test!($(#[$meta])*, $api, test_revoke_twinoid_refresh_token);
    // register_test!($(#[$meta])*, $api, test_touch_hammerfest_session);
    // register_test!($(#[$meta])*, $api, test_touch_hammerfest_session_to_update_atime_but_not_ctime);
    // register_test!($(#[$meta])*, $api, test_touch_hammerfest_session_and_retrieve_it_without_atime_change);
    // register_test!($(#[$meta])*, $api, test_returns_none_for_session_with_an_unknown_hammerfest_user);
    // register_test!($(#[$meta])*, $api, test_returns_none_for_session_with_a_known_unauthenticated_hammerfest_user);
    // register_test!($(#[$meta])*, $api, test_returns_none_for_a_revoked_hammerfest_session);
    // register_test!($(#[$meta])*, $api, test_touch_revoke_touch_hammerfest_session_same_user);
    // register_test!($(#[$meta])*, $api, test_touch_revoke_touch_hammerfest_session_different_user);
    // register_test!($(#[$meta])*, $api, test_touch_hammerfest_session_again_with_different_user_without_revoking_first);
    // register_test!($(#[$meta])*, $api, test_touch_multiple_hammerfest_sessions_with_same_user);
    // register_test!($(#[$meta])*, $api, test_touch_hammerfest_session_causing_auto_revocation_of_both_other_key_and_user);
    register_test!($(#[$meta])*, $api, test_touch_dinoparc_session);
    register_test!($(#[$meta])*, $api, test_touch_dinoparc_session_to_update_atime_but_not_ctime);
    register_test!($(#[$meta])*, $api, test_touch_dinoparc_session_and_retrieve_it_without_atime_change);
    register_test!($(#[$meta])*, $api, test_returns_none_for_session_with_an_unknown_dinoparc_user);
    register_test!($(#[$meta])*, $api, test_returns_none_for_session_with_a_known_unauthenticated_dinoparc_user);
    register_test!($(#[$meta])*, $api, test_returns_none_for_a_revoked_dinoparc_session);
    register_test!($(#[$meta])*, $api, test_touch_revoke_touch_dinoparc_session_same_user);
    register_test!($(#[$meta])*, $api, test_touch_revoke_touch_dinoparc_session_different_user);
    register_test!($(#[$meta])*, $api, test_touch_dinoparc_session_again_with_different_user_without_revoking_first);
    register_test!($(#[$meta])*, $api, test_touch_multiple_dinoparc_sessions_with_same_user);
    register_test!($(#[$meta])*, $api, test_touch_dinoparc_session_causing_auto_revocation_of_both_other_key_and_user);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyClock, TyDinoparcStore, TyTokenStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) dinoparc_store: TyDinoparcStore,
  pub(crate) token_store: TyTokenStore,
}

// pub(crate) async fn test_touch_twinoid_oauth<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>(
//   api: TestApi<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   api
//     .twinoid_store
//     .twinoid_store()
//     .touch_short_user(TouchShortUser {
//       user: ShortTwinoidUser {
//         id: "1".parse().unwrap(),
//         display_name: "alice".parse().unwrap(),
//       },
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_twinoid_oauth(&TouchOauthTokenOptions {
//       access_token: "X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap(),
//       refresh_token: Some("HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap()),
//       expiration_time: Instant::ymd_hms(2021, 1, 1, 1, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api
//     .token_store
//     .get_twinoid_oauth(TwinoidUserId::from_str("1").unwrap().as_ref())
//     .await
//     .unwrap();
//   let expected = TwinoidOauth {
//     access_token: Some(TwinoidAccessToken {
//       key: "X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap(),
//       created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       accessed_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       expires_at: Instant::ymd_hms(2021, 1, 1, 1, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     }),
//     refresh_token: Some(TwinoidRefreshToken {
//       key: "HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap(),
//       created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       accessed_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       twinoid_user_id: "1".parse().unwrap(),
//     }),
//   };
//   assert_eq!(actual, expected);
// }

// pub(crate) async fn test_touch_twinoid_oauth_twice<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>(
//   api: TestApi<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   api
//     .twinoid_store
//     .twinoid_store()
//     .touch_short_user(TouchShortUser {
//       user: ShortTwinoidUser {
//         id: "1".parse().unwrap(),
//         display_name: "alice".parse().unwrap(),
//       },
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_twinoid_oauth(&TouchOauthTokenOptions {
//       access_token: "X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap(),
//       refresh_token: Some("HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap()),
//       expiration_time: Instant::ymd_hms(2021, 1, 1, 1, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_twinoid_oauth(&TouchOauthTokenOptions {
//       access_token: "BD8AdH420AukbvExGxL5KcJNrdRMK80s".parse().unwrap(),
//       refresh_token: Some("HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap()),
//       expiration_time: Instant::ymd_hms(2021, 1, 1, 2, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api
//     .token_store
//     .get_twinoid_oauth(TwinoidUserId::from_str("1").unwrap().as_ref())
//     .await
//     .unwrap();
//   let expected = TwinoidOauth {
//     access_token: Some(TwinoidAccessToken {
//       key: "BD8AdH420AukbvExGxL5KcJNrdRMK80s".parse().unwrap(),
//       created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//       accessed_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//       expires_at: Instant::ymd_hms(2021, 1, 1, 2, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     }),
//     refresh_token: Some(TwinoidRefreshToken {
//       key: "HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap(),
//       created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       accessed_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//       twinoid_user_id: "1".parse().unwrap(),
//     }),
//   };
//   assert_eq!(actual, expected);
// }

// pub(crate) async fn test_revoke_twinoid_access_token<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>(
//   api: TestApi<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   api
//     .twinoid_store
//     .twinoid_store()
//     .touch_short_user(TouchShortUser {
//       user: ShortTwinoidUser {
//         id: "1".parse().unwrap(),
//         display_name: "alice".parse().unwrap(),
//       },
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_twinoid_oauth(&TouchOauthTokenOptions {
//       access_token: "X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap(),
//       refresh_token: Some("HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap()),
//       expiration_time: Instant::ymd_hms(2021, 1, 1, 1, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .revoke_twinoid_access_token(&"X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api
//     .token_store
//     .get_twinoid_oauth(TwinoidUserId::from_str("1").unwrap().as_ref())
//     .await
//     .unwrap();
//   let expected = TwinoidOauth {
//     access_token: None,
//     refresh_token: Some(TwinoidRefreshToken {
//       key: "HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap(),
//       created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       accessed_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       twinoid_user_id: "1".parse().unwrap(),
//     }),
//   };
//   assert_eq!(actual, expected);
// }

// pub(crate) async fn test_revoke_twinoid_refresh_token<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>(
//   api: TestApi<TyClock, TyDinoparcStore, TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   api
//     .twinoid_store
//     .twinoid_store()
//     .touch_short_user(TouchShortUser {
//       user: ShortTwinoidUser {
//         id: "1".parse().unwrap(),
//         display_name: "alice".parse().unwrap(),
//       },
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_twinoid_oauth(&TouchOauthTokenOptions {
//       access_token: "X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap(),
//       refresh_token: Some("HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap()),
//       expiration_time: Instant::ymd_hms(2021, 1, 1, 1, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     })
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .revoke_twinoid_refresh_token(&"HfznfQUg1C2p87ESIp6WRq945ppG6swD".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api
//     .token_store
//     .get_twinoid_oauth(TwinoidUserId::from_str("1").unwrap().as_ref())
//     .await
//     .unwrap();
//   let expected = TwinoidOauth {
//     access_token: Some(TwinoidAccessToken {
//       key: "X6nhMR2zwwfLNOR6EoQ9cM03BI3i66Q6".parse().unwrap(),
//       created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       accessed_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//       expires_at: Instant::ymd_hms(2021, 1, 1, 1, 0, 0),
//       twinoid_user_id: "1".parse().unwrap(),
//     }),
//     refresh_token: None,
//   };
//   assert_eq!(actual, expected);
// }

// pub(crate) async fn test_touch_hammerfest_session<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api
//     .token_store
//     .touch_hammerfest(
//       HammerfestUserIdRef {
//         server: HammerfestServer::HammerfestFr,
//         id: "1".parse().unwrap(),
//       },
//       &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     )
//     .await
//     .unwrap();
//   let expected = StoredHammerfestSession {
//     key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     user: alice.as_ref(),
//     ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//     atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//   };
//   assert_eq!(actual, expected);
// }
//
// pub(crate) async fn test_touch_hammerfest_session_to_update_atime_but_not_ctime<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(
//       HammerfestUserIdRef {
//         server: HammerfestServer::HammerfestFr,
//         id: "1".parse().unwrap(),
//       },
//       &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     )
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api
//     .token_store
//     .touch_hammerfest(
//       HammerfestUserIdRef {
//         server: HammerfestServer::HammerfestFr,
//         id: "1".parse().unwrap(),
//       },
//       &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     )
//     .await
//     .unwrap();
//   let expected = StoredHammerfestSession {
//     key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     user: alice.as_ref(),
//     ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//     atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//   };
//   assert_eq!(actual, expected);
// }
//
// pub(crate) async fn test_touch_hammerfest_session_and_retrieve_it_without_atime_change<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(
//       HammerfestUserIdRef {
//         server: HammerfestServer::HammerfestFr,
//         id: "1".parse().unwrap(),
//       },
//       &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     )
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//   let expected = Some(StoredHammerfestSession {
//     key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//     user: alice.as_ref(),
//     ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//     atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
//   });
//   assert_eq!(actual, expected);
// }
//
// pub(crate) async fn test_returns_none_for_session_with_an_unknown_hammerfest_user<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let actual = api
//     .token_store
//     .get_hammerfest(HammerfestUserIdRef {
//       server: HammerfestServer::HammerfestFr,
//       id: "1".parse().unwrap(),
//     })
//     .await
//     .unwrap();
//   let expected = None;
//   assert_eq!(actual, expected);
// }
//
// pub(crate) async fn test_returns_none_for_session_with_a_known_unauthenticated_hammerfest_user<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//   let expected = None;
//   assert_eq!(actual, expected);
// }
//
// pub(crate) async fn test_returns_none_for_a_revoked_hammerfest_session<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .revoke_hammerfest(alice.server, &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//   let expected = None;
//   assert_eq!(actual, expected);
// }
//
// pub(crate) async fn test_touch_revoke_touch_hammerfest_session_same_user<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .revoke_hammerfest(alice.server, &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api
//       .token_store
//       .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//       .await
//       .unwrap();
//     let expected = StoredHammerfestSession {
//       key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//       user: alice.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//     };
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//     let expected = Some(StoredHammerfestSession {
//       key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//       user: alice.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//     });
//     assert_eq!(actual, expected);
//   }
// }
//
// pub(crate) async fn test_touch_revoke_touch_hammerfest_session_different_user<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let bob = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "2".parse().unwrap(),
//     username: "bob".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&bob).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .revoke_hammerfest(alice.server, &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api
//       .token_store
//       .touch_hammerfest(bob.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//       .await
//       .unwrap();
//     let expected = StoredHammerfestSession {
//       key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//       user: bob.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//     };
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//     let expected = None;
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(bob.as_ref()).await.unwrap();
//     let expected = Some(StoredHammerfestSession {
//       key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//       user: bob.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//     });
//     assert_eq!(actual, expected);
//   }
// }
//
// pub(crate) async fn test_touch_hammerfest_session_again_with_different_user_without_revoking_first<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let bob = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "2".parse().unwrap(),
//     username: "bob".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&bob).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api
//       .token_store
//       .touch_hammerfest(bob.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//       .await
//       .unwrap();
//     let expected = StoredHammerfestSession {
//       key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//       user: bob.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//     };
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//     let expected = None;
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(bob.as_ref()).await.unwrap();
//     let expected = Some(StoredHammerfestSession {
//       key: "aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
//       user: bob.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
//     });
//     assert_eq!(actual, expected);
//   }
// }
//
// pub(crate) async fn test_touch_multiple_hammerfest_sessions_with_same_user<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api
//       .token_store
//       .touch_hammerfest(alice.as_ref(), &"bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap())
//       .await
//       .unwrap();
//     let expected = StoredHammerfestSession {
//       key: "bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
//       user: alice.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//     };
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//     let expected = Some(StoredHammerfestSession {
//       key: "bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
//       user: alice.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
//     });
//     assert_eq!(actual, expected);
//   }
// }
//
// pub(crate) async fn test_touch_hammerfest_session_causing_auto_revocation_of_both_other_key_and_user<
//   TyClock,
//   TyDinoparcStore,
//
//   TyTokenStore,
//   TyTwinoidStore,
// >(
//   api: TestApi<TyClock, TyDinoparcStore,  TyTokenStore, TyTwinoidStore>,
// ) where
//   TyClock: SyncRef<VirtualClock>,
//   TyDinoparcStore: DinoparcStore,
//
//   TyTokenStore: TokenStore,
//   TyTwinoidStore: TwinoidStoreRef,
// {
//   api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
//   let alice = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "1".parse().unwrap(),
//     username: "alice".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&alice).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   let bob = ShortHammerfestUser {
//     server: HammerfestServer::HammerfestFr,
//     id: "2".parse().unwrap(),
//     username: "bob".parse().unwrap(),
//   };
//   api.hammerfest_store.touch_short_user(&bob).await.unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   api
//     .token_store
//     .touch_hammerfest(bob.as_ref(), &"bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap())
//     .await
//     .unwrap();
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api
//       .token_store
//       .touch_hammerfest(alice.as_ref(), &"bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap())
//       .await
//       .unwrap();
//     let expected = StoredHammerfestSession {
//       key: "bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
//       user: alice.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//     };
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(alice.as_ref()).await.unwrap();
//     let expected = Some(StoredHammerfestSession {
//       key: "bbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
//       user: alice.as_ref(),
//       ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//       atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
//     });
//     assert_eq!(actual, expected);
//   }
//   api.clock.as_ref().advance_by(Duration::from_seconds(1));
//   {
//     let actual = api.token_store.get_hammerfest(bob.as_ref()).await.unwrap();
//     let expected = None;
//     assert_eq!(actual, expected);
//   }
// }

pub(crate) async fn test_touch_dinoparc_session<TyClock, TyDinoparcStore, TyTokenStore>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .token_store
    .token_store()
    .touch_dinoparc(
      DinoparcUserIdRef {
        server: DinoparcServer::DinoparcCom,
        id: "1".parse().unwrap(),
      },
      &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    )
    .await
    .unwrap();
  let expected = StoredDinoparcSession {
    key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    user: alice.as_ref(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
    atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_dinoparc_session_to_update_atime_but_not_ctime<TyClock, TyDinoparcStore, TyTokenStore>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(
      DinoparcUserIdRef {
        server: DinoparcServer::DinoparcCom,
        id: "1".parse().unwrap(),
      },
      &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    )
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .token_store
    .token_store()
    .touch_dinoparc(
      DinoparcUserIdRef {
        server: DinoparcServer::DinoparcCom,
        id: "1".parse().unwrap(),
      },
      &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    )
    .await
    .unwrap();
  let expected = StoredDinoparcSession {
    key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    user: alice.as_ref(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
    atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_dinoparc_session_and_retrieve_it_without_atime_change<
  TyClock,
  TyDinoparcStore,
  TyTokenStore,
>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(
      DinoparcUserIdRef {
        server: DinoparcServer::DinoparcCom,
        id: "1".parse().unwrap(),
      },
      &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    )
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .token_store
    .token_store()
    .get_dinoparc(alice.as_ref())
    .await
    .unwrap();
  let expected = Some(StoredDinoparcSession {
    key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
    user: alice.as_ref(),
    ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
    atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
  });
  assert_eq!(actual, expected);
}

pub(crate) async fn test_returns_none_for_session_with_an_unknown_dinoparc_user<
  TyClock,
  TyDinoparcStore,
  TyTokenStore,
>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .token_store
    .token_store()
    .get_dinoparc(DinoparcUserIdRef {
      server: DinoparcServer::DinoparcCom,
      id: "1".parse().unwrap(),
    })
    .await
    .unwrap();
  let expected = None;
  assert_eq!(actual, expected);
}

pub(crate) async fn test_returns_none_for_session_with_a_known_unauthenticated_dinoparc_user<
  TyClock,
  TyDinoparcStore,
  TyTokenStore,
>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .token_store
    .token_store()
    .get_dinoparc(alice.as_ref())
    .await
    .unwrap();
  let expected = None;
  assert_eq!(actual, expected);
}

pub(crate) async fn test_returns_none_for_a_revoked_dinoparc_session<TyClock, TyDinoparcStore, TyTokenStore>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .revoke_dinoparc(alice.server, &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .token_store
    .token_store()
    .get_dinoparc(alice.as_ref())
    .await
    .unwrap();
  let expected = None;
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_revoke_touch_dinoparc_session_same_user<TyClock, TyDinoparcStore, TyTokenStore>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .revoke_dinoparc(alice.server, &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
      .await
      .unwrap();
    let expected = StoredDinoparcSession {
      key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
      user: alice.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    };
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .get_dinoparc(alice.as_ref())
      .await
      .unwrap();
    let expected = Some(StoredDinoparcSession {
      key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
      user: alice.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    });
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_revoke_touch_dinoparc_session_different_user<TyClock, TyDinoparcStore, TyTokenStore>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "2".parse().unwrap(),
    username: "bob".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&bob)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .revoke_dinoparc(alice.server, &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .touch_dinoparc(bob.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
      .await
      .unwrap();
    let expected = StoredDinoparcSession {
      key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
      user: bob.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
    };
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .get_dinoparc(alice.as_ref())
      .await
      .unwrap();
    let expected = None;
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api.token_store.token_store().get_dinoparc(bob.as_ref()).await.unwrap();
    let expected = Some(StoredDinoparcSession {
      key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
      user: bob.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
    });
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_dinoparc_session_again_with_different_user_without_revoking_first<
  TyClock,
  TyDinoparcStore,
  TyTokenStore,
>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "2".parse().unwrap(),
    username: "bob".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&bob)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .touch_dinoparc(bob.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
      .await
      .unwrap();
    let expected = StoredDinoparcSession {
      key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
      user: bob.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    };
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .get_dinoparc(alice.as_ref())
      .await
      .unwrap();
    let expected = None;
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api.token_store.token_store().get_dinoparc(bob.as_ref()).await.unwrap();
    let expected = Some(StoredDinoparcSession {
      key: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap(),
      user: bob.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    });
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_multiple_dinoparc_sessions_with_same_user<TyClock, TyDinoparcStore, TyTokenStore>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .touch_dinoparc(alice.as_ref(), &"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap())
      .await
      .unwrap();
    let expected = StoredDinoparcSession {
      key: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
      user: alice.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
    };
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .get_dinoparc(alice.as_ref())
      .await
      .unwrap();
    let expected = Some(StoredDinoparcSession {
      key: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
      user: alice.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
    });
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_dinoparc_session_causing_auto_revocation_of_both_other_key_and_user<
  TyClock,
  TyDinoparcStore,
  TyTokenStore,
>(
  api: TestApi<TyClock, TyDinoparcStore, TyTokenStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyDinoparcStore: DinoparcStoreRef,
  TyTokenStore: TokenStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "1".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&alice)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let bob = ShortDinoparcUser {
    server: DinoparcServer::DinoparcCom,
    id: "2".parse().unwrap(),
    username: "bob".parse().unwrap(),
  };
  api
    .dinoparc_store
    .dinoparc_store()
    .touch_short_user(&bob)
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(alice.as_ref(), &"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  api
    .token_store
    .token_store()
    .touch_dinoparc(bob.as_ref(), &"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap())
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .touch_dinoparc(alice.as_ref(), &"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap())
      .await
      .unwrap();
    let expected = StoredDinoparcSession {
      key: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
      user: alice.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
    };
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .token_store
      .token_store()
      .get_dinoparc(alice.as_ref())
      .await
      .unwrap();
    let expected = Some(StoredDinoparcSession {
      key: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".parse().unwrap(),
      user: alice.as_ref(),
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 4),
    });
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api.token_store.token_store().get_dinoparc(bob.as_ref()).await.unwrap();
    let expected = None;
    assert_eq!(actual, expected);
  }
}
