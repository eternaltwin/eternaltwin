use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::Instant;
use eternaltwin_core::dinoparc::{DinoparcServer, DinoparcSessionKey, DinoparcUserIdRef, StoredDinoparcSession};
use eternaltwin_core::hammerfest::{HammerfestSessionKey, HammerfestUserIdRef, StoredHammerfestSession};
use eternaltwin_core::token::{GetDinoparcTokenError, RevokeDinoparcTokenError, TokenStore, TouchDinoparcTokenError};
use std::collections::HashMap;
use std::fmt::Debug;
use std::sync::RwLock;

struct MemDinoparcServers {
  dinoparc_com: MemSessions<StoredDinoparcSession>,
  en_dinoparc_com: MemSessions<StoredDinoparcSession>,
  sp_dinoparc_com: MemSessions<StoredDinoparcSession>,
}

impl MemDinoparcServers {
  fn new() -> Self {
    Self {
      dinoparc_com: MemSessions::new(),
      en_dinoparc_com: MemSessions::new(),
      sp_dinoparc_com: MemSessions::new(),
    }
  }

  fn get(&self, server: DinoparcServer) -> &MemSessions<StoredDinoparcSession> {
    match server {
      DinoparcServer::DinoparcCom => &self.dinoparc_com,
      DinoparcServer::EnDinoparcCom => &self.en_dinoparc_com,
      DinoparcServer::SpDinoparcCom => &self.sp_dinoparc_com,
    }
  }

  fn get_mut(&mut self, server: DinoparcServer) -> &mut MemSessions<StoredDinoparcSession> {
    match server {
      DinoparcServer::DinoparcCom => &mut self.dinoparc_com,
      DinoparcServer::EnDinoparcCom => &mut self.en_dinoparc_com,
      DinoparcServer::SpDinoparcCom => &mut self.sp_dinoparc_com,
    }
  }
}

struct MemSessions<Session: MemSession> {
  sessions: HashMap<Session::SessionKey, Session>,
  session_by_user: HashMap<Session::UserId, Session::SessionKey>,
}

impl<Session: MemSession> MemSessions<Session> {
  fn new() -> Self {
    Self {
      sessions: HashMap::new(),
      session_by_user: HashMap::new(),
    }
  }

  fn touch(&mut self, now: Instant, key: &Session::SessionKey, user: Session::UserId) -> Session {
    touch_session(&mut self.sessions, &mut self.session_by_user, now, key, user)
  }

  fn get(&self, user: Session::UserId) -> Option<Session> {
    get_session(&self.sessions, &self.session_by_user, &user).cloned()
  }

  fn revoke(&mut self, key: &Session::SessionKey) {
    revoke_session::<Session>(&mut self.sessions, &mut self.session_by_user, key)
  }
}

trait MemSession: Clone {
  type SessionKey: Debug + Clone + Eq + core::hash::Hash;
  type UserId: Debug + Clone + Eq + core::hash::Hash;

  fn new(now: Instant, key: Self::SessionKey, user_id: Self::UserId) -> Self;

  fn user_id(&self) -> &Self::UserId;

  fn atime_mut(&mut self) -> &mut Instant;
}

fn touch_session<Session: MemSession>(
  sessions: &mut HashMap<Session::SessionKey, Session>,
  session_by_user: &mut HashMap<Session::UserId, Session::SessionKey>,
  now: Instant,
  key: &Session::SessionKey,
  user_id: Session::UserId,
) -> Session {
  let old_session = sessions.get_mut(key);
  if let Some(old_session) = old_session {
    let old_user_id = old_session.user_id();
    if old_user_id == &user_id {
      // Same user: simply update atime
      *old_session.atime_mut() = now;
      old_session.clone()
    } else {
      // User changed: revoke and insert
      session_by_user.remove(old_user_id);
      session_by_user.insert(user_id.clone(), key.clone());
      let session = Session::new(now, key.clone(), user_id);
      sessions.insert(key.clone(), session.clone());
      session
    }
  } else {
    // Fresh insert
    session_by_user.insert(user_id.clone(), key.clone());
    let session = Session::new(now, key.clone(), user_id);
    sessions.insert(key.clone(), session.clone());
    session
  }
}

fn get_session<'a, Session: MemSession>(
  sessions: &'a HashMap<Session::SessionKey, Session>,
  session_by_user: &HashMap<Session::UserId, Session::SessionKey>,
  user_id: &Session::UserId,
) -> Option<&'a Session> {
  let key = session_by_user.get(user_id)?;
  Some(sessions.get(key).unwrap())
}

impl MemSession for StoredHammerfestSession {
  type SessionKey = HammerfestSessionKey;
  type UserId = HammerfestUserIdRef;

  fn new(now: Instant, key: Self::SessionKey, user_id: Self::UserId) -> Self {
    Self {
      key,
      user: user_id,
      ctime: now,
      atime: now,
    }
  }

  fn user_id(&self) -> &Self::UserId {
    &self.user
  }

  fn atime_mut(&mut self) -> &mut Instant {
    &mut self.atime
  }
}

impl MemSession for StoredDinoparcSession {
  type SessionKey = DinoparcSessionKey;
  type UserId = DinoparcUserIdRef;

  fn new(now: Instant, key: Self::SessionKey, user_id: Self::UserId) -> Self {
    Self {
      key,
      user: user_id,
      ctime: now,
      atime: now,
    }
  }

  fn user_id(&self) -> &Self::UserId {
    &self.user
  }

  fn atime_mut(&mut self) -> &mut Instant {
    &mut self.atime
  }
}

fn revoke_session<Session: MemSession>(
  sessions: &mut HashMap<Session::SessionKey, Session>,
  session_by_user: &mut HashMap<Session::UserId, Session::SessionKey>,
  key: &Session::SessionKey,
) {
  let session = sessions.remove(key);
  if let Some(session) = session {
    let old_key = session_by_user.remove(session.user_id());
    debug_assert_eq!(old_key.as_ref(), Some(key))
  }
}

struct StoreState {
  dinoparc: MemDinoparcServers,
}

impl StoreState {
  fn new() -> Self {
    Self {
      dinoparc: MemDinoparcServers::new(),
    }
  }
}

pub struct MemTokenStore<TyClock>
where
  TyClock: ClockRef,
{
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemTokenStore<TyClock>
where
  TyClock: ClockRef,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> TokenStore for MemTokenStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn touch_dinoparc(
    &self,
    user: DinoparcUserIdRef,
    key: &DinoparcSessionKey,
  ) -> Result<StoredDinoparcSession, TouchDinoparcTokenError> {
    let mut state = self.state.write().unwrap();
    let server = state.dinoparc.get_mut(user.server);
    let now = self.clock.clock().now();
    Ok(server.touch(now, key, user))
  }

  async fn revoke_dinoparc(
    &self,
    server: DinoparcServer,
    key: &DinoparcSessionKey,
  ) -> Result<(), RevokeDinoparcTokenError> {
    let mut state = self.state.write().unwrap();
    let server = state.dinoparc.get_mut(server);
    server.revoke(key);
    Ok(())
  }

  async fn get_dinoparc(
    &self,
    user: DinoparcUserIdRef,
  ) -> Result<Option<StoredDinoparcSession>, GetDinoparcTokenError> {
    let state = self.state.read().unwrap();
    let server = state.dinoparc.get(user.server);
    Ok(server.get(user))
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemTokenStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::dinoparc::DinoparcStore;
  use eternaltwin_core::token::TokenStore;
  use eternaltwin_dinoparc_store::mem::MemDinoparcStore;
  use std::sync::Arc;

  #[allow(clippy::type_complexity)]
  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn DinoparcStore>, Arc<dyn TokenStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let dinoparc_store: Arc<dyn DinoparcStore> = Arc::new(MemDinoparcStore::new(Arc::clone(&clock)));
    let token_store: Arc<dyn TokenStore> = Arc::new(MemTokenStore::new(Arc::clone(&clock)));

    TestApi {
      clock,
      dinoparc_store,
      token_store,
    }
  }

  test_token_store!(|| make_test_api());
}
