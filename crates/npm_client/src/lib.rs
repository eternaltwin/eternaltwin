use async_trait::async_trait;
use auto_impl::auto_impl;
use chrono::{DateTime, Utc};
use indexmap::IndexMap;
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use thiserror::Error;
use url::Url;

#[cfg(feature = "http")]
pub mod http;

#[async_trait]
#[auto_impl(&, Arc)]
pub trait NpmClient: Send + Sync {
  async fn get_package(&self, req: GetPackageRequestView<'_>) -> Result<Packument, GetPackageError>;
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum GetPackageError {
  #[error("failed to send `GetPackage` request: {0}")]
  Send(String),
  #[error("failed to receive `GetPackage` response: {0}")]
  Receive(String),
  #[error("package not found")]
  NotFound,
  #[error("unexpected `GetPackage` error: {0}")]
  Other(String),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetPackageRequest<Str = String> {
  pub name: Str,
}

pub type GetPackageRequestView<'req> = GetPackageRequest<&'req str>;

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Packument {
  pub _id: String,
  pub _rev: Option<String>,
  pub name: String,
  pub versions: IndexMap<String, PackageVersion>,
  pub time: PackumentTime,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PackageVersion {
  pub name: String,
  pub version: String,
  pub homepage: Option<Url>,
  pub description: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PackumentTime {
  pub created: DateTime<Utc>,
  pub modified: DateTime<Utc>,
  #[serde(flatten)]
  pub versions: BTreeMap<String, DateTime<Utc>>,
}
