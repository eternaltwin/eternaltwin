use crate::{GetPackageError, GetPackageRequestView, NpmClient, Packument};
use async_trait::async_trait;
use reqwest::{Client, StatusCode};
use url::Url;

pub struct HttpNpmClient {
  client: Client,
  server: Url,
}

impl HttpNpmClient {
  pub fn new() -> Self {
    Self {
      client: Client::builder()
        .user_agent("npm_client")
        .build()
        .expect("building the client always succeeds"),
      server: Url::parse("https://registry.npmjs.org/").expect("the npm registry URL is well-formed"),
    }
  }

  pub fn api_url<I>(&self, segments: I) -> Url
  where
    I: IntoIterator,
    I::Item: AsRef<str>,
  {
    let mut res = self.server.clone();
    {
      let mut p = res.path_segments_mut().expect("npm registry URL has path segments");
      p.extend(segments);
    }
    res
  }
}

impl Default for HttpNpmClient {
  fn default() -> Self {
    Self::new()
  }
}

#[async_trait]
impl NpmClient for HttpNpmClient {
  async fn get_package(&self, req: GetPackageRequestView<'_>) -> Result<Packument, GetPackageError> {
    let url = self.api_url([req.name]);
    let res = self
      .client
      .get(url)
      .send()
      .await
      .map_err(|e| GetPackageError::Send(format!("{e:?}")))?;

    match res.status() {
      StatusCode::OK => {
        let body: String = res
          .text()
          .await
          .map_err(|e| GetPackageError::Receive(format!("{e:?}")))?;
        let body: Packument = serde_json::from_str(&body).map_err(|e| GetPackageError::Receive(format!("{e:?}")))?;
        Ok(body)
      }
      StatusCode::NOT_FOUND => Err(GetPackageError::NotFound),
      code => Err(GetPackageError::Receive(format!("unexpected status code {code}"))),
    }
  }
}
