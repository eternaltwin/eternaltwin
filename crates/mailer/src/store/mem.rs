use crate::store::sqlite::SqliteMailerStore;
use async_trait::async_trait;
use eternaltwin_core::core::{Handler, Listing};
use eternaltwin_core::mailer::store::acquire_outbound_email_request::{
  AcquireOutboundEmailRequest, AcquireOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::create_outbound_email::{CreateOutboundEmail, CreateOutboundEmailError};
use eternaltwin_core::mailer::store::get_outbound_email_requests::{
  GetOutboundEmailRequests, GetOutboundEmailRequestsError,
};
use eternaltwin_core::mailer::store::get_outbound_emails::{GetOutboundEmails, GetOutboundEmailsError};
use eternaltwin_core::mailer::store::release_outbound_email_request::{
  ReleaseOutboundEmailRequest, ReleaseOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::{OutboundEmail, OutboundEmailRequest};
use eternaltwin_core::uuid::UuidGeneratorRef;
use eternaltwin_db_schema::force_create_latest_sqlite;
use serde_json::Value as JsonValue;
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::{Pool, Sqlite};
use std::sync::Arc;

pub struct MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  inner: SqliteMailerStore<Arc<Pool<Sqlite>>, TyUuidGenerator>,
}

impl<TyUuidGenerator> MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  pub async fn new(uuid_generator: TyUuidGenerator) -> Self {
    // each in-memory connection gets its own DB, so limit to 1 connection max
    let database = SqlitePoolOptions::new()
      .max_connections(1)
      .connect_with(SqliteConnectOptions::new())
      .await
      .expect("creating in-memory sqlite succeeds");

    force_create_latest_sqlite(&database)
      .await
      .expect("db initialization succeeds");

    let database = Arc::new(database);

    Self {
      inner: SqliteMailerStore::new(database, uuid_generator),
    }
  }
}

#[async_trait]
impl<TyUuidGenerator> Handler<CreateOutboundEmail<JsonValue>> for MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: CreateOutboundEmail<JsonValue>,
  ) -> Result<OutboundEmail<JsonValue>, CreateOutboundEmailError> {
    self.inner.handle(cmd).await
  }
}

#[async_trait]
impl<TyUuidGenerator> Handler<AcquireOutboundEmailRequest> for MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: AcquireOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, AcquireOutboundEmailRequestError> {
    self.inner.handle(cmd).await
  }
}

#[async_trait]
impl<TyUuidGenerator> Handler<ReleaseOutboundEmailRequest> for MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: ReleaseOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, ReleaseOutboundEmailRequestError> {
    self.inner.handle(cmd).await
  }
}

#[async_trait]
impl<TyUuidGenerator> Handler<GetOutboundEmails<JsonValue>> for MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    query: GetOutboundEmails<JsonValue>,
  ) -> Result<Listing<OutboundEmail<JsonValue>>, GetOutboundEmailsError> {
    self.inner.handle(query).await
  }
}

#[async_trait]
impl<TyUuidGenerator> Handler<GetOutboundEmailRequests> for MemMailerStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    query: GetOutboundEmailRequests,
  ) -> Result<Listing<OutboundEmailRequest>, GetOutboundEmailRequestsError> {
    self.inner.handle(query).await
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::store::test::test_mailer_store;
  use crate::store::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::mailer::store::MailerStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_user_store::mem::MemUserStore;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn MailerStore<JsonValue>>, Arc<dyn UserStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);

    let mailer_store: Arc<dyn MailerStore<JsonValue>> =
      Arc::new(MemMailerStore::new(Arc::clone(&uuid_generator)).await);
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(Arc::clone(&clock), uuid_generator));

    TestApi {
      clock,
      mailer_store,
      user_store,
    }
  }

  test_mailer_store!(|| make_test_api().await);
}
