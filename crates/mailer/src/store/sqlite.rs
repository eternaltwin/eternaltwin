use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::core::{Duration, FinitePeriod, Handler, Instant, Listing, PeriodLower};
use eternaltwin_core::mailer::store::acquire_outbound_email_request::{
  AcquireOutboundEmailRequest, AcquireOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::create_outbound_email::{CreateOutboundEmail, CreateOutboundEmailError};
use eternaltwin_core::mailer::store::get_outbound_email_requests::{
  GetOutboundEmailRequests, GetOutboundEmailRequestsError,
};
use eternaltwin_core::mailer::store::get_outbound_emails::{GetOutboundEmails, GetOutboundEmailsError};
use eternaltwin_core::mailer::store::release_outbound_email_request::{
  ReleaseOutboundEmailRequest, ReleaseOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::{
  EmailDeliveryStatus, EmailRequestStatus, OutboundEmail, OutboundEmailId, OutboundEmailIdRef, OutboundEmailRequest,
  OutboundEmailRequestId,
};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use serde_json::Value as JsonValue;
use sqlx::{Pool, Sqlite};

pub struct SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
}

impl<TyDatabase, TyUuidGenerator> SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(database: TyDatabase, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      database,
      uuid_generator,
    }
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<CreateOutboundEmail<JsonValue>>
  for SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: CreateOutboundEmail<JsonValue>,
  ) -> Result<OutboundEmail<JsonValue>, CreateOutboundEmailError> {
    if cmd.deadline <= cmd.now {
      return Err(CreateOutboundEmailError::InvalidDeadline(cmd.deadline, cmd.now));
    }

    let outbound_email_id = OutboundEmailId::from_uuid(self.uuid_generator.uuid_generator().next());

    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      outbound_email_id: OutboundEmailId,
    }
    // language=sqlite
    let written = sqlx::query_as::<Sqlite, Row>(
      r"
          insert
          into outbound_email(
            outbound_email_id, idempotency_key, created_by,
            submitted_at, deadline,
            sender, recipient,
            payload_kind, payload_version, payload,
            text_size, text_sha2_256, text_sha3_256,
            html_size, html_sha2_256, html_sha3_256,
            delivery_status, next_request_at, read_at)
          values (
            ?, ?, ?,
            ?, ?,
            ?, ?,
            ?, ?, ?,
            ?, ?, ?,
            ?, ?, ?,
            'Pending', ?, null
          )
          returning outbound_email_id;
        ",
    )
    .bind(outbound_email_id)
    .bind(cmd.idempotency_key)
    .bind(cmd.actor.id)
    .bind(cmd.now)
    .bind(cmd.deadline)
    .bind(cmd.sender.as_str())
    .bind(cmd.recipient.as_str())
    .bind(cmd.payload.kind)
    .bind(cmd.payload.version)
    .bind(&cmd.payload.data)
    .bind(cmd.payload.text.size)
    .bind(cmd.payload.text.sha2_256)
    .bind(cmd.payload.text.sha3_256)
    .bind(cmd.payload.html.size)
    .bind(cmd.payload.html.sha2_256)
    .bind(cmd.payload.html.sha3_256)
    .bind(cmd.now)
    .fetch_one(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    assert_eq!(written.outbound_email_id, outbound_email_id);

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(OutboundEmail {
      id: outbound_email_id,
      submitted_at: cmd.now,
      created_by: cmd.actor,
      deadline: cmd.deadline,
      sender: cmd.sender,
      recipient: cmd.recipient,
      payload: cmd.payload,
      read_at: None,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<AcquireOutboundEmailRequest>
  for SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: AcquireOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, AcquireOutboundEmailRequestError> {
    let outbound_email_request_id = OutboundEmailRequestId::from_uuid(self.uuid_generator.uuid_generator().next());
    let period = FinitePeriod::new(cmd.now, cmd.now + Duration::from_hours(1));

    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    {
      let rate_limiter_period = PeriodLower::new(cmd.now - Duration::from_hours(12), Some(cmd.now));

      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        count: u32,
      }
      // language=SQLite
      let res = sqlx::query_as::<_, Row>(
        r"
          select count(*) as count
          from outbound_email_request
          where ? < period_end and period_start < ?
        ",
      )
      .bind(rate_limiter_period.start())
      .bind(rate_limiter_period.end())
      .fetch_one(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      if res.count >= 40 {
        return Err(AcquireOutboundEmailRequestError::Limit(
          cmd.now + Duration::from_hours(1),
        ));
      }
    }

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      outbound_email_request_id: OutboundEmailRequestId,
      outbound_email_id: OutboundEmailId,
    }
    // language=SQLite
    let res = sqlx::query_as::<_, Row>(
      r"
          insert
          into outbound_email_request(
            outbound_email_request_id,
            outbound_email_id, period_start, period_end,
            status, result_version, result
          )
          values (
            ?,
            ?, ?, ?,
            'Pending', null, null
          )
          returning outbound_email_request_id, outbound_email_id;
        ",
    )
    .bind(outbound_email_request_id)
    .bind(cmd.outbound_email)
    .bind(period.start)
    .bind(period.end)
    .fetch_one(&mut *tx)
    .await;

    let written = match res {
      Ok(written) => written,
      Err(e) => return Err(AcquireOutboundEmailRequestError::Other(WeakError::wrap(e))),
    };

    assert_eq!(written.outbound_email_request_id, outbound_email_request_id);

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(OutboundEmailRequest {
      id: outbound_email_request_id,
      email: OutboundEmailIdRef::new(written.outbound_email_id),
      period,
      status: EmailRequestStatus::Pending,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<ReleaseOutboundEmailRequest>
  for SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: ReleaseOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, ReleaseOutboundEmailRequestError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    let new_status = match cmd.result {
      None => EmailRequestStatus::Cancelled,
      Some(Ok(())) => EmailRequestStatus::Ok,
      Some(Err(())) => EmailRequestStatus::Error,
    };

    #[derive(Debug, sqlx::FromRow)]
    struct ReqRow {
      outbound_email_id: OutboundEmailId,
      period_start: Instant,
      period_end: Instant,
    }
    // language=SQLite
    let req_row = sqlx::query_as::<_, ReqRow>(
      r"
          update outbound_email_request
          set period_end = ?, status = ?, result_version = null, result = null
          where outbound_email_request_id = ? and status = 'Pending' and period_start <= ? and ? < period_end
          returning outbound_email_id, period_start, period_end
        ",
    )
    .bind(cmd.now)
    .bind(new_status)
    .bind(cmd.outbound_email_request.id)
    .bind(cmd.now)
    .bind(cmd.now)
    .fetch_one(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    if new_status == EmailRequestStatus::Ok {
      // language=SQLite
      let res = sqlx::query::<_>(
        r"
          update outbound_email
          set delivery_status = ?
          where outbound_email_id = ? and delivery_status = 'Pending'
        ",
      )
      .bind(EmailDeliveryStatus::Sent)
      .bind(req_row.outbound_email_id)
      .execute(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;

      if res.rows_affected() != 1 {
        return Err(ReleaseOutboundEmailRequestError::Other(WeakError::new(format!(
          "no pending `outbound_email` row found for id {}",
          req_row.outbound_email_id
        ))));
      }
    }

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(OutboundEmailRequest {
      id: cmd.outbound_email_request.id,
      email: OutboundEmailIdRef::new(req_row.outbound_email_id),
      period: FinitePeriod::new(req_row.period_start, req_row.period_end),
      status: new_status,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<GetOutboundEmails<JsonValue>>
  for SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    _query: GetOutboundEmails<JsonValue>,
  ) -> Result<Listing<OutboundEmail<JsonValue>>, GetOutboundEmailsError> {
    todo!()
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<GetOutboundEmailRequests> for SqliteMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<Pool<Sqlite>>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    _query: GetOutboundEmailRequests,
  ) -> Result<Listing<OutboundEmailRequest>, GetOutboundEmailRequestsError> {
    todo!()
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::store::test::test_mailer_store;
  use crate::store::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::mailer::store::MailerStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest_sqlite;
  use eternaltwin_user_store::mem::MemUserStore;
  use serial_test::serial;
  use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn MailerStore<JsonValue>>, Arc<dyn UserStore>> {
    let config = eternaltwin_config::Config::for_test();
    let db_file = config.sqlite.file.value;
    std::fs::File::create(db_file.as_str()).expect("failed to create Sqlite DB");

    let database = SqlitePoolOptions::new()
      .max_connections(1)
      .connect_with(SqliteConnectOptions::new().filename(db_file.as_str()))
      .await
      .expect("creating in-memory sqlite succeeds");

    force_create_latest_sqlite(&database)
      .await
      .expect("db initialization succeeds");

    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);

    let mailer_store: Arc<dyn MailerStore<JsonValue>> =
      Arc::new(SqliteMailerStore::new(database, Arc::clone(&uuid_generator)));
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(Arc::clone(&clock), uuid_generator));

    TestApi {
      clock,
      mailer_store,
      user_store,
    }
  }

  test_mailer_store!(
    #[serial]
    || make_test_api().await
  );
}
