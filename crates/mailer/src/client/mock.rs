use async_trait::async_trait;
use eternaltwin_core::email::{EmailAddress, EmailContent, Mailer, MailerRef};
use eternaltwin_core::types::WeakError;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::sync::RwLock;

struct MemMailerState {
  inboxes: HashMap<EmailAddress, Vec<EmailContent>>,
  /// If true, inboxes are inserted automatically when an email is sent.
  ///
  /// This means that there will be no failure due to an unknown recipient email address.
  touch_inbox_on_send: bool,
}

impl MemMailerState {
  pub fn new(touch_inbox_on_send: bool) -> Self {
    Self {
      inboxes: HashMap::new(),
      touch_inbox_on_send,
    }
  }
}

pub struct MemMailer {
  state: RwLock<MemMailerState>,
}

impl Default for MemMailer {
  fn default() -> Self {
    Self::new(false)
  }
}

impl MemMailer {
  pub fn new(touch_inbox_on_send: bool) -> Self {
    Self {
      state: RwLock::new(MemMailerState::new(touch_inbox_on_send)),
    }
  }

  pub fn create_inbox(&self, address: EmailAddress) {
    let mut state = self.state.write().unwrap();
    state.inboxes.insert(address, Vec::new());
  }

  pub fn read_inbox(&self, address: &EmailAddress) -> Vec<EmailContent> {
    let state = self.state.read().unwrap();
    state.inboxes.get(address).unwrap().clone()
  }
}

impl MailerRef for MemMailer {
  type Mailer = Self;

  fn mailer(&self) -> &Self::Mailer {
    self
  }
}

#[async_trait]
impl Mailer for MemMailer {
  async fn send_email(&self, recipient: &EmailAddress, content: &EmailContent) -> Result<(), WeakError> {
    let mut state = self.state.write().unwrap();
    let state = &mut *state;
    let mut entry = state.inboxes.entry(recipient.clone());
    let inbox: &mut Vec<_> = match entry {
      Entry::Vacant(e) => {
        if state.touch_inbox_on_send {
          e.insert(Vec::new())
        } else {
          return Err(WeakError::new("RecipientNotFound"));
        }
      }
      Entry::Occupied(ref mut e) => e.get_mut(),
    };

    inbox.push(content.clone());
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use crate::client::mock::MemMailer;
  use eternaltwin_core::email::{EmailAddress, EmailContent, Mailer};

  #[tokio::test]
  async fn verify_registration_en() {
    let mailer = MemMailer::new(false);

    let alice: EmailAddress = "alice@example.com".parse().unwrap();

    mailer.create_inbox(alice.clone());

    {
      let actual = mailer.read_inbox(&alice);
      let expected = Vec::new();
      assert_eq!(actual, expected);
    }

    mailer
      .send_email(
        &alice,
        &EmailContent {
          subject: "Hi".parse().unwrap(),
          body_text: "Hello Alice!\n".parse().unwrap(),
          body_html: None,
        },
      )
      .await
      .unwrap();

    {
      let actual = mailer.read_inbox(&alice);
      let expected = vec![EmailContent {
        subject: "Hi".parse().unwrap(),
        body_text: "Hello Alice!\n".parse().unwrap(),
        body_html: None,
      }];
      assert_eq!(actual, expected);
    }

    mailer
      .send_email(
        &alice,
        &EmailContent {
          subject: "RE: Hi".parse().unwrap(),
          body_text: "Hello again!\n".parse().unwrap(),
          body_html: None,
        },
      )
      .await
      .unwrap();

    {
      let actual = mailer.read_inbox(&alice);
      let expected = vec![
        EmailContent {
          subject: "Hi".parse().unwrap(),
          body_text: "Hello Alice!\n".parse().unwrap(),
          body_html: None,
        },
        EmailContent {
          subject: "RE: Hi".parse().unwrap(),
          body_text: "Hello again!\n".parse().unwrap(),
          body_html: None,
        },
      ];
      assert_eq!(actual, expected);
    }
  }
}
