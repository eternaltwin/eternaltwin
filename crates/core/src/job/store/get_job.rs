use crate::core::Request;
use crate::job::{JobId, StoreJob};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct GetJob {
  pub id: JobId,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetJobError {
  #[error("job {0} not found")]
  NotFound(JobId),
  #[error(transparent)]
  Other(WeakError),
}

impl GetJobError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetJob {
  type Response = Result<StoreJob, GetJobError>;
}
