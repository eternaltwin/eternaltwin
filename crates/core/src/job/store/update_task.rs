use crate::core::Request;
use crate::job::{TaskId, TaskRevId, TaskStatus, Tick};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct UpdateTask<OpaqueTask, OpaqueValue> {
  pub rev_id: TaskRevId,
  pub tick: Tick,
  pub status: TaskStatus,
  pub starvation: i32,
  pub state: OpaqueTask,
  pub output: Option<OpaqueValue>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum UpdateTaskError {
  #[error("task {0} not found")]
  NotFound(TaskId),
  #[error("cannot update task {task} due to revision conflict: expected={expected}, actual={actual}")]
  Conflict { task: TaskId, expected: u32, actual: u32 },
  #[error("task dependency {0} not found")]
  DependencyNotFound(TaskId),
  #[error("detected circular dependency from task {0}")]
  CircularDependency(TaskId),
  #[error("updating task {0} leads to overflow of the revision")]
  RevisionOverflow(TaskId),
  #[error(transparent)]
  Other(WeakError),
}

impl UpdateTaskError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl<OpaqueTask, OpaqueValue> Request for UpdateTask<OpaqueTask, OpaqueValue> {
  type Response = Result<TaskRevId, UpdateTaskError>;
}
