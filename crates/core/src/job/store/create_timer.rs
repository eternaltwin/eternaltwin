use crate::core::{Instant, Request};
use crate::job::TaskId;
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct CreateTimer {
  pub task_id: TaskId,
  pub deadline: Instant,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CreateTimerError {
  #[error("task {0} not found")]
  NotFound(TaskId),
  #[error(transparent)]
  Other(WeakError),
}

impl CreateTimerError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for CreateTimer {
  type Response = Result<(), CreateTimerError>;
}
