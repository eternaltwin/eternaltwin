pub mod create_job;
pub mod create_timer;
pub mod get_job;
pub mod get_jobs;
pub mod get_task;
pub mod get_tasks;
pub mod next_timer;
pub mod on_timer;
pub mod update_task;

pub use self::create_job::{CreateJob, CreateJobError};
pub use self::create_timer::{CreateTimer, CreateTimerError};
pub use self::get_job::{GetJob, GetJobError};
pub use self::get_jobs::{GetJobs, GetJobsError};
pub use self::get_task::{GetTask, GetTaskError};
pub use self::get_tasks::{GetTasks, GetTasksError};
pub use self::next_timer::{NextTimer, NextTimerError};
pub use self::on_timer::{OnTimer, OnTimerError};
pub use self::update_task::{UpdateTask, UpdateTaskError};
use crate::core::{Handler, Instant, Listing};
use crate::job::{StoreJob, StoreTask, TaskRevId};
use async_trait::async_trait;
use core::ops::Deref;

#[async_trait]
pub trait JobStore<OpaqueTask, OpaqueValue>:
  Send
  + Sync
  + Handler<CreateJob<OpaqueTask>>
  + Handler<GetJobs>
  + Handler<GetJob>
  + Handler<GetTask, fn() -> (OpaqueTask, OpaqueValue)>
  + Handler<GetTasks, fn() -> (OpaqueTask, OpaqueValue)>
  + Handler<UpdateTask<OpaqueTask, OpaqueValue>>
  + Handler<CreateTimer>
  + Handler<NextTimer>
  + Handler<OnTimer>
{
  async fn create_job(&self, command: CreateJob<OpaqueTask>) -> Result<StoreJob, CreateJobError>
  where
    CreateJob<OpaqueTask>: Send + 'async_trait,
  {
    Handler::<CreateJob<OpaqueTask>>::handle(self, command).await
  }

  async fn get_jobs(&self, query: GetJobs) -> Result<Listing<StoreJob>, GetJobsError> {
    Handler::<GetJobs>::handle(self, query).await
  }

  async fn get_job(&self, query: GetJob) -> Result<StoreJob, GetJobError> {
    Handler::<GetJob>::handle(self, query).await
  }

  async fn get_task(&self, query: GetTask) -> Result<StoreTask<OpaqueTask, OpaqueValue>, GetTaskError> {
    Handler::<GetTask, fn() -> (OpaqueTask, OpaqueValue)>::handle(self, query).await
  }

  async fn get_tasks(&self, query: GetTasks) -> Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, GetTasksError> {
    Handler::<GetTasks, fn() -> (OpaqueTask, OpaqueValue)>::handle(self, query).await
  }

  async fn update_task(&self, command: UpdateTask<OpaqueTask, OpaqueValue>) -> Result<TaskRevId, UpdateTaskError>
  where
    UpdateTask<OpaqueTask, OpaqueValue>: Send + 'async_trait,
  {
    Handler::<UpdateTask<OpaqueTask, OpaqueValue>>::handle(self, command).await
  }

  async fn create_timer(&self, command: CreateTimer) -> Result<(), CreateTimerError> {
    Handler::<CreateTimer>::handle(self, command).await
  }

  async fn next_timer(&self, query: NextTimer) -> Result<Option<Instant>, NextTimerError> {
    Handler::<NextTimer>::handle(self, query).await
  }

  async fn on_timer(&self, command: OnTimer) -> Result<(), OnTimerError> {
    Handler::<OnTimer>::handle(self, command).await
  }
}

impl<OpaqueTask, OpaqueValue, T> JobStore<OpaqueTask, OpaqueValue> for T where
  T: Send
    + Sync
    + Handler<CreateJob<OpaqueTask>>
    + Handler<GetJobs>
    + Handler<GetJob>
    + Handler<GetTask, fn() -> (OpaqueTask, OpaqueValue)>
    + Handler<GetTasks, fn() -> (OpaqueTask, OpaqueValue)>
    + Handler<UpdateTask<OpaqueTask, OpaqueValue>>
    + Handler<CreateTimer>
    + Handler<NextTimer>
    + Handler<OnTimer>
{
}

/// Like [`Deref`], but the target has the bound [`JobStore`]
pub trait JobStoreRef<OpaqueTask, OpaqueValue>: Send + Sync {
  type JobStore: JobStore<OpaqueTask, OpaqueValue> + ?Sized;

  fn job_store(&self) -> &Self::JobStore;
}

impl<TyRef, OpaqueTask, OpaqueValue> JobStoreRef<OpaqueTask, OpaqueValue> for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: JobStore<OpaqueTask, OpaqueValue>,
{
  type JobStore = TyRef::Target;

  fn job_store(&self) -> &Self::JobStore {
    self.deref()
  }
}
