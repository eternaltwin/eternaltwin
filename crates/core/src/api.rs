use std::ops::Deref;

pub trait SyncRef<TyTarget>: Deref<Target = TyTarget> + Send + Sync {}

impl<Ref, TyTarget> SyncRef<TyTarget> for Ref where Ref: Deref<Target = TyTarget> + Send + Sync {}

pub struct OwnRef<T>(pub T);

impl<T> Deref for OwnRef<T> {
  type Target = T;

  fn deref(&self) -> &Self::Target {
    &self.0
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  #[cfg(feature = "sqlx")]
  fn test_pg_pool() {
    // This test is about successful compilation, the body of `consumer` does
    // not really matter
    use sqlx::PgPool;
    fn consumer<TyPool: SyncRef<PgPool>>(pool: Option<TyPool>) -> bool {
      pool.is_none()
    }
    assert!(consumer::<OwnRef<PgPool>>(None));
    assert!(consumer::<&'static PgPool>(None));
    assert!(consumer::<std::sync::Arc<PgPool>>(None));
  }
}
