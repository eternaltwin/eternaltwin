use crate::core::{Duration, Instant};
use core::ops::Deref;
use core::pin::Pin;
use std::collections::BTreeMap;
use std::future::Future;
use std::sync::atomic::{AtomicI64, AtomicUsize, Ordering};
use std::sync::{Arc, RwLock};
use std::task::{Context, Poll, Waker};

pub trait Clock: Send + Sync {
  fn now(&self) -> Instant;
}

/// Like [`Deref`], but the target has the bound [`Clock`]
pub trait ClockRef: Send + Sync {
  type Clock: Clock + ?Sized;

  fn clock(&self) -> &Self::Clock;
}

impl<TyRef> ClockRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: Clock,
{
  type Clock = TyRef::Target;

  fn clock(&self) -> &Self::Clock {
    self.deref()
  }
}

/// Like [`Deref`], but the target has the bound [`Scheduler`]
pub trait SchedulerRef: ClockRef<Clock = Self::Scheduler> {
  type Scheduler: Scheduler + ?Sized;

  fn scheduler(&self) -> &Self::Scheduler;
}

impl<TyRef> SchedulerRef for TyRef
where
  TyRef: ClockRef,
  TyRef::Clock: Scheduler,
{
  type Scheduler = TyRef::Clock;

  fn scheduler(&self) -> &Self::Scheduler {
    self.clock()
  }
}

pub trait Scheduler: Clock {
  type Timer: Future<Output = ()> + Send;

  fn schedule(&self, deadline: Instant) -> Self::Timer;

  fn sleep(&self, duration: Duration) -> Self::Timer {
    let now = self.now();
    self.schedule(now + duration)
  }
}

pub type DynScheduler = dyn Scheduler<Timer = Pin<Box<dyn Future<Output = ()> + Send>>>;

pub struct VirtualClock {
  state: Arc<VirtualClockState>,
}

impl ClockRef for VirtualClock {
  type Clock = Self;

  fn clock(&self) -> &Self::Clock {
    self
  }
}

impl VirtualClock {
  pub fn new(start: Instant) -> Self {
    Self {
      state: Arc::new(VirtualClockState::new(start)),
    }
  }

  pub fn advance_by(&self, d: Duration) -> Instant {
    self.state.advance_by(d)
  }

  /// Updates the clock to the supplied time, if it more recent
  ///
  /// Returns the time after the update.
  pub fn advance_to(&self, t: Instant) -> Instant {
    self.state.advance_to(t)
  }
}

pub struct VirtualClockState {
  start: Instant,
  offset: AtomicI64,
  timer_id: AtomicUsize,
  timers: RwLock<BTreeMap<(Instant, usize), Waker>>,
}

impl VirtualClockState {
  pub fn new(start: Instant) -> Self {
    Self {
      start,
      offset: AtomicI64::new(0),
      timer_id: AtomicUsize::new(0),
      timers: RwLock::new(BTreeMap::new()),
    }
  }

  /// Returns the time after the update
  pub fn advance_by(&self, d: Duration) -> Instant {
    let d: i64 = d.milliseconds();
    assert!(d >= 0);
    let old = self.offset.fetch_add(d, Ordering::SeqCst);
    let now = self.to_instant(old + d);
    self.signal_ready(now);
    now
  }

  /// Updates the clock to the supplied time, if it more recent
  ///
  /// Returns the time after the update.
  pub fn advance_to(&self, t: Instant) -> Instant {
    assert!(t >= self.start);
    let new_duration = t.into_chrono() - self.start.into_chrono();
    let new_offset = new_duration.num_milliseconds();
    let old_offset = self.offset.fetch_max(new_offset, Ordering::SeqCst);
    assert!(new_offset >= old_offset);
    let now = self.to_instant(new_offset);
    self.signal_ready(now);
    now
  }

  fn signal_ready(&self, now: Instant) {
    let timers = self.timers.read().expect("failed to lock virtual clock timers");
    let mut ready: Vec<Waker> = Vec::new();
    for ((deadline, _id), waker) in timers.iter() {
      if *deadline > now {
        break;
      }
      ready.push(waker.clone());
    }
    drop(timers);
    for waker in ready {
      waker.wake()
    }
  }

  fn now(&self) -> Instant {
    let offset = self.offset.load(Ordering::SeqCst);
    self.to_instant(offset)
  }

  fn to_instant(&self, offset: i64) -> Instant {
    Instant::new_round_down(
      self.start.into_chrono()
        + ::chrono::Duration::try_milliseconds(offset).expect("VirtualClockState::to_instant out of bounds"),
    )
  }

  /// Create a new [`VirtualTimer`] key for the provided deadline.
  fn schedule(&self, deadline: Instant) -> (Instant, usize) {
    let id = self.timer_id.fetch_add(1, Ordering::SeqCst);
    (deadline, id)
  }

  fn clear(&self, key: (Instant, usize)) -> bool {
    let mut timers = self.timers.write().expect("failed to lock virtual clock timers");
    timers.remove(&key).is_some()
  }

  fn clear_expired(&self, key: (Instant, usize), waker: &Waker) -> bool {
    let now = self.now();
    if now < key.0 {
      let mut timers = self.timers.write().expect("failed to lock virtual clock timers");
      timers.entry(key).or_insert_with(|| waker.clone());
      false
    } else {
      self.clear(key);
      true
    }
  }
}

impl Clock for VirtualClock {
  fn now(&self) -> Instant {
    self.state.now()
  }
}

pub struct VirtualTimer {
  key: (Instant, usize),
  state: Arc<VirtualClockState>,
}

impl Drop for VirtualTimer {
  fn drop(&mut self) {
    self.state.clear(self.key);
  }
}

impl Future for VirtualTimer {
  type Output = ();

  fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
    let state = &*self.state;
    let key = self.key;
    if state.clear_expired(key, cx.waker()) {
      Poll::Ready(())
    } else {
      Poll::Pending
    }
  }
}

impl Scheduler for VirtualClock {
  type Timer = VirtualTimer;

  fn schedule(&self, deadline: Instant) -> Self::Timer {
    let state = Arc::clone(&self.state);
    let key = state.schedule(deadline);
    VirtualTimer { key, state }
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SystemClock;

impl ClockRef for SystemClock {
  type Clock = Self;

  fn clock(&self) -> &Self::Clock {
    self
  }
}

impl Clock for SystemClock {
  fn now(&self) -> Instant {
    Instant::new_round_down(::chrono::Utc::now())
  }
}

pub type SystemTimer = ::tokio::time::Sleep;

impl Scheduler for SystemClock {
  type Timer = SystemTimer;

  fn schedule(&self, deadline: Instant) -> Self::Timer {
    tokio::time::sleep_until(tokio::time::Instant::from_std(deadline.into_std()))
  }
}

pub struct ErasedScheduler<T: SchedulerRef> {
  inner: T,
}

impl<T: SchedulerRef> ErasedScheduler<T> {
  pub fn new(inner: T) -> Self {
    Self { inner }
  }
}

impl<T: SchedulerRef> Clock for ErasedScheduler<T> {
  fn now(&self) -> Instant {
    self.inner.scheduler().now()
  }
}

impl<T: SchedulerRef> Scheduler for ErasedScheduler<T>
where
  <T::Scheduler as Scheduler>::Timer: 'static,
{
  type Timer = Pin<Box<dyn Future<Output = ()> + Send>>;

  fn schedule(&self, deadline: Instant) -> Self::Timer {
    Box::pin(self.inner.scheduler().schedule(deadline))
  }
}
