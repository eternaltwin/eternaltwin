use crate::core::Request;
use crate::forum::ForumSectionRef;
use crate::types::WeakError;
use crate::user::UserIdRef;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DeleteModerator {
  pub section: ForumSectionRef,
  /// Moderator to remove
  pub target: UserIdRef,
  /// User who triggered the revocation
  pub revoker: UserIdRef,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum DeleteModeratorError {
  #[error(transparent)]
  Other(WeakError),
}

impl DeleteModeratorError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for DeleteModerator {
  type Response = Result<(), DeleteModeratorError>;
}
