use crate::core::{Listing, Request};
use crate::forum::{ForumThreadRef, RawShortForumPost};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetPosts {
  pub thread: ForumThreadRef,
  /// Post offset
  pub offset: u32,
  /// Post limit
  pub limit: u32,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetPostsError {
  #[error(transparent)]
  Other(WeakError),
}

impl GetPostsError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetPosts {
  type Response = Result<Listing<RawShortForumPost>, GetPostsError>;
}
