use crate::core::{Instant, Listing, Request};
use crate::mailer::store::OutboundEmail;
use crate::types::WeakError;
use std::marker::PhantomData;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetOutboundEmailsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

/// Retrieve the list of outbound emails
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetOutboundEmails<Opaque> {
  /// System time
  pub now: Instant,
  /// Query time
  pub time: Instant,
  /// Maximum number of results to return
  pub limit: u32,
  /// Type for opaque data
  pub opaque: PhantomData<Opaque>,
}

impl<Opaque> Request for GetOutboundEmails<Opaque> {
  type Response = Result<Listing<OutboundEmail<Opaque>>, GetOutboundEmailsError>;
}
