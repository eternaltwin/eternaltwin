use crate::core::{Instant, Request};
use crate::mailer::store::{OutboundEmailId, OutboundEmailRequest};
use crate::types::WeakError;

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum AcquireOutboundEmailRequestError {
  #[error("reached outbound email limit, retry at time {0}")]
  Limit(Instant),
  #[error(transparent)]
  Other(#[from] WeakError),
}

/// Acquire a permit to send an outbound email
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AcquireOutboundEmailRequest {
  /// Current system time
  pub now: Instant,
  /// Email triggered sending this email
  pub outbound_email: OutboundEmailId,
}

impl Request for AcquireOutboundEmailRequest {
  type Response = Result<OutboundEmailRequest, AcquireOutboundEmailRequestError>;
}
