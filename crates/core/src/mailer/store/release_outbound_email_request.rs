use crate::core::{Instant, Request};
use crate::mailer::store::{OutboundEmailRequest, OutboundEmailRequestIdRef};
use crate::types::WeakError;

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum ReleaseOutboundEmailRequestError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

/// Acquire a permit to send an outbound email
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ReleaseOutboundEmailRequest {
  /// Current system time
  pub now: Instant,
  /// Id for the outbound email request permit
  pub outbound_email_request: OutboundEmailRequestIdRef,
  /// Release result:
  ///
  /// - `None`: Cancelled (permit not consumed)
  /// - `Some(Ok(()))`: Permit consumed with success
  /// - `Some(Err(()))`: Permit consumed with error
  pub result: Option<Result<(), ()>>,
}

impl Request for ReleaseOutboundEmailRequest {
  type Response = Result<OutboundEmailRequest, ReleaseOutboundEmailRequestError>;
}
