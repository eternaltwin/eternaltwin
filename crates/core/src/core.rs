use crate::digest::DigestSha2_256;
#[cfg(feature = "sqlx-postgres")]
use crate::pg_num::PgU8;
use crate::user::{ShortUser, UserIdRef};
use async_trait::async_trait;
use chrono::{DateTime, TimeZone, Timelike, Utc};
use core::any::type_name;
use core::fmt;
use core::ops;
#[cfg(feature = "sqlx-postgres")]
use sqlx::postgres::types::PgRange;
#[cfg(feature = "sqlx-postgres")]
use sqlx::{postgres, Postgres};
use std::cmp::Ordering;
use std::ops::{Range, RangeFrom};
use thiserror::Error;
use uuid::Uuid;

declare_new_uuid! {
  /// UUID used as an idempotency key.
  ///
  /// Eternaltwin requires the use of UUIDv7 for idempotency.
  ///
  /// For REST requests, it should be passed through the `Idempotency-Key` header
  ///
  /// See <https://datatracker.ietf.org/doc/draft-ietf-httpapi-idempotency-key-header/>
  pub struct IdempotencyKey(Uuid);
  pub type ParseError = IdempotencyKeyParseError;
  const SQL_NAME = "idempotency_key";
}

pub struct Cursor {
  /// time for the query
  pub time: Instant,
  /// query fingerprint (to ensure the same options are passed)
  pub fingerprint: DigestSha2_256,
  /// stream position
  pub position: Uuid,
  /// direction
  /// - Ascending: return items with an id strictly higher than `position`
  /// - Descending: return items with an id strictly lower than `position`
  pub dir: CursorDir,
}

pub enum CursorDir {
  Ascending,
  Descending,
}

pub type HtmlFragment = String;

/// Protocol-relative URL
///
/// Should be avoided nowadays (in favor of HTTPS only) but Twinoid uses
/// them.
///
/// See <https://en.wikipedia.org/wiki/URL#prurl>
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PrUrl {
  full: url::Url,
  // Was the scheme added automatically?
  auto_scheme: bool,
}

impl fmt::Debug for PrUrl {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.debug_struct(type_name::<Self>())
      .field("full", &self.full.as_str())
      .field("auto_scheme", &self.auto_scheme)
      .finish()
  }
}

impl PrUrl {
  pub fn as_url(&self) -> &url::Url {
    &self.full
  }

  pub fn as_prurl(&self) -> &str {
    let url_str = self.full.as_str();
    let scheme = self.full.scheme();
    let after_scheme = &url_str[scheme.len()..];
    if after_scheme.starts_with("://") {
      &after_scheme[1..]
    } else {
      after_scheme.strip_prefix(':').unwrap_or(after_scheme)
    }
  }

  pub fn parse(input: &str) -> Result<Self, url::ParseError> {
    if input.starts_with("//") {
      url::Url::parse(&format!("https:{input}")).map(|full| Self {
        full,
        auto_scheme: true,
      })
    } else {
      url::Url::parse(input).map(|full| Self {
        full,
        auto_scheme: false,
      })
    }
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for PrUrl {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    self.full.serialize(serializer)
  }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for PrUrl {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    use serde::de::Error;
    let input = std::borrow::Cow::<'de, str>::deserialize(deserializer)?;
    Self::parse(input.as_ref()).map_err(|e| D::Error::custom(format!("{}", crate::types::DisplayErrorChain(&e))))
  }
}

declare_new_enum!(
  pub enum LocaleId {
    #[str("de-DE")]
    DeDe,
    #[str("en-US")]
    EnUs,
    #[str("eo")]
    Eo,
    #[str("es-SP")]
    EsSp,
    #[str("fr-FR")]
    FrFr,
    #[str("it-IT")]
    ItIt,
    #[str("pt-PT")]
    PtPt,
  }
  pub type ParseError = LocaleIdParseError;
  const SQL_NAME = "locale_id";
);

/// Year, month and date (no timezone attached)
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Date(chrono::NaiveDate);

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Error)]
#[error("invalid date ymd components: year = {year}, month = {month}, day = {day}")]
pub struct InvalidDateYmd {
  pub year: i32,
  pub month: u32,
  pub day: u32,
}

impl Date {
  pub fn into_chrono(self) -> chrono::NaiveDate {
    self.0
  }

  /// Create a new date from its ymd components.
  ///
  /// Returns an error if the components do not represent a valid date.
  ///
  /// ```
  /// use eternaltwin_core::core::Date;
  ///
  /// let d = Date::ymd(2020, 1, 1).unwrap();
  /// assert_eq!(&d.to_string(), "2020-01-01");
  /// ```
  pub fn ymd(year: i32, month: u32, day: u32) -> Result<Self, InvalidDateYmd> {
    chrono::NaiveDate::from_ymd_opt(year, month, day)
      .map(Self)
      .ok_or(InvalidDateYmd { year, month, day })
  }
}

impl fmt::Display for Date {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    const DATE_FORMAT: &str = "%F";
    write!(f, "{}", self.0.format(DATE_FORMAT))
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for Date {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    self.to_string().serialize(serializer)
  }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for Date {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    let inner = chrono::NaiveDate::deserialize(deserializer)?;
    Ok(Self(inner))
  }
}

/// A point in time, with a millisecond precision.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Instant(DateTime<Utc>);

const NANOSECOND_PER_MILLISECOND: u32 = 1_000_000;
const MILLISECOND_PER_SECOND: u32 = 1_000;

impl Instant {
  /// Round down nanosecond precision Chrono DateTime to millisecond precision Instant.
  pub fn new_round_down(inner: DateTime<Utc>) -> Self {
    let nanos = inner.nanosecond();
    Self(
      inner
        .with_nanosecond(nanos - (nanos % NANOSECOND_PER_MILLISECOND))
        .expect("invalid time"),
    )
  }

  // TODO: Return result (error on invalid data)
  // TODO: Avoid deprecated functions in implementation
  pub fn ymd_hms(year: i32, month: u32, day: u32, hour: u32, min: u32, sec: u32) -> Self {
    #[allow(deprecated)]
    Self(Utc.ymd(year, month, day).and_hms(hour, min, sec))
  }

  // TODO: Return result (error on invalid data)
  // TODO: Avoid deprecated functions in implementation
  pub fn ymd_hms_milli(year: i32, month: u32, day: u32, hour: u32, min: u32, sec: u32, milli: u32) -> Self {
    #[allow(deprecated)]
    Self(Utc.ymd(year, month, day).and_hms_milli(hour, min, sec, milli))
  }

  /// Create an Instant from the number of POSIX seconds since 1970-01-01T00:00:00Z.
  ///
  /// Note: POSIX seconds are defined as 1/86400 of a day (they ignore leap seconds).
  ///
  /// TODO: Rename to UNIX epoch timestamp / unix timestamp
  pub fn from_posix_timestamp(secs: i64) -> Self {
    Self(Utc.timestamp_opt(secs, 0).single().expect("invalid timestamp"))
  }

  pub fn into_chrono(self) -> DateTime<Utc> {
    self.0
  }

  pub const fn from_chrono(t: DateTime<Utc>) -> Self {
    Self(t)
  }

  /// Converts to an [`std::time::Instant`]
  pub fn into_std(self) -> ::std::time::Instant {
    let chrono_now = Utc::now();
    let chrono_duration = self.0.signed_duration_since(chrono_now);
    let (is_neg, std_duration) = if chrono_duration < ::chrono::Duration::zero() {
      (true, (-chrono_duration).to_std().expect("duration out of range"))
    } else {
      (false, chrono_duration.to_std().expect("duration out of range"))
    };
    let std_now = ::std::time::Instant::now();
    if is_neg {
      std_now - std_duration
    } else {
      std_now + std_duration
    }
  }

  /// Converts to an [`std::time::SystemTime`]
  pub fn into_system(self) -> ::std::time::SystemTime {
    let (secs, nanos) = (self.0.timestamp(), self.0.timestamp_subsec_nanos());
    let (is_neg, secs) = if secs < 0 {
      (true, secs.checked_neg().expect("impossible to turn `secs` positive"))
    } else {
      (false, secs)
    };
    debug_assert!(secs >= 0);
    let secs = u64::try_from(secs).expect("failed to convert positive i64 to u64");
    let std_since_epoch = std::time::Duration::new(secs, nanos);
    if is_neg {
      std::time::SystemTime::UNIX_EPOCH - std_since_epoch
    } else {
      std::time::SystemTime::UNIX_EPOCH + std_since_epoch
    }
  }

  pub fn from_system(t: ::std::time::SystemTime) -> Self {
    Self::from_chrono(DateTime::<Utc>::from(t))
  }

  pub const fn into_posix_timestamp(self) -> i64 {
    self.0.timestamp()
  }

  pub fn duration_since(&self, earlier: Instant) -> Duration {
    Duration(self.0.signed_duration_since(earlier.0))
  }

  pub const fn const_cmp(self, other: Self) -> Ordering {
    let left: i64 = self.into_posix_timestamp();
    let right: i64 = other.into_posix_timestamp();
    if left == right {
      Ordering::Equal
    } else if left < right {
      Ordering::Less
    } else {
      Ordering::Greater
    }
  }
}

impl fmt::Display for Instant {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for Instant {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    const INSTANT_FORMAT: &str = "%FT%T%.3fZ";

    self.0.format(INSTANT_FORMAT).to_string().serialize(serializer)
  }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for Instant {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    let inner = DateTime::<Utc>::deserialize(deserializer)?;
    Ok(Self::new_round_down(inner))
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<Postgres> for Instant {
  fn type_info() -> postgres::PgTypeInfo {
    postgres::PgTypeInfo::with_name("instant")
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    *ty == <Self as ::sqlx::Type<Postgres>>::type_info()
      || *ty == <DateTime<Utc> as ::sqlx::Type<Postgres>>::type_info()
  }
}

#[cfg(feature = "sqlx-sqlite")]
impl ::sqlx::Type<sqlx::Sqlite> for Instant {
  fn type_info() -> ::sqlx::sqlite::SqliteTypeInfo {
    <DateTime<Utc> as ::sqlx::Type<::sqlx::Sqlite>>::type_info()
  }

  fn compatible(ty: &::sqlx::sqlite::SqliteTypeInfo) -> bool {
    <DateTime<Utc> as ::sqlx::Type<::sqlx::Sqlite>>::compatible(ty)
  }
}

#[cfg(feature = "sqlx")]
impl<'r, Db: ::sqlx::Database> ::sqlx::Decode<'r, Db> for Instant
where
  DateTime<Utc>: ::sqlx::Decode<'r, Db>,
{
  fn decode(value: <Db as ::sqlx::database::HasValueRef<'r>>::ValueRef) -> Result<Self, sqlx::error::BoxDynError> {
    let value: DateTime<Utc> = <DateTime<Utc> as ::sqlx::Decode<Db>>::decode(value)?;
    Ok(Self::new_round_down(value))
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Encode<'_, Postgres> for Instant {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let v: DateTime<Utc> = self.into_chrono();
    <DateTime<Utc> as sqlx::Encode<'_, Postgres>>::encode(v, buf)
  }
}

#[cfg(feature = "sqlx-sqlite")]
impl ::sqlx::Encode<'_, ::sqlx::Sqlite> for Instant {
  fn encode_by_ref(&self, args: &mut Vec<::sqlx::sqlite::SqliteArgumentValue<'_>>) -> ::sqlx::encode::IsNull {
    <DateTime<Utc> as sqlx::Encode<'_, ::sqlx::Sqlite>>::encode(self.0, args)
  }
}

impl ops::Add<Duration> for Instant {
  type Output = Instant;

  fn add(self, rhs: Duration) -> Self::Output {
    Self::new_round_down(self.into_chrono() + rhs.0)
  }
}

impl ops::Sub<Duration> for Instant {
  type Output = Instant;

  fn sub(self, rhs: Duration) -> Self::Output {
    Self::new_round_down(self.into_chrono() - rhs.0)
  }
}

impl ops::Sub<Instant> for Instant {
  type Output = Duration;

  fn sub(self, rhs: Instant) -> Self::Output {
    Duration::from_chrono(self.into_chrono().signed_duration_since(rhs.into_chrono()))
  }
}

/// A difference between two `Instant`
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Duration(::chrono::Duration);

impl Duration {
  #[inline(always)]
  pub const fn into_std(self) -> std::time::Duration {
    match self.0.to_std() {
      Ok(d) => d,
      Err(_) => panic!("invalid `Durattion` value"),
    }
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("standard Duration does not fit in Eternaltwin duration range: {0:?}")]
pub struct StdDurationRangeError(pub std::time::Duration);

impl Duration {
  pub const ZERO: Self = Self(::chrono::Duration::zero());

  pub fn from_std(d: std::time::Duration) -> Result<Self, StdDurationRangeError> {
    ::chrono::Duration::from_std(d)
      .map(Self)
      .map_err(|_| StdDurationRangeError(d))
  }

  pub const fn from_chrono(d: ::chrono::Duration) -> Self {
    Self(d)
  }

  pub const fn from_days(days: i64) -> Self {
    Self::from_seconds(days * 86400)
  }

  pub const fn from_minutes(minutes: i64) -> Self {
    Self::from_seconds(minutes * 60)
  }

  pub const fn from_seconds(seconds: i64) -> Self {
    Self::from_millis(seconds * 1000)
  }

  pub const fn from_hours(hours: i64) -> Self {
    Self::from_seconds(hours * 3600)
  }

  pub const fn from_millis(millis: i64) -> Self {
    Self(match ::chrono::Duration::try_milliseconds(millis) {
      Some(d) => d,
      None => panic!("Duration::from_millis out of bounds"),
    })
  }

  pub const fn seconds(&self) -> i64 {
    self.0.num_seconds()
  }

  pub const fn milliseconds(&self) -> i64 {
    self.0.num_milliseconds()
  }

  pub const fn nanoseconds(&self) -> i128 {
    // todo: avoid `as` casts once `i128::from` is supported in const context
    (self.0.num_seconds() as i128) * 1_000_000_000i128 + (self.0.subsec_nanos() as i128)
  }

  pub fn div_floor(&self, rhs: Self) -> i64 {
    f64::floor(self.seconds_f64() / rhs.seconds_f64()) as i64
  }

  // TODO: Avoid using `num_seconds` as it does not support leap seconds.
  pub fn seconds_f64(&self) -> f64 {
    let secs = self.0.num_seconds() as f64;
    let nanos = self.0.subsec_nanos() as f64;
    secs + (nanos / 1e9)
  }

  pub fn from_seconds_f64_lossy(seconds: f64) -> Self {
    let millis = (seconds * (MILLISECOND_PER_SECOND as f64)) as i64;
    Self::from_millis(millis)
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for Duration {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    use serde::ser::Error;
    let duration = self.0;
    let millis = duration.num_milliseconds();
    let seconds = (millis as f64) / (MILLISECOND_PER_SECOND as f64);
    if ((seconds * (MILLISECOND_PER_SECOND as f64)) as i64) != millis {
      return Err(S::Error::custom("lossy duration serialization"));
    }
    serializer.serialize_f64(seconds)
  }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for Duration {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    use crate::types::DisplayErrorChain;
    use serde::de::Error;

    #[derive(serde::Deserialize)]
    #[serde(untagged)]
    enum StrOrNum<'s> {
      Str(std::borrow::Cow<'s, str>),
      Num(f64),
    }

    match StrOrNum::deserialize(deserializer)? {
      StrOrNum::Str(s) => {
        let std_duration = humantime::parse_duration(s.as_ref())
          .map_err(|e| D::Error::custom(format!("invalid duration format: {}", DisplayErrorChain(&e))))?;
        Self::from_std(std_duration)
          .map_err(|e| D::Error::custom(format!("invalid duration: {}", DisplayErrorChain(&e))))
      }
      StrOrNum::Num(n) => Ok(Self::from_seconds_f64_lossy(n)),
    }
  }
}

impl ops::Div<i32> for Duration {
  type Output = Self;

  fn div(self, rhs: i32) -> Self::Output {
    Self(self.0 / rhs)
  }
}

impl ops::Mul<i32> for Duration {
  type Output = Self;

  fn mul(self, rhs: i32) -> Self::Output {
    Self(self.0 * rhs)
  }
}

/// Private type used to serialize PeriodLower and its variants.
#[cfg(feature = "serde")]
#[derive(Debug, Copy, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
struct SerializablePeriodLower {
  pub start: Instant,
  pub end: Option<Instant>,
}

#[cfg(feature = "serde")]
impl From<PeriodFrom> for SerializablePeriodLower {
  fn from(period: PeriodFrom) -> Self {
    Self {
      start: period.start,
      end: None,
    }
  }
}

#[cfg(feature = "serde")]
impl From<FinitePeriod> for SerializablePeriodLower {
  fn from(period: FinitePeriod) -> Self {
    Self {
      start: period.start,
      end: Some(period.end),
    }
  }
}

#[cfg(feature = "serde")]
impl From<PeriodLower> for SerializablePeriodLower {
  fn from(period: PeriodLower) -> Self {
    match period {
      PeriodLower::From(p) => p.into(),
      PeriodLower::Finite(p) => p.into(),
    }
  }
}

#[cfg_attr(feature = "serde", derive(serde::Deserialize))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PeriodFrom {
  pub start: Instant,
}

#[cfg(feature = "serde")]
impl serde::Serialize for PeriodFrom {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    SerializablePeriodLower::from(*self).serialize(serializer)
  }
}

impl From<RangeFrom<Instant>> for PeriodFrom {
  fn from(r: RangeFrom<Instant>) -> Self {
    Self { start: r.start }
  }
}

impl From<PeriodFrom> for RangeFrom<Instant> {
  fn from(r: PeriodFrom) -> Self {
    r.start..
  }
}

/// Represents the finite time period between `start` and `end`.
///
/// More specifically, it represents instants `t` such that `start <= t < end`.
///
/// Note that `end` may be lesser than or equal to `start`, in such case there
/// is no valid instant contained by the period.
#[cfg_attr(feature = "serde", derive(serde::Deserialize))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FinitePeriod {
  pub start: Instant,
  pub end: Instant,
}

impl FinitePeriod {
  pub const fn new(start: Instant, end: Instant) -> Self {
    Self { start, end }
  }

  /// Check if this period contains the instant `time`.
  pub const fn contains(self, time: Instant) -> bool {
    matches!(self.start.const_cmp(time), Ordering::Less | Ordering::Equal)
      && matches!(time.const_cmp(self.end), Ordering::Less)
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for FinitePeriod {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    SerializablePeriodLower::from(*self).serialize(serializer)
  }
}

impl From<Range<Instant>> for FinitePeriod {
  fn from(r: Range<Instant>) -> Self {
    Self {
      start: r.start,
      end: r.end,
    }
  }
}

impl From<FinitePeriod> for Range<Instant> {
  fn from(r: FinitePeriod) -> Self {
    r.start..r.end
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<Postgres> for FinitePeriod {
  fn type_info() -> postgres::PgTypeInfo {
    PeriodLower::type_info()
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    PeriodLower::compatible(ty)
  }
}

#[cfg(feature = "sqlx-postgres")]
#[derive(Debug, Error)]
#[error("decoded invalid Postgres finite PERIOD_LOWER as {:?}", .0)]
struct InvalidFinitePeriod(PgRange<Instant>);

#[cfg(feature = "sqlx-postgres")]
impl<'r> sqlx::Decode<'r, Postgres> for FinitePeriod {
  fn decode(value: postgres::PgValueRef<'r>) -> Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
    let range = PgRange::<Instant>::decode(value)?;
    match (range.start, range.end) {
      (std::ops::Bound::Included(start), std::ops::Bound::Excluded(end)) => Ok(FinitePeriod { start, end }),
      _ => Err(Box::new(InvalidFinitePeriod(range))),
    }
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Encode<'_, Postgres> for FinitePeriod {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let range: PgRange<Instant> = (self.start..self.end).into();
    range.encode_by_ref(buf)
  }
}

/// Represents the raw `PERIOD` Postgres type. It should not be used directly.
#[cfg(feature = "sqlx-postgres")]
struct PgPeriod;

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<Postgres> for PgPeriod {
  fn type_info() -> postgres::PgTypeInfo {
    postgres::PgTypeInfo::with_name("period")
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info()
  }
}

/// Represents any period with a lower bound
#[cfg_attr(feature = "serde", derive(serde::Deserialize), serde(untagged))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum PeriodLower {
  Finite(FinitePeriod),
  From(PeriodFrom),
}

impl PeriodLower {
  pub const fn new(start: Instant, end: Option<Instant>) -> Self {
    match end {
      Some(end) => Self::bounded(start, end),
      None => Self::unbounded(start),
    }
  }

  pub const fn unbounded(start: Instant) -> Self {
    Self::From(PeriodFrom { start })
  }

  pub const fn bounded(start: Instant, end: Instant) -> Self {
    Self::Finite(FinitePeriod { start, end })
  }

  pub const fn start(self) -> Instant {
    match self {
      Self::Finite(p) => p.start,
      Self::From(p) => p.start,
    }
  }

  pub const fn end(self) -> Option<Instant> {
    match self {
      Self::Finite(p) => Some(p.end),
      Self::From(_) => None,
    }
  }

  /// Updates the end instant to be the minimum of the current end and provided value
  pub fn end_min(self, other_end: Option<Instant>) -> Self {
    if let Some(end) = other_end {
      Self::Finite(self.bounded_end_min(end))
    } else {
      self
    }
  }

  /// Updates the end instant to be the minimum of the current end and provided value
  pub fn bounded_end_min(self, other_end: Instant) -> FinitePeriod {
    match self {
      Self::From(PeriodFrom { start }) => FinitePeriod { start, end: other_end },
      Self::Finite(FinitePeriod { start, end }) => FinitePeriod {
        start,
        end: Instant::min(end, other_end),
      },
    }
  }
}

#[cfg(feature = "serde")]
impl serde::Serialize for PeriodLower {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    SerializablePeriodLower::from(*self).serialize(serializer)
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Type<Postgres> for PeriodLower {
  fn type_info() -> postgres::PgTypeInfo {
    postgres::PgTypeInfo::with_name("period_lower")
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == PgPeriod::type_info() || *ty == (PgRange::<DateTime<Utc>>::type_info())
  }
}

#[cfg(feature = "sqlx-postgres")]
#[derive(Debug, Error)]
#[error("decoded invalid Postgres PERIOD_LOWER as {:?}", .0)]
struct InvalidPeriodLower(PgRange<Instant>);

#[cfg(feature = "sqlx-postgres")]
impl<'r> sqlx::Decode<'r, Postgres> for PeriodLower {
  fn decode(value: postgres::PgValueRef<'r>) -> Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
    let range = PgRange::<Instant>::decode(value)?;
    match (range.start, range.end) {
      (std::ops::Bound::Included(start), std::ops::Bound::Unbounded) => Ok(PeriodLower::From(PeriodFrom { start })),
      (std::ops::Bound::Included(start), std::ops::Bound::Excluded(end)) => {
        Ok(PeriodLower::Finite(FinitePeriod { start, end }))
      }
      _ => Err(Box::new(InvalidPeriodLower(range))),
    }
  }
}

#[cfg(feature = "sqlx-postgres")]
impl sqlx::Encode<'_, Postgres> for PeriodLower {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let range: PgRange<Instant> = match *self {
      Self::Finite(FinitePeriod { start, end }) => (start..end).into(),
      Self::From(PeriodFrom { start }) => (start..).into(),
    };
    range.encode_by_ref(buf)
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserDot {
  pub time: Instant,
  pub user: ShortUser,
}

/// Revision at two points in time: now and then
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BiRev<T> {
  pub now: T,
  pub then: T,
}

/// Wrapper for a value (allows future extensions without breakage)
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Value<T> {
  pub value: T,
}

pub type BiValue<T> = BiRev<Value<T>>;

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawUserDot {
  pub time: Instant,
  pub user: UserIdRef,
}

/// Wrapper around a string that should be kept secret
#[derive(Clone)]
pub struct SecretString(Box<str>);

impl SecretString {
  pub fn new(str: String) -> Self {
    Self(str.into_boxed_str())
  }

  pub fn as_str(&self) -> &str {
    &self.0
  }
}

/// Wrapper around a byte buffer that should be kept secret
#[derive(Clone)]
pub struct SecretBytes(Box<[u8]>);

impl SecretBytes {
  pub fn new(bytes: Vec<u8>) -> Self {
    Self(bytes.into_boxed_slice())
  }

  pub fn as_slice(&self) -> &[u8] {
    &self.0
  }
}

declare_new_int! {
  /// A percentage value between 0 and 100 (inclusive) supporting only integer
  /// values.
  pub struct IntPercentage(u8);
  pub type RangeError = IntPercentageRangeError;
  const BOUNDS = 0..=100;
  type SqlType = PgU8;
  const SQL_NAME = "int_percentage";
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Listing<T> {
  pub offset: u32,
  pub limit: u32,
  pub count: u32,
  pub items: Vec<T>,
}

impl<T> Listing<T> {
  pub fn map<B, F>(self, f: F) -> Listing<B>
  where
    F: FnMut(T) -> B,
  {
    Listing {
      offset: self.offset,
      limit: self.limit,
      count: self.count,
      items: self.items.into_iter().map(f).collect(),
    }
  }
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ListingCount {
  pub count: u32,
}

pub trait Request<Brand = ()> {
  type Response;
}

#[async_trait]
pub trait Handler<Req, Brand = ()>
where
  Req: Request<Brand>,
{
  async fn handle(&self, req: Req) -> Req::Response;
}

#[cfg(test)]
mod test {
  #[cfg(feature = "serde")]
  use crate::core::{FinitePeriod, Instant, PeriodFrom, PeriodLower};
  #[cfg(feature = "serde")]
  use std::fs;

  #[cfg(feature = "serde")]
  #[allow(clippy::unnecessary_wraps)]
  fn get_finite_period_one_millisecond() -> FinitePeriod {
    FinitePeriod {
      start: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      end: Instant::ymd_hms_milli(2021, 1, 1, 0, 0, 0, 1),
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_finite_period_one_millisecond() {
    let s = fs::read_to_string("../../test-resources/core/core/finite-period/one-millisecond/value.json").unwrap();
    let actual: FinitePeriod = serde_json::from_str(&s).unwrap();
    let expected = get_finite_period_one_millisecond();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_finite_period_one_millisecond() {
    let value = get_finite_period_one_millisecond();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected =
      fs::read_to_string("../../test-resources/core/core/finite-period/one-millisecond/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  #[allow(clippy::unnecessary_wraps)]
  fn get_finite_period_one_second() -> FinitePeriod {
    FinitePeriod {
      start: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      end: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_finite_period_one_second() {
    let s = fs::read_to_string("../../test-resources/core/core/finite-period/one-second/value.json").unwrap();
    let actual: FinitePeriod = serde_json::from_str(&s).unwrap();
    let expected = get_finite_period_one_second();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_finite_period_one_second() {
    let value = get_finite_period_one_second();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/core/finite-period/one-second/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  #[allow(clippy::unnecessary_wraps)]
  fn get_period_from_unbounded() -> PeriodFrom {
    PeriodFrom {
      start: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_period_from_unbounded() {
    let s = fs::read_to_string("../../test-resources/core/core/period-from/unbounded/value.json").unwrap();
    let actual: PeriodFrom = serde_json::from_str(&s).unwrap();
    let expected = get_period_from_unbounded();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_period_from_unbounded() {
    let value = get_period_from_unbounded();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/core/period-from/unbounded/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  #[allow(clippy::unnecessary_wraps)]
  fn get_period_lower_one_millisecond() -> PeriodLower {
    PeriodLower::Finite(FinitePeriod {
      start: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      end: Instant::ymd_hms_milli(2021, 1, 1, 0, 0, 0, 1),
    })
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_period_lower_one_millisecond() {
    let s = fs::read_to_string("../../test-resources/core/core/period-lower/one-millisecond/value.json").unwrap();
    let actual: PeriodLower = serde_json::from_str(&s).unwrap();
    let expected = get_period_lower_one_millisecond();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_period_lower_one_millisecond() {
    let value = get_period_lower_one_millisecond();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected =
      fs::read_to_string("../../test-resources/core/core/period-lower/one-millisecond/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  #[allow(clippy::unnecessary_wraps)]
  fn get_period_lower_one_second() -> PeriodLower {
    PeriodLower::Finite(FinitePeriod {
      start: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      end: Instant::ymd_hms(2021, 1, 1, 0, 0, 1),
    })
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_period_lower_one_second() {
    let s = fs::read_to_string("../../test-resources/core/core/period-lower/one-second/value.json").unwrap();
    let actual: PeriodLower = serde_json::from_str(&s).unwrap();
    let expected = get_period_lower_one_second();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_period_lower_one_second() {
    let value = get_period_lower_one_second();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/core/period-lower/one-second/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  #[allow(clippy::unnecessary_wraps)]
  fn get_period_lower_unbounded() -> PeriodLower {
    PeriodLower::unbounded(Instant::ymd_hms(2021, 1, 1, 0, 0, 0))
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_period_lower_unbounded() {
    let s = fs::read_to_string("../../test-resources/core/core/period-lower/unbounded/value.json").unwrap();
    let actual: PeriodLower = serde_json::from_str(&s).unwrap();
    let expected = get_period_lower_unbounded();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_period_lower_unbounded() {
    let value = get_period_lower_unbounded();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/core/period-lower/unbounded/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }
}
