pub mod event;
pub mod store;

use crate::core::{Duration, FinitePeriod, Handler, Instant, RawUserDot};
use crate::dinoparc::{DinoparcPassword, DinoparcServer, DinoparcUserId, DinoparcUsername};
use crate::email::EmailAddress;
use crate::hammerfest::{
  HammerfestPassword, HammerfestServer, HammerfestSessionKey, HammerfestUserId, HammerfestUsername,
};
use crate::link::VersionedLinks;
use crate::oauth::RfcOauthAccessToken;
use crate::password::{Password, PasswordHash};
use crate::patch::SimplePatch;
use crate::twinoid::TwinoidUserId;
use crate::types::WeakError;
use crate::user::store::enable_user::{EnableUser as RawEnableUser, EnableUserError, EnableUserSummary};
use async_trait::async_trait;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{deserialize_explicit_option, Deserialize, Serialize};
use std::error::Error;
use std::ops::Deref;

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CompleteSimpleUser {
  pub id: UserId,
  pub created_at: Instant,
  pub deleted_at: Option<Instant>,
  pub display_name: UserDisplayNameVersions,
  pub is_administrator: bool,
  #[cfg_attr(feature = "serde", serde(deserialize_with = "deserialize_explicit_option"))]
  pub username: Option<Username>,
  #[cfg_attr(feature = "serde", serde(deserialize_with = "deserialize_explicit_option"))]
  pub email_address: Option<EmailAddress>,
  pub has_password: bool,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CompleteUser {
  pub id: UserId,
  pub created_at: Instant,
  pub deleted_at: Option<Instant>,
  pub display_name: UserDisplayNameVersions,
  pub is_administrator: bool,
  pub links: VersionedLinks,
  pub username: Option<Username>,
  pub email_address: Option<EmailAddress>,
  pub has_password: bool,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CreateUserOptions {
  pub display_name: UserDisplayName,
  pub email: Option<EmailAddress>,
  pub username: Option<Username>,
  pub password: Option<PasswordHash>,
}

#[derive(Debug, thiserror::Error)]
pub enum RawCreateUserError {
  #[error("insufficient permission to create user")]
  Forbidden,
  #[error(transparent)]
  Other(WeakError),
}

impl RawCreateUserError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

// Service command
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpsertSeedUser {
  /// Ref uniquely identifying the user to upsert
  pub r#ref: UserRef,
  pub username: Option<Username>,
  pub display_name: UserDisplayName,
  pub email: Option<EmailAddress>,
  pub password: Option<Password>,
  pub is_administrator: bool,
}

// Store command
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpsertSeedUserOptions {
  pub now: Instant,
  /// Ref uniquely identifying the user to upsert
  pub r#ref: UserRef,
  pub display_name: UserDisplayName,
  /// User name value to set
  /// - `None`: no username
  /// - `Some`: use provided value
  ///
  /// Errors on conflict.
  pub username: Option<Username>,
  pub email: Option<EmailAddress>,
  pub password: Option<PasswordHash>,
  pub is_administrator: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawUpsertSeedUserError {
  #[error("username {0:?} already used by user {}", .1.id)]
  UsernameConflict(Username, UserIdRef),
  #[error("email {0:?} already used by user {}", .1.id)]
  EmailConflict(EmailAddress, UserIdRef),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpdateUserOptions {
  pub r#ref: UserIdRef,
  pub patch: UpdateUserPatch,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpdateUserPatch {
  #[cfg_attr(feature = "serde", serde(default, skip_serializing_if = "SimplePatch::is_skip"))]
  pub display_name: SimplePatch<UserDisplayName>,
  #[cfg_attr(feature = "serde", serde(default, skip_serializing_if = "SimplePatch::is_skip"))]
  pub username: SimplePatch<Option<Username>>,
  #[cfg_attr(feature = "serde", serde(default, skip_serializing_if = "SimplePatch::is_skip"))]
  pub password: SimplePatch<Option<Password>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawUpdateUserOptions {
  pub actor: UserIdRef,
  pub r#ref: UserIdRef,
  pub skip_rate_limit: bool,
  pub patch: RawUpdateUserPatch,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawUpdateUserPatch {
  #[cfg_attr(feature = "serde", serde(default, skip_serializing_if = "SimplePatch::is_skip"))]
  pub display_name: SimplePatch<UserDisplayName>,
  #[cfg_attr(feature = "serde", serde(default, skip_serializing_if = "SimplePatch::is_skip"))]
  pub username: SimplePatch<Option<Username>>,
  #[cfg_attr(feature = "serde", serde(default, skip_serializing_if = "SimplePatch::is_skip"))]
  pub password: SimplePatch<Option<PasswordHash>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DeleteUserOptions {
  pub r#ref: UserIdRef,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "GetUserTos"))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetUserTos {
  pub id: UserId,
  pub is_tos_accepted: bool,
  pub acceptation_date: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetUserTosOptions {
  pub r#ref: UserIdRef,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawGetUserTosOptions {
  pub r#ref: UserIdRef,
  pub time: Instant,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawGetUserTosError {
  #[error(transparent)]
  Other(WeakError),
}

impl RawGetUserTosError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawUpdateUserTosOptions {
  pub r#ref: UserIdRef,
  pub now: Instant,
  pub is_tos_accepted: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawUpdateUserTosError {
  #[error("Failed to update user ToS for ref: {:?}", .0)]
  NotFound(UserIdRef),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl RawUpdateUserTosError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(tag = "type"))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum UserFields {
  CompleteIfSelf { self_user_id: UserId },
  Complete,
  Default,
  Short,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetUserOptions {
  pub r#ref: UserRef,
  pub fields: UserFields,
  pub time: Option<Instant>,
  pub skip_deleted: bool,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawGetUser {
  pub r#ref: UserRef,
  pub fields: UserFields,
  pub time: Instant,
  pub skip_deleted: bool,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawGetUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl RawGetUserError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetShortUserOptions {
  pub r#ref: UserRef,
  pub time: Option<Instant>,
  pub skip_deleted: bool,
}

impl From<UserRef> for GetShortUserOptions {
  fn from(r#ref: UserRef) -> Self {
    Self {
      r#ref,
      time: None,
      skip_deleted: false,
    }
  }
}

impl From<UserIdRef> for GetShortUserOptions {
  fn from(r: UserIdRef) -> Self {
    let r: UserRef = r.into();
    r.into()
  }
}

impl From<UserId> for GetShortUserOptions {
  fn from(id: UserId) -> Self {
    let r: UserRef = id.into();
    r.into()
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(untagged))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum GetUserResult {
  Complete(CompleteUser),
  Default(User),
}

impl GetUserResult {
  pub const fn deleted_at(&self) -> Option<Instant> {
    match self {
      Self::Complete(u) => u.deleted_at,
      Self::Default(u) => u.deleted_at,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(untagged))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum RawGetUserResult {
  Complete(CompleteSimpleUser),
  Default(SimpleUser),
  Short(ShortUser),
}

impl RawGetUserResult {
  pub fn id(&self) -> UserId {
    match self {
      Self::Complete(u) => u.id,
      Self::Default(u) => u.id,
      Self::Short(u) => u.id,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortUser {
  pub id: UserId,
  pub display_name: UserDisplayNameVersions,
}

impl ShortUser {
  pub const fn as_ref(&self) -> UserIdRef {
    UserIdRef { id: self.id }
  }
}

impl From<SimpleUser> for ShortUser {
  fn from(user: SimpleUser) -> Self {
    Self {
      id: user.id,
      display_name: user.display_name,
    }
  }
}

impl From<CompleteSimpleUser> for ShortUser {
  fn from(user: CompleteSimpleUser) -> Self {
    Self {
      id: user.id,
      display_name: user.display_name,
    }
  }
}

impl From<User> for ShortUser {
  fn from(user: User) -> Self {
    Self {
      id: user.id,
      display_name: user.display_name,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortUserWithPassword {
  pub id: UserId,
  pub created_at: Instant,
  pub deleted: Option<RawUserDot>,
  pub is_administrator: bool,
  pub display_name: UserDisplayNameVersions,
  pub username: UsernameVersions,
  pub password: Option<PasswordHash>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SimpleUser {
  pub id: UserId,
  pub created_at: Instant,
  pub deleted_at: Option<Instant>,
  pub display_name: UserDisplayNameVersions,
  pub is_administrator: bool,
}

impl From<CompleteSimpleUser> for SimpleUser {
  fn from(user: CompleteSimpleUser) -> Self {
    Self {
      id: user.id,
      created_at: user.created_at,
      deleted_at: user.deleted_at,
      display_name: user.display_name,
      is_administrator: user.is_administrator,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct User {
  pub id: UserId,
  pub created_at: Instant,
  pub deleted_at: Option<Instant>,
  pub display_name: UserDisplayNameVersions,
  pub is_administrator: bool,
  pub links: VersionedLinks,
}

impl User {
  pub fn to_short(&self) -> ShortUser {
    // TODO: Avoid `.clone()`
    ShortUser::from(self.clone())
  }
}

declare_new_string! {
  pub struct UserDisplayName(String);
  pub type ParseError = UserDisplayNameParseError;
  const PATTERN = r"^[\p{Letter}_][\p{Letter}_ ()0-9]{0,62}[\p{Letter}_()0-9]$";
  const SQL_NAME = "user_display_name";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserDisplayNameVersion {
  pub value: UserDisplayName,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserDisplayNameVersions {
  pub current: UserDisplayNameVersion,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(tag = "method"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum LinkToDinoparcOptions {
  Ref(LinkToDinoparcWithRefOptions),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToDinoparcWithCredentialsOptions {
  pub user_id: UserId,
  pub dinoparc_server: DinoparcServer,
  pub dinoparc_username: DinoparcUsername,
  pub dinoparc_password: DinoparcPassword,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToDinoparcWithRefOptions {
  pub user_id: UserId,
  pub dinoparc_server: DinoparcServer,
  pub dinoparc_user_id: DinoparcUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UnlinkFromDinoparcOptions {
  pub user_id: UserId,
  pub dinoparc_server: DinoparcServer,
  pub dinoparc_user_id: DinoparcUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(tag = "method"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum LinkToHammerfestOptions {
  Ref(LinkToHammerfestWithRefOptions),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToHammerfestWithCredentialsOptions {
  pub user_id: UserId,
  pub hammerfest_server: HammerfestServer,
  pub hammerfest_username: HammerfestUsername,
  pub hammerfest_password: HammerfestPassword,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToHammerfestWithSessionKeyOptions {
  pub user_id: UserId,
  pub hammerfest_server: HammerfestServer,
  pub hammerfest_session_key: HammerfestSessionKey,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToHammerfestWithRefOptions {
  pub user_id: UserId,
  pub hammerfest_server: HammerfestServer,
  pub hammerfest_user_id: HammerfestUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UnlinkFromHammerfestOptions {
  pub user_id: UserId,
  pub hammerfest_server: HammerfestServer,
  pub hammerfest_user_id: HammerfestUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(tag = "method"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum LinkToTwinoidOptions {
  Ref(LinkToTwinoidWithRefOptions),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToTwinoidWithOauthOptions {
  pub user_id: UserId,
  pub access_token: RfcOauthAccessToken,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkToTwinoidWithRefOptions {
  pub user_id: UserId,
  pub twinoid_user_id: TwinoidUserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UnlinkFromTwinoidOptions {
  pub user_id: UserId,
  pub twinoid_user_id: TwinoidUserId,
}

declare_new_uuid! {
  pub struct UserId(Uuid);
  pub type ParseError = UserIdParseError;
  const SQL_NAME = "user_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserIdRef {
  pub id: UserId,
}

impl UserIdRef {
  pub const fn new(id: UserId) -> Self {
    Self { id }
  }
}

impl From<UserId> for UserIdRef {
  fn from(id: UserId) -> Self {
    Self::new(id)
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserUsernameRef {
  pub username: Username,
}

impl UserUsernameRef {
  pub fn new(username: Username) -> Self {
    Self { username }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserEmailRef {
  pub email: EmailAddress,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(untagged))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum UserRef {
  Id(UserIdRef),
  Username(UserUsernameRef),
  Email(UserEmailRef),
}

impl UserRef {
  pub const fn as_id_ref(&self) -> Option<UserIdRef> {
    match self {
      UserRef::Id(user_id_ref) => Some(*user_id_ref),
      _ => None,
    }
  }
}

impl From<UserId> for UserRef {
  fn from(id: UserId) -> Self {
    Self::Id(id.into())
  }
}

impl From<UserIdRef> for UserRef {
  fn from(r: UserIdRef) -> Self {
    Self::Id(r)
  }
}

impl From<UserUsernameRef> for UserRef {
  fn from(r: UserUsernameRef) -> Self {
    Self::Username(r)
  }
}

impl From<UserEmailRef> for UserRef {
  fn from(r: UserEmailRef) -> Self {
    Self::Email(r)
  }
}

declare_new_string! {
  pub struct Username(String);
  pub type ParseError = UsernameParseError;
  const PATTERN = "^[a-z_][a-z0-9_]{1,31}$";
  const SQL_NAME = "username";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UsernameVersion {
  pub value: Option<Username>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UsernameVersions {
  pub current: UsernameVersion,
}

pub const USERNAME_LOCK_DURATION: Duration = Duration::from_days(7);
pub const USER_DISPLAY_NAME_LOCK_DURATION: Duration = Duration::from_days(30);
pub const USER_PASSWORD_LOCK_DURATION: Duration = Duration::from_minutes(10);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawGetShortUserError {
  #[error("Failed to get user for ref: {:?}", .0)]
  NotFound(UserRef),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawGetUserWithPasswordError {
  #[error("Failed to get user for ref: {:?}", .0)]
  NotFound(UserRef),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum RawUpdateUserError {
  #[error("failed to find user to update for ref: {0:?}")]
  NotFound(UserIdRef),
  #[error("failed to update user {0:?}, display_name locked during {1:?}, current time is {2}")]
  LockedDisplayName(UserIdRef, FinitePeriod, Instant),
  #[error("failed to update user {0:?}, username locked during {1:?}, current time is {2}")]
  LockedUsername(UserIdRef, FinitePeriod, Instant),
  #[error("failed to update user {0:?}, password locked during {1:?}, current time is {2}")]
  LockedPassword(UserIdRef, FinitePeriod, Instant),
  #[error(transparent)]
  Other(WeakError),
}

impl PartialEq for RawUpdateUserError {
  fn eq(&self, other: &Self) -> bool {
    match (self, other) {
      (RawUpdateUserError::NotFound(l), RawUpdateUserError::NotFound(r)) if l == r => true,
      (RawUpdateUserError::LockedDisplayName(l0, l1, l2), RawUpdateUserError::LockedDisplayName(r0, r1, r2))
        if (l0, l1, l2) == (r0, r1, r2) =>
      {
        true
      }
      (RawUpdateUserError::LockedUsername(l0, l1, l2), RawUpdateUserError::LockedUsername(r0, r1, r2))
        if (l0, l1, l2) == (r0, r1, r2) =>
      {
        true
      }
      (RawUpdateUserError::LockedPassword(l0, l1, l2), RawUpdateUserError::LockedPassword(r0, r1, r2))
        if (l0, l1, l2) == (r0, r1, r2) =>
      {
        true
      }
      _ => false,
    }
  }
}

impl RawUpdateUserError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawDeleteUser {
  pub actor: UserIdRef,
  pub user: UserIdRef,
  pub now: Instant,
}

#[derive(Debug, PartialEq, Eq, thiserror::Error)]
pub enum RawDeleteUserError {
  #[error("failed to find user to delete for ref: {:?}", .0)]
  NotFound(UserIdRef),
  #[error("user is already deleted for ref: {:?}", .0)]
  Gone(UserIdRef),
  #[error("insufficient permission to delete user")]
  Forbidden,
  #[error(transparent)]
  Other(WeakError),
}

impl RawDeleteUserError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawHardDeleteUser {
  pub actor: UserIdRef,
  pub user: UserIdRef,
}

#[derive(Debug, PartialEq, Eq, thiserror::Error)]
pub enum RawHardDeleteUserError {
  #[error("Failed to find user to delete for ref: {:?}", .0)]
  NotFound(UserIdRef),
  #[error(transparent)]
  Other(WeakError),
}

impl RawHardDeleteUserError {
  pub fn other<E: 'static + Error + Send + Sync>(e: E) -> Self {
    Self::Other(WeakError::new(e))
  }
}

#[async_trait]
pub trait UserStore: Send + Sync + Handler<RawEnableUser> {
  async fn create_user(&self, options: &CreateUserOptions) -> Result<CompleteSimpleUser, RawCreateUserError>;

  async fn upsert_seed_user(
    &self,
    options: &UpsertSeedUserOptions,
  ) -> Result<CompleteSimpleUser, RawUpsertSeedUserError>;

  async fn get_user(&self, options: &GetUserOptions) -> Result<RawGetUserResult, RawGetUserError>;

  async fn get_short_user(&self, options: &GetShortUserOptions) -> Result<ShortUser, RawGetShortUserError>;

  async fn get_user_with_password(
    &self,
    options: RawGetUser,
  ) -> Result<ShortUserWithPassword, RawGetUserWithPasswordError>;

  async fn get_user_tos(&self, options: &RawGetUserTosOptions) -> Result<GetUserTos, RawGetUserTosError>;

  async fn update_user(&self, options: &RawUpdateUserOptions) -> Result<CompleteSimpleUser, RawUpdateUserError>;

  async fn create_user_tos(&self, options: &RawUpdateUserTosOptions) -> Result<(), RawUpdateUserTosError>;

  async fn delete_user(&self, cmd: &RawDeleteUser) -> Result<CompleteSimpleUser, RawDeleteUserError>;

  async fn hard_delete_user(&self, cmd: &RawHardDeleteUser) -> Result<(), RawHardDeleteUserError>;

  async fn enable_user(&self, command: RawEnableUser) -> Result<EnableUserSummary, EnableUserError> {
    Handler::<RawEnableUser>::handle(self, command).await
  }
}

/// Like [`Deref`], but the target has the bound [`UserStore`]
pub trait UserStoreRef: Send + Sync {
  type UserStore: UserStore + ?Sized;

  fn user_store(&self) -> &Self::UserStore;
}

impl<TyRef> UserStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: UserStore,
{
  type UserStore = TyRef::Target;

  fn user_store(&self) -> &Self::UserStore {
    self.deref()
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EnableUser {
  pub r#ref: UserId,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EnableUserOut {
  pub user: CompleteUser,
  pub deleted_at: Option<Instant>,
  pub username: Result<(), UserIdRef>,
  pub dinoparc_com: Result<(), UserIdRef>,
  pub en_dinoparc_com: Result<(), UserIdRef>,
  pub hammerfest_es: Result<(), UserIdRef>,
  pub hammerfest_fr: Result<(), UserIdRef>,
  pub hfest_net: Result<(), UserIdRef>,
  pub sp_dinoparc_com: Result<(), UserIdRef>,
  pub twinoid: Result<(), UserIdRef>,
}

#[cfg(test)]
mod test {
  #[cfg(feature = "serde")]
  use crate::core::Instant;
  #[cfg(feature = "serde")]
  use crate::password::PasswordHash;
  use crate::patch::SimplePatch;
  #[cfg(feature = "serde")]
  use crate::user::{
    CompleteSimpleUser, RawGetUserResult, RawUpdateUserPatch, ShortUser, SimpleUser, UserDisplayName,
    UserDisplayNameVersion, UserDisplayNameVersions,
  };
  #[cfg(feature = "serde")]
  use std::fs;
  #[cfg(feature = "serde")]
  use std::str::FromStr;

  mod user_display_name {
    #[cfg(feature = "serde")]
    use super::*;

    #[cfg(feature = "serde")]
    fn get_demurgos() -> UserDisplayName {
      UserDisplayName::from_str("Demurgos").unwrap()
    }

    #[cfg(feature = "serde")]
    #[test]
    fn read_demurgos() {
      let s = fs::read_to_string("../../test-resources/core/user/user-display-name/demurgos/value.json").unwrap();
      let actual: UserDisplayName = serde_json::from_str(&s).unwrap();
      let expected = get_demurgos();
      assert_eq!(actual, expected);
    }

    #[cfg(feature = "serde")]
    #[test]
    fn write_demurgos() {
      let value = get_demurgos();
      let actual: String = serde_json::to_string_pretty(&value).unwrap();
      let expected =
        fs::read_to_string("../../test-resources/core/user/user-display-name/demurgos/value.json").unwrap();
      assert_eq!(&actual, expected.trim());
    }

    #[cfg(feature = "serde")]
    #[test]
    fn read_err_demurgos_space_both() {
      let s =
        fs::read_to_string("../../test-resources/core/user/user-display-name.error/demurgos-space-both/value.json")
          .unwrap();
      let actual = serde_json::from_str::<UserDisplayName>(&s);
      assert!(actual.is_err());
    }

    #[cfg(feature = "serde")]
    #[test]
    fn read_err_demurgos_space_end() {
      let s =
        fs::read_to_string("../../test-resources/core/user/user-display-name.error/demurgos-space-end/value.json")
          .unwrap();
      let actual = serde_json::from_str::<UserDisplayName>(&s);
      assert!(actual.is_err());
    }

    #[cfg(feature = "serde")]
    #[test]
    fn read_err_demurgos_space_start() {
      let s =
        fs::read_to_string("../../test-resources/core/user/user-display-name.error/demurgos-space-start/value.json")
          .unwrap();
      let actual = serde_json::from_str::<UserDisplayName>(&s);
      assert!(actual.is_err());
    }
  }

  #[cfg(feature = "serde")]
  fn get_short_user_demurgos() -> ShortUser {
    ShortUser {
      id: "9f310484-963b-446b-af69-797feec6813f".parse().unwrap(),
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: "Demurgos".parse().unwrap(),
        },
      },
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_short_user_demurgos() {
    let s = fs::read_to_string("../../test-resources/core/user/short-user/demurgos/value.json").unwrap();
    let actual: ShortUser = serde_json::from_str(&s).unwrap();
    let expected = get_short_user_demurgos();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_short_user_demurgos() {
    let value = get_short_user_demurgos();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/user/short-user/demurgos/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_get_user_result_short() -> RawGetUserResult {
    RawGetUserResult::Short(ShortUser {
      id: "e9c17533-633e-4f60-be9e-72883ae0174a".parse().unwrap(),
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: "Alice".parse().unwrap(),
        },
      },
    })
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_get_user_result_short() {
    let s = fs::read_to_string("../../test-resources/core/user/get-user-result/short/value.json").unwrap();
    let actual: RawGetUserResult = serde_json::from_str(&s).unwrap();
    let expected = get_get_user_result_short();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_get_user_result_short() {
    let value = get_get_user_result_short();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/user/get-user-result/short/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_get_user_result_default() -> RawGetUserResult {
    RawGetUserResult::Default(SimpleUser {
      id: "28dbb0bf-0fdc-40fe-ae5a-dde193f9fea8".parse().unwrap(),
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: "Alice".parse().unwrap(),
        },
      },
      created_at: Instant::ymd_hms_milli(2021, 1, 15, 14, 17, 14, 15),
      deleted_at: None,
      is_administrator: true,
    })
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_get_user_result_default() {
    let s = fs::read_to_string("../../test-resources/core/user/get-user-result/default/value.json").unwrap();
    let actual: RawGetUserResult = serde_json::from_str(&s).unwrap();
    let expected = get_get_user_result_default();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_get_user_result_default() {
    let value = get_get_user_result_default();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/user/get-user-result/default/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_get_user_result_complete() -> RawGetUserResult {
    RawGetUserResult::Complete(CompleteSimpleUser {
      id: "abeb9363-2035-4c20-9bb8-21edfb432cbf".parse().unwrap(),
      display_name: UserDisplayNameVersions {
        current: UserDisplayNameVersion {
          value: "Alice".parse().unwrap(),
        },
      },
      created_at: Instant::ymd_hms_milli(2021, 1, 15, 14, 17, 14, 15),
      deleted_at: None,
      is_administrator: true,
      username: Some("alice".parse().unwrap()),
      email_address: None,
      has_password: true,
    })
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_get_user_result_complete() {
    let s = fs::read_to_string("../../test-resources/core/user/get-user-result/complete/value.json").unwrap();
    let actual: RawGetUserResult = serde_json::from_str(&s).unwrap();
    let expected = get_get_user_result_complete();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_get_user_result_complete() {
    let value = get_get_user_result_complete();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/user/get-user-result/complete/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_update_user_patch_no_op() -> RawUpdateUserPatch {
    RawUpdateUserPatch {
      display_name: SimplePatch::Skip,
      username: SimplePatch::Skip,
      password: SimplePatch::Skip,
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_update_user_patch_no_op() {
    let s = fs::read_to_string("../../test-resources/core/user/update-user-patch/no-op/value.json").unwrap();
    let actual: RawUpdateUserPatch = serde_json::from_str(&s).unwrap();
    let expected = get_update_user_patch_no_op();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_update_user_patch_no_op() {
    let value = get_update_user_patch_no_op();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/user/update-user-patch/no-op/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_update_user_patch_remove_username() -> RawUpdateUserPatch {
    RawUpdateUserPatch {
      display_name: SimplePatch::Skip,
      username: SimplePatch::Set(None),
      password: SimplePatch::Skip,
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_update_user_patch_remove_username() {
    let s = fs::read_to_string("../../test-resources/core/user/update-user-patch/remove-username/value.json").unwrap();
    let actual: RawUpdateUserPatch = serde_json::from_str(&s).unwrap();
    let expected = get_update_user_patch_remove_username();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_update_user_patch_remove_username() {
    let value = get_update_user_patch_remove_username();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected =
      fs::read_to_string("../../test-resources/core/user/update-user-patch/remove-username/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_update_user_patch_set_all() -> RawUpdateUserPatch {
    let hash: Vec<u8> = hex::decode("736372797074000c0000000800000001c5ec1067adb434a19cb471dcfc13a8cec8c6e935ec7e14eda9f51a386924eeeb9fce39bb3d36f6101cc06189da63e0513a54553efbee9d2a058bafbda5231093c4ae5e9b3f87a2d002fa49ff75b868fd").unwrap();
    RawUpdateUserPatch {
      display_name: SimplePatch::Set("Demurgos".parse().unwrap()),
      username: SimplePatch::Set(Some("demurgos".parse().unwrap())),
      password: SimplePatch::Set(Some(PasswordHash::from(&hash[..]))),
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_update_user_patch_set_all() {
    let s = fs::read_to_string("../../test-resources/core/user/update-user-patch/set-all/value.json").unwrap();
    let actual: RawUpdateUserPatch = serde_json::from_str(&s).unwrap();
    let expected = get_update_user_patch_set_all();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_update_user_patch_set_all() {
    let value = get_update_user_patch_set_all();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected = fs::read_to_string("../../test-resources/core/user/update-user-patch/set-all/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }

  #[cfg(feature = "serde")]
  fn get_update_user_patch_set_username() -> RawUpdateUserPatch {
    RawUpdateUserPatch {
      display_name: SimplePatch::Skip,
      username: SimplePatch::Set(Some("demurgos".parse().unwrap())),
      password: SimplePatch::Skip,
    }
  }

  #[cfg(feature = "serde")]
  #[test]
  fn read_update_user_patch_set_username() {
    let s = fs::read_to_string("../../test-resources/core/user/update-user-patch/set-username/value.json").unwrap();
    let actual: RawUpdateUserPatch = serde_json::from_str(&s).unwrap();
    let expected = get_update_user_patch_set_username();
    assert_eq!(actual, expected);
  }

  #[cfg(feature = "serde")]
  #[test]
  fn write_update_user_patch_set_username() {
    let value = get_update_user_patch_set_username();
    let actual: String = serde_json::to_string_pretty(&value).unwrap();
    let expected =
      fs::read_to_string("../../test-resources/core/user/update-user-patch/set-username/value.json").unwrap();
    assert_eq!(&actual, expected.trim());
  }
}
