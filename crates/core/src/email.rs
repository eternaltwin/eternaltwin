use crate::core::{HtmlFragment, LocaleId};
#[cfg(feature = "sqlx-postgres")]
use crate::core::{Instant, SecretString};
use crate::forum::MarktwinText;
use crate::types::WeakError;
use async_trait::async_trait;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
#[cfg(feature = "sqlx-postgres")]
use sqlx::{Postgres, Transaction};
use std::ops::Deref;
use std::str::FromStr;

declare_new_string! {
  /// A string guaranteed to be a well-formed email address.
  ///
  /// It may be invalid to deliver mail there, but the email address is guaranteed to be at least syntactically valid.
  /// Use [`RawEmailAddress`] when the validity can't be checked.
  pub struct EmailAddress(String);
  pub type ParseError = EmailAddressParseError;
  const PATTERN = r"@";
  const SQL_NAME = "email_address";
}

impl EmailAddress {
  pub fn to_raw(&self) -> RawEmailAddress {
    RawEmailAddress::from_str(self.0.as_str()).expect("`EmailAddress::to_raw` always succeeds")
  }
}

declare_new_string! {
  /// A string expected to be an email address, but that was not checked (can be malformed)
  ///
  /// This does not even enforce `@` (since some archived "email addresses" from Motion Twin profiles were so
  /// broken that they did not have it).
  pub struct RawEmailAddress(String);
  pub type ParseError = RawEmailAddressParseError;
  const PATTERN = r".+";
  const SQL_NAME = "raw_email_address";
}

impl<'a> TryFrom<&'a RawEmailAddress> for EmailAddress {
  type Error = EmailAddressParseError;

  fn try_from(value: &'a RawEmailAddress) -> Result<Self, Self::Error> {
    Self::from_str(value.as_str())
  }
}

declare_new_string! {
  pub struct EmailBody(String);
  pub type ParseError = EmailBodyParseError;
  const PATTERN = r"^(?s:.){0,1000}";
  const SQL_NAME = "email_body";
}

declare_new_string! {
  pub struct EmailSubject(String);
  pub type ParseError = EmailTitleParseError;
  const PATTERN = r"^(?s:.){1,100}$";
  const SQL_NAME = "email_title";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum TouchEmailAddressError {
  /// Database error
  #[error(transparent)]
  Database(#[from] WeakError),
}

// TODO: Move this function to a crate with Postgres helpers
#[cfg(feature = "sqlx-postgres")]
pub async fn touch_email_address(
  tx: &mut Transaction<'_, Postgres>,
  secret: &SecretString,
  email: &RawEmailAddress,
  now: Instant,
) -> Result<Vec<u8>, TouchEmailAddressError> {
  let is_well_formed = EmailAddress::try_from(email).is_ok();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    email: RawEmailAddress,
    hash: Vec<u8>,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
      WITH
        input_row(email_address, _hash, created_at, is_valid) AS (
          VALUES(pgp_sym_encrypt($1::EMAIL_ADDRESS, $2::TEXT)::BYTEA, digest($1::EMAIL_ADDRESS, 'sha256')::EMAIL_ADDRESS_HASH, $3::INSTANT, $4::boolean)
        ),
        inserted_row AS (
          INSERT
          INTO email_addresses(email_address, _hash, created_at, is_well_formed)
            SELECT * FROM input_row
            ON CONFLICT DO NOTHING
            RETURNING $1::EMAIL_ADDRESS AS email, _hash AS hash
      )
      SELECT email, hash FROM inserted_row
      UNION ALL
      SELECT pgp_sym_decrypt(old.email_address, $2::TEXT), old._hash AS hash FROM email_addresses AS old INNER JOIN input_row USING(_hash);
      ",
  )
    .bind(email)
    .bind(secret.as_str())
    .bind(now)
    .bind(is_well_formed)
    .fetch_one(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;
  // Check for hash collision
  assert_eq!(&row.email, email);
  Ok(row.hash)
}

pub enum EmailTemplate {
  Custom,
}

pub struct EmailPayloadCustom {
  pub marktwin: MarktwinText,
}

pub struct EmailPayloadVerify {
  pub salt: [u8; 32],
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct VerifyRegistrationEmail {
  // TODO: Use `new_string` wrapper
  pub token: String,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EmailContent {
  pub subject: EmailSubject,
  pub body_text: EmailBody,
  pub body_html: Option<HtmlFragment>,
}

#[async_trait]
pub trait EmailFormatter: Send + Sync {
  async fn verify_registration_email(
    &self,
    locale: LocaleId,
    data: &VerifyRegistrationEmail,
  ) -> Result<EmailContent, WeakError>;

  async fn marktwin(&self, subject: EmailSubject, data: &MarktwinText) -> Result<EmailContent, WeakError>;
}

/// Like [`Deref`], but the target has the bound [`EmailFormatter`]
pub trait EmailFormatterRef: Send + Sync {
  type EmailFormatter: EmailFormatter + ?Sized;

  fn email_formatter(&self) -> &Self::EmailFormatter;
}

impl<TyRef> EmailFormatterRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: EmailFormatter,
{
  type EmailFormatter = TyRef::Target;

  fn email_formatter(&self) -> &Self::EmailFormatter {
    self.deref()
  }
}

#[async_trait]
pub trait Mailer: Send + Sync {
  async fn send_email(&self, recipient: &EmailAddress, content: &EmailContent) -> Result<(), WeakError>;
}

/// Like [`Deref`], but the target has the bound [`Mailer`]
pub trait MailerRef: Send + Sync {
  type Mailer: Mailer + ?Sized;

  fn mailer(&self) -> &Self::Mailer;
}

impl<TyRef> MailerRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: Mailer,
{
  type Mailer = TyRef::Target;

  fn mailer(&self) -> &Self::Mailer {
    self.deref()
  }
}
