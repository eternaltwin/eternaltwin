use crate::core::Request;
use crate::twinoid::api::Projector;
use crate::twinoid::api::User;
use crate::twinoid::client::SafeUserProjector;
use crate::twinoid::{TwinoidApiAuth, TwinoidUserId};
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum GetUsersError {
  #[error("invalid token")]
  InvalidToken,
  #[error("server failed to encode response")]
  Encoding,
  #[error(transparent)]
  Other(#[from] WeakError),
}

pub struct GetUsers<P: Projector<User>> {
  pub auth: TwinoidApiAuth,
  pub ids: Vec<TwinoidUserId>,
  pub projector: P,
}

// The impl uses `SafeUserProjector` but it's just a placeholder.
impl GetUsers<SafeUserProjector> {
  pub const MAX_LEN: usize = 50;
}

impl<P: Projector<User>> Request for GetUsers<P> {
  type Response = Result<Vec<P::Projection>, GetUsersError>;
}
