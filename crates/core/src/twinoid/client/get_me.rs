use crate::core::Request;
use crate::twinoid::api::ShortUser;
use crate::twinoid::TwinoidApiAuth;
use crate::types::WeakError;
use thiserror::Error;

pub struct GetMeShort {
  pub auth: TwinoidApiAuth,
}

#[derive(Debug, Error)]
pub enum GetMeShortError {
  #[error("invalid token")]
  InvalidToken,
  #[error("server failed to encode response")]
  Encoding,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl Request for GetMeShort {
  type Response = Result<ShortUser, GetMeShortError>;
}
