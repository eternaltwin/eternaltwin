use crate::core::Request;
use crate::twinoid::{TwinoidCredentials, TwinoidSession};
use crate::types::WeakError;
use thiserror::Error;

pub struct CreateSession {
  pub credentials: TwinoidCredentials,
}

#[derive(Debug, Error)]
pub enum TwinoidClientCreateSessionError {
  #[error("wrong credentials")]
  WrongCredentials,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl Request for CreateSession {
  type Response = Result<TwinoidSession, TwinoidClientCreateSessionError>;
}
