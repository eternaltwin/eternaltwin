use crate::{
  core::Request,
  twinoid::{ArchivedTwinoidScores, TwinoidUserIdRef},
  types::WeakError,
};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum RawGetScoresError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl RawGetScoresError {
  pub fn other<E: std::error::Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetScoresError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetScores {
  pub user: TwinoidUserIdRef,
}

impl Request for GetScores {
  type Response = Result<ArchivedTwinoidScores, RawGetScoresError>;
}
