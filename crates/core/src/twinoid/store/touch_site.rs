use crate::core::Request;
use crate::twinoid::client::FullSite;
use crate::twinoid::TwinoidApiAuth;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum TouchSiteError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TouchSite {
  pub auth: TwinoidApiAuth,
  pub site: FullSite,
}

impl Request for TouchSite {
  type Response = Result<(), TouchSiteError>;
}
