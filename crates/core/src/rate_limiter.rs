use crate::core::{Duration, Instant};
use std::num::NonZeroU64;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum RateLimiterError {
  /// Request number of tokens is over the limit: it will never be fulfilled
  OverLimit,
  /// Not enough tokens available, but will be ready at the provided instant if no other tokens are used.
  Wait(Instant),
}

/// This rate limiter implements the leaky bucket rate limiter algorithm.
///
/// Each request adds some tokens to the bucket. If the bucket is full, the
/// request is reject. Every period of time (a tick), a fixed amount of
/// tokens is removed. The buckets start empty.
///
/// This is equivalent to the token bucket algorithm: a leaky bucket can
/// handle the highest burst when empty; a token bucket can handle the
/// highest burst when full. The leaky bucket interpretation is preferred
/// because the base case (empty bucket) is independent of any config; this
/// makes it easier to use a leaky bucket rate limiter as part of a larger
/// hierarchical or partitioned rate limiter.
///
/// The time of last update is represented implicitly as
/// `start + tick * config.period_duration`. The bucket is drained lazily,
/// only when close the max limit.
///
/// This rate limiter supports two-step transactions where you first
/// reserve some capacity and can later commit it.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LeakyBucketRateLimiter {
  config: LeakyBucketRateLimiterConfig,
  state: Option<LeakyBucketRateLimiterState>,
}

impl LeakyBucketRateLimiter {
  pub fn new(config: LeakyBucketRateLimiterConfig) -> Self {
    Self { config, state: None }
  }

  pub fn acquire(&mut self, now: Instant, count: u64) -> Result<(), RateLimiterError> {
    let state = self.state.get_or_insert(LeakyBucketRateLimiterState {
      start: now,
      period: 0,
      used: 0,
    });
    state.acquire(&self.config, now, count)
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LeakyBucketRateLimiterConfig {
  /// Maximum number of tokens in use
  pub limit: NonZeroU64,
  pub tokens_per_period: NonZeroU64,
  /// Time to generate `limit` tokens
  pub period_duration: Duration,
}

/// This struct defines only the current state of rate limiter, the config
/// (`LeakyBucketRateLimiterConfig`) must be passed explicitly for each call.
/// This config is not store
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LeakyBucketRateLimiterState {
  /// Time when the tick "zero" occurred
  start: Instant,
  /// Number of ticks/periods since the start
  period: u64,
  /// Tokens in the bucket, they are drained on each tick
  used: u64,
}

impl LeakyBucketRateLimiterState {
  fn acquire(
    &mut self,
    config: &LeakyBucketRateLimiterConfig,
    now: Instant,
    count: u64,
  ) -> Result<(), RateLimiterError> {
    if count > config.limit.get() {
      return Err(RateLimiterError::OverLimit);
    }
    if self.used + count <= config.limit.get() {
      self.used += count;
      return Ok(());
    }
    self.leak_tokens(config, now);
    let overdraw = (self.used + count).saturating_sub(config.limit.get());
    if overdraw == 0 {
      self.used += count;
      return Ok(());
    }
    let mut periods_needed = overdraw / config.tokens_per_period.get();
    if overdraw % config.tokens_per_period.get() != 0 {
      periods_needed += 1;
    }
    let ready_period = i32::try_from(self.period + periods_needed).unwrap_or(i32::MAX);
    let ready_time = self.start + config.period_duration * ready_period;
    Err(RateLimiterError::Wait(ready_time))
  }

  fn leak_tokens(&mut self, config: &LeakyBucketRateLimiterConfig, now: Instant) {
    let elapsed = now.duration_since(self.start);
    let current_period: i64 = elapsed.div_floor(config.period_duration);
    let current_period = u64::try_from(current_period).unwrap_or(0);
    let elapsed_periods = current_period.saturating_sub(self.period);
    if elapsed_periods > 0 {
      let leaked_token = elapsed_periods.saturating_mul(config.tokens_per_period.get());
      self.used = self.used.saturating_sub(leaked_token);
      self.period = current_period;
    }
  }
}

#[cfg(test)]
mod test {
  use crate::core::{Duration, Instant};
  use crate::rate_limiter::{LeakyBucketRateLimiterConfig, LeakyBucketRateLimiterState, RateLimiterError};
  use std::num::NonZeroU64;

  #[test]
  fn test_reserve() {
    // Rate Limiter Config
    let rlc = LeakyBucketRateLimiterConfig {
      limit: NonZeroU64::new(100).unwrap(),
      tokens_per_period: NonZeroU64::new(5).unwrap(),
      period_duration: Duration::from_seconds(2),
    };
    let now = Instant::ymd_hms(2020, 1, 1, 0, 0, 0);
    let start = now;
    // Rate Limiter State
    let mut rls = LeakyBucketRateLimiterState {
      start,
      period: 0,
      used: 0,
    };
    for _ in 0..6 {
      assert_eq!(rls.acquire(&rlc, now, 15), Ok(()));
    }
    assert_eq!(
      rls,
      LeakyBucketRateLimiterState {
        start,
        period: 0,
        used: 90
      }
    );
    assert_eq!(
      rls.acquire(&rlc, now, 15),
      Err(RateLimiterError::Wait(now + Duration::from_seconds(2)))
    );
    assert_eq!(
      rls.acquire(&rlc, now, 30),
      Err(RateLimiterError::Wait(now + Duration::from_seconds(2) * 4))
    );
    assert_eq!(rls.acquire(&rlc, now, 300), Err(RateLimiterError::OverLimit));
    let now = now + Duration::from_seconds(2) * 4;
    assert_eq!(rls.acquire(&rlc, now, 15), Ok(()));
    assert_eq!(
      rls,
      LeakyBucketRateLimiterState {
        start,
        period: 4,
        used: 85
      }
    );
    let now = now + Duration::from_seconds(2) * 3;
    assert_eq!(rls.acquire(&rlc, now, 15), Ok(()));
    assert_eq!(
      rls,
      LeakyBucketRateLimiterState {
        start,
        period: 4,
        used: 100
      }
    );
    assert_eq!(rls.acquire(&rlc, now, 15), Ok(()));
    assert_eq!(
      rls,
      LeakyBucketRateLimiterState {
        start,
        period: 7,
        used: 100
      }
    );
  }
}
