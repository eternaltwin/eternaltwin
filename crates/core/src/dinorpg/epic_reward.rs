declare_new_enum! (
  /// Defines all the possible epic rewards a user can have in DinoRPG
  pub enum DinorpgEpicReward {
    #[str("PerleDeLaFontaine")]
    PerleDeLaFontaine,
    // TODO
  }
  pub type ParseError = DinorpgEpicRewardNameParseError;
  const SQL_NAME = "dinorpg_epic_reward_name";
);
