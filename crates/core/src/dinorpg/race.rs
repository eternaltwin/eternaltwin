declare_new_enum!(
  /// Defines all the races available in DinoRPG
  pub enum DinorpgRace {
    #[str("Castivore")]
    Castivore,
    #[str("Feross")]
    Feross,
    #[str("Gorilloz")]
    Gorilloz,
    #[str("Kabuki")]
    Kabuki,
    #[str("Mahamuti")]
    Mahamuti,
    #[str("Moueffe")]
    Moueffe,
    #[str("Nuagoz")]
    Nuagoz,
    #[str("Pigmou")]
    Pigmou,
    #[str("Planaille")]
    Planaille,
    #[str("Pteroz")]
    Pteroz,
    #[str("Quetzu")]
    Quetzu,
    #[str("Rocky")]
    Rocky,
    #[str("Santaz")]
    Santaz,
    #[str("Sirain")]
    Sirain,
    #[str("Hippoclamp")]
    Hippoclamp,
    #[str("Smog")]
    Smog,
    #[str("Soufflet")]
    Soufflet,
    #[str("Toufufu")]
    Toufufu,
    #[str("Triceragnon")]
    Triceragnon,
    #[str("Wanwan")]
    Wanwan,
    #[str("Winks")]
    Winks,
  }
  pub type ParseError = DinorpgRaceParseError;
  const SQL_NAME = "dinorpg_race_name";
);

impl DinorpgRace {
  pub fn from_skin_code(skin: &str) -> Option<Self> {
    if let Some(c) = skin.chars().next() {
      match c {
        '0' => Some(Self::Moueffe),
        '1' => Some(Self::Pigmou),
        '2' => Some(Self::Winks),
        '3' => Some(Self::Planaille),
        '4' => Some(Self::Castivore),
        '5' => Some(Self::Rocky),
        '6' => Some(Self::Pteroz),
        '7' => Some(Self::Nuagoz),
        '8' => Some(Self::Sirain),
        '9' => Some(Self::Hippoclamp),
        'A' => Some(Self::Gorilloz),
        'B' => Some(Self::Wanwan),
        'C' => Some(Self::Santaz),
        'D' => Some(Self::Feross),
        'E' => Some(Self::Kabuki),
        'F' => Some(Self::Mahamuti),
        'H' => Some(Self::Toufufu),
        'I' => Some(Self::Quetzu),
        'J' => Some(Self::Smog),
        'K' => Some(Self::Triceragnon),
        _ => None,
      }
    } else {
      None
    }
  }
}
