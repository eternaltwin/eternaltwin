use crate::dinorpg::dino::DinorpgDinozId;
use crate::dinorpg::{
  DinorpgAllDinozMissionsResponse, DinorpgDemonShopResponse, DinorpgDinoResponse, DinorpgDinozSkillsResponse,
  DinorpgIngredientResponse, DinorpgServer, DinorpgSession, DinorpgUserResponse,
};
use crate::twinoid::TwinoidSessionKey;
use crate::types::WeakError;
use async_trait::async_trait;

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientCreateSessionError {
  #[error("invalid Twinoid session key")]
  InvalidTwinoidSessionKey,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientGetOwnUserError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientGetOwnDinoError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientGetOwnIngredientsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientGetOwnMissionsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientGetOwnDemonShopError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum DinorpgClientGetOwnDinozSkillsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[async_trait]
pub trait DinorpgClient: Send + Sync {
  async fn create_session(
    &self,
    server: DinorpgServer,
    tid_key: &TwinoidSessionKey,
  ) -> Result<DinorpgSession, DinorpgClientCreateSessionError>;

  // From the API
  /// Get the user info from the Twinoid API
  async fn get_own_user(&self, session: &DinorpgSession) -> Result<DinorpgUserResponse, DinorpgClientGetOwnUserError>;

  /// Get a dino info from the Twinoid API
  async fn get_own_dino(&self, session: &DinorpgSession) -> Result<DinorpgDinoResponse, DinorpgClientGetOwnDinoError>;

  // From the scraper
  /// Get the list of ingredients from the user
  async fn get_own_ingredients(
    &self,
    session: &DinorpgSession,
  ) -> Result<DinorpgIngredientResponse, DinorpgClientGetOwnIngredientsError>;

  /// Get the list of missions from all dinoz
  async fn get_own_missions(
    &self,
    session: &DinorpgSession,
  ) -> Result<DinorpgAllDinozMissionsResponse, DinorpgClientGetOwnMissionsError>;

  /// Get the list of dinoz from the demon shop
  async fn get_own_demon_shop(
    &self,
    session: &DinorpgSession,
  ) -> Result<DinorpgDemonShopResponse, DinorpgClientGetOwnDemonShopError>;

  /// Get the list of skills from the dinoz page
  async fn get_own_dinoz_skills(
    &self,
    session: &DinorpgSession,
    dinoz_id: DinorpgDinozId,
  ) -> Result<DinorpgDinozSkillsResponse, DinorpgClientGetOwnDinozSkillsError>;
}
