use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::oauth::{
  CreateStoredAccessTokenOptions, GetOauthAccessTokenOptions, GetOauthClientOptions, OauthProviderStore,
  RawCreateAccessTokenError, RawGetAccessTokenError, RawGetOauthClientError, RawGetOauthClientWithSecretError,
  RawUpsertSystemOauthClientError, SimpleOauthClient, SimpleOauthClientWithSecret, StoredOauthAccessToken,
  UpsertSystemClientOptions,
};
use eternaltwin_core::types::DisplayErrorChain;
use opentelemetry::trace::{FutureExt, Status, TraceContextExt, Tracer};
use opentelemetry::{Context, KeyValue};
use std::borrow::Cow;

pub struct TraceOauthProviderStore<TyClock, TyOauthProviderStore, TyTracer> {
  clock: TyClock,
  inner: TyOauthProviderStore,
  tracer: TyTracer,
}

impl<TyClock, TyOauthProviderStore, TyTracer> TraceOauthProviderStore<TyClock, TyOauthProviderStore, TyTracer> {
  pub fn new(clock: TyClock, inner: TyOauthProviderStore, tracer: TyTracer) -> Self {
    Self { clock, tracer, inner }
  }
}

#[async_trait]
impl<TyClock, TyOauthProviderStore, TyTracer> OauthProviderStore
  for TraceOauthProviderStore<TyClock, TyOauthProviderStore, TyTracer>
where
  TyClock: ClockRef,
  TyOauthProviderStore: OauthProviderStore + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn upsert_system_client(
    &self,
    cmd: &UpsertSystemClientOptions,
  ) -> Result<SimpleOauthClient, RawUpsertSystemOauthClientError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("OauthProviderStore::upsert_system_client")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.upsert_system_client(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        if let Some(key) = v.key.as_ref() {
          span.set_attribute(KeyValue::new("key", key.to_string()));
        }
        span.set_attribute(KeyValue::new("callback_uri", v.callback_uri.to_string()));
        span.set_attribute(KeyValue::new("app_uri", v.app_uri.to_string()));
        span.set_attribute(KeyValue::new("display_name", v.display_name.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_client(&self, query: &GetOauthClientOptions) -> Result<SimpleOauthClient, RawGetOauthClientError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("OauthProviderStore::get_client")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_client(query).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_client_with_secret(
    &self,
    query: &GetOauthClientOptions,
  ) -> Result<SimpleOauthClientWithSecret, RawGetOauthClientWithSecretError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("OauthProviderStore::get_client_with_secret")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_client_with_secret(query).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn create_access_token(
    &self,
    cmd: &CreateStoredAccessTokenOptions,
  ) -> Result<StoredOauthAccessToken, RawCreateAccessTokenError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("OauthProviderStore::create_access_token")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.create_access_token(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("client.id", v.client.id.to_string()));
        span.set_attribute(KeyValue::new("user.id", v.user.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_access_token(
    &self,
    query: &GetOauthAccessTokenOptions,
  ) -> Result<StoredOauthAccessToken, RawGetAccessTokenError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("OauthProviderStore::get_access_token")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_access_token(query).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("client.id", v.client.id.to_string()));
        span.set_attribute(KeyValue::new("user.id", v.user.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}
