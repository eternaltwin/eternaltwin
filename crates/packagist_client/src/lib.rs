use async_trait::async_trait;
use auto_impl::auto_impl;
use chrono::{DateTime, Utc};
pub use compact_str;
use compact_str::CompactString;
use indexmap::IndexMap;
use thiserror::Error;

#[cfg(feature = "http")]
pub mod http;

#[async_trait]
#[auto_impl(&, Arc)]
pub trait PackagistClient: Send + Sync {
  async fn get_package(&self, req: GetPackageRequestView<'_>) -> Result<Vec<PackageMeta2>, GetPackageError>;
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum GetPackageError {
  #[error("failed to send `GetPackage` request: {0}")]
  Send(String),
  #[error("failed to receive `GetPackage` response: {0}")]
  Receive(String),
  #[error("package not found")]
  NotFound,
  #[error("unexpected `GetPackage` error: {0}")]
  Other(String),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetPackageRequest<Str = CompactString> {
  pub vendor: Str,
  pub package: Str,
}

pub type GetPackageRequestView<'req> = GetPackageRequest<&'req str>;

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PackageMeta2 {
  pub name: CompactString,
  pub description: CompactString,
  pub keywords: Vec<CompactString>,
  pub homepage: CompactString,
  pub version: CompactString,
  pub version_normalized: CompactString,
  pub license: Vec<CompactString>,
  pub authors: Vec<Author>,
  pub source: PackageSource,
  pub dist: PackageDist,
  pub r#type: CompactString,
  pub support: Option<PackageSupport>,
  pub funding: Vec<PackageFunding>,
  pub time: DateTime<Utc>,
  pub autoload: serde_json::Value,
  pub extra: Option<serde_json::Value>,
  pub bin: Option<Vec<CompactString>>,
  pub require: Option<IndexMap<CompactString, CompactString>>,
  #[cfg_attr(feature = "serde", serde(rename = "require-dev"))]
  pub require_dev: Option<IndexMap<CompactString, CompactString>>,
  pub suggest: Option<IndexMap<CompactString, CompactString>>,
  pub provide: Option<IndexMap<CompactString, CompactString>>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Author {
  pub name: CompactString,
  pub email: CompactString,
  pub homepage: Option<CompactString>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PackageSource {
  pub url: CompactString,
  pub r#type: CompactString,
  pub reference: CompactString,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PackageDist {
  pub url: CompactString,
  pub r#type: CompactString,
  pub shasum: CompactString,
  pub reference: CompactString,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PackageSupport {
  pub issues: CompactString,
  pub source: CompactString,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PackageFunding {
  pub url: CompactString,
  pub r#type: CompactString,
}
