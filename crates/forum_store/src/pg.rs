use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Handler, HtmlFragment, Instant, Listing, ListingCount, LocaleId};
use eternaltwin_core::forum::{
  store, ForumPostId, ForumPostRevisionComment, ForumPostRevisionContent, ForumPostRevisionId, ForumRole,
  ForumRoleGrant, ForumSection, ForumSectionDisplayName, ForumSectionId, ForumSectionKey, ForumSectionRef,
  ForumSectionSelf, ForumThreadId, ForumThreadKey, ForumThreadListing, ForumThreadRef, ForumThreadTitle, MarktwinText,
  RawCreateForumPostResult, RawCreateForumPostRevisionResult, RawCreateForumThreadResult, RawForumActor, RawForumPost,
  RawForumPostRevision, RawForumPostRevisionListing, RawForumRoleGrant, RawForumSectionMeta, RawForumThreadMeta,
  RawLatestForumPostRevisionListing, RawShortForumPost, RawUserForumActor,
};
use eternaltwin_core::pg_num::PgU32;
use eternaltwin_core::user::UserId;
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternaltwin_db_schema::schema::ForumRoleGrantBySectionArray;
use sqlx::postgres::{PgHasArrayType, PgTypeInfo};
use sqlx::PgPool;

pub struct PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(clock: TyClock, database: TyDatabase, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      database,
      uuid_generator,
    }
  }
}

const THREADS_PER_PAGE: u32 = 20;

#[derive(Debug, sqlx::Type)]
struct PgForumSectionMeta {
  forum_section_id: ForumSectionId,
  key: Option<ForumSectionKey>,
  ctime: Instant,
  display_name: ForumSectionDisplayName,
  locale: Option<LocaleId>,
  thread_count: PgU32,
  role_grants: ForumRoleGrantBySectionArray,
}

impl PgHasArrayType for PgForumSectionMeta {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_forum_section_meta")
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::AddModerator>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, cmd: store::AddModerator) -> Result<(), store::AddModeratorError> {
    let now = self.clock.clock().now();
    let (section_id, section_key) = cmd.section.split_deref();

    // language=PostgreSQL
    let res = sqlx::query(
      r"
      WITH section AS (
        SELECT forum_section_id
        FROM forum_sections
        WHERE
          forum_section_id = $1::FORUM_SECTION_ID OR key = $2::FORUM_SECTION_KEY
      )
      INSERT
      INTO forum_role_grants(
        forum_section_id, user_id, start_time, granted_by
      )
        (
          SELECT forum_section_id, $3::USER_ID AS user_id, $5::INSTANT AS start_time, $4::USER_ID AS granted_by
          FROM section
        )
      ON CONFLICT (forum_section_id, user_id) DO NOTHING;",
    )
    .bind(section_id)
    .bind(section_key)
    .bind(cmd.target.id)
    .bind(cmd.granter.id)
    .bind(now)
    .execute(&*self.database)
    .await
    .map_err(store::AddModeratorError::other)?;
    assert!(res.rows_affected() <= 1);
    Ok(())
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::DeleteModerator>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, cmd: store::DeleteModerator) -> Result<(), store::DeleteModeratorError> {
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(store::DeleteModeratorError::other)?;

    let (section_id, section_key) = cmd.section.split_deref();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      start_time: Instant,
      granted_by: UserId,
    }
    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      WITH section AS (
        SELECT forum_section_id
        FROM forum_sections
        WHERE
          forum_section_id = $1::FORUM_SECTION_ID OR key = $2::FORUM_SECTION_KEY
      )
      DELETE
      FROM forum_role_grants
        USING section
      WHERE forum_role_grants.forum_section_id = section.forum_section_id AND user_id = $3::USER_ID
      RETURNING user_id, start_time, granted_by;",
    )
    .bind(section_id)
    .bind(section_key)
    .bind(cmd.target.id)
    .fetch_optional(&mut *tx)
    .await
    .map_err(store::DeleteModeratorError::other)?;

    let row = match row {
      Some(row) => row,
      None => return Ok(()),
    };

    debug_assert_eq!(row.user_id, cmd.target.id);

    // language=PostgreSQL
    let res = sqlx::query(
      r"
      WITH section AS (
        SELECT forum_section_id
        FROM forum_sections
        WHERE
          forum_section_id = $1::FORUM_SECTION_ID OR key = $2::FORUM_SECTION_KEY
      )
      INSERT
      INTO forum_role_revocations(
        forum_section_id, user_id, start_time, end_time, granted_by, revoked_by
      )
        (
          SELECT
            forum_section_id, $3::USER_ID AS user_id,
            $4::INSTANT AS start_time, $5::INSTANT AS end_time,
            $6::USER_ID AS granted_by, $7::USER_ID AS revoked_by
          FROM section
        )
      ON CONFLICT (forum_section_id, user_id, start_time) DO NOTHING;",
    )
    .bind(section_id)
    .bind(section_key)
    .bind(cmd.target.id)
    .bind(row.start_time)
    .bind(now)
    .bind(row.granted_by)
    .bind(cmd.revoker.id)
    .execute(&mut *tx)
    .await
    .map_err(store::DeleteModeratorError::other)?;
    assert!(res.rows_affected() <= 1);

    tx.commit().await.map_err(store::DeleteModeratorError::other)?;

    Ok(())
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetSections>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetSections) -> Result<Listing<RawForumSectionMeta>, store::GetSectionsError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      count: PgU32,
      items: Vec<PgForumSectionMeta>,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
        WITH
          sections AS (
            SELECT ROW(forum_section_id, key, ctime, display_name, locale, thread_count, role_grants)::forum_section_meta AS section
            FROM forum_section_meta
            ORDER BY ctime, key, forum_section_id
            LIMIT $1::U32 OFFSET $2::U32
          ),
          section_array AS (
            SELECT COALESCE(ARRAY_AGG(section), '{}') AS items
            FROM sections
          ),
          section_count AS (
            SELECT COUNT(*) AS count
            FROM forum_section_meta
          )
        SELECT count, items::forum_section_meta[]
        FROM section_count, section_array
        ;
    ",
    )
      .bind(PgU32::from(query.limit))
      .bind(PgU32::from(query.offset))
      .fetch_one(&*self.database)
      .await.map_err(store::GetSectionsError::other)?;

    let items: Vec<_> = row
      .items
      .into_iter()
      .map(|pg_section| RawForumSectionMeta {
        id: pg_section.forum_section_id,
        key: pg_section.key,
        display_name: pg_section.display_name,
        ctime: pg_section.ctime,
        locale: pg_section.locale,
        threads: ListingCount {
          count: pg_section.thread_count.into(),
        },
        role_grants: pg_section
          .role_grants
          .into_inner()
          .into_iter()
          .map(|grant| RawForumRoleGrant {
            role: ForumRole::Moderator,
            user: grant.user_id.into(),
            start_time: grant.start_time,
            granted_by: grant.granted_by.into(),
          })
          .collect(),
      })
      .collect();

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count: row.count.into(),
      items,
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetSectionMeta>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetSectionMeta) -> Result<RawForumSectionMeta, store::GetSectionMetaError> {
    let mut section_id: Option<ForumSectionId> = None;
    let mut section_key: Option<&ForumSectionKey> = None;
    match &query.section {
      ForumSectionRef::Id(r) => section_id = Some(r.id),
      ForumSectionRef::Key(r) => section_key = Some(&r.key),
    };
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      forum_section_id: ForumSectionId,
      key: Option<ForumSectionKey>,
      ctime: Instant,
      display_name: ForumSectionDisplayName,
      locale: Option<LocaleId>,
      thread_count: PgU32,
      role_grants: ForumRoleGrantBySectionArray,
    }
    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        SELECT
          forum_section_id, key, ctime, display_name, locale,
          thread_count,
          role_grants
        FROM forum_section_meta
        WHERE forum_section_id = $1::FORUM_SECTION_ID OR key = $2::FORUM_SECTION_KEY
        ;
    ",
    )
    .bind(section_id)
    .bind(section_key)
    .fetch_optional(&*self.database)
    .await
    .map_err(store::GetSectionMetaError::other)?;
    let row = row.ok_or(store::GetSectionMetaError::NotFound)?;
    Ok(RawForumSectionMeta {
      id: row.forum_section_id,
      key: row.key,
      display_name: row.display_name,
      ctime: row.ctime,
      locale: row.locale,
      threads: ListingCount {
        count: row.thread_count.into(),
      },
      role_grants: row
        .role_grants
        .into_inner()
        .into_iter()
        .map(|rg| RawForumRoleGrant {
          role: ForumRole::Moderator,
          user: rg.user_id.into(),
          start_time: rg.start_time,
          granted_by: rg.granted_by.into(),
        })
        .collect(),
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetThreads>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetThreads) -> Result<Listing<RawForumThreadMeta>, store::GetThreadsError> {
    let (section_id, section_key) = query.section.split_deref();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      count: PgU32,
      forum_thread_id: ForumThreadId,
      key: Option<ForumThreadKey>,
      ctime: Instant,
      title: ForumThreadTitle,
      is_pinned: bool,
      is_locked: bool,
      post_count: PgU32,
      forum_section_id: ForumSectionId,
    }
    // TODO: Differentiate `notFound` from "empty"?
    // language=PostgreSQL
    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
        WITH
          section AS (
            SELECT forum_section_id
            FROM forum_sections
            WHERE
              forum_section_id = $1::FORUM_SECTION_ID OR key = $2::FORUM_SECTION_KEY
          ),
          items AS (
            SELECT forum_thread_id, key, ctime, title, is_pinned, is_locked, post_count, forum_section_id
            FROM forum_thread_meta
            WHERE forum_section_id = (SELECT forum_section_id FROM section)
            ORDER BY last_post_ctime DESC
          ),
          item_count AS (
            SELECT COUNT(*) AS count
            FROM items
          )
        SELECT count, items.*
        FROM item_count, items
        LIMIT $3::U32 OFFSET $4::U32
        ;
    ",
    )
    .bind(section_id)
    .bind(section_key)
    .bind(PgU32::from(query.limit))
    .bind(PgU32::from(query.offset))
    .fetch_all(&*self.database)
    .await
    .map_err(store::GetThreadsError::other)?;

    let mut count: u32 = 0;

    let items: Vec<_> = rows
      .into_iter()
      .map(|row| {
        count = row.count.into();
        RawForumThreadMeta {
          id: row.forum_thread_id,
          key: row.key,
          title: row.title,
          section: row.forum_section_id.into(),
          ctime: row.ctime,
          is_locked: row.is_locked,
          is_pinned: row.is_pinned,
          posts: ListingCount {
            count: row.post_count.into(),
          },
        }
      })
      .collect();

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetThreadMeta>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetThreadMeta) -> Result<RawForumThreadMeta, store::GetThreadMetaError> {
    let mut thread_id: Option<ForumThreadId> = None;
    let mut thread_key: Option<&ForumThreadKey> = None;
    match &query.thread {
      ForumThreadRef::Id(r) => thread_id = Some(r.id),
      ForumThreadRef::Key(r) => thread_key = Some(&r.key),
    };
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      forum_thread_id: ForumThreadId,
      key: Option<ForumThreadKey>,
      ctime: Instant,
      title: ForumThreadTitle,
      is_locked: bool,
      is_pinned: bool,
      forum_section_id: ForumSectionId,
      post_count: PgU32,
    }
    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        SELECT
          forum_thread_id, key, ctime, title, is_locked, is_pinned, forum_section_id,
          post_count
        FROM forum_thread_meta
        WHERE forum_thread_id = $1::FORUM_THREAD_ID OR key = $2::FORUM_THREAD_KEY
        ;
    ",
    )
    .bind(thread_id)
    .bind(thread_key)
    .fetch_optional(&*self.database)
    .await
    .map_err(store::GetThreadMetaError::other)?;
    let row = row.ok_or(store::GetThreadMetaError::NotFound)?;
    Ok(RawForumThreadMeta {
      id: row.forum_thread_id,
      key: row.key,
      title: row.title,
      section: row.forum_section_id.into(),
      ctime: row.ctime,
      is_pinned: row.is_pinned,
      is_locked: row.is_locked,
      posts: ListingCount {
        count: row.post_count.into(),
      },
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::CreateThread>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, command: store::CreateThread) -> Result<RawCreateForumThreadResult, store::CreateThreadError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(store::CreateThreadError::other)?;
    let forum_thread_id = ForumThreadId::from_uuid(self.uuid_generator.uuid_generator().next());
    let (section_id, section_key) = command.section.split_deref();
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      ctime: Instant,
      forum_section_id: ForumSectionId,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH section AS (
        SELECT forum_section_id
        FROM forum_sections
        WHERE
        forum_section_id = $4::FORUM_SECTION_ID OR key = $5::FORUM_SECTION_KEY
      )
      INSERT INTO forum_threads(
        forum_thread_id, key, ctime,
        title, title_mtime,
        forum_section_id,
        is_pinned, is_pinned_mtime,
        is_locked, is_locked_mtime
      )
        (
          SELECT
            $2::FORUM_THREAD_ID AS forum_thread_id, NULL as key, $1::INSTANT AS ctime,
            $3::FORUM_THREAD_TITLE AS title, $1::INSTANT AS title_mtime,
            forum_section_id,
            FALSE AS is_pinned, $1::INSTANT AS is_pinned_mtime,
            FALSE AS is_locked, $1::INSTANT AS is_locked_mtime
          FROM section
        )
      RETURNING ctime, forum_section_id;
      ",
    )
    .bind(now)
    .bind(forum_thread_id)
    .bind(&command.title)
    .bind(section_id)
    .bind(section_key)
    .fetch_one(&mut *tx)
    .await
    .map_err(store::CreateThreadError::other)?;
    debug_assert_eq!(row.ctime, now);
    let forum_section_id = row.forum_section_id;

    let forum_post_id = ForumPostId::from_uuid(self.uuid_generator.uuid_generator().next());
    #[derive(Debug, sqlx::FromRow)]
    struct PostRow {
      ctime: Instant,
    }
    // language=PostgreSQL
    let row: PostRow = sqlx::query_as::<_, PostRow>(
      r"
      INSERT INTO forum_posts(
        forum_post_id, ctime, forum_thread_id
      )
      VALUES (
        $2::FORUM_POST_ID, $1::INSTANT, $3::FORUM_THREAD_ID
      )
      RETURNING ctime;
      ",
    )
    .bind(now)
    .bind(forum_post_id)
    .bind(forum_thread_id)
    .fetch_one(&mut *tx)
    .await
    .map_err(store::CreateThreadError::other)?;
    debug_assert_eq!(row.ctime, now);

    let revision_id = ForumPostRevisionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let (raw_actor, user_actor_id) = match &command.actor {
      RawForumActor::ClientForumActor(_) => todo!(),
      RawForumActor::RoleForumActor(_) => todo!(),
      RawForumActor::UserForumActor(a) => (
        RawForumActor::UserForumActor(RawUserForumActor {
          role: a.role,
          user: a.user,
        }),
        a.user.id,
      ),
    };
    let revision = RawForumPostRevision {
      id: revision_id,
      time: now,
      author: raw_actor,
      content: Some(ForumPostRevisionContent {
        marktwin: command.body_mkt.clone(),
        html: command.body_html.clone(),
      }),
      moderation: None,
      comment: None,
    };

    #[derive(Debug, sqlx::FromRow)]
    struct RevisionRow {
      time: Instant,
    }
    // language=PostgreSQL
    let row: RevisionRow = sqlx::query_as::<_, RevisionRow>(
      r"
      INSERT INTO forum_post_revisions(
        forum_post_revision_id, time, body, _html_body, mod_body, _html_mod_body, forum_post_id, author_id, comment
      )
      VALUES (
        $2::FORUM_POST_REVISION_ID, $1::INSTANT, $3::TEXT, $4::TEXT, NULL, NULL, $5::FORUM_POST_ID, $6::USER_ID, NULL
      )
      RETURNING time;
      ",
    )
    .bind(revision.time)
    .bind(revision.id)
    .bind(command.body_mkt.as_str())
    .bind(command.body_html.as_str())
    .bind(forum_post_id)
    .bind(user_actor_id)
    .fetch_one(&mut *tx)
    .await
    .map_err(store::CreateThreadError::other)?;

    tx.commit().await.map_err(store::CreateThreadError::other)?;

    Ok(RawCreateForumThreadResult {
      id: forum_thread_id,
      key: None,
      title: command.title.clone(),
      section: forum_section_id.into(),
      ctime: row.time,
      is_pinned: false,
      is_locked: false,
      post_id: forum_post_id,
      post_revision: revision,
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetPosts>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetPosts) -> Result<Listing<RawShortForumPost>, store::GetPostsError> {
    let (thread_id, thread_key) = query.thread.split_deref();
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      count: PgU32,
      forum_post_id: ForumPostId,
      ctime: Instant,
      revision_count: PgU32,
      latest_revision_id: ForumPostRevisionId,
      latest_revision_time: Instant,
      latest_revision_body: Option<MarktwinText>,
      latest_revision_html_body: Option<HtmlFragment>,
      latest_revision_mod_body: Option<MarktwinText>,
      latest_revision_html_mod_body: Option<HtmlFragment>,
      latest_revision_comment: Option<ForumPostRevisionComment>,
      latest_revision_author_id: UserId,
      first_revision_author_id: UserId,
    }
    // TODO: Differentiate `notFound` from "empty"?
    // language=PostgreSQL
    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
        WITH
          thread AS (
            SELECT forum_thread_id
            FROM forum_threads
            WHERE
              forum_thread_id = $1::FORUM_THREAD_ID OR key = $2::FORUM_THREAD_KEY
          ),
          items AS (
                    SELECT forum_post_id, ctime,
          LAST_VALUE(forum_post_revision_id) OVER w AS latest_revision_id,
          LAST_VALUE(time) OVER w AS latest_revision_time,
          LAST_VALUE(body) OVER w AS latest_revision_body,
          LAST_VALUE(_html_body) OVER w AS latest_revision_html_body,
          LAST_VALUE(mod_body) OVER w AS latest_revision_mod_body,
          LAST_VALUE(_html_mod_body) OVER w AS latest_revision_html_mod_body,
          LAST_VALUE(comment) OVER w AS latest_revision_comment,
          LAST_VALUE(author_id) OVER w AS latest_revision_author_id,
          FIRST_VALUE(author_id) OVER w AS first_revision_author_id,
                           COUNT(forum_post_revision_id) OVER w as revision_count,
          ROW_NUMBER() OVER w AS rn
        FROM forum_post_revisions
               INNER JOIN forum_posts USING (forum_post_id)
            WHERE forum_thread_id = (SELECT forum_thread_id FROM thread)
          WINDOW w AS (PARTITION BY forum_post_id ORDER BY time ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
          ),
          item_count AS (
            SELECT COUNT(*) AS count
            FROM items
          )
        SELECT count, items.*
        FROM item_count, items
        WHERE items.rn = 1
        ORDER BY ctime
        LIMIT $3::U32 OFFSET $4::U32
        ;
    ",
    )
      .bind(thread_id)
      .bind(thread_key)
      .bind(PgU32::from(query.limit))
      .bind(PgU32::from(query.offset))
      .fetch_all(&*self.database)
      .await.map_err(store::GetPostsError::other)?;

    let mut count: u32 = 0;

    let items: Vec<_> = rows
      .into_iter()
      .map(|row| {
        count = row.count.into();
        RawShortForumPost {
          id: row.forum_post_id,
          ctime: row.ctime,
          author: RawForumActor::UserForumActor(RawUserForumActor {
            role: None,
            user: row.first_revision_author_id.into(),
          }),
          revisions: RawLatestForumPostRevisionListing {
            count: row.revision_count.into(),
            last: RawForumPostRevision {
              id: row.latest_revision_id,
              time: row.latest_revision_time,
              author: RawForumActor::UserForumActor(RawUserForumActor {
                role: None,
                user: row.latest_revision_author_id.into(),
              }),
              content: match (row.latest_revision_body, row.latest_revision_html_body) {
                (Some(marktwin), Some(html)) => Some(ForumPostRevisionContent { marktwin, html }),
                (None, None) => None,
                _ => todo!(),
              },
              moderation: match (row.latest_revision_mod_body, row.latest_revision_html_mod_body) {
                (Some(marktwin), Some(html)) => Some(ForumPostRevisionContent { marktwin, html }),
                (None, None) => None,
                _ => todo!(),
              },
              comment: row.latest_revision_comment,
            },
          },
        }
      })
      .collect();

    Ok(Listing {
      offset: query.offset,
      limit: query.limit,
      count,
      items,
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::CreatePost>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, command: store::CreatePost) -> Result<RawCreateForumPostResult, store::CreatePostError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(store::CreatePostError::other)?;
    let (thread_id, thread_key) = command.thread.split_deref();

    let forum_post_id = ForumPostId::from_uuid(self.uuid_generator.uuid_generator().next());
    #[derive(Debug, sqlx::FromRow)]
    struct PostRow {
      forum_thread_id: ForumThreadId,
      forum_section_id: ForumSectionId,
    }
    // language=PostgreSQL
    let row: PostRow = sqlx::query_as::<_, PostRow>(
      r"
      WITH thread AS (
        SELECT forum_thread_id
        FROM forum_threads
        WHERE
        forum_thread_id = $3::FORUM_THREAD_ID OR key = $4::FORUM_THREAD_KEY
      )
      INSERT INTO forum_posts(
        forum_post_id, ctime, forum_thread_id
      )
        (
          SELECT
            $2::FORUM_POST_ID AS forum_post_id,
            $1::INSTANT AS ctime,
            forum_thread_id
          FROM thread
        )
      RETURNING forum_thread_id, (SELECT forum_section_id FROM forum_threads INNER JOIN thread USING(forum_thread_id)) AS forum_section_id;
      ",
    )
      .bind(now)
      .bind(forum_post_id)
      .bind(thread_id)
      .bind(thread_key)
      .fetch_one(&mut *tx)
      .await.map_err(store::CreatePostError::other)?;

    let forum_thread_id = row.forum_thread_id;
    let forum_section_id = row.forum_section_id;

    let revision_id = ForumPostRevisionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let (raw_actor, user_actor_id) = match &command.actor {
      RawForumActor::ClientForumActor(_) => todo!(),
      RawForumActor::RoleForumActor(_) => todo!(),
      RawForumActor::UserForumActor(a) => (RawForumActor::UserForumActor(a.clone()), a.user.id),
    };
    let revision = RawForumPostRevision {
      id: revision_id,
      time: now,
      author: raw_actor,
      content: Some(command.body.clone()),
      moderation: None,
      comment: None,
    };

    #[derive(Debug, sqlx::FromRow)]
    struct RevisionRow {
      time: Instant,
    }
    // language=PostgreSQL
    let row: RevisionRow = sqlx::query_as::<_, RevisionRow>(
      r"
      INSERT INTO forum_post_revisions(
        forum_post_revision_id, time, body, _html_body, mod_body, _html_mod_body, forum_post_id, author_id, comment
      )
      VALUES (
        $2::FORUM_POST_REVISION_ID, $1::INSTANT, $3::TEXT, $4::TEXT, NULL, NULL, $5::FORUM_POST_ID, $6::USER_ID, NULL
      )
      RETURNING time;
      ",
    )
    .bind(revision.time)
    .bind(revision.id)
    .bind(command.body.marktwin.as_str())
    .bind(command.body.html.as_str())
    .bind(forum_post_id)
    .bind(user_actor_id)
    .fetch_one(&mut *tx)
    .await
    .map_err(store::CreatePostError::other)?;

    tx.commit().await.map_err(store::CreatePostError::other)?;

    Ok(RawCreateForumPostResult {
      id: forum_post_id,
      thread: forum_thread_id.into(),
      section: forum_section_id.into(),
      revision: RawForumPostRevision {
        time: row.time,
        ..revision
      },
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetPost>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetPost) -> Result<RawForumPost, store::GetPostError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      count: PgU32,
      first_revision_time: Instant,
      first_revision_author: UserId,
      revisions: Vec<(
        ForumPostRevisionId,
        Instant,
        UserId,
        Option<MarktwinText>,
        Option<HtmlFragment>,
        Option<MarktwinText>,
        Option<HtmlFragment>,
        Option<ForumPostRevisionComment>,
      )>,
      forum_post_id: ForumPostId,
      forum_thread_id: ForumThreadId,
      key: Option<ForumThreadKey>,
      thread_ctime: Instant,
      title: ForumThreadTitle,
      is_locked: bool,
      is_pinned: bool,
      post_count: PgU32,
      forum_section_id: ForumSectionId,
    }
    // We need to cast to primitive types in the query below because SQLx 0.6.1 does not support custom types in composite types.
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
        WITH
          first_revision AS (
            SELECT time AS first_revision_time, author_id AS first_revision_author
            FROM forum_post_revisions
            WHERE forum_post_id = $1::FORUM_POST_ID
            ORDER BY time, forum_post_revision_id
            LIMIT 1 OFFSET 0
          ),
          revisions AS (
            SELECT forum_post_revision_id::UUID, time::TIMESTAMPTZ, author_id::UUID, body::TEXT, _html_body::TEXT, mod_body::TEXT, _html_mod_body::TEXT, comment::TEXT
--             SELECT forum_post_revision_id, time, author_id, body, _html_body, mod_body, _html_mod_body, comment
            FROM forum_post_revisions
            WHERE forum_post_id = $1::FORUM_POST_ID
            ORDER BY time, forum_post_revision_id
            LIMIT $2::U32 OFFSET $3::U32
          ),
          revision_array AS (
            SELECT COALESCE(ARRAY_AGG(revisions.*), '{}') AS items
            FROM revisions
          ),
          revision_count AS (
            SELECT COUNT(*) AS count
            FROM forum_post_revisions
          ),
          post_meta AS (
            SELECT
              forum_post_id,
              forum_thread_id, key, forum_thread_meta.ctime AS thread_ctime, title, is_locked, is_pinned, post_count,
              forum_section_id
            FROM forum_posts
              INNER JOIN forum_thread_meta USING (forum_thread_id)
            WHERE forum_post_id = $1::FORUM_POST_ID
          )
        SELECT count, items as revisions, post_meta.*, first_revision.*
        FROM revision_count, revision_array, post_meta, first_revision
        ;
    ",
    )
      .bind(query.post.id)
      .bind(PgU32::from(query.limit))
      .bind(PgU32::from(query.offset))
      .fetch_one(&*self.database)
      .await.map_err(store::GetPostError::other)?;

    let first_time = row.first_revision_time;
    let first_author = row.first_revision_author;

    Ok(RawForumPost {
      id: row.forum_post_id,
      ctime: first_time,
      author: RawForumActor::UserForumActor(RawUserForumActor {
        role: None,
        user: first_author.into(),
      }),
      revisions: RawForumPostRevisionListing {
        count: row.count.into(),
        offset: query.offset,
        limit: query.limit,
        items: row
          .revisions
          .into_iter()
          .map(
            |(forum_post_revision_id, time, author_id, body_mkt, body_html, mod_mkt, mod_html, comment)| {
              RawForumPostRevision {
                id: forum_post_revision_id,
                time,
                author: RawForumActor::UserForumActor(RawUserForumActor {
                  role: None,
                  user: author_id.into(),
                }),
                content: match (body_mkt, body_html) {
                  (Some(marktwin), Some(html)) => Some(ForumPostRevisionContent { marktwin, html }),
                  (None, None) => None,
                  _ => todo!(),
                },
                moderation: match (mod_mkt, mod_html) {
                  (Some(marktwin), Some(html)) => Some(ForumPostRevisionContent { marktwin, html }),
                  (None, None) => None,
                  _ => todo!(),
                },
                comment,
              }
            },
          )
          .collect(),
      },
      thread: RawForumThreadMeta {
        id: row.forum_thread_id,
        key: row.key,
        title: row.title,
        section: row.forum_section_id.into(),
        ctime: row.thread_ctime,
        is_pinned: row.is_pinned,
        is_locked: row.is_locked,
        posts: ListingCount {
          count: row.post_count.into(),
        },
      },
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::CreatePostRevision>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    command: store::CreatePostRevision,
  ) -> Result<RawCreateForumPostRevisionResult, store::CreatePostRevisionError> {
    let now = self.clock.clock().now();
    let revision_id = ForumPostRevisionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let (body_mkt, body_html) = match command.body.as_ref() {
      None => (None, None),
      Some(content) => (Some(&content.marktwin), Some(&content.html)),
    };
    let (mod_mkt, mod_html) = match command.mod_body.as_ref() {
      None => (None, None),
      Some(content) => (Some(&content.marktwin), Some(&content.html)),
    };
    let user_actor_id = match &command.actor {
      RawForumActor::UserForumActor(u) => u.user.id,
      _ => todo!(),
    };
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      forum_post_id: ForumPostId,
      forum_thread_id: ForumThreadId,
      forum_section_id: ForumSectionId,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH new_revision AS (
        INSERT INTO forum_post_revisions(
          forum_post_revision_id, time, body, _html_body, mod_body, _html_mod_body, forum_post_id, author_id, comment
        )
        VALUES (
          $2::FORUM_POST_REVISION_ID, $1::INSTANT, $3::TEXT, $4::TEXT, $5::TEXT, $6::TEXT, $7::FORUM_POST_ID, $8::USER_ID, $9::FORUM_POST_REVISION_COMMENT
        )
        RETURNING forum_post_id
      )
      SELECT forum_post_id, forum_posts.forum_thread_id, forum_section_id
      FROM new_revision
        INNER JOIN forum_posts USING (forum_post_id)
        INNER JOIN forum_thread_meta USING (forum_thread_id);
      ",
    )
      .bind(now)
      .bind(revision_id)
      .bind(body_mkt)
      .bind(body_html)
      .bind(mod_mkt)
      .bind(mod_html)
      .bind(command.post.id)
      .bind(user_actor_id)
      .bind(command.comment.as_ref())
      .fetch_one(&*self.database)
      .await.map_err(store::CreatePostRevisionError::other)?;
    Ok(RawCreateForumPostRevisionResult {
      revision: RawForumPostRevision {
        id: revision_id,
        time: now,
        author: RawForumActor::UserForumActor(RawUserForumActor {
          role: None,
          user: user_actor_id.into(),
        }),
        content: command.body.clone(),
        moderation: command.mod_body.clone(),
        comment: command.comment.clone(),
      },
      post: row.forum_post_id.into(),
      thread: row.forum_thread_id.into(),
      section: row.forum_section_id.into(),
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetRoleGrants>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetRoleGrants) -> Result<Vec<RawForumRoleGrant>, store::GetRoleGrantsError> {
    let section_id: ForumSectionId = query.section.id;
    let user_id: Option<UserId> = query.user.map(|u| u.id);

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      start_time: Instant,
      granted_by: UserId,
    }
    // language=PostgreSQL
    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT user_id, start_time, granted_by
      FROM forum_role_grants
      WHERE forum_section_id = $1::forum_section_id AND user_id = COALESCE($2::user_id, user_id);",
    )
    .bind(section_id)
    .bind(user_id)
    .fetch_all(&*self.database)
    .await
    .map_err(store::GetRoleGrantsError::other)?;
    Ok(
      rows
        .iter()
        .map(|row| RawForumRoleGrant {
          role: ForumRole::Moderator,
          user: row.user_id.into(),
          start_time: row.start_time,
          granted_by: row.granted_by.into(),
        })
        .collect(),
    )
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::UpsertSystemSection>
  for PgForumStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, command: store::UpsertSystemSection) -> Result<ForumSection, store::UpsertSystemSectionError> {
    let now = self.clock.clock().now();
    let mut tx = self
      .database
      .begin()
      .await
      .map_err(store::UpsertSystemSectionError::other)?;
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      forum_section_id: ForumSectionId,
      key: ForumSectionKey,
      ctime: Instant,
      display_name: ForumSectionDisplayName,
      locale: Option<LocaleId>,
    }
    // language=PostgreSQL
    let old_row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
        WITH section AS (
          SELECT forum_section_id, key, ctime, display_name, locale
          FROM forum_sections
          WHERE key = $1::FORUM_SECTION_KEY
        )
        SELECT forum_section_id, key, ctime, display_name, locale
        FROM section;
    ",
    )
    .bind(&command.key)
    .fetch_optional(&mut *tx)
    .await
    .map_err(store::UpsertSystemSectionError::other)?;
    let section = match old_row {
      None => {
        let forum_section_id = ForumSectionId::from_uuid(self.uuid_generator.uuid_generator().next());
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          ctime: Instant,
        }
        let row: Row = sqlx::query_as::<_, Row>(
          r"
            INSERT INTO forum_sections(
              forum_section_id, key, ctime,
              display_name, display_name_mtime,
              locale, locale_mtime
            )
            VALUES (
              $1::FORUM_SECTION_ID, $2::FORUM_SECTION_KEY, $3::INSTANT,
              $4::FORUM_SECTION_DISPLAY_NAME, $3::INSTANT,
              $5::LOCALE_ID, $3::INSTANT
            )
            RETURNING ctime;
          ",
        )
        .bind(forum_section_id)
        .bind(&command.key)
        .bind(now)
        .bind(&command.display_name)
        .bind(command.locale)
        .fetch_one(&mut *tx)
        .await
        .map_err(store::UpsertSystemSectionError::other)?;
        ForumSection {
          id: forum_section_id,
          key: Some(command.key.clone()),
          display_name: command.display_name.clone(),
          ctime: row.ctime,
          locale: command.locale,
          threads: Listing {
            offset: 0,
            limit: THREADS_PER_PAGE,
            count: 0,
            items: vec![],
          },
          role_grants: vec![],
          this: ForumSectionSelf { roles: vec![] },
        }
      }
      Some(old_row) => {
        let display_name_patch: Option<&ForumSectionDisplayName> = if old_row.display_name != command.display_name {
          Some(&command.display_name)
        } else {
          None
        };
        let locale_id_patch: Option<Option<LocaleId>> = if old_row.locale != command.locale {
          Some(command.locale)
        } else {
          None
        };
        match (display_name_patch, locale_id_patch) {
          (None, None) => {
            // No-op
          }
          _ => todo!(),
        }
        let threads = get_sections_tx().await;
        let role_grants = get_role_grants_tx().await;
        let this = get_section_self_tx().await;
        ForumSection {
          id: old_row.forum_section_id,
          key: Some(old_row.key),
          display_name: old_row.display_name,
          ctime: old_row.ctime,
          locale: old_row.locale,
          threads,
          role_grants,
          this,
        }
      }
    };
    tx.commit().await.map_err(store::UpsertSystemSectionError::other)?;
    Ok(section)
  }
}

async fn get_sections_tx() -> ForumThreadListing {
  ForumThreadListing {
    offset: 0,
    limit: 20,
    count: 0,
    items: vec![],
  }
}

async fn get_role_grants_tx() -> Vec<ForumRoleGrant> {
  vec![]
}

async fn get_section_self_tx() -> ForumSectionSelf {
  ForumSectionSelf { roles: vec![] }
}

#[cfg(test)]
mod test {
  use super::PgForumStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::forum::ForumStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use eternaltwin_user_store::pg::PgUserStore;
  use opentelemetry::trace::noop::NoopTracerProvider;
  use opentelemetry::trace::TracerProvider;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn ForumStore>, Arc<dyn UserStore>> {
    let config = eternaltwin_config::Config::for_test();
    let tracer_provider = NoopTracerProvider::new();
    let tracer = tracer_provider.tracer("forum_store_test");

    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let forum_store: Arc<dyn ForumStore> = Arc::new(PgForumStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      Arc::clone(&uuid_generator),
    ));
    let database_secret = SecretString::new("dev_secret".to_string());
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      database_secret,
      tracer,
      uuid_generator,
    ));

    TestApi {
      clock,
      forum_store,
      user_store,
    }
  }

  test_forum_store!(
    #[serial]
    || make_test_api().await
  );
}
