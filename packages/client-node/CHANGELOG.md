# 0.16.3 (2025-02-03)

- **[Feature]** Expose `authorizationHeader`. This function formats authorization
  information as an `Authorization` header value. This is suitable for Opentelemetry
  authentication for example.

# 0.16.1 (2024-11-18)

- **[Feature]** Add error code list
