# 0.16.1 (2024-11-18)

- **[Feature]** Provide more context on token claim error.

# 0.1.0 (2020-05-25)

- **[Feature]** First release
