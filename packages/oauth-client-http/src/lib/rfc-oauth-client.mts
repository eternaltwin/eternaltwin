import {$Url, Url} from "@eternaltwin/core/core/url";
import {$OauthAccessToken, OauthAccessToken} from "@eternaltwin/core/oauth/oauth-access-token";
import {OauthGrantType} from "@eternaltwin/core/oauth/oauth-grant-type";
import {
  $RfcOauthAccessTokenRequest,
  RfcOauthAccessTokenRequest
} from "@eternaltwin/core/oauth/rfc-oauth-access-token-request";
import authHeader from "auth-header";
import {Buffer} from "buffer";
import {readOrThrow} from "kryo";
import {JsonValue} from "kryo-json/json-value";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import superagent from "superagent";

export type GetAccessTokenErrorCode =
  "BAD_REQUEST"
  | "FORBIDDEN"
  | "NOT_FOUND"
  | "RESPONSE_FORMAT"
  | "SERVER_ERROR"
  | "UNAUTHORIZED";

interface GetAccessTokenErrorOptions {
  code?: GetAccessTokenErrorCode;
  uri: URL;
  status?: number;
  bodyText?: string;
  eternaltwin?: EternaltwinErrorInfo;
  cause?: unknown;
}

export class GetAccessTokenError extends Error {
  public readonly code?: GetAccessTokenErrorCode;
  public readonly uri: URL;
  public readonly status?: number;
  public readonly bodyText?: unknown;
  public readonly eternaltwin?: EternaltwinErrorInfo;

  private constructor(message: string, options: GetAccessTokenErrorOptions) {
    const codePrefix = options.code !== undefined ? `[${options.code}] ` : "";
    let eternaltwinSuffix: string = "";
    if (options.eternaltwin !== undefined) {
      let extra = "";
      if (options.eternaltwin.extra !== undefined) {
        // we got `extra` from JSON parsing, so stringification should not throw
        extra = `extra=${JSON.stringify(options.eternaltwin.extra)}`;
      }
      eternaltwinSuffix = `, remote: ${options.eternaltwin.message}${extra}`;
    }
    const statusSuffix = options.status !== undefined ? `, status=${options.status}` : "";
    message = `${codePrefix}${message}, uri=${options.uri}${statusSuffix}${eternaltwinSuffix}`;
    super(message, {cause: options.cause});
    this.code = options.code;
    this.uri = options.uri;
    this.status = options.status;
    this.bodyText = options.bodyText;
    this.eternaltwin = options.eternaltwin;
  }

  public static fromResponseError(err: superagent.ResponseError, uri: URL): GetAccessTokenError {
    let message: string = "unexpected HTTP response error, details in `cause`";
    const options: GetAccessTokenErrorOptions = {
      uri,
      cause: err,
    };
    const status: number | undefined = options.status;
    options.status = status;
    const response: superagent.Response | undefined = err.response;
    if (typeof response === "object" && response !== null) {
      // `response.text` is typed as `string`, but the typing is suspicious so we avoid trusting it
      const text: unknown = response.text;
      if (typeof text === "string") {
        options.bodyText = text;
        options.eternaltwin = tryParseEternaltwinErrorInfo(text);
      }
    }
    switch (status) {
      case 401:
        options.code = "UNAUTHORIZED";
        message = "missing or invalid client authentication";
        break;
      case 403:
        options.code = "FORBIDDEN";
        message = "permission denied by authorization server";
        break;
      case 404:
        options.code = "NOT_FOUND";
        message = "token claim endpoint not found";
        break;
      case 422:
        options.code = "BAD_REQUEST";
        message = "invalid request";
        break;
      case 500:
        options.code = "SERVER_ERROR";
        message = "authorization server error";
        break;
    }
    return Object.freeze(new GetAccessTokenError(message, options));
  }

  public static bodyParseError(err: superagent.ResponseError, uri: URL): GetAccessTokenError {
    return Object.freeze(new GetAccessTokenError("unexpected response format, details in `cause`", {
      code: "RESPONSE_FORMAT",
      uri,
      cause: err,
    }));
  }
}

export interface RfcOauthClientOptions {
  authorizationEndpoint: Url;
  tokenEndpoint: Url;
  callbackEndpoint: Url;
  clientId: string;
  clientSecret: string;
}

export class RfcOauthClient {
  readonly #authorizationEndpoint: Url;
  readonly #tokenEndpoint: Url;
  readonly #callbackEndpoint: Url;
  readonly #clientId: string;
  readonly #clientSecret: string;

  public constructor(options: Readonly<RfcOauthClientOptions>) {
    this.#authorizationEndpoint = Object.freeze(new Url(options.authorizationEndpoint.toString()));
    this.#tokenEndpoint = Object.freeze(new Url(options.tokenEndpoint.toString()));
    this.#callbackEndpoint = Object.freeze(new Url(options.callbackEndpoint.toString()));
    this.#clientId = options.clientId;
    this.#clientSecret = options.clientSecret;
  }

  public getAuthorizationUri(scope: string, state: string): Url {
    const url: Url = new Url(this.#authorizationEndpoint.toString());
    url.searchParams.set("access_type", "offline");
    url.searchParams.set("response_type", "code");
    url.searchParams.set("redirect_uri", this.#callbackEndpoint.toString());
    url.searchParams.set("client_id", this.#clientId);
    url.searchParams.set("scope", scope);
    url.searchParams.set("state", state);
    return url;
  }

  // todo: rename to `claimAccessToken` for consistency with server impl
  public async getAccessToken(code: string): Promise<OauthAccessToken> {
    const endpoint = this.#tokenEndpoint;
    const accessTokenReq: RfcOauthAccessTokenRequest = {
      clientId: this.#clientId,
      clientSecret: this.#clientSecret,
      redirectUri: $Url.clone(this.#callbackEndpoint),
      code,
      grantType: OauthGrantType.AuthorizationCode,
    };
    const rawReq = $RfcOauthAccessTokenRequest.write(JSON_VALUE_WRITER, accessTokenReq);
    let rawRes: superagent.Response;
    try {
      rawRes = await superagent.post(endpoint.toString())
        .set("Authorization", this.getAuthorizationHeader())
        .type("application/x-www-form-urlencoded")
        .send(rawReq);
    } catch (err: unknown) {
      // hopefully this cast is ok
      throw GetAccessTokenError.fromResponseError(err as superagent.ResponseError, endpoint);
    }
    // Some OAuth providers (e.g. Twinoid) fail to set the response content-type to `application/json` despite senging
    // a JSON response. This prevents superagent from parsing the body automatically. The code below tries to parse
    // the response as JSON even if the content-type is `text/html`.
    let parsedBody: JsonValue | undefined;
    let bodyParseError: Error | undefined;
    if (rawRes.type === "application/json") {
      parsedBody = rawRes.body;
    } else if (rawRes.type === "text/html") {
      try {
        parsedBody = JSON.parse(rawRes.text);
      } catch (err: unknown) {
        if (err instanceof Error) {
          bodyParseError = err;
        } else {
          throw err;
        }
      }
    }
    if (parsedBody === undefined) {
      if (bodyParseError === undefined) {
        bodyParseError = new Error("failed to parse body as JSON");
      }
      throw GetAccessTokenError.bodyParseError(bodyParseError, endpoint);
    }
    let accessToken: OauthAccessToken;
    try {
      accessToken = readOrThrow($OauthAccessToken, JSON_VALUE_READER, parsedBody);
    } catch (err) {
      if (err instanceof Error) {
        throw GetAccessTokenError.bodyParseError(err, endpoint);
      } else {
        throw err;
      }
    }
    return accessToken;
  }

  public getAuthorizationHeader(): string {
    const credentials: string = `${this.#clientId}:${this.#clientSecret}`;
    const token: string = Buffer.from(credentials).toString("base64");
    return authHeader.format({scheme: "Basic", token});
  }
}

// todo: move this outside of this package
/**
 * Extra error details returned by the Eternaltwin server
 */
export interface EternaltwinErrorInfo {
  code: string;
  message: string;
  extra?: unknown;
}

function tryParseEternaltwinErrorInfo(input: string): EternaltwinErrorInfo | undefined {
  try {
    return parseEternaltwinErrorInfo(input);
  } catch {
    return undefined;
  }
}

function parseEternaltwinErrorInfo(input: string): EternaltwinErrorInfo {
  const json: unknown = JSON.parse(input);
  if (json === null || typeof json !== "object") {
    throw new Error("expected root value to be an object");
  }
  const code: unknown = Reflect.get(json, "code");
  if (typeof code !== "string") {
    throw new Error("expected `code` to be a string");
  }
  const message: unknown = Reflect.get(json, "message");
  if (typeof message !== "string") {
    throw new Error("expected `message` to be a string");
  }
  const info: EternaltwinErrorInfo = {
    code,
    message,
  };
  if (Reflect.has(json, "extra")) {
    info.extra = Reflect.get(json, "extra");
  }
  return info;
}
