import { Component, Input } from "@angular/core";
import { AuthType } from "@eternaltwin/core/auth/auth-type";
import { ObjectType } from "@eternaltwin/core/core/object-type";
import { DinoparcLink } from "@eternaltwin/core/link/dinoparc-link";
import { HammerfestLink } from "@eternaltwin/core/link/hammerfest-link";
import { TwinoidLink } from "@eternaltwin/core/link/twinoid-link";
import { UserId } from "@eternaltwin/core/user/user-id";
import { Observable, of as rxOf } from "rxjs";
import { catchError as rxCatchError, map as rxMap, startWith as rxStartWith } from "rxjs/operators";

import { AuthService } from "../../modules/auth/auth.service.mjs";

@Component({
  selector: "etwin-linked-user",
  templateUrl: "./linked-user.component.html",
  styleUrls: ["./linked-user.component.scss"],
})
export class LinkedUserComponent {
  public readonly ObjectType = ObjectType;

  @Input()
  public userId!: UserId;

  @Input()
  public link!: DinoparcLink | HammerfestLink | TwinoidLink;

  public isAdministrator$: Observable<boolean>;

  constructor(auth: AuthService) {
    this.isAdministrator$ = auth.auth().pipe(
      rxMap((acx) => acx.type === AuthType.User && acx.isAdministrator),
      rxStartWith(false),
      rxCatchError(() => rxOf(false)),
    );
  }
}
