import { Component, Input } from "@angular/core";
import { ObjectType } from "@eternaltwin/core/core/object-type";
import { ForumActor } from "@eternaltwin/core/forum/forum-actor";
import { UserForumActor } from "@eternaltwin/core/forum/user-forum-actor";

@Component({
  selector: "etwin-forum-actor",
  templateUrl: "./forum-actor.component.html",
  styleUrls: [],
})
export class ForumActorComponent {
  @Input()
  public actor!: ForumActor;

  public ObjectType: typeof ObjectType = ObjectType;

  constructor() {
  }

  public asUserForumActor(author: ForumActor): UserForumActor {
    return author as UserForumActor;
  }
}
