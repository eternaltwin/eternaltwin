import { Component, Input } from "@angular/core";
import { ShortForumPost } from "@eternaltwin/core/forum/short-forum-post";

@Component({
  selector: "etwin-forum-post",
  templateUrl: "./short-forum-post.component.html",
  styleUrls: ["./short-forum-post.component.scss"],
})
export class ShortForumPostComponent {
  @Input()
  public post!: ShortForumPost;

  @Input()
  public canEdit: boolean = false;

  constructor() {
  }
}
