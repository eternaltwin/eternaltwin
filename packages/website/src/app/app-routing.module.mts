import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {HomeView} from "./home/home.component.mjs";

export const routes: Routes = [
  {path: "", component: HomeView, pathMatch: "full"},
  {path: "admin", loadChildren: () => import("./admin/admin.module.mjs").then(({AdminModule}) => AdminModule)},
  {path: "archive", loadChildren: () => import("./archive/archive.module.mjs").then(({ArchiveModule}) => ArchiveModule)},
  {path: "docs", loadChildren: () => import("./docs/docs.module.mjs").then(({DocsModule}) => DocsModule)},
  {path: "donate", loadChildren: () => import("./donate/donate.module.mjs").then(({DonateModule}) => DonateModule)},
  {path: "login", loadChildren: () => import("./auth/login/login.module.mjs").then(({LoginModule}) => LoginModule)},
  {
    path: "register",
    loadChildren: () => import("./auth/register/register.module.mjs").then(({RegisterModule}) => RegisterModule),
  },
  {path: "legal", loadChildren: () => import("./legal/legal.module.mjs").then(({LegalModule}) => LegalModule)},
  {path: "games", loadChildren: () => import("./games/games.module.mjs").then(({GamesModule}) => GamesModule)},
  {path: "users", loadChildren: () => import("./users/users.module.mjs").then(({UsersModule}) => UsersModule)},
  {path: "forum", loadChildren: () => import("./forum/forum.module.mjs").then(({ForumModule}) => ForumModule)},
  {
    path: "settings",
    loadChildren: () => import("./settings/settings.module.mjs").then(({SettingsModule}) => SettingsModule),
  },
  {path: "support", loadChildren: () => import("./support/support.module.mjs").then(({SupportModule}) => SupportModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {initialNavigation: "enabledBlocking", onSameUrlNavigation: "reload"}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
