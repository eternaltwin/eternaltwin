import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {AdminViewComponent} from "./admin-view.component.mjs";
import {EnableUserComponent} from "./enable-user.component.mjs";
import {JobsViewComponent} from "./jobs-view.component.mjs";
import {OutboundEmailRequestsViewComponent} from "./outbound-email-requests-view.component.mjs";
import {OutboundEmailsViewComponent} from "./outbound-emails-view.component.mjs";
import {SendMarktwinEmailViewComponent} from "./send-marktwin-email-view.component.mjs";

const routes: Routes = [
  {
    path: "",
    component: AdminViewComponent,
  },
  {
    path: "enable-user",
    component: EnableUserComponent,
  },
  {
    path: "get-outbound-emails",
    component: OutboundEmailsViewComponent,
  },
  {
    path: "get-outbound-email-requests",
    component: OutboundEmailRequestsViewComponent,
  },
  {
    path: "jobs",
    component: JobsViewComponent,
  },
  {
    path: "send-marktwin-email",
    component: SendMarktwinEmailViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AdminRoutingModule {
}
