import {Component} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {OutboundEmail} from "@eternaltwin/core/mailer/outbound-email";
import {$SendMarktwinEmail, SendMarktwinEmail} from "@eternaltwin/core/mailer/send-marktwin-email";
import {UuidHex} from "kryo/uuid-hex";
import {NEVER, Observable} from "rxjs";

import {MailerService} from "../../modules/mailer/mailer.service.mjs";

interface FormModel {
  recipient: FormControl<string>,
  subject: FormControl<string>,
  body: FormControl<string>,
}

@Component({
  selector: "et-admin-send-marktwin-email-view",
  template:
`
  <etwin-main-layout>
    <h1>Send Marktwin Email</h1>
    <form
      method="POST"
      enctype="application/x-www-form-urlencoded"
      action="#"
      [formGroup]="form"
      (ngSubmit)="onSubmit($event)"
    >
      <label>
        Recipient<br />
        <input class="field" type="text" [formControl]="recipient" value=""/>
      </label>
      <label>
        Subject<br />
        <input class="field" type="text" [formControl]="subject" value=""/>
      </label>
      <textarea [formControl]="body"></textarea>
      <input type="submit" class="btn primary" name="apply"
             value="Send"/>
    </form>


    @if (result$ | async; as result) {
      <pre>{{result | json}}</pre>
    }
    @if (error$ | async; as error) {
      <pre>{{error | json}}</pre>
    }

  </etwin-main-layout>
`,
  styleUrls: [],
})
export class SendMarktwinEmailViewComponent {
  public readonly recipient: FormModel["recipient"];
  public readonly subject: FormModel["subject"];
  public readonly body: FormModel["body"];
  public readonly form: FormGroup<FormModel>;
  public error$: Observable<string>;
  public result$: Observable<OutboundEmail>;
  #mailerService: MailerService;

  constructor(mailerService: MailerService) {
    this.recipient = new FormControl(
      "",
      {
        nonNullable: true,
        validators: [Validators.required, Validators.min(1)]
      },
    );
    this.subject = new FormControl(
      "",
      {
        nonNullable: true,
        validators: [Validators.required, Validators.min(1)]
      },
    );
    this.body = new FormControl(
      "",
      {
        nonNullable: true,
        validators: [Validators.required, Validators.min(1)]
      },
    );
    this.form = new FormGroup({recipient: this.recipient, subject: this.subject, body: this.body});
    this.result$ = NEVER;
    this.error$ = NEVER;
    this.#mailerService = mailerService;
  }

  onSubmit(ev: SubmitEvent): void {
    const MILLISECONDS_IN_DAY = 86400 * 1000;
    ev.preventDefault();
    this.result$ = NEVER;
    this.error$ = NEVER;
    const model = this.form.getRawValue();
    const rawCmd: SendMarktwinEmail = {
      deadline: new Date(Date.now() + MILLISECONDS_IN_DAY),
      idempotencyKey: genUuidv4(),
      recipient: model.recipient,
      subject: model.subject,
      body: model.body,
    };
    const cmd = $SendMarktwinEmail.test(null, rawCmd);
    if (cmd.ok) {
      this.result$ = this.#mailerService.sendMarktwinEmail(cmd.value);
    } else {
      console.error("model read error");
      // todo: set `this.error$`
    }
  }
}

/**
 * UUIDv4 following the spec.
 *
 * See <https://datatracker.ietf.org/doc/html/rfc9562#name-uuid-version-4>
 */
function genUuidv4(): UuidHex {
  const alphabet = "0123456789abcdef";
  let res = "";
  for (let i: number = 0; i < 32; i++) {
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      res += "-";
    }
    let halfByte = Math.floor(16 * Math.random());
    if (i === 12) {
      halfByte = 0b0100;
    } else if (i === 16) {
      halfByte = 0b1000 | (halfByte & 0b0011);
    }
    res += alphabet[halfByte];
  }
  return res;
}
