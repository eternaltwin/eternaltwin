import {ApplicationConfig} from "@angular/core";
import {provideClientHydration} from "@angular/platform-browser";
import {provideRouter} from "@angular/router";

import {routes} from "./app-routing.module.mjs";

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), provideClientHydration()]
};
