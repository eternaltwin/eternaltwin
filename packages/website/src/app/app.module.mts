import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component.mjs";
import { AppRoutingModule } from "./app-routing.module.mjs";
import { HomeView } from "./home/home.component.mjs";
import { SharedModule } from "./shared/shared.module.mjs";
import { AsideStyleComponent } from "./shared/socials/aside-style/aside-style.component.mjs";
import { FlagStyleComponent } from "./shared/socials/flag-style/flag-style.component.mjs";
import { PlayerTagComponent } from "./shared/socials/player-tag/player-tag.component.mjs";
import { QuoteStyleComponent } from "./shared/socials/quote-style/quote-style.component.mjs";
import { RoleplayStyleComponent } from "./shared/socials/roleplay-style/roleplay-style.component.mjs";
import { SpoilerStyleComponent } from "./shared/socials/spoiler-style/spoiler-style.component.mjs";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    SharedModule,
    QuoteStyleComponent,
    AsideStyleComponent,
    RoleplayStyleComponent,
    SpoilerStyleComponent,
    FlagStyleComponent,
    PlayerTagComponent
  ],
  declarations: [AppComponent, HomeView],
  exports: [AppComponent],
})
export class AppModule {
  // Force the following services to be instantiated at the root
  constructor() {
    // if (config === null || config === undefined) {
    //   console.warn("MissingConfig");
    // }
    // console.log(config);
  }
}
