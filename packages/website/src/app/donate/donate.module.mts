import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module.mjs";
import { DonateComponent } from "./donate.component.mjs";
import { DonateRoutingModule } from "./donate-routing.module.mjs";

@NgModule({
  declarations: [DonateComponent],
  imports: [
    DonateRoutingModule,
    SharedModule,
  ],
})
export class DonateModule {
}
