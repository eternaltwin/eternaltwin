import {Component, Input, OnInit} from "@angular/core";
import {HammerfestItemId} from "@eternaltwin/core/hammerfest/hammerfest-item-id";

import {HammerfestService} from "../../../modules/hammerfest/hammerfest.service.mjs";

@Component({
  selector: "etwin-hammerfest-item",
  templateUrl: "./hammerfest-item.component.html",
  styleUrls: [],
})
export class HammerfestItemComponent implements OnInit {
  @Input("id")
    itemId!: HammerfestItemId;
  @Input("count")
    count!: number;
  @Input("unlocked")
    unlocked?: boolean;

  isLocked: boolean;
  isUnknown: boolean;

  readonly #hammerfestService: HammerfestService;

  constructor(hammerfestService: HammerfestService) {
    this.#hammerfestService = hammerfestService;
    this.isUnknown = true;
    this.isLocked = true;
  }

  ngOnInit(): void {
    this.isLocked = this.unlocked === undefined ? this.count < this.#hammerfestService.getItemUnlock(this.itemId) : !this.unlocked;
  }
}
