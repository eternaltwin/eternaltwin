import {XhrFactory} from "@angular/common";
import {provideHttpClient, withFetch} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {provideClientHydration} from "@angular/platform-browser";
import {provideServerRendering, ServerModule} from "@angular/platform-server";
import {provideRouter} from "@angular/router";
import xhr2 from "xhr2";

import {ServerAuthModule} from "../modules/auth/auth.module.server.mjs";
import {ServerConfigModule} from "../modules/config/config.module.server.mjs";
import {DinoparcModule} from "../modules/dinoparc/dinoparc.module.mjs";
import {ForumModule} from "../modules/forum/forum.module.mjs";
import {HammerfestModule} from "../modules/hammerfest/hammerfest.module.mjs";
import {ServerRestModule} from "../modules/rest/rest.module.server.mjs";
import {TwinoidModule} from "../modules/twinoid/twinoid.module.mjs";
import {UserModule} from "../modules/user/user.module.mjs";
import {BACKEND_URI} from "../server/tokens.mjs";
import {AppComponent} from "./app.component.mjs";
import {AppModule} from "./app.module.mjs";
import {routes} from "./app-routing.module.mjs";

/**
 * Workaround for https://github.com/angular/angular/issues/15730
 *
 * (allow sending cookies)
 */
class ServerXhr implements XhrFactory {
  build(): XMLHttpRequest {
    const xhr = new xhr2.XMLHttpRequest();
    xhr._restrictedHeaders.cookie = false;
    return xhr;
  }
}

@NgModule({
  imports: [
    AppModule,
    ServerAuthModule,
    ServerConfigModule,
    DinoparcModule,
    ForumModule,
    HammerfestModule,
    ServerModule,
    ServerRestModule,
    TwinoidModule,
    UserModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: XhrFactory, useClass: ServerXhr, deps: []},
    {provide: BACKEND_URI, useValue: "http://localhost:50321/"},
    provideHttpClient(withFetch()),
    provideServerRendering(),
    provideRouter(routes),
    provideClientHydration()
  ],
})
export class AppServerModule {
}
