import { Component, OnInit } from "@angular/core";

@Component({
  selector: "etwin-games",
  templateUrl: "./games.component.html",
  styleUrls: [],
})

export class GamesComponent {
  games = [
    {
      id: "eternalfest",
      cover: "./assets/box_eternalfest.png",
      color: "#2a2484",
      title: "Eternalfest",
      titleI18n: "Title of the Eternalfest game description@@games.eternalfestTitle",
      description: "Eternalfest is a reimplementation of Hammerfest. It has both the official levels, and new content!",
      descriptionI18n: "Description of the Eternalfest game@@games.eternalfestDescription",
      link: "https://eternalfest.net/",
      buttonText: "Play now!",
      buttonI18n: "Button to access the Eternalfest game@@games.eternalfestButton",
    },
    {
      id: "neoparc",
      cover: "./assets/box_neoparc.png",
      color: "#d6853d",
      title: "Neoparc",
      titleI18n: "Title of the Neoparc game description@@games.neoparcTitle",
      description: "Neoparc is a fresh implementation of Dinoparc. Raise your Dinoz in this improved version!",
      descriptionI18n: "Description of the Neoparc game@@games.neoparcDescription",
      link: "https://neoparc.eternaltwin.org/",
      buttonText: "Play now!",
      buttonI18n: "Button to access the Neoparc game@@games.neoparcButton",
    },
    {
      id: "emush",
      cover: "./assets/box_mush.png",
      color: "#2e2485",
      title: "eMush",
      titleI18n: "Title of the eMush game description@@games.emushTitle",
      description: "Co-operative multiplayer space survival and escape game",
      descriptionI18n: "Description of the eMush game@@games.emushDescription",
      status: "This game is still in development.",
      statusI18n: "Current status of the eMush game@@games.emushStatus",
      link: "https://emush.eternaltwin.org/",
      buttonText: "Test the alpha!",
      buttonI18n: "Button to access the eMush game@@games.emushButton",
    },
    {
      id: "myhordes",
      cover: "./assets/box_myhordes.png",
      color: "#650021",
      title: "MyHordes",
      titleI18n: "Title of the MyHordes game description@@games.myhordesTitle",
      description: "MyHordes is an open implementation of the browser game 'Die2Nite' and its other versions by MotionTwin and developed by the community.",
      descriptionI18n: "Description of the MyHordes game@@games.myhordesDescription",
      link: "https://myhordes.eu/",
      buttonText: "Play now!",
      buttonI18n: "Button to access the MyHordes game@@games.myhordesButton",
    },
    {
      id: "studioquiz",
      cover: "./assets/box_studioquiz.png",
      color: "#5e1da4",
      title: "Directquiz",
      titleI18n: "Title of the Directquiz game description@@games.directquizTitle",
      description: "Jeu de quiz : votre culture mise à rude épreuve !",
      descriptionI18n: "Description of the Directquiz game@@games.directquizDescription",
      status: "This game is still in development.",
      statusI18n: "Current status of the Directquiz game@@games.directquizStatus",
      link: "https://directquiz.org/",
      buttonText: "Play now!",
      buttonI18n: "Button to access the eMush game@@games.directquizButton",
    },
    {
      id: "labrute",
      cover: "./assets/box_labrute.png",
      color: "#a97747",
      title: "My Brute",
      titleI18n: "Title of the Labrute game description@@games.labruteTitle",
      description: "A free combat game: Create the most brutal fighter ever!",
      descriptionI18n: "Description of the Labrute game@@games.labruteDescription",
      status: "This game is still in development.",
      statusI18n: "Current status of the Labrute game@@games.labruteStatus",
      link: "https://brute.eternaltwin.org/",
      buttonText: "Play now!",
      buttonI18n: "Button to access the Labrute game@@games.labruteButton",
    },
    {
      id: "epopotamo",
      cover: "./assets/box_cover_ft_wip.svg",
      color: "#2e2485",
      title: "ePopotamo",
      titleI18n: "Title of the ePopotamo game description@@games.epopotamoTitle",
      status: "This game is still in development and only available in french.",
      statusI18n: "Current status of the ePopotamo game@@games.epopotamoStatus",
      link: "https://epopotamo.eternaltwin.org/",
      buttonText: "Test the alpha!",
      buttonI18n: "Button to access the ePopotamo game@@games.epopotamoButton",
    },
    {
      id: "dinorpg",
      cover: "./assets/box_dinorpg.png",
      color: "#578722",
      title: "DinoRPG",
      titleI18n: "Title of the DinoRPG game description@@games.dinorpgTitle",
      description: "Train your dinoz and set off on an extraordinary adventure...",
      descriptionI18n: "Description of the DinoRPG game@@games.dinorpgDescription",
      status: "This game is still in development.",
      statusI18n: "Current status of the DinoRPG game@@games.dinorpgStatus",
      link: "https://dinorpg.eternaltwin.org/",
      buttonText: "Test the beta!",
      buttonI18n: "Button to access the DinoRPG game@@games.dinorpgButton",
    },
    {
      id: "kingdom",
      cover: "./assets/box_kingdom.png",
      color: "#914718",
      title: "EternalKingdom",
      titleI18n: "Title of the Kingdom game description@@games.kingdom",
      description: "Expand your domain and become emperor!",
      descriptionI18n: "Description of the Kingdom game@@games.dinorpgDescription",
      status: "This game is still in development.",
      statusI18n: "Current status of the Kingdom game@@games.kingdomStatus",
      link: "https://kingdom.eternaltwin.org/",
      buttonText: "Play the alpha!",
      buttonI18n: "Button to access the Kingdom game@@games.kingdomButton",
    },
    // Ajoute ici les autres jeux de la même manière
  ];


  constructor() {}

  ngOnInit(): void {
    this.games = this.shuffleArray(this.games);
  }

  shuffleArray(array: any[]): any[] {
    return array
      .map(value => ({ value, sort: Math.random() }))
      .sort((a, b) => a.sort - b.sort)
      .map(({ value }) => value);
  }
}
