import { Component, Input } from "@angular/core";
import { DinoparcServer } from "@eternaltwin/core/dinoparc/dinoparc-server";
import { VersionedDinoparcLink } from "@eternaltwin/core/link/versioned-dinoparc-link";
import { UserId } from "@eternaltwin/core/user/user-id";

@Component({
  selector: "etwin-linked-dinoparc-settings",
  templateUrl: "./linked-dinoparc-settings.component.html",
  styleUrls: [],
})
export class LinkedDinoparcSettingsComponent {
  @Input()
  public link!: VersionedDinoparcLink;

  @Input()
  public server!: DinoparcServer;

  @Input()
  public userId!: UserId;

  constructor() {
  }
}
