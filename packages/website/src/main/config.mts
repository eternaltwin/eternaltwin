import { readFile } from "node:fs/promises";
import process from "node:process";
import { pathToFileURL } from "node:url";

import {$Url} from "@eternaltwin/core/core/url";
import {$ForumConfig, ForumConfig} from "@eternaltwin/core/forum/forum-config";
import * as furi from "furi";
import {CaseStyle, readOrThrow} from "kryo";
import {$Boolean} from "kryo/boolean";
import {$Uint16} from "kryo/integer";
import {RecordType} from "kryo/record";
import {$Ucs2String} from "kryo/ucs2-string";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";

import rawDefaultConfig from "./default-frontend-config.json" with { type: "json" };

const PROJECT_ROOT: URL = furi.join(import .meta.url, "../..");
const IS_PRODUCTION: boolean = process.env.NODE_ENV === "production";
const APP_DIR: URL = furi.join(PROJECT_ROOT as any, "app");

export interface FrontendConfig {
  backendSecret: string,
  backendPort: number,
  port: number,
  appDir: URL,
  url: URL,
  forum: ForumConfig,
  isProduction: boolean,
}

export const $FrontendConfig = new RecordType<FrontendConfig>({
  properties: {
    backendSecret: {type: $Ucs2String},
    backendPort: {type: $Uint16},
    port: {type: $Uint16},
    appDir: {type: $Url},
    url: {type: $Url},
    forum: {type: $ForumConfig},
    isProduction: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});

export async function frontendConfigFromProcess(): Promise<FrontendConfig> {
  return frontendConfigFromCliArgs(pathToFileURL(process.cwd()), process.argv, Reflect.get(process.env, "NODE_ENV") !== "production");
}

export async function frontendConfigFromCliArgs(cwd: URL, args: readonly string[], useDefault: boolean): Promise<FrontendConfig> {
  let backendSecret: string | undefined;
  let backendPort: number | undefined;
  let port: number | undefined;
  let appDir: URL | undefined;
  let url: URL | undefined;
  let forum: ForumConfig | undefined;
  let isProduction: boolean = IS_PRODUCTION;
  if (useDefault) {
    const defaultConfig: FrontendConfig = readOrThrow($FrontendConfig, JSON_VALUE_READER, rawDefaultConfig);
    defaultConfig.appDir = APP_DIR;
    defaultConfig.isProduction = IS_PRODUCTION;
    backendSecret = defaultConfig.backendSecret;
    backendPort = defaultConfig.backendPort;
    port = defaultConfig.port;
    appDir = defaultConfig.appDir;
    url = defaultConfig.url;
    forum = defaultConfig.forum;
    isProduction = defaultConfig.isProduction;
  }

  // Skip the first two arguments: node path and script path
  let index = 2;
  const argLen = args.length;
  while (index < argLen) {
    const arg = args[index];
    switch (arg) {
      case "--config": {
        if (index + 1 < argLen) {
          const configArg = args[index + 1];
          const config = readOrThrow($FrontendConfig, JSON_READER, configArg);
          backendSecret = config.backendSecret;
          backendPort = config.backendPort;
          port = config.port;
          appDir = config.appDir;
          url = config.url;
          forum = config.forum;
          isProduction = config.isProduction;
          index += 2;
        } else {
          throw new Error("missing `--config` value");
        }
        break;
      }
      case "--backend-secret": {
        if (index + 1 < argLen) {
          backendSecret = args[index + 1];
          index += 2;
        } else {
          throw new Error("missing `--backend-secret` value");
        }
        break;
      }
      case "--backend-secret-url": {
        if (index + 1 < argLen) {
          const secretRelativeUrl = args[index + 1];
          let secretUrl: URL;
          if (secretRelativeUrl.startsWith("../") || secretRelativeUrl.startsWith("./")) {
            secretUrl = furi.join(cwd, secretRelativeUrl);
          } else if (secretRelativeUrl.startsWith("file://")) {
            secretUrl = new URL(secretRelativeUrl);
          } else {
            throw new Error("invalid `--backed-secret-url` value, must start with `file://`, `./` or `../`");
          }
          backendSecret = await readFile(secretUrl, {encoding: "utf-8"});
          index += 2;
        } else {
          throw new Error("missing `--backend-secret` value");
        }
        break;
      }
      case "--backend-port": {
        if (index + 1 < argLen) {
          backendPort = readOrThrow($Uint16, SEARCH_PARAMS_VALUE_READER, args[index + 1]);
          index += 2;
        } else {
          throw new Error("missing `--backend-port` value");
        }
        break;
      }
      case "--port": {
        if (index + 1 < argLen) {
          port = readOrThrow($Uint16, SEARCH_PARAMS_VALUE_READER, args[index + 1]);
          index += 2;
        } else {
          throw new Error("missing `--port` value");
        }
        break;
      }
      case "--url": {
        if (index + 1 < argLen) {
          url = new URL(args[index + 1]);
          index += 2;
        } else {
          throw new Error("missing `--url` value");
        }
        break;
      }
      default: {
        throw new Error(`unknown argument ${JSON.stringify(arg)}`);
      }
    }
  }
  if (useDefault) {
    backendSecret ??= "dev";
    backendPort ??= 50321;
    port ??= 50320;
    appDir ??= APP_DIR;
    url ??= new URL("http://localhost:50320/");
    forum ??= {
      threadsPerPage: 15,
      postsPerPage: 20,
    };
  } else {
    if (backendSecret === undefined) {
      throw new Error("missing `backendSecret` config");
    }
    if (backendPort === undefined) {
      throw new Error("missing `backendPort` config");
    }
    if (port === undefined) {
      throw new Error("missing `port` config");
    }
    if (appDir === undefined) {
      throw new Error("missing `appDir` config");
    }
    if (forum === undefined) {
      throw new Error("missing `forum` config");
    }
    if (url === undefined) {
      throw new Error("missing `url` config");
    }
  }
  return {
    backendSecret,
    backendPort,
    port,
    appDir,
    forum,
    url,
    isProduction,
  };
}
