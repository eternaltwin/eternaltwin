import { Injectable } from "@angular/core";
import { TransferState } from "@angular/platform-browser";
import { $ListingQuery } from "@eternaltwin/core/core/listing-query";
import { ObjectType } from "@eternaltwin/core/core/object-type";
import { $CreatePostOptions, CreatePostOptions } from "@eternaltwin/core/forum/create-post-options";
import { $CreateThreadOptions, CreateThreadOptions } from "@eternaltwin/core/forum/create-thread-options";
import { ForumConfig } from "@eternaltwin/core/forum/forum-config";
import { $ForumPost, ForumPost } from "@eternaltwin/core/forum/forum-post";
import { ForumPostId } from "@eternaltwin/core/forum/forum-post-id";
import { $ForumSection, ForumSection } from "@eternaltwin/core/forum/forum-section";
import { ForumSectionId } from "@eternaltwin/core/forum/forum-section-id";
import { ForumSectionKey } from "@eternaltwin/core/forum/forum-section-key";
import { $ForumSectionListing, ForumSectionListing } from "@eternaltwin/core/forum/forum-section-listing";
import { $ForumThread, ForumThread } from "@eternaltwin/core/forum/forum-thread";
import { ForumThreadId } from "@eternaltwin/core/forum/forum-thread-id";
import { ForumThreadKey } from "@eternaltwin/core/forum/forum-thread-key";
import { $UpdatePostOptions, UpdatePostOptions } from "@eternaltwin/core/forum/update-post-options";
import { UserId } from "@eternaltwin/core/user/user-id";
import { $UserIdRef } from "@eternaltwin/core/user/user-id-ref";
import { Observable } from "rxjs";

import { ConfigService } from "../config/config.service.mjs";
import { RestService } from "../rest/rest.service.mjs";

@Injectable()
export class ForumService {
  readonly #config: ForumConfig;
  readonly #rest: RestService;

  constructor(transferState: TransferState, config: ConfigService, rest: RestService) {
    this.#config = config.forum();
    this.#rest = rest;
  }

  getForumSections(): Observable<ForumSectionListing> {
    return this.#rest.get(["forum", "sections"], {resType: $ForumSectionListing});
  }

  getForumSection(idOrKey: ForumSectionId | ForumSectionKey, page0: number): Observable<ForumSection> {
    return this.#rest.get(
      ["forum", "sections", idOrKey],
      {
        queryType: $ListingQuery,
        query: {offset: page0 * this.#config.threadsPerPage, limit: this.#config.threadsPerPage},
        resType: $ForumSection,
      },
    );
  }

  getForumThread(idOrKey: ForumThreadId | ForumThreadKey, page0: number): Observable<ForumThread> {
    return this.#rest.get(
      ["forum", "threads", idOrKey],
      {
        queryType: $ListingQuery,
        query: {offset: page0 * this.#config.postsPerPage, limit: this.#config.postsPerPage},
        resType: $ForumThread,
      },
    );
  }

  getForumPost(postId: ForumPostId, page0: number): Observable<ForumPost> {
    return this.#rest.get(
      ["forum", "posts", postId],
      {
        // queryType: $ListingQuery,
        // query: {offset: page0 * 10, limit: 10},
        resType: $ForumPost,
      },
    );
  }

  createThread(
    sectionIdOrKey: ForumSectionId | ForumSectionKey,
    options: CreateThreadOptions,
  ): Observable<ForumThread> {
    return this.#rest.post(["forum", "sections", sectionIdOrKey], {
      reqType: $CreateThreadOptions,
      req: options,
      resType: $ForumThread,
    });
  }

  createPost(threadIdOrKey: ForumThreadId | ForumThreadKey, options: CreatePostOptions): Observable<ForumPost> {
    return this.#rest.post(["forum", "threads", threadIdOrKey], {
      reqType: $CreatePostOptions,
      req: options,
      resType: $ForumPost,
    });
  }

  updatePost(postId: ForumPostId, options: UpdatePostOptions): Observable<ForumPost> {
    return this.#rest.patch(["forum", "posts", postId], {
      reqType: $UpdatePostOptions,
      req: options,
      resType: $ForumPost,
    });
  }

  addModerator(sectionIdOrKey: ForumSectionId | ForumSectionKey, userId: UserId): Observable<ForumSection> {
    return this.#rest.post(["forum", "sections", sectionIdOrKey, "role_grants"], {
      reqType: $UserIdRef,
      req: {type: ObjectType.User, id: userId},
      resType: $ForumSection,
    });
  }

  deleteModerator(sectionIdOrKey: ForumSectionId | ForumSectionKey, userId: UserId): Observable<ForumSection> {
    return this.#rest.delete(["forum", "sections", sectionIdOrKey, "role_grants"], {
      reqType: $UserIdRef,
      req: {type: ObjectType.User, id: userId},
      resType: $ForumSection,
    });
  }
}
