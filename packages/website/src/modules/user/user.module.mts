import { NgModule } from "@angular/core";

import { UserService } from "./user.service.mjs";

@NgModule({
  imports: [],
  providers: [
    UserService,
  ],
})
export class UserModule {
}
