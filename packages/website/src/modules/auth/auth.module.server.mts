import { NgModule } from "@angular/core";

import { AuthService } from "./auth.service.mjs";
import { ServerAuthService } from "./auth.service.server.mjs";

@NgModule({
  providers: [
    {provide: AuthService, useClass: ServerAuthService},
  ],
  imports: [],
})
export class ServerAuthModule {
}
