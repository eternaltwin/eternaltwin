import { Injectable } from "@angular/core";
import * as marktwin from "@eternaltwin/marktwin";
import { Grammar } from "@eternaltwin/marktwin/grammar";

import { MarktwinService } from "./marktwin.service.mjs";

@Injectable()
export class BrowserMarktwinService extends MarktwinService {
  public constructor() {
    super();
  }

  public renderMarktwin(grammar: Readonly<Grammar>, input: string): string {
    return marktwin.renderMarktwin(grammar, input);
  }
}
