import { NgModule } from "@angular/core";

import { BrowserMarktwinService } from "./marktwin.service.browser.mjs";
import { MarktwinService } from "./marktwin.service.mjs";

@NgModule({
  providers: [
    {provide: MarktwinService, useClass: BrowserMarktwinService},
  ],
  imports: [],
})
export class MarktwinModule {
}
