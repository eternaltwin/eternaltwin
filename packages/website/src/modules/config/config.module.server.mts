import { NgModule } from "@angular/core";

import { ConfigService } from "./config.service.mjs";
import { ServerConfigService } from "./config.service.server.mjs";

@NgModule({
  providers: [
    {provide: ConfigService, useClass: ServerConfigService},
  ],
  imports: [],
})
export class ServerConfigModule {
}
