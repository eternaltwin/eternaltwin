import {Inject, Injectable, LOCALE_ID} from "@angular/core";
import {LocaleId} from "@eternaltwin/core/core/locale-id";
import {$TimeQuery} from "@eternaltwin/core/core/time-query";
import {GetHammerfestUserOptions} from "@eternaltwin/core/hammerfest/get-hammerfest-user-options";
import {HammerfestItemId} from "@eternaltwin/core/hammerfest/hammerfest-item-id";
import {$HammerfestUser, HammerfestUser} from "@eternaltwin/core/hammerfest/hammerfest-user";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/operators";

import {RestService} from "../rest/rest.service.mjs";
import {HAMMERFEST_DB} from "./db.mjs";

interface DbItem {
  id: string;
  name: Record<LocaleId, string>;
  unlock?: number;
}

@Injectable()
export class HammerfestService {
  readonly #localeId: LocaleId;
  readonly #db: ReadonlyMap<HammerfestItemId, DbItem>;
  readonly #rest: RestService;

  constructor(@Inject(LOCALE_ID) localeId: string, rest: RestService) {
    if (/^fr(?:-|$)/.test(localeId)) {
      this.#localeId = "fr-FR";
    } else if (/^es(?:-|$)/.test(localeId)) {
      this.#localeId = "es-SP";
    } else {
      this.#localeId = "en-US";
    }
    const db: Map<HammerfestItemId, DbItem> = new Map();
    for (const item of HAMMERFEST_DB) {
      db.set(item.id, item);
    }
    this.#db = db;
    this.#rest = rest;
  }

  getUser(options: Readonly<GetHammerfestUserOptions>): Observable<HammerfestUser | null> {
    return this.#rest
      .get(
        ["archive", "hammerfest", options.server, "users", options.id],
        {
          queryType: $TimeQuery,
          query: {time: options.time},
          resType: $HammerfestUser,
        },
      )
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }

  getItemName(itemId: HammerfestItemId): string | undefined {
    const dbItem: DbItem | undefined = this.#db.get(itemId);
    if (dbItem === undefined) {
      return undefined;
    }
    return dbItem.name[this.#localeId];
  }

  getItemUnlock(itemId: HammerfestItemId): number {
    const dbItem: DbItem | undefined = this.#db.get(itemId);
    if (dbItem === undefined) {
      return 10;
    }
    return dbItem.unlock ?? 10;
  }

  getItemIds(): HammerfestItemId[] {
    return [...this.#db.keys()];
  }
}
