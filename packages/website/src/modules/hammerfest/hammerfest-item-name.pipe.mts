import { Pipe, PipeTransform } from "@angular/core";
import {HammerfestItemId} from "@eternaltwin/core/hammerfest/hammerfest-item-id";

import {HammerfestService} from "./hammerfest.service.mjs";

@Pipe({
  name: "hammerfestItemName",
  pure: true,
})
export class HammerfestItemNamePipe implements PipeTransform {
  readonly #hammerfestService: HammerfestService;

  constructor(hammerfestService: HammerfestService) {
    this.#hammerfestService = hammerfestService;
  }

  transform(itemId: HammerfestItemId): string {
    const name: string | undefined = this.#hammerfestService.getItemName("" + itemId);
    return name ?? `Item #${itemId}`;
  }
}
