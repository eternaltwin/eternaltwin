#!/usr/bin/env node
import childProcess from "child_process";
import { PathLike } from "fs";
import fs from "fs/promises";
import { join } from "furi";
import sysPath from "path";
import { env, exit, platform } from "process";
import readline from "readline";

import { getRustTarget } from "../lib/internal.mjs";

const FORCE_BUILD_VAR = "ETERNALTWIN_FORCE_BUILD";
const PACKAGE_ROOT = join(import.meta.url, "../..");
const NATIVE = join(PACKAGE_ROOT, "./native");

async function main() {
  if (isForceBuild() || getRustTarget() === null) {
    const executable = await build("cargo", ["build", "--manifest-path", "./Cargo.toml", "--bin", "eternaltwin", "--release", "--message-format=json"]);
    const isWindows = platform === "win32" || platform === "cygwin";
    const out = `./eternaltwin${isWindows ? ".exe" : ""}`;
    await forceCopy(executable, sysPath.join("./lib", out));
    const localMjs = [
      `const executableName = ${JSON.stringify(out)};`,
      "export const executableUri = new URL(executableName, import.meta.url);",
      ""
    ].join("\n");
    await fs.writeFile("./lib/local.mjs", localMjs);
  }
}

function isForceBuild(): boolean {
  switch (env[FORCE_BUILD_VAR]) {
    case "1":
    case "true":
      return true;
    default:
      return false;
  }
}

async function forceCopy(src: string, dest: string) {
  await unlinkIfExists(dest);
  await fs.copyFile(src, dest);
}

async function unlinkIfExists(p: PathLike) {
  try {
    await fs.unlink(p);
  } catch (e) {
    const isEnoent = typeof e === "object" && e !== null && Reflect.get(e, "code") === "ENOENT";
    if (!isEnoent) {
      throw e;
    } // else ignore
  }
}

export async function build(cargoBin: string, args: string[], extraEnv?: Record<string, string>): Promise<string> {
  return new Promise((resolve, reject) => {
    let child: childProcess.ChildProcess | null = childProcess.spawn(
      cargoBin,
      args,
      {
        cwd: NATIVE.toSysPath(),
        stdio: ["inherit", "pipe", "inherit"],
        env: extraEnv !== undefined ? {...process.env, ...extraEnv} : undefined,
      }
    );
    let lineReader: readline.Interface | null = readline.createInterface({input: child.stdout!});
    const end = (err: unknown, path: string | null) => {
      if (lineReader !== null) {
        lineReader.removeAllListeners();
        lineReader.close();
        lineReader = null;
      }
      if (child !== null) {
        child.removeAllListeners();
        if (!child.killed) {
          child.kill();
        }
        child = null;
      }
      if (err !== null) {
        reject(err);
      } else if (path === null) {
        reject(new Error("build completed but `etwin_native` executable was not found"));
      } else {
        resolve(path);
      }
    };
    let executablePath: string | null = null;
    lineReader.on("line", (line: string) => {
      try {
        if (executablePath === null) {
          executablePath = getExecutablePath(line);
        }
      } catch (err) {
        end(err, null);
      }
    });
    lineReader.on("error", e => end(e, null));
    child.on("error", e => end(e, null));
    child.on("exit", (code: number | null, signal: unknown) => {
      if (code === 0) {
        end(null, executablePath);
      } else {
        end(new Error(`non-zero exit status code: code = ${code}, signal = ${signal}`), null);
      }
    });
  });
}

/**
 * Retrieve the executable path from a cargo JSON line diagnostic
 *
 * It searches for a row such as:
 *
 * ```json
 * {"reason":"compiler-artifact","package_id":"etwin_native 0.11.0 (path+file:///data/projects/eternaltwin/etwin/packages/native/native)","manifest_path":"/data/projects/eternaltwin/etwin/packages/native/native/Cargo.toml","target":{"kind":["bin"],"crate_types":["bin"],"name":"etwin_native","src_path":"/data/projects/eternaltwin/etwin/packages/native/native/src/main.rs","edition":"2021","doc":true,"doctest":false,"test":true},"profile":{"opt_level":"3","debuginfo":null,"debug_assertions":false,"overflow_checks":false,"test":false},"features":[],"filenames":["/data/projects/eternaltwin/etwin/target/release/etwin_native"],"executable":"/data/projects/eternaltwin/etwin/target/release/etwin_native","fresh":true}
 * ```
 */
function getExecutablePath(line: string): string | null {
  const diagnostic: unknown = JSON.parse(line);
  // console.log(diagnostic);
  if (diagnostic === null || typeof diagnostic !== "object") {
    throw new Error(`expected cargo diagnostic line to be a JSON object: ${line}`);
  }
  const reason: unknown = Reflect.get(diagnostic, "reason");
  if (reason !== "compiler-artifact") {
    return null;
  }
  const target: unknown = Reflect.get(diagnostic, "target");
  if (target === null || typeof target !== "object") {
    return null;
  }
  const targetKind: unknown = Reflect.get(target, "kind");
  const targetName: unknown = Reflect.get(target, "name");
  if (!(Array.isArray(targetKind) && targetKind.includes("bin") && targetName === "eternaltwin")) {
    return null;
  }
  const executable: unknown = Reflect.get(diagnostic, "executable");
  if (typeof executable !== "string") {
    return null;
  }
  return executable;
}

main().catch((err: Error): never => {
  console.error(err.stack);
  exit(1);
});
