import { arch, platform } from "node:process";

/**
 * If a precompiled executable is available for the current platform, return its Rust target
 */
export function getRustTargetSync(): string | null {
  switch (`${arch}.${platform}`) {
    case "x64.darwin":
      return "x86_64-apple-darwin";
    case "x64.linux":
      return "x86_64-unknown-linux-gnu";
    case "x64.win32":
      return "x86_64-pc-windows-msvc";
    default:
      return null;
  }
}

export async function getRustTarget(): Promise<string | null> {
  switch (`${arch}.${platform}`) {
    case "x64.darwin":
      return "x86_64-apple-darwin";
    case "x64.linux":
      return "x86_64-unknown-linux-gnu";
    case "x64.win32":
      return "x86_64-pc-windows-gnu";
    default:
      return null;
  }
}
