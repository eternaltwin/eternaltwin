# Eternaltwin executable

Eternaltwin executable packaged as a Node package. This package tries to use prebuilt binaries if possible, or
it defaults to compiling the package if no prebuilt binary is available.

##

Usage

```ts
import { getExecutableUri } from "@eternaltwin/exe";

const exe = getExecutableUri();
console.log(`the Eternaltwin executable is available at: ${exe}`);
```

## Contributing

- When working locally inside the Eternaltwin repository, this package uses the `<repo>/bin/` directory at the root to
  build the executable.
- When packaging for publication, `<repo>/bin/` is copied into `<package>/native/`. This is used when installing
  in other projects.

Cross-compilation and prebuilt binaries are generated through `cargo xtask precompile`.
