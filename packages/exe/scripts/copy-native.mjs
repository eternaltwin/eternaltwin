import { cp, readFile, writeFile } from "node:fs/promises"

async function main() {
  await cp("../../bin/", "./native", {force: true, recursive: true});
  await uncommentWorkspace();
}

async function uncommentWorkspace() {
  const oldManifest = await readFile("./native/Cargo.toml", {encoding: "utf-8"});
  const newManifest = oldManifest.replace("# [workspace]\n", "[workspace]\n");
  await writeFile("./native/Cargo.toml", newManifest);
}

await main();
