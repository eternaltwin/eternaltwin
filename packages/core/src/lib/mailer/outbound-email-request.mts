import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import {$OutboundEmailRequestId, OutboundEmailRequestId} from "./outbound-email-request-id.mjs";

export interface OutboundEmailRequest {
  id: OutboundEmailRequestId;
}

export const $OutboundEmailRequest: RecordIoType<OutboundEmailRequest> = new RecordType<OutboundEmailRequest>({
  properties: {
    id: {type: $OutboundEmailRequestId},
  },
  changeCase: CaseStyle.SnakeCase,
});
