import { RecordIoType } from "kryo/record";

import {$Listing, Listing} from "../core/listing.mjs";
import {$OutboundEmailRequest, OutboundEmailRequest} from "./outbound-email-request.mjs";

export type OutboundEmailRequestListing = Listing<OutboundEmailRequest>;

export const $OutboundEmailRequestListing = $Listing.apply($OutboundEmailRequest) as RecordIoType<OutboundEmailRequestListing>;
