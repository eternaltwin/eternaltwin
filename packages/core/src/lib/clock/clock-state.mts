import { CaseStyle } from "kryo";
import { $Date } from "kryo/date";
import { RecordIoType, RecordType } from "kryo/record";

export interface ClockState {
  time: Date;
}

export const $ClockState: RecordIoType<ClockState> = new RecordType<ClockState>({
  properties: {
    time: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
});
