import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";

import {$ObjectType, ObjectType} from "../core/object-type.mjs";
import {$NullableTwinoidSiteHost, NullableTwinoidSiteHost} from "./twinoid-site-host.mjs";
import {$TwinoidSiteId, TwinoidSiteId} from "./twinoid-site-id.mjs";

export interface ShortTwinoidSite {
  type: ObjectType.TwinoidSite;
  id: TwinoidSiteId;
  host: NullableTwinoidSiteHost;
}

export const $ShortTwinoidSite: RecordIoType<ShortTwinoidSite> = new RecordType<ShortTwinoidSite>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.TwinoidSite})},
    id: {type: $TwinoidSiteId},
    host: {type: $NullableTwinoidSiteHost},
  },
  changeCase: CaseStyle.SnakeCase,
});
