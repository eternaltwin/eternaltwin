import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {$ObjectType, ObjectType} from "../core/object-type.mjs";
import {$TwinoidSiteUserId, TwinoidSiteUserId} from "./twinoid-site-user-id.mjs";

export interface TwinoidLinkUser {
  type: ObjectType.TwinoidSiteUser;
  id: TwinoidSiteUserId;
}

export const $TwinoidLinkUser: RecordIoType<TwinoidLinkUser> = new RecordType<TwinoidLinkUser>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.TwinoidSiteUser})},
    id: {type: $TwinoidSiteUserId},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableTwinoidLinkUser = null | TwinoidLinkUser;

export const $NullableTwinoidLinkUser: TryUnionType<NullableTwinoidLinkUser> = new TryUnionType({variants: [$Null, $TwinoidLinkUser]});
