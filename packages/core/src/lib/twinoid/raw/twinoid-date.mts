import {CheckId, IoType, KryoContext, Reader, Result, writeError, Writer} from "kryo";
import {CheckKind} from "kryo/checks/check-kind";
import {readVisitor} from "kryo/readers/read-visitor";

/**
 * An UTC Date.
 *
 * Example serialization: `"2012-02-25"`
 */
export type TwinoidDate = Date;

const PATTERN: RegExp = /(\d{4})-(\d{2})-(\d{2})/;

export const $TwinoidDate: IoType<TwinoidDate> = {
  name: "TwinoidDate",
  test(cx: KryoContext, value: unknown): Result<TwinoidDate, CheckId> {
    if (value === null || typeof value !== "object") {
      return writeError(cx, {check: CheckKind.BaseType, expected: ["Object"]});
    }
    if (!(value instanceof Date)) {
      return writeError(cx, {check: CheckKind.InstanceOf, class: "Date"});
    }
    if(value.getUTCHours() === 0 && value.getUTCMinutes() === 0 && value.getUTCSeconds() === 0 && value.getMilliseconds() === 0) {
      return {ok: true, value};
    } else {
      return writeError(cx, {check: CheckKind.Custom, message: "hours, minutes, seconds and milliseconds must all be zero"});
    }
  },
  equals(left: TwinoidDate, right: TwinoidDate): boolean {
    return left.getTime() === right.getTime();
  },
  lte(left: TwinoidDate, right: TwinoidDate): boolean {
    return left.getTime() <= right.getTime();
  },
  clone(value: TwinoidDate): TwinoidDate {
    return new Date(value.getTime());
  },
  write<W>(writer: Writer<W>, value: TwinoidDate): W {
    const year = value.getUTCFullYear().toString(10).padStart(4, "0");
    const month = (value.getUTCMonth() + 1).toString(10).padStart(2, "0");
    const date = value.getUTCDate().toString(10).padStart(2, "0");
    return writer.writeString(`${year}-${month}-${date}`);
  },
  read<R>(cx: KryoContext, reader: Reader<R>, raw: R): Result<TwinoidDate, CheckId> {
    return reader.readString(
      cx,
      raw,
      readVisitor({
        fromString(input: string): Result<TwinoidDate, CheckId> {
          const match: RegExpMatchArray | null = PATTERN.exec(input);
          if (match === null) {
            return writeError(cx, {check: CheckKind.Custom, message: `invalid TwinoidDate: ${JSON.stringify(input)}`});
          }
          const year = parseInt(match[1], 10);
          // Subtract 1 because JS Date uses 0-indexed months but we get a 1-indexed month
          const month = parseInt(match[2], 10) - 1;
          const date = parseInt(match[3], 10);
          const result: TwinoidDate = new Date(0);
          result.setUTCFullYear(year, month, date);
          if (result.getUTCFullYear() !== year || result.getUTCMonth() !== month || result.getUTCDate() !== date
            || result.getUTCHours() !== 0 || result.getUTCMinutes() !== 0 || result.getUTCSeconds() !== 0
            || result.getUTCMilliseconds() !== 0
          ) {
            throw new Error("FailedToConstructTwinoidDate");
          }
          return {ok: true, value: result};
        },
      }),
    );
  },
};
