import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";

import {$ObjectType, ObjectType} from "../core/object-type.mjs";
import {$ShortTwinoidSite, ShortTwinoidSite} from "./short-twinoid-site.mjs";
import {$TwinoidSiteUserId, TwinoidSiteUserId} from "./twinoid-site-user-id.mjs";

export interface TwinoidSiteUser {
  type: ObjectType.TwinoidSiteUser;
  site: ShortTwinoidSite;
  id: TwinoidSiteUserId;
}

export const $TwinoidSiteUser: RecordIoType<TwinoidSiteUser> = new RecordType<TwinoidSiteUser>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.TwinoidSiteUser})},
    site: {type: $ShortTwinoidSite},
    id: {type: $TwinoidSiteUserId},
  },
  changeCase: CaseStyle.SnakeCase,
});
