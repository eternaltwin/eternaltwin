import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {$Uint8} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";
import {$Ucs2String} from "kryo/ucs2-string";

import {$PeriodLower, PeriodLower} from "../core/period-lower.mjs";

export interface GetJobs {
  /**
   * API time, for time-travel querying
   */
  time?: Date;
  /**
   * Filter jobs created in the provided period
   */
  createdAt?: PeriodLower;
  /**
   * Maximum number of results to return
   */
  limit: number;
  /**
   * If provided, cursor token.
   */
  cursor?: string;
}

export const $GetJobs: RecordIoType<GetJobs> = new RecordType<GetJobs>({
  properties: {
    time: {type: $Date, optional: true},
    createdAt: {type: $PeriodLower, optional: true},
    limit: {type: $Uint8},
    cursor: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
