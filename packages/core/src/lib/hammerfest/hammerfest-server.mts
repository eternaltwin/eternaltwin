import { LiteralUnionType } from "kryo/literal-union";
import { $Ucs2String } from "kryo/ucs2-string";

/**
 * A Hammerfest server.
 */
export type HammerfestServer = "hammerfest.fr" | "hfest.net" | "hammerfest.es";

export const $HammerfestServer: LiteralUnionType<HammerfestServer> = new LiteralUnionType({
  type: $Ucs2String,
  values: ["hammerfest.fr", "hfest.net", "hammerfest.es"],
});
